package test.com.erranda.prototype;

import static com.erranda.prototype.Config.IMAGES;
import static com.erranda.prototype.Config.REMEMBER_ME_COOKIE;

import java.io.File;

import javax.servlet.http.Cookie;

import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.markup.html.IPackageResourceGuard;
import org.apache.wicket.markup.html.SecurePackageResourceGuard;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.http.WebRequest;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.request.resource.ResourceStreamResource;
import org.apache.wicket.request.resource.SharedResourceReference;
import org.apache.wicket.settings.RequestCycleSettings.RenderStrategy;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.util.resource.FileResourceStream;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.wicketstuff.htmlcompressor.HtmlCompressingMarkupFactory;

import com.erranda.prototype.Neo4jConfiguration;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.Authenticated;
import com.erranda.prototype.web.BlockList;
import com.erranda.prototype.web.ConfirmRegisteration;
import com.erranda.prototype.web.Deactivate;
import com.erranda.prototype.web.ErrandPostPage;
import com.erranda.prototype.web.Home;
import com.erranda.prototype.web.Index;
import com.erranda.prototype.web.Login;
import com.erranda.prototype.web.Logout;
import com.erranda.prototype.web.MobileHome;
import com.erranda.prototype.web.PasswordReset;
import com.erranda.prototype.web.PasswordResetConfirm;
import com.erranda.prototype.web.PersonProfile;
import com.erranda.prototype.web.SearchHome;
import com.erranda.prototype.web.SearchResults;
import com.erranda.prototype.web.TestChild;

@Configuration
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableNeo4jRepositories
@Import(RepositoryRestMvcConfiguration.class)
@ComponentScan ({"com.erranda.prototype"})
public class AppTest extends Neo4jConfiguration implements CommandLineRunner {/*
																		 * 
																		 * @
																		 * Autowired
																		 * ErrandRepository
																		 * errandRepo
																		 * ;
																		 * 
																		 * @
																		 * Autowired
																		 * PersonRepository
																		 * perRepo
																		 * ;
																		 * 
																		 * @
																		 * Autowired
																		 * GraphDatabase
																		 * graphDatabase
																		 * ;
																		 * 
																		 * @
																		 * Autowired
																		 * private
																		 * PlaceRepository
																		 * placeRepo
																		 * ;
																		 */

	@Autowired
	ApplicationContext applicationContext;

	public AppTest() {
		setBasePackage("com.erranda.prototype");
	}

	@Bean
	public GraphDatabaseService graphDatabaseService() {
		return new GraphDatabaseFactory().newEmbeddedDatabase("../../test.db");
		// return new SpringRestGraphDatabase("http://localhost:7474/db/data",
		// "neo4j", "3c0a0a6ea165ce3b6f7edd269e22a94d");
	}

	@Override
	public Session newSession(Request request, Response response) {
		WebRequest webRequest = (WebRequest) RequestCycle.get().getRequest();
		UserSession session = new UserSession(webRequest);
		Cookie key = webRequest.getCookie(REMEMBER_ME_COOKIE);
		if (key != null) {
			Controller controller = applicationContext
					.getBean(Controller.class);
			try {
				PlatformUser user = controller.tokenLogin(key.getValue());
				if (user != null)
					session.setUser(user);
			} catch (ControllerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return session;
	}

	@Override
	public void init() {

		super.init();
		getMarkupSettings().setStripWicketTags(true);
		getJavaScriptLibrarySettings().setJQueryReference(
				new PackageResourceReference(Authenticated.class,
						"jquery-1.11.2.min.js"));
		// this.getRequestCycleSettings().getRenderStrategy();
		getMarkupSettings().setMarkupFactory(new
		HtmlCompressingMarkupFactory());
		getApplicationSettings().setUploadProgressUpdatesEnabled(true);
		mountPage("/login", Login.class);
		mountPage("/m", MobileHome.class);
		mountPage("/test", TestChild.class);
		mountPage("/logout", Logout.class);
		mountPage("/deactivate", Deactivate.class).setCaseSensitiveMatch(false);
		mountPage("/profile/${username}", PersonProfile.class)
				.setCaseSensitiveMatch(false);
		mountPage("/home", Home.class).setCaseSensitiveMatch(false);
		mountPage("/errand/${pid}", ErrandPostPage.class);
		mountPage("/search/${query}", SearchResults.class);
		mountPage("/search", SearchResults.class);
		mountPage("/search-home", SearchHome.class);
		mountPage("/account/forgot-password", PasswordReset.class);
		mountPage("/account/reset-password/${key}", PasswordResetConfirm.class);
		mountPage("/confirm-registeration/${key}", ConfirmRegisteration.class);
		mountPage("/blocked", BlockList.class);
		IPackageResourceGuard packageResourceGuard = getResourceSettings()
				.getPackageResourceGuard();
		if (packageResourceGuard instanceof SecurePackageResourceGuard) {
			SecurePackageResourceGuard securePackageResourceGuard = (SecurePackageResourceGuard) packageResourceGuard;
			securePackageResourceGuard.addPattern("+*.woff2");
		}
		getRequestCycleSettings().setRenderStrategy(
				RenderStrategy.ONE_PASS_RENDER);
		getComponentInstantiationListeners().add(
				new SpringComponentInjector(this, applicationContext));
		File file = new File(IMAGES);
		if (!file.exists())
			file.mkdirs();
		getSharedResources().add("images",
				new FolderContentResource(new File(IMAGES)));
		mountResource("images", new SharedResourceReference("images"));
		try {
			test();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(AppTest.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public Class<? extends Page> getHomePage() {
		// TODO Auto-generated method stub
		return Index.class;
	}

	static class FolderContentResource implements IResource {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private final File rootFolder;

		public FolderContentResource(File rootFolder) {
			this.rootFolder = rootFolder;
		}

		@Override
		public void respond(Attributes attributes) {
			PageParameters parameters = attributes.getParameters();
			String fileName = parameters.get("id").toString();
			if (fileName != null) {
				File file = new File(rootFolder, fileName);
				FileResourceStream fileResourceStream = new FileResourceStream(
						file);
				ResourceStreamResource resource = new ResourceStreamResource(
						fileResourceStream);
				resource.respond(attributes);
			}
		}
	}

	private void test() throws ControllerException {
		/*	
	*/

	}

	public void setApplicationContext(ApplicationContext ctx) {
		this.applicationContext = ctx;
	}

}
