package test.com.erranda.prototype;

import static org.junit.Assert.assertNotNull;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.erranda.prototype.App;
import com.erranda.prototype.web.AcceptErrand;
import com.erranda.prototype.web.Home;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@ActiveProfiles("test")
public class TestHomePage {

	@Autowired
	ApplicationContext ctx;

	private WicketTester tester;

	@Before
	public void setUp() {
		App application = new App();
		application.setApplicationContext(ctx);
		tester = new WicketTester(application);
	}

	@Test
	public void homepageRendersSuccessfully() {
		// start and render the test page
		tester.startPage(Home.class);
		// assert rendered page class
		tester.assertRenderedPage(Home.class);
	}

	@Test
	public void acceptErrandForm() {
		tester.startComponentInPage(new AcceptErrand("d", null, null));
	}

	@Test
	public void acceptErrand() {
		assertNotNull(tester.startComponentInPage(new AcceptErrand("d", null,
				null)));
	}

}