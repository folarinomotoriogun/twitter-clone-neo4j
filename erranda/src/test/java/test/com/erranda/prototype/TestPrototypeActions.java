/*
 * package test.com.erranda.prototype;
 * 
 * import static org.junit.Assert.*;
 * 
 * import java.io.IOException; import java.util.Arrays; import java.util.Date;
 * import java.util.List;
 * 
 * import org.junit.After; import org.junit.Before; import org.junit.Test;
 * import org.junit.runner.RunWith; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.boot.test.SpringApplicationConfiguration; import
 * org.springframework.data.domain.PageRequest; import
 * org.springframework.data.neo4j.core.GraphDatabase; import
 * org.springframework.data.neo4j.template.Neo4jOperations; import
 * org.springframework.test.context.ActiveProfiles; import
 * org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 * 
 * import com.erranda.prototype.App; import
 * com.erranda.prototype.domain.Controller; import
 * com.erranda.prototype.domain.ControllerException; import
 * com.erranda.prototype.domain.Conversation; import
 * com.erranda.prototype.domain.ConversationRepository; import
 * com.erranda.prototype.domain.Errand; import
 * com.erranda.prototype.domain.ErrandRepository; import
 * com.erranda.prototype.domain.Group; import
 * com.erranda.prototype.domain.GroupRepository; import
 * com.erranda.prototype.domain.ThankyouRepository; import
 * com.erranda.prototype.domain.Message; import
 * com.erranda.prototype.domain.MessageRepository; import
 * com.erranda.prototype.domain.Person; import
 * com.erranda.prototype.domain.PersonControllerException; import
 * com.erranda.prototype.domain.PersonRepository; import
 * com.erranda.prototype.domain.Place; import
 * com.erranda.prototype.domain.PlaceRepository; import
 * com.erranda.prototype.domain.Thankyou;
 * 
 * @RunWith(SpringJUnit4ClassRunner.class)
 * 
 * @SpringApplicationConfiguration(classes = App.class)
 * 
 * @ActiveProfiles("test") public class TestPrototypeActions {
 * 
 * @Autowired Controller controller;
 * 
 * @Autowired PersonRepository personRepo;
 * 
 * @Autowired ErrandRepository errandRepo;
 * 
 * @Autowired GraphDatabase graphDatabase;
 * 
 * @Autowired Neo4jOperations template;
 * 
 * @Autowired PlaceRepository placeRepo;
 * 
 * @Autowired ConversationRepository conversationRepo;
 * 
 * @Autowired GroupRepository groupRepo;
 * 
 * @Autowired MessageRepository messageRepo;
 * 
 * @Autowired ThankyouRepository helpedRepo;
 * 
 * @Before public void setup() throws ControllerException { Place place = new
 * Place("St Andrews"); Place place2 = new Place("Mexico City"); Place place3 =
 * new Place("Leeds City"); List<String> smarty =
 * Arrays.asList("Local Shopping", "Holiday Shopping", "Delivery", "Cleaning",
 * "Assistance", "Handyman");
 * 
 * Person marc = new Person("Marc Cole", "marc", "marc@erranda.com", "password",
 * "1989", "Male", place3, smarty); Person linda = new Person("Linda", "linda",
 * "linda@erranda.com", "password", "1989", "Male", place2, smarty); Person
 * simon = new Person("Simon", "geeky", "geeky@erranda.com", "password", "1989",
 * "Male", place, smarty); Person folarin = new Person("Folarin", "macho",
 * "folarin@erranda.com", "password", "1989", "Male", place, smarty);
 * 
 * List<Person> persons = Arrays.asList(marc, linda, simon, folarin);
 * 
 * for (Person p : persons) { controller.signUp(p); } }
 * 
 * @After public void destroy() throws IOException { errandRepo.deleteAll();
 * groupRepo.deleteAll(); personRepo.deleteAll();
 * 
 * }
 * 
 * @Test public void testSignUp() throws ControllerException { String email =
 * "simon@erranda.com"; String username = "erranda"; Person p =
 * createMockUser(email, username); controller.signUp(p);
 * assertTrue(personRepo.findByEmail(email) != null); }
 * 
 * @Test(expected = PersonControllerException.class) public void
 * testSignUpFailEmail() throws ControllerException { String email =
 * "simon@erranda.com"; String username = "erranda"; Person p =
 * createMockUser(email, username); controller.signUp(p); p =
 * createMockUser(email, "xyz"); controller.signUp(p); }
 * 
 * @Test public void testSignUpFailUsername() throws ControllerException {
 * String email = "simon@erranda.com"; String username = "erranda"; Person p =
 * createMockUser(email, username); controller.signUp(p); p =
 * createMockUser("folarin@sys.com", username); controller.signUp(p); }
 * 
 * @Test public void testSendMessage() throws ControllerException { Person marc
 * = personRepo.findByUsername("marc"); Person linda =
 * personRepo.findByUsername("linda"); Message message = new Message();
 * message.setPayload("Hello"); message.setReceiver(linda);
 * message.setSender(marc); controller.sendMessage(message); Conversation c =
 * conversationRepo.findBySenderAndReceiver(marc, linda); assertTrue(c != null);
 * assertTrue(messageRepo.findByConversation(c).size() == 1);
 * 
 * }
 * 
 * @Test public void testSavePlace() throws ControllerException { Place place =
 * controller.savePlace("St Andrews, United Kingdom"); assertTrue
 * (placeRepo.findByName(place.getName()) != null); }
 * 
 * @Test public void testSavePlaceThread() throws ControllerException {
 * controller.savePlace("St Andrews, United Kingdom");
 * 
 * controller.savePlace("St Andrews, Fife, United Kingdom");
 * 
 * assertTrue(placeRepo.findByNameLike("St Andrews").size() == 1); }
 * 
 * @Test public void testLogin() throws ControllerException { String email =
 * "linda@erranda.com"; String password = "password"; Person simon =
 * controller.login(email, password); assertTrue(simon != null); }
 * 
 * @Test public void testLoginUsername() throws ControllerException { String
 * username = "linda"; String password = "password"; Person simon =
 * controller.login(username, password); assertTrue(simon != null); }
 * 
 * @Test public void testPostErrand() throws ControllerException { Person linda
 * = personRepo.findByUsername("linda"); Errand item6 = new Errand();
 * item6.setOwner(linda); item6.setAdded(new Date()); item6.setAnonymous(true);
 * item6.setDetails("I want some pop tarts!"); item6.setIsExternalUrl(false);
 * item6.setLocation("St Andrews"); item6.setType("Assistance");
 * controller.postErrand(item6);
 * assertTrue(errandRepo.findByOwnerOrderByAddedDesc(linda, new PageRequest(0,
 * 10)).size() == 1); }
 * 
 * @Test public void testInterestFeed() throws ControllerException { Person
 * simon = personRepo.findByUsername("geeky"); Person linda =
 * personRepo.findByUsername("linda"); Person marc =
 * personRepo.findByUsername("marc"); Person folarin =
 * personRepo.findByUsername("macho"); simon.follow(marc);
 * personRepo.save(simon);
 * 
 * Group group = new Group (); group.setName("St A"); group =
 * groupRepo.save(group); simon.joinGroup(group); personRepo.save(simon);
 * 
 * Errand item6 = new Errand(); item6.setOwner(linda); item6.setAdded(new
 * Date()); item6.setAnonymous(true); item6.addGroup(group);
 * item6.setDetails("I want some pop tarts!"); item6.setIsExternalUrl(false);
 * item6.setLocation("St Andrews"); item6.setType("Assistance");
 * controller.postErrand(item6);
 * 
 * Errand item8 = new Errand(); item8.setOwner(folarin); item8.setAdded(new
 * Date()); item8.setAnonymous(true);
 * item8.setDetails("I want some pop tarts!"); item8.setIsExternalUrl(false);
 * item8.setLocation("St Andrews"); item8.setType("Assistance");
 * 
 * controller.postErrand(item8); Errand item7 = new Errand();
 * item7.setOwner(marc); item7.setAdded(new Date()); item7.setAnonymous(true);
 * item7.setDetails("I want some pop tarts!"); item7.setIsExternalUrl(false);
 * item7.setLocation("St Andrews"); item7.setType("Assistance");
 * controller.postErrand(item7);
 * 
 * assertTrue (controller.interestFeed(simon, 0, 100).size() == 3);
 * 
 * }
 * 
 * @Test public void testGetSimilarPlaces() { Place place = new Place ();
 * place.setName("Agege, Lagos, Nigeria");
 * 
 * Place place2 = new Place (); place2.setName("Agege, Nigeria");
 * 
 * placeRepo.save(place); placeRepo.save(place2);
 * assertTrue(controller.getSimilarPlaces("Agege, Nigeria").size() == 2); }
 * 
 * @Test public void testPersonFeed() throws ControllerException { Person linda
 * = personRepo.findByUsername("linda"); Errand item6 = new Errand();
 * item6.setOwner(linda); item6.setAdded(new Date()); item6.setAnonymous(true);
 * item6.setDetails("I want some pop tarts!"); item6.setIsExternalUrl(false);
 * item6.setLocation("St Andrews"); item6.setType("Assistance");
 * errandRepo.save(item6); assertTrue(controller.personFeed(linda,0, 10).size()
 * == 1); }
 * 
 * @Test public void testDeleteErrand() throws ControllerException { Person
 * linda = personRepo.findByUsername("linda"); Errand item6 = new Errand();
 * item6.setOwner(linda); item6.setAdded(new Date()); item6.setAnonymous(true);
 * item6.setDetails("I want some pop tarts!"); item6.setIsExternalUrl(false);
 * item6.setLocation("St Andrews"); item6.setType("Assistance");
 * errandRepo.save(item6); controller.deleteErrand(item6);
 * assertTrue(errandRepo.findByOwnerOrderByAddedDesc(linda, new PageRequest(0,
 * 10)).size() == 0);
 * 
 * }
 * 
 * @Test public void testCreateGroup() throws ControllerException { Person linda
 * = personRepo.findByUsername("linda"); Group sta = new Group ();
 * sta.setDescription("The University of St Andrews");
 * sta.setLocality(linda.getLocality()); sta.setOwner(linda);
 * sta.setName("St A"); controller.createGroup(sta); }
 * 
 * @Test (expected = ControllerException.class) public void
 * testCreateGroupDuplicate() throws ControllerException { Person linda =
 * personRepo.findByUsername("linda"); Group sta = new Group ();
 * sta.setDescription("The University of St Andrews");
 * sta.setLocality(linda.getLocality()); sta.setOwner(linda);
 * sta.setName("St A"); controller.createGroup(sta);
 * controller.createGroup(sta); }
 * 
 * @Test public void testJoinGroupImplicit() throws ControllerException { Person
 * linda = personRepo.findByUsername("linda"); Group sta = new Group ();
 * sta.setName("University of St Andrews");
 * sta.setDescription("The University of St Andrews");
 * sta.setLocality(linda.getLocality()); sta.setOwner(linda);
 * controller.createGroup(sta); assertTrue (personRepo.findByGroups(sta).size()
 * == 1); }
 * 
 * @Test public void testJoinGroupExplicit() throws ControllerException { Person
 * linda = personRepo.findByUsername("linda"); Group sta = new Group ();
 * sta.setName("University of St Andrews");
 * sta.setDescription("The University of St Andrews");
 * sta.setLocality(linda.getLocality()); sta.setOwner(linda); sta =
 * controller.createGroup(sta);
 * 
 * Person marc = personRepo.findByUsername("marc"); controller.joinGroup(sta,
 * marc); assertTrue (personRepo.findByGroups(sta).size() == 2); }
 * 
 * @Test public void testLeaveGroup() throws ControllerException { Person linda
 * = personRepo.findByUsername("linda"); Group sta = new Group ();
 * sta.setName("University of St Andrews");
 * sta.setDescription("The University of St Andrews");
 * sta.setLocality(linda.getLocality()); sta.setOwner(linda); sta =
 * controller.createGroup(sta);
 * 
 * Person marc = personRepo.findByUsername("marc"); marc.joinGroup(sta);
 * personRepo.save(marc);
 * 
 * assertTrue(personRepo.findByGroups(sta).size() == 2);
 * controller.leaveGroup(sta, marc);
 * assertTrue(personRepo.findByGroups(sta).size() == 1); }
 * 
 * @Test public void testDeleteGroup() throws ControllerException { Person linda
 * = personRepo.findByUsername("linda"); Group sta = new Group ();
 * sta.setName("University of St Andrews");
 * sta.setDescription("The University of St Andrews");
 * sta.setLocality(linda.getLocality()); sta.setOwner(linda); sta =
 * controller.createGroup(sta); assertTrue(personRepo.findByGroups(sta).size()
 * == 1); controller.deleteGroup(sta, linda);
 * assertTrue(groupRepo.findOne(sta.getId()) == null); }
 * 
 * @Test public void testGroupFeed() throws ControllerException { Person linda =
 * personRepo.findByUsername("linda"); Group sta = new Group ();
 * sta.setName("University of St Andrews");
 * sta.setDescription("The University of St Andrews");
 * sta.setLocality(linda.getLocality()); sta.setOwner(linda); sta =
 * controller.createGroup(sta);
 * 
 * Person marc = personRepo.findByUsername("marc"); controller.joinGroup(sta,
 * marc);
 * 
 * Errand item6 = new Errand(); item6.setOwner(linda); item6.setAdded(new
 * Date()); item6.setAnonymous(true);
 * item6.setDetails("I want some pop tarts!"); item6.setIsExternalUrl(false);
 * item6.setLocation("St Andrews"); item6.setType("Assistance");
 * item6.addGroup(sta); controller.postErrand(item6);
 * 
 * assertTrue(controller.groupFeed(marc,sta, 0, 10).size() == 1);
 * 
 * }
 * 
 * @Test public void testHelpedPerson() throws ControllerException { Person
 * linda = personRepo.findByUsername("linda"); Person marc =
 * personRepo.findByUsername("marc"); Thankyou helped = new Thankyou();
 * helped.setData("Thanks for helping me get my stuff online");
 * helped.setReceiver(marc); helped.setSender(linda);
 * controller.helpedMe(helped); helpedRepo.findBySenderAndReceiver(linda, marc);
 * }
 * 
 * @Test public void testHelped() throws ControllerException { Person linda =
 * personRepo.findByUsername("linda"); Person marc =
 * personRepo.findByUsername("marc"); Thankyou helped = new Thankyou();
 * helped.setData("Thanks for helping me get my stuff online");
 * helped.setReceiver(marc); helped.setSender(linda);
 * controller.helpedMe(helped); assertTrue(controller.helpedWho(marc,0,
 * 10).size() == 1); }
 * 
 * @Test public void testFollowPerson() throws ControllerException { Person
 * linda = personRepo.findByUsername("linda"); Person marc =
 * personRepo.findByUsername("marc"); controller.followPerson(linda, marc);
 * assertTrue(personRepo.followers(marc,new PageRequest(0, 10)).size() == 1); }
 * 
 * @Test public void testFollowers() throws ControllerException { Person linda =
 * personRepo.findByUsername("linda"); Person marc =
 * personRepo.findByUsername("marc"); linda.follow( marc);
 * personRepo.save(linda); assertTrue(controller.followers(marc,0, 10).size() ==
 * 1); }
 * 
 * @Test public void testFollowing() throws ControllerException { Person linda =
 * personRepo.findByUsername("linda"); Person marc =
 * personRepo.findByUsername("marc"); controller.followPerson(linda, marc);
 * assertTrue(personRepo.followers(marc,new PageRequest(0, 10)).size() == 1); }
 * 
 * @Test public void testUnfollowPerson() throws ControllerException { Person
 * linda = personRepo.findByUsername("linda"); Person marc =
 * personRepo.findByUsername("marc"); linda.follow( marc);
 * personRepo.save(linda); controller.unfollowPerson(linda, marc); assertTrue
 * (personRepo.following(linda, new PageRequest(0, 10)).size() == 0); }
 * 
 * @Test public void testBlockPerson() throws ControllerException { Person linda
 * = personRepo.findByUsername("linda"); Person marc =
 * personRepo.findByUsername("marc"); controller.blockPerson(linda, marc);
 * Errand item7 = new Errand(); item7.setOwner(marc); item7.setAdded(new
 * Date()); item7.setAnonymous(true);
 * item7.setDetails("I want some pop tarts!"); item7.setIsExternalUrl(false);
 * item7.setLocation("St Andrews"); item7.setType("Assistance");
 * controller.postErrand(item7); assertTrue (controller.interestFeed(linda, 0,
 * 100).size() == 0); }
 * 
 * 
 * 
 * @Test public void testChangeLocation() throws ControllerException { Person
 * marc = personRepo.findByUsername("marc"); controller.changeLocation(marc,
 * "Utah, United States");
 * assertTrue(personRepo.findByUsername("marc").getLocality
 * ().getName().equals("Utah, United States")); }
 * 
 * @Test public void testDeactivateAccount() throws ControllerException { Person
 * marc = personRepo.findByUsername("marc"); controller.deactivateAccount(marc);
 * assertTrue
 * (personRepo.findOne(marc.getId()).getStatus().equals("deactivated")); }
 * 
 * @Test public void testSearchPersonByNameOrUsername() throws
 * ControllerException {
 * assertTrue(controller.searchPersonByNameOrUsername("Marc ColE").size() == 1);
 * assertTrue(controller.searchPersonByNameOrUsername("linda").size() == 1); }
 * 
 * @Test public void testSendMail() { //fail("Not yet implemented"); }
 * 
 * private Person createMockUser(String email, String username) { Place place =
 * new Place("St Andrews, Fife, United Kingdom"); List<String> smarty =
 * Arrays.asList("Local Shopping", "Holiday Shopping", "Delivery", "Cleaning",
 * "Assistance", "Handyman"); Person simon = new Person("Simon", username,
 * email, "Password", "1989", "Male", place, smarty); return simon;
 * 
 * }
 * 
 * }
 */