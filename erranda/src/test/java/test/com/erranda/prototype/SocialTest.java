package test.com.erranda.prototype;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class SocialTest extends ControllerTest {

	public SocialTest() {

	}

	@Test
	public void friendSuggestion() throws ControllerException {
		PlatformUser person = personRepo.findByEmail("simon@sparekg.com");
		if (person == null) {
			List<String> shopper = Arrays.asList("Local Shopping",
					"Holiday Shopping");
			List<String> smarty = Arrays.asList("Delivery", "Cleaning",
					"Assistance", "Handyman");
			PlatformUser simon = new PlatformUser("Simon", "Substepr", "simon@sparekg.com",
					"Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);
			PlatformUser ali = new PlatformUser("Ali West", "AliEast", "ali@sparekg.com",
					"Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);
			PlatformUser gates = new PlatformUser("Bill Gates", "Gates",
					"gates@sparekg.com", "Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);
			PlatformUser jobs = new PlatformUser("Steve Jobs", "Jobs", "jobs@sparekg.com",
					"Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);
			PlatformUser zuck = new PlatformUser("Mark Zuckerberg", "Zuck",
					"zuck@sparekg.com", "Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);
			PlatformUser jazzy = new PlatformUser("Don Jazzy", "Jazzy",
					"jazzy@sparekg.com", "Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);
			PlatformUser hamel = new PlatformUser("Linda Hamel", "Hammel",
					"hamel@sparekg.com", "Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);

			PlatformUser rasha = new PlatformUser("Rasha Hatoum", "ItsRasha",
					"rasha@sparekg.com", "Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);
			PlatformUser frank = new PlatformUser("Frank", "Frank", "frank@sparekg.com",
					"Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", smarty);
			PlatformUser linda = new PlatformUser("Linda", "Mehico", "linda@sparekg.com",
					"Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", shopper);
			PlatformUser folarin = new PlatformUser("Folarin", "Erranda2",
					"folarin@sparekg.com", "Password", "1989", "Male",
					"Lagos, Nigeria", smarty);
			List<PlatformUser> g = Arrays.asList(simon, rasha, frank, linda, folarin,
					hamel, jazzy, zuck, jobs, gates, ali);
			for (PlatformUser p : g)
				person = controller.signUp(p);
			p(simon.getId());
			Long frid = controller.sendFriendRequest(simon.getId(),
					linda.getId()).getId();
			controller.acceptFriendRequest(frid);
			Long frid2 = controller.sendFriendRequest(simon.getId(),
					folarin.getId()).getId();
			controller.acceptFriendRequest(frid2);
			p(controller.friendSuggestions(folarin.getId(), 0, 10).size());
			assertTrue(controller.friendSuggestions(folarin.getId(), 0, 10)
					.size() > 0);
			assertTrue(controller.friendSuggestions(folarin.getId(), 0, 10)
					.get(0).getPerson().getId().longValue() == linda.getId());

		}
	}

}
