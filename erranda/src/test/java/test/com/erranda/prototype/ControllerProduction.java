package test.com.erranda.prototype;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.erranda.prototype.App;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.repo.ErrandRepository;
import com.erranda.prototype.domain.repo.PlatformUserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = App.class)
@ActiveProfiles("test")
public class ControllerProduction {
	@Autowired
	ErrandRepository errandRepo;

	@Autowired
	Controller controller;

	@Autowired
	PlatformUserRepository personRepo;

	@Autowired
	Neo4jTemplate template;

	void p(Object o) {
		System.out.println(o.toString());
	}

}