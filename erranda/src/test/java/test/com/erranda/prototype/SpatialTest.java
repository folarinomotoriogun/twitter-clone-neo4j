package test.com.erranda.prototype;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Date;

import org.apache.commons.collections.iterators.IteratorEnumeration;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.erranda.prototype.domain.Commented;
import com.erranda.prototype.domain.CommentKudos;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.FriendRequest;
import com.erranda.prototype.domain.Friends;
import com.erranda.prototype.domain.Notification;
import com.erranda.prototype.domain.PlatformUser;

public class SpatialTest extends ControllerTest {
	

	@Test
	@Transactional
	public void test() throws ControllerException {
		PlatformUser simon = new PlatformUser();
		if ((simon = personRepo.findByEmail("simon@sparekg.com")) == null) {
			simon = new PlatformUser("simon", "simon", "simon@sparekg.com",
					"Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", null);
			simon.setFirstName("Simon");
			simon.setLastName("C");
			p (simon.username);
			simon = controller.signUp(simon);
		}
		
		assertTrue (personRepo.findByUsername("simon") != null);
		assertTrue (controller.findUsername("simon") != null);
		Errand item2 = new Errand();
		item2.setOwner(simon);
		item2.setAdded(new Date());
		item2.setAnonymous(false);
		item2.setDetails("I want some pop tarts!");
		item2.setIsExternalUrl(false);
		item2.setErrandLocation("St Andrews, Fife, United Kingdom");
		item2.setPrivacy("LOCAL");
		item2 = controller.postErrand(item2, simon.getId(), null);
		controller.updatePostDetails(simon.getId(), item2.getId(), "sdad'sd");
		Commented c = new Commented ();
		c.setSender(simon);
		c.setErrand(item2);
		c.setCtime(new Date ());
		c = controller.comment(simon.getId(), item2.getId(), "Hello", false);
		controller.commentKudos(c.getId(), simon.getId());
		//controller.kudos(item2.getId(), simon.getId());
		p(controller.myKudosCount(simon.getId()));
		PlatformUser linda = new PlatformUser("Linda", "Mehico", "linda@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				null);
		linda.setFirstName("");
		linda.setLastName("");
		linda = controller.signUp(linda);
		
		p (controller.interestFeed(linda.getId(), 0, 100).size());
		assertTrue (controller.interestFeed(linda.getId(), 0, 100).size() == 1);
		Long frid = controller.sendFriendRequest(simon.getId(), linda.getId()).getId();
		controller.acceptFriendRequest(frid);
		assertTrue (controller.areWeFriends(simon.getId(), linda.getId()));
		controller.nominatePerson(Arrays.asList(simon), item2.getId(), linda.getId(), null);
		controller.acceptErrand(item2.getId(), simon.getId(), null);
		controller.comment(linda.getId(), item2.getId(), "cloo", false);
		assertTrue (controller.areWeFriends(simon.getId(), linda.getId()));
		
		Errand item3 = new Errand();
		item3.setOwner(simon);
		item3.setAdded(new Date());
		item3.setAnonymous(false);
		item3.setDetails("I want some pop tarts!");
		item3.setIsExternalUrl(false);
		item3.setErrandLocation("St Andrews, Fife, United Kingdom");
		item3.setPrivacy("LOCAL");
		item3 = controller.postErrand(item3, simon.getId(), null);
		
		
		p (controller.findPerson(simon.getId()).getPreferredPrivacyOption());
		assertTrue (controller.findPerson(simon.getId()).getPreferredPrivacyOption().equals("LOCAL"));
		controller.comment (linda.getId(), item2.getId(), "God is good", false);

		controller.blockPerson(simon.getId(), linda.getId());
		p(controller.isBlocked(simon.getId(), linda.getId()));
		p(controller.myBlockList(simon.getId()));
		// Long f = controller.sendFriendRequest(linda.getId(),
		// simon.getId()).getId();

		p(controller.interestFeed(simon.getId(), 0, 100).size());
		p(controller.personFeed(simon.getId(), simon.getId(), 0, 100).size());
		
		// controller.acceptFriendRequest(f);

		FriendRequest fr = template.findOne(
				controller.sendFriendRequest(linda.getId(), simon.getId())
						.getId(), FriendRequest.class);
		Friends friend = new Friends();
		friend.sender = fr.sender;
		friend.receiver = fr.receiver;
		friend.ctime = new Date();
		template.save(friend);
		template.delete(fr);
		PlatformUser sender = fr.sender;
		Notification n = new Notification();
		n.setCtime(new Date());
		n.setLink(fr.receiver.getLink());
		n.setPictureLink(fr.receiver.getProfilePictureUrl());
		n.setType("Friends");
		n.setDetails(fr.receiver.getName() + " accepted your friend request");
		save(n);
		p(controller.interestFeed(simon.getId(), 0, 100).size());
		p(controller.personFeed(simon.getId(), simon.getId(), 0, 100).size());
	}

	@Transactional
	@Before
	public void cleanup() {
		personRepo.deleteAll();
		errandRepo.deleteAll();
	}

	@Transactional
	private Object save(Object object) {
		return template.save(object);
	}

	@Override
	void p(Object o) {
		System.out.println(o.toString());
	}
}
