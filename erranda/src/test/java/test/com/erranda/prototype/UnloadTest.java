package test.com.erranda.prototype;

import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.erranda.prototype.domain.ControllerException;

public class UnloadTest extends ControllerProduction {

	@Test
	@Transactional
	public void test() throws ControllerException {
		personRepo.deleteAll();
		errandRepo.deleteAll();
	}

	@Override
	void p(Object o) {
		System.out.println(o.toString());
	}
}
