package test.com.erranda.prototype;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.junit.Test;

import com.erranda.prototype.web.component.ImageUtils;
import com.erranda.prototype.web.component.TimeConverter;

public class Wicket {

	@Test
	public void cropImage() throws IOException {
		File file = new File(
				"C:/Users/Folarin/git/erranda/erranda/0323560c-8855-44c7-a6cd-28996966197fpicture000.jpg");
		ImageUtils.cropImage(file, "C:/Users/Folarin/git/erranda/erranda/0323560c-8855-44c7-a6cd-28996966197fpicture000small.jpg", 140, 16, 400, 400);
	}

	@Test
	public void timeAgo() throws IOException {
		System.out.println(TimeConverter.timeAgo(new Date()));
	}

}
