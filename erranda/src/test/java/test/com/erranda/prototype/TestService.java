package test.com.erranda.prototype;

import org.apache.wicket.util.tester.WicketTester;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.erranda.prototype.App;

/**
 * This class is used mainly to encapsulate the initialization of the
 * WicketTester.
 * 
 * Each WebApplication can only be used for tester instantiation once. App is a
 * spring bean (singleton) therefore only a WicketTester singleton is possible.
 * 
 * @author kloe
 * 
 */
@Component
public class TestService implements InitializingBean {

	@Autowired
	private App app;

	private WicketTester wicketTester;

	@Override
	public void afterPropertiesSet() throws Exception {
		wicketTester = new WicketTester();
	}

	public WicketTester getWicketTester() {
		return wicketTester;
	}

}