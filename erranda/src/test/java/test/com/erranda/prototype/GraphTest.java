package test.com.erranda.prototype;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.erranda.prototype.App;
import com.erranda.prototype.domain.Commented;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.FriendRequest;
import com.erranda.prototype.domain.Node;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.SuggestionObject;
import com.erranda.prototype.domain.repo.AbstractRepository;
import com.erranda.prototype.domain.repo.ConversationRepository;
import com.erranda.prototype.domain.repo.ErrandRepository;
import com.erranda.prototype.domain.repo.PlatformUserRepository;
import com.erranda.prototype.domain.repo.RelationshipRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppTest.class)
@ActiveProfiles("test")
public class GraphTest {

	@Autowired
	ErrandRepository errandRepo;

	@Autowired
	Controller controller;

	@Autowired
	PlatformUserRepository personRepo;

	@Autowired
	Neo4jTemplate template;

	@Autowired
	AbstractRepository abstractRepo;

	@Autowired
	ConversationRepository conversationRepo;

	@Autowired
	RelationshipRepository relationshipRepo;

	@Before
	public void setup() throws ControllerException, IOException {
		errandRepo.deleteAll();
		personRepo.deleteAll();
	}

	@After
	public void destroy() throws IOException {
		// errandRepo.deleteAll();
		// personRepo.deleteAll();
	}

	// @Test
	public void batchCreate() throws ControllerException {
		PlatformUser simon = personRepo.findByEmail("simon@sparekg.com");
		List<Errand> errands = new ArrayList<Errand>();
		for (int i = 0; i < 30; i++) {
			Errand item2 = new Errand();
			item2.setOwner(simon);
			item2.setAdded(new Date());
			item2.setAnonymous(false);
			item2.setDetails("I want some pop tarts!");
			item2.setIsExternalUrl(false);
			item2.setErrandLocation("St Andrews, Fife, United Kingdom");
			errands.add(item2);
		}
		controller.batchInsertErrand(errands);
	}

	@Test
	@Transactional
	public void positive() throws ControllerException {
		List<String> shopper = Arrays.asList("Local Shopping",
				"Holiday Shopping");
		List<String> smarty = Arrays.asList("Delivery", "Cleaning",
				"Assistance", "Handyman");
		PlatformUser simon = new PlatformUser("Simon Constan", "Substepr", "simon@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser ali = new PlatformUser("Ali West", "AliEast", "ali@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser gates = new PlatformUser("Bill Gates", "Gates", "gates@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser jobs = new PlatformUser("Steve Jobs", "Jobs", "jobs@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser zuck = new PlatformUser("Mark Zuckerberg", "Zuck", "zuck@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser jazzy = new PlatformUser("Don Jazzy", "Jazzy", "jazzy@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser hamel = new PlatformUser("Linda Hamel", "Hammel", "hamel@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);

		PlatformUser rasha = new PlatformUser("Rasha Hatoum", "ItsRasha",
				"rasha@sparekg.com", "Password", "1989", "Male",
				"St Andrews, Fife, United Kingdom", smarty);
		PlatformUser frank = new PlatformUser("Frank Jowe", "Frank", "frank@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser linda = new PlatformUser("Linda Mendez", "Mehico", "linda@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				shopper);
		PlatformUser folarin = new PlatformUser("Folarin O", "Erranda2",
				"folarin@sparekg.com", "Password", "1989", "Male",
				"St Andrews, Fife, United Kingdom", smarty);
		List<PlatformUser> g = Arrays.asList(simon, rasha, frank, linda, folarin,
				hamel, jazzy, zuck, jobs, gates, ali);
		for (PlatformUser p : g)
			p = controller.signUp(p);
		Errand item2 = new Errand();
		item2.setOwner(simon);
		item2.setAdded(new Date());
		item2.setAnonymous(false);
		item2.setDetails("I want some pop tarts!");
		item2.setIsExternalUrl(false);
		item2.setErrandLocation("St Andrews, Fife, United Kingdom");
		item2.setPrivacy("LOCAL");
		item2 = controller.postErrand(item2, simon.getId(), null);
		controller.followUser(simon.getId(), linda.getId());
		p (controller.following(simon.getId(), simon.getId(), 0, 100).size());
		assertTrue(controller.following(linda.getId(), simon.getId(), 0, 100).size() == 1);
		Long frid = controller.sendFriendRequest(simon.getId(), linda.getId())
				.getId();
		controller.acceptFriendRequest(frid);
		Long frid5 = controller.sendFriendRequest(ali.getId(), linda.getId())
				.getId();
		controller.acceptFriendRequest(frid5);
		Long frid3 = controller.sendFriendRequest(simon.getId(), ali.getId())
				.getId();
		controller.acceptFriendRequest(frid3);
		Long frid2 = controller.sendFriendRequest(simon.getId(),
				folarin.getId()).getId();
		controller.acceptFriendRequest(frid2);
		Long frid4 = controller.sendFriendRequest(folarin.getId(), ali.getId())
				.getId();
		controller.acceptFriendRequest(frid4);
		
		p(controller.friendSuggestions(folarin.getId(), 0, 10).size());

		assertTrue(controller.friendSuggestions(folarin.getId(), 0, 10).size() > 0);
		for (SuggestionObject s : controller.friendSuggestions(folarin.getId(),
				0, 10)) {
			p(s.getPerson().getName() + " " + s.getMetaData());
		}

		Long sid = linda.getId();
		Long rid = simon.getId();
		controller.interestFeed(rid, 0, 100);
		Long fid = folarin.getId();

		PlatformUser a = new PlatformUser();
		a.setId(sid);
		PlatformUser b = new PlatformUser();
		b.setId(fid);

		/**
		 * Errand Feed
		 *
		 */
		
		p(controller.interestFeed(sid, 0, 100).size());
		assertTrue(controller.interestFeed(linda.getId(), 0, 100).size() == 1);

		Errand item4 = new Errand();
		item4.setOwner(simon);
		item4.setAdded(new Date());
		item4.setAnonymous(false);
		item4.setDetails("I want some pop tarts!");
		item4.setIsExternalUrl(false);
		item4.setErrandLocation("Agege, Lagos, Nigeria");
		item4.setPrivacy("FRIENDS");
		item4 = controller.postErrand(item4, simon.getId(), null);
		controller.blockPerson(fid, simon.getId());
		p(controller.interestFeed(folarin.getId(), 0, 100).size());
		p(controller.personFeed(folarin.getId(), sid, 0, 100).size());
		assertTrue(controller.interestFeed(sid, 0, 100).size() == 2);

		controller.nominatePerson(new ArrayList<PlatformUser>(Arrays.asList(linda)),
				item2.getId(), simon.getId(), "");
		controller.nominatePerson(Arrays.asList(linda), item2.getId(),
				folarin.getId(), "");
		p(controller.nominationsCountByErrand(item2.getId()));
		assertTrue(controller.nominationsCountByErrand(item2.getId()) == 1);
		p(controller.interestFeed(sid, 0, 100).size());
		assertTrue(controller.interestFeed(sid, 0, 100).size() == 1);
		assertTrue(controller.interestFeed(rid, 0, 100).size() == 1);
		controller.deleteErrand(item2.getId());
		assertTrue(controller.interestFeed(rid, 0, 100).size() == 0);

		controller.sendMessage(sid, rid, "Love God");
		assertTrue(controller.inbox(rid, 0, 10).size() == 1);
		Long cid = controller.getConversation(sid, rid).getId();
		assertTrue(conversationRepo.unread(cid, rid).size() == 1);
		conversationRepo.markMyMessagesReadInConversation(cid, rid);
		assertTrue(conversationRepo.unread(cid, rid).size() == 0);
		assertTrue(controller.messages(cid, 0, 10).size() == 1);

		Errand item3 = new Errand();
		item3.setOwner(simon);
		item3.setOwnerId(simon.getId());
		item3.setAdded(new Date());
		item3.setAnonymous(true);
		item3.setDetails("I want some pop tarts!");
		item3.setIsExternalUrl(false);
		item3.setErrandLocation("St Andrews, Fife, United Kingdom");
		item3 = controller.postErrand(item3, simon.getId(), null);

		/**
		 * Accpet actions
		 * 
		 */
		// Proposal proposal = controller.acceptErrand(item3.getId(), sid,
		// "Could do for 20 buck");
		// assertTrue (abstractRepo.myNotifications(rid, 0, 10).size() == 1);
		// assertTrue (controller.getProposalByErrand(item3.getId(), 0,
		// 10).size() == 1);
		// assertTrue (controller.getProposalsByPerson(simon.getId(), 0,
		// 10).size() == 1);
		// controller.confirmProposal(proposal.getId(),
		// "Thanks again for helping");
		// assertTrue (controller.acceptedErrands(sid, 0, 10).size() == 1);
		// assertTrue (controller.pointsCount(sid) == 100);
		// assertTrue (abstractRepo.myNotifications(sid, 0, 10).size() == 1);
		// controller.deleteProposal(proposal.getId());
		// assertTrue (controller.getProposalsByPerson(simon.getId(), 0,
		// 10).size() == 0);
		// assertTrue (controller.pointsCount(sid) == 0);

		Integer ridnot = abstractRepo.myNotifications(rid, 0, 1000).size();
		Integer sidnot = abstractRepo.myNotifications(sid, 0, 1000).size();
		/**
		 * Comment Actions
		 */
		// Simon owns post but linda comments.
		Commented c = controller.comment(linda.getId(), item3.getId(),
				"I love you!", false);
		assertTrue(abstractRepo.myNotifications(rid, 0, 10).size() == ridnot + 1);
		abstractRepo.markAllNotificationsAsRead(rid);
		Commented c1 = controller.comment(simon.getId(), item3.getId(),
				"I love you!", false);
		assertTrue(abstractRepo.unseenNotifications(rid, 0, 10).size() == 0);
		assertTrue(abstractRepo.unseenNotifications(sid, 0, 10).size() == sidnot + 1);
		abstractRepo.markAllNotificationsAsRead(sid);
		Commented c2 = controller.comment(folarin.getId(), item3.getId(),
				"I love you!", false);
		assertTrue(abstractRepo.unseenNotifications(rid, 0, 10).size() == 1);
		assertTrue(abstractRepo.unseenNotifications(sid, 0, 10).size() == 1);
		assertTrue(abstractRepo.comments(item3.getId(), 0, 100).size() == 3);
		assertTrue(controller.getComments(simon.getId(), item3.getId(), 0, 100).size() == 3);
		assertTrue(abstractRepo.findComment(c.getId()).getAnonymous() != null);
		assertTrue(abstractRepo.findComments(c.getId()).getCtime() != null);
		controller.deleteComment(c.getId());
		assertTrue(controller.commentCountByErrand(item3.getId()) == 2);

		/**
		 * Nominate actions
		 */
		controller.nominatePerson(Arrays.asList(linda), item3.getId(),
				linda.getId(), "Guys check this out!");

		/**
		 * Friend actions
		 */
		FriendRequest freq = controller.sendFriendRequest(linda.getId(),
				simon.getId());
		assertTrue(controller.getFriendRequestsByPerson(simon.getId(), 0, 100)
				.size() == 1);
		controller.acceptFriendRequest(freq.getId());
		assertTrue(controller.areWeFriends(sid, rid) != null);
		assertTrue(controller.areWeFriends(rid, sid) != null);
		assertTrue(controller.searchFriends(simon.getId(), "Linda ").size() == 1);
		assertTrue(controller.searchFriends(simon.getId(), "Lin").size() == 1);

		p(controller.searchFriends(simon.getId(), "Linda ").get(0));
		assertTrue(controller.searchFriends(simon.getId(), "Folarin ").size() == 0);
		assertTrue(controller.getFriendRequestsByPerson(rid, 0, 100).size() == 0);
		controller.removeFriend(sid, rid);
		FriendRequest freq2 = controller.sendFriendRequest(linda.getId(),
				simon.getId());
		assertTrue(controller.getFriendRequestsByPerson(simon.getId(), 0, 100)
				.size() == 1);
		controller.acceptFriendRequest(freq2.getId());
		controller.blockPerson(sid, rid);
		assertTrue(controller.blockList(sid, 0, 10).size() == 1);
		
		// .sender.equals(linda));
		// assertTrue(controller.theFriendRequestISent(sid, rid) != null);
		// assertTrue(controller.friends(sid, 0, 10).size() == 1);
		// assertTrue(controller.friends(rid, 0, 10).size() == 1);
		/*
		 * linda.follow(folarin); Group group = new Group ();
		 * group.setName("St A"); group = groupersonRepo.save(group);
		 * linda.joinGroup(group); personRepo.save(linda); p ("Saved");
		 * controller.blockPerson(simon, linda); p ("Blocked"); Errand item2 =
		 * new Errand (); item2.setOwner(simon);
		 * item2.setOwnerId(simon.getId()); item2.setAdded(new Date());
		 * item2.setAnonymous(true); item2.setDetails("I want some pop tarts!");
		 * item2.setIsExternalUrl(false); item2.setType("Local Shopping");
		 * item2.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * 
		 * Errand item3 = new Errand (); item3.setOwner(simon);
		 * item3.setOwnerId(simon.getId()); item3.setAdded(new Date());
		 * item3.setAnonymous(true); item3.setDetails("I want some pop tarts!");
		 * item3.setIsExternalUrl(false); item3.setLocation(16, 20);
		 * item3.setType("Local Shopping");
		 * item3.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * 
		 * Errand item4 = new Errand (); item4.setOwner(simon);
		 * item4.setOwnerId(simon.getId()); item4.setAdded(new Date());
		 * item4.setAnonymous(true); item4.setDetails("I want some pop tarts!");
		 * item4.setLocation(16, 20); item4.setIsExternalUrl(false);
		 * item4.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * item4.setType("Local Shopping");
		 * 
		 * Errand item5 = new Errand (); item5.setOwner(simon);
		 * item5.setOwnerId(simon.getId()); item5.setAdded(new Date());
		 * item5.setAnonymous(true); item5.setDetails("I want some pop tarts!");
		 * item5.setLocation(16, 20); item5.setIsExternalUrl(false);
		 * item5.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * item5.setType("Local Shopping");
		 * 
		 * Errand item6 = new Errand (); item6.setOwner(simon);
		 * item6.setOwnerId(simon.getId()); item6.setAdded(new Date());
		 * item6.setAnonymous(true); item6.setDetails("I want some pop tarts!");
		 * item6.setLocation(16, 20); item6.setIsExternalUrl(false);
		 * item6.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * item6.setType("Local Shopping");
		 * 
		 * Errand item7 = new Errand (); item7.setOwner(simon);
		 * item7.setOwnerId(simon.getId()); item7.setAdded(new Date());
		 * item7.setAnonymous(true); item7.setDetails("I want some pop tarts!");
		 * item7.setLocation(16, 20); item7.setIsExternalUrl(false);
		 * item7.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * item7.setType("Local Shopping");
		 * 
		 * Errand item8 = new Errand (); item8.setOwner(simon);
		 * item8.setOwnerId(simon.getId()); item8.setAdded(new Date());
		 * item8.setAnonymous(true); item8.setDetails("I want some pop tarts!");
		 * item8.setLocation(16, 20); item8.setIsExternalUrl(false);
		 * item8.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * item8.setType("Local Shopping");
		 * 
		 * Errand item9 = new Errand (); item9.setOwner(simon);
		 * item9.setOwnerId(simon.getId()); item9.setAdded(new Date());
		 * item9.setAnonymous(true); item9.setDetails("I want some pop tarts!");
		 * item9.setLocation(16, 20); item9.setIsExternalUrl(false);
		 * item9.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * item9.setType("Local Shopping");
		 * 
		 * Errand item10 = new Errand (); item10.setOwner(simon);
		 * item10.setOwnerId(simon.getId()); item10.setAdded(new Date());
		 * item10.setAnonymous(true);
		 * item10.setDetails("I want some pop tarts!"); item10.setLocation(16,
		 * 20); item10.setIsExternalUrl(false);
		 * item10.setErrandLocation("St Andrews, Fife, United Kingdom");
		 * item10.setType("Local Shopping");
		 * 
		 * try { controller.postErrand(item2, item3, item4, item5, item6, item7,
		 * item8, item9, item10); } catch (ControllerException e1) { // TODO
		 * Auto-generated catch block e1.printStackTrace(); }
		 * 
		 * List<Errand> lindaFeed = errandRepo.localFeed(linda, new PageRequest
		 * (0, 10)); System.out.println (lindaFeed.size());
		 * assertTrue(lindaFeed.size() == 4);
		 * 
		 * List<Errand> gFeed = errandRepo.groupFeed(linda, new PageRequest (0,
		 * 10)); System.out.println (gFeed.size()); assertTrue(gFeed.size() ==
		 * 1);
		 * 
		 * List<Errand> fFeed = errandRepo.followingFeed(linda, new PageRequest
		 * (0, 10)); System.out.println (fFeed.size()); assertTrue(fFeed.size()
		 * == 1); System.out.println (folarin.getId()); for (Errand e :
		 * controller.personFeed(simon, 0, 10)) {
		 * p(query.findErrand(e.getId()).getOwner().getName()); }
		 * 
		 * p(personRepo.findOne(folarin.getId()).getName()); assertTrue
		 * (personRepo.countFollowers(folarin.getId()) == 1); p
		 * ("Followers counted"); assertTrue
		 * (personRepo.isFollowing(folarin.getId(), linda.getId())); p
		 * ("Following counted"); assertTrue
		 * (personRepo.isBlocked(simon.getId(), linda.getId())); p
		 * ("Person blocked"); controller.interestFeed(folarin, 0, 10);
		 */
		// p (errandRepo.errandsNearMe(folarin.getLongitude() + "",
		// folarin.getLatitude() + "", "12.0").size());
		// assertTrue (errandRepo.errandsNearMe(folarin.getLongitude() + "",
		// folarin.getLatitude() + "", "12.0").size() == 12);
		/*
		 * Thankyou thanks = new Thankyou (); thanks.setSender(folarin);
		 * thanks.setReceiver(linda);
		 * thanks.setData("Thank you for your time at the event");
		 * controller.sayThankYou(thanks); p ("Thank you");
		 * assertTrue(thanksRepo.countThankyous(linda) == 1); p
		 * ("Following counted posted"); assertTrue
		 * (groupersonRepo.countFollowers(group.getId()) == 1); p
		 * ("Group counted posted"); assertTrue
		 * (groupersonRepo.isFollowing(group.getId(), linda.getId())); p
		 * ("Is followinf posted"); Set<NamedNode> nodes = new
		 * TreeSet<NamedNode>();
		 * 
		 * //addNodes(personRepo.findByNameContainsIgnoreCaseOrderByNameAsc("Linda"
		 * , new PageRequest (0, 10)), nodes);
		 * //addNodes(personRepo.findByNameContainsIgnoreCaseOrderByNameAsc
		 * ("Mendez", new PageRequest (0, 10)), nodes); //-+
		 * //addNodes(groupersonRepo.findByNameContainsIgnoreCaseOrderByNameAsc
		 * ("Linda", new PageRequest (0, 10)), nodes);
		 * //nodes.addAll(placeRepo.findByNameContainsIgnoreCaseOrderByNameAsc
		 * ("Linda", new PageRequest (0, 10)));
		 * 
		 * assertTrue (nodes.size() == 1);
		 */

	}

	private void p(Object size) {
		System.out.println(size);

	}

	private Node pruneToOne(List<Node> nodes) {
		Node returnNode = null;
		if (nodes != null && nodes.size() > 1) {
			returnNode = nodes.get(0);
			for (Node n : nodes) {
				if (!n.equals(returnNode))
					template.delete(n);
			}
		}
		return returnNode;
	}

}
