package test.com.erranda.prototype;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;

public class TestApp extends ControllerProduction {

	//@Test
	public void test() throws ControllerException {

		PlatformUser object = personRepo.findByUsername("foo");
		assertTrue (object != null);
		List<String> shopper = Arrays.asList("Local Shopping",
				"Holiday Shopping");
		List<String> smarty = Arrays.asList("Delivery", "Cleaning",
				"Assistance", "Handyman");
		PlatformUser simon = new PlatformUser("Simon", "Substepr", "simon@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser ali = new PlatformUser("Ali West", "AliEast", "ali@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser gates = new PlatformUser("Bill Gates", "Gates", "gates@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser jobs = new PlatformUser("Steve Jobs", "Jobs", "jobs@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser zuck = new PlatformUser("Mark Zuckerberg", "Zuck", "zuck@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser jazzy = new PlatformUser("Don Jazzy", "Jazzy", "jazzy@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser hamel = new PlatformUser("Linda Hamel", "Hammel", "hamel@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);

		PlatformUser rasha = new PlatformUser("Rasha Hatoum", "ItsRasha",
				"rasha@sparekg.com", "Password", "1989", "Male",
				"St Andrews, Fife, United Kingdom", smarty);
		PlatformUser frank = new PlatformUser("Frank", "Frank", "frank@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				smarty);
		PlatformUser linda = new PlatformUser("Linda", "Mehico", "linda@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				shopper);
		PlatformUser folarin = new PlatformUser("Folarin", "Erranda2",
				"folarin@sparekg.com", "Password", "1989", "Male",
				"St Andrews, Fife, United Kingdom", smarty);
		List<PlatformUser> g = Arrays.asList(simon, rasha, frank, linda, folarin,
				hamel, jazzy, zuck, jobs, gates, ali);
		for (PlatformUser p : g) {
			p = controller.signUp(p);
			controller.sendFriendRequest(p.getId(), object.getId());
		}
			
	}
	
	@Test
	@Transactional
	public void runTest () {
		Iterator<Errand> all = errandRepo.findAll().to(Errand.class).iterator();
		while (all.hasNext()) {
			Errand e = all.next();
			e.setKudosCount(0);
			template.save(e);
		}
	}
}
