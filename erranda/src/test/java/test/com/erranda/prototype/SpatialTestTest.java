package test.com.erranda.prototype;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;

public class SpatialTestTest extends ControllerTest {
	
	PlatformUser simon = null;
	
	PlatformUser linda = null;

	@Before
	public void setUp() throws Exception {
		simon = new PlatformUser();
		if ((simon = personRepo.findByEmail("simon@sparekg.com")) == null) {
			simon = new PlatformUser("Simon", "simon", "simon@sparekg.com",
					"Password", "1989", "Male",
					"St Andrews, Fife, United Kingdom", null);
			simon = controller.signUp(simon);
		}
		
		linda = new PlatformUser("Linda", "Mehico", "linda@sparekg.com",
				"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
				null);
		linda = controller.signUp(linda);
	}

	@After
	public void tearDown() throws Exception {
		personRepo.deleteAll();
	}


	@Test
	public void testNominatePerson() throws ControllerException {
		Long sid = simon.getId();
		Long rid = linda.getId();
		Long fr = controller.sendFriendRequest(linda.getId(), simon.getId()).getId();
		controller.acceptFriendRequest(fr);
		assertTrue (controller.areWeFriends(sid, rid));
		Errand item2 = new Errand();
		item2.setOwner(simon);
		item2.setAdded(new Date());
		item2.setAnonymous(false);
		item2.setDetails("I want some pop tarts!");
		item2.setIsExternalUrl(false);
		item2.setErrandLocation("St Andrews, Fife, United Kingdom");
		item2.setPrivacy("LOCAL");
		item2 = controller.postErrand(item2, simon.getId(), null);
		p(item2.getId());
		controller.nominatePerson(Arrays.asList(linda), item2.getId(), simon.getId(), null);
		assertTrue (controller.areWeFriends(sid, rid));
	}
	
	
	public void testSignUp() {
		fail("Not yet implemented");
	}

	
	public void testSendMessage() {
		fail("Not yet implemented");
	}

	
	public void testLogin() {
		fail("Not yet implemented");
	}

	
	public void testPostErrandErrandLong() {
		fail("Not yet implemented");
	}

	
	public void testPostErrandErrandArray() {
		fail("Not yet implemented");
	}

	
	public void testInterestFeed() {
		fail("Not yet implemented");
	}

	
	public void testAssertNotNullObjectControllerException() {
		fail("Not yet implemented");
	}

	
	public void testAssertNotNullObjectString() {
		fail("Not yet implemented");
	}

	
	public void testP() {
		fail("Not yet implemented");
	}

	
	public void testPersonFeed() {
		fail("Not yet implemented");
	}

	
	public void testUpdateProfile() {
		fail("Not yet implemented");
	}

	
	public void testUpdatePassword() {
		fail("Not yet implemented");
	}

	
	public void testDeactivateAccount() {
		fail("Not yet implemented");
	}

	
	public void testSendMailLongString() {
		fail("Not yet implemented");
	}

	
	public void testSendMailStringString() {
		fail("Not yet implemented");
	}

	
	public void testUpdateLocation() {
		fail("Not yet implemented");
	}

	
	public void testChangePassword() {
		fail("Not yet implemented");
	}

	
	public void testGetConversation() {
		fail("Not yet implemented");
	}


	
	public void testAcceptErrand() {
		fail("Not yet implemented");
	}

	
	public void testConfirmProposal() {
		fail("Not yet implemented");
	}

	
	public void testRejectProposal() {
		fail("Not yet implemented");
	}

	
	public void testArchiveErrand() {
		fail("Not yet implemented");
	}

	
	public void testSendFriendRequest() {
		fail("Not yet implemented");
	}

	
	public void testSave() {
		fail("Not yet implemented");
	}

	
	public void testAcceptFriendRequest() {
		fail("Not yet implemented");
	}

	
	public void testFindErrand() {
		fail("Not yet implemented");
	}

	
	public void testFindPerson() {
		fail("Not yet implemented");
	}

	
	public void testRemoveFriend() {
		fail("Not yet implemented");
	}

	
	public void testComment() {
		fail("Not yet implemented");
	}

	
	public void testRemoveComment() {
		fail("Not yet implemented");
	}

	
	public void testRejectFriendRequestLong() {
		fail("Not yet implemented");
	}

	
	public void testRejectFriendRequestLongLong() {
		fail("Not yet implemented");
	}

	
	public void testGetComments() {
		fail("Not yet implemented");
	}

	
	public void testGetProposalsByPerson() {
		fail("Not yet implemented");
	}

	
	public void testGetProposalByErrand() {
		fail("Not yet implemented");
	}

	
	public void testBlockPerson() {
		fail("Not yet implemented");
	}

	
	public void testUnblockPerson() {
		fail("Not yet implemented");
	}

	
	public void testFriends() {
		fail("Not yet implemented");
	}

	
	public void testIsPendingFriendRequest() {
		fail("Not yet implemented");
	}

	
	public void testTheFriendRequestISent() {
		fail("Not yet implemented");
	}

	
	public void testDeleteComment() {
		fail("Not yet implemented");
	}

	
	public void testGetFriendRequestsByPerson() {
		fail("Not yet implemented");
	}

	
	public void testBlockList() {
		fail("Not yet implemented");
	}

	
	public void testCommentCountByErrand() {
		fail("Not yet implemented");
	}

	
	public void testNominationsCountByErrand() {
		fail("Not yet implemented");
	}

	
	public void testProposeCountByErrand() {
		fail("Not yet implemented");
	}

	
	public void testFindOneComment() {
		fail("Not yet implemented");
	}

	
	public void testSearchFriends() {
		fail("Not yet implemented");
	}

	
	public void testGetMyNotifications() {
		fail("Not yet implemented");
	}

	
	public void testFindOneNotification() {
		fail("Not yet implemented");
	}

	
	public void testUnseenNotificationsCount() {
		fail("Not yet implemented");
	}

	
	public void testMarkAllNotificationAsSeen() {
		fail("Not yet implemented");
	}

	
	public void testFindOneProposal() {
		fail("Not yet implemented");
	}

	
	public void testCountFriends() {
		fail("Not yet implemented");
	}

	
	public void testAreWeFriends() {
		fail("Not yet implemented");
	}

	
	public void testInbox() {
		fail("Not yet implemented");
	}

	
	public void testMessages() {
		fail("Not yet implemented");
	}

	
	public void testFindOneConversation() {
		fail("Not yet implemented");
	}

	
	public void testFindOneMessage() {
		fail("Not yet implemented");
	}

	
	public void testMarkAllMessagesAsRead() {
		fail("Not yet implemented");
	}

	
	public void testPointsCount() {
		fail("Not yet implemented");
	}

	
	public void testDeleteProposal() {
		fail("Not yet implemented");
	}

	
	public void testDeleteErrand() {
		fail("Not yet implemented");
	}

	
	public void testReportPost() {
		fail("Not yet implemented");
	}

	
	public void testTwoWayBlockList() {
		fail("Not yet implemented");
	}

	
	public void testCountInterestFeed() {
		fail("Not yet implemented");
	}

	
	public void testBatchInsertErrand() {
		fail("Not yet implemented");
	}

	
	public void testFindFriendRequest() {
		fail("Not yet implemented");
	}

	
	public void testResetUnseenFriendRequestCount() {
		fail("Not yet implemented");
	}

	
	public void testUpdateProfilePicture() {
		fail("Not yet implemented");
	}

	
	public void testSearchPeople() {
		fail("Not yet implemented");
	}

	
	public void testSearchSize() {
		fail("Not yet implemented");
	}

	
	public void testGetPrivacySettings() {
		fail("Not yet implemented");
	}

	
	public void testFriendSuggestions() {
		fail("Not yet implemented");
	}

	
	public void testMutualFriends() {
		fail("Not yet implemented");
	}

	
	public void testSecurePersonFeed() {
		fail("Not yet implemented");
	}

	
	public void testNewMessagesCount() {
		fail("Not yet implemented");
	}

	
	public void testMarkAllNewMessages() {
		fail("Not yet implemented");
	}

	
	public void testPinAtPost() {
		fail("Not yet implemented");
	}

	
	public void testUnpinAtPost() {
		fail("Not yet implemented");
	}

	
	public void testPinCount() {
		fail("Not yet implemented");
	}

	
	public void testFindOnePost() {
		fail("Not yet implemented");
	}

	
	public void testForgotPassword() {
		fail("Not yet implemented");
	}

	
	public void testVerifyUser() {
		fail("Not yet implemented");
	}

	
	public void testTokenLogin() {
		fail("Not yet implemented");
	}

	
	public void testRemoveProfilePicture() {
		fail("Not yet implemented");
	}

	
	public void testOthersThatCommented() {
		fail("Not yet implemented");
	}

	
	public void testMyBlockList() {
		fail("Not yet implemented");
	}

	
	public void testIsBlocked() {
		fail("Not yet implemented");
	}

	
	public void testIsBlockedTwoWay() {
		fail("Not yet implemented");
	}

}
