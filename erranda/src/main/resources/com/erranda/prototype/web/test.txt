<html><head>
  <meta charset="utf-8">
  <title>Password Reset</title>
  <link href="https://abs-0.twimg.com/login/base.b99ec1b9d50fd508f22403d6f93df523dde2e845.css" rel="stylesheet">
  
</head>
<body class="ResponsiveLayout">
    <div class="TopNav">
      <div class="TopNav--container u-cf">
        
        
        <div class="Icon--logo u-pullLeft">
          <a class="nav-logo-link" href="/" data-nav="front">
                <span><strong>Errandr</strong></span>
          </a>
        </div>
        <div class="TopNav-title u-pullLeft">Password Reset</div>

      </div>
    </div>
  <div class="PageContainer">
        <div class="Section">
        <div class="PageHeader">
          Send me a link to reset my password
        </div>
        <p>
          Enter your email
        </p>

      <form wicket:id="begin-password-reset-form" class="Form" action="/account/begin_password_reset" method="post">
         <span wicket:id="login-fd"></span>
       <input wicket:id="email" type="email" name="account_identifier" class="Form-textbox is-required is-validatedRemotely " value="" autofocus="" autocorrect="off" autocapitalize="off">
        <input wicket:id="submit" type="submit" class="Button" value="Send">
      </form>
    </div>

  </div>
  


</body></html>