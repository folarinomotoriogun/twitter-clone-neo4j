package com.erranda.prototype.mobile;

import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.Home;
import com.erranda.prototype.web.component.StatelessIndicatingAjaxButton;
import com.erranda.prototype.web.mailer.EmailWelcome;

/**
 * Login page
 * 
 * @author kloe and Folarin
 * 
 */
public class SignupMobile extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8991168083149150709L;
	
	@SpringBean
	Controller controller;

	public SignupMobile() {
		if (UserSession.get().userLoggedIn()) {
			//throw new RestartResponseAtInterceptPageException (MobileIndex.class);
		}
		
		final PlatformUser person = new PlatformUser();
		
		StatelessForm<PlatformUser> signup = new StatelessForm<PlatformUser>("signup-form");

		// FeedbackPanel
		final FeedbackPanel feedback = new FeedbackPanel("signup-feedback");
		feedback.setOutputMarkupId(true);
		feedback.setMarkupId("signup-feedback");
		add(feedback);

		final TextField<String> fname = new TextField<String>("signup-firstname",
				new PropertyModel<String>(person, "firstName"));
		fname.setOutputMarkupId(true);
		
		final TextField<String> lname = new TextField<String>("signup-lastname",
				new PropertyModel<String>(person, "lastName"));
		lname.setOutputMarkupId(true);
		
		TextField<String> email = new TextField<String>("signup-email",
				new PropertyModel<String>(person, "email"));
		email.setOutputMarkupId(true);
		email.setLabel(new ResourceModel("label.email"));
		
		final TextField<String> username = new TextField<String>("signup-username",
				new PropertyModel<String>(person, "usernameRaw"));
		username.setOutputMarkupId(true);
		username.setLabel(new ResourceModel("label.username"));

		final TextField<String> password = new PasswordTextField("signup-password",
				new PropertyModel<String>(person, "password"));
		password.setOutputMarkupId(true);
		password.setRequired(false);
		
		final HiddenField<String> location = new HiddenField <String> ("user-location", new PropertyModel<String>(person, "localityName"));
		
		final StatelessIndicatingAjaxButton submit = new StatelessIndicatingAjaxButton("signup-button", signup) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				if (target == null)
					return;
				try {
					person.setName(person.getFirstName() + " "
							+ person.getLastName());
					PlatformUser session = controller.signUp(person);
					if(UserSession.get().isTemporary())
						UserSession.get().bind();
					UserSession.get().setPerson(session);
					UserSession.get().setUserId(session.getId());
					String html = new String(ComponentRenderer.renderPage(
							new PageProvider(new EmailWelcome(person)))
							.toString());
					MailMan man = new MailMan();
					man.sendMessage("Welcome to Erranda - Confirm Account",
							session.getEmail(), html);
					throw new RestartResponseAtInterceptPageException(Home.class);
				} catch (ControllerException e) {
					error(e.getMessage());
					target.add(feedback);
					String script = String
							.format("$('#%s').html('Sign Up');",
									this.getMarkupId());
					target.appendJavaScript(script);
				} catch (MessagingException e) {
					error(e.getMessage());
					target.add(feedback);
					String errorScript = String.format("$('#%s').popover('destroy').popover ({title: '', content: '%s', placement: 'left'}).popover('show');", this.getMarkupId() , e.getMessage());
					target.appendJavaScript(errorScript);
				}
				String script = String
						.format("$('#%s').html('Sign Up');",
								this.getMarkupId());
				target.appendJavaScript(script);
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(feedback);
			}
		};
		submit.setMarkupId("signup-button");
		submit.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').click(function () {$('#%s').html('Signing Up...');})",
								component.getMarkupId(), component.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		
		@SuppressWarnings("unchecked")
		List<TextField<String>> cs = Arrays.asList(fname, lname, username, password, email);
		for (TextField<String> t : cs) {
			t.add(new Behavior() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void renderHead(Component component, IHeaderResponse response) {
					String script = String
							.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13) {$('#%s').click();} })});",
									component.getMarkupId(),
									submit.getMarkupId());
					
					response.render(OnDomReadyHeaderItem.forScript(script));
				}
			});
		}
		
		signup.add(submit);

		signup.add(fname, lname, email, password, username, location);

		add(signup.setOutputMarkupId(true));
	}
}
