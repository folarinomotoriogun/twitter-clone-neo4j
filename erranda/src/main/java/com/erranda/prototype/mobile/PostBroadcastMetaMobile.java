package com.erranda.prototype.mobile;

import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostRecommend;
import com.erranda.prototype.web.Authenticated;
import com.erranda.prototype.web.ModalContainer;
import com.erranda.prototype.web.ModalSignUp;
import com.erranda.prototype.web.OtherPeopleNominated;
import com.erranda.prototype.web.component.ModalAjaxLink;
import com.erranda.prototype.web.component.TimeConverter;

public class PostBroadcastMetaMobile extends HtmlPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	WebMarkupContainer othersNominated = null;

	public PostBroadcastMetaMobile(String id, IModel<Errand> model)
			throws ControllerException {
		super(id, model);
		Errand errand = model.getObject();
		if (errand.getAnonymous()) {
			setVisible(false);
			return;
		}
		final Long eid = errand.getId();
		Integer count = controller.countFriendsWhoBroadcastedPost(loggedIn.getId(), eid);
		
		PlatformUser nominated = model.getObject().getOwner();
		if (count > 1) {
			othersNominated = new ModalAjaxLink<String> ("others") {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void onClick(AjaxRequestTarget target) {
					if (UserSession.get().userNotLoggedIn()) {
						Authenticated page = (Authenticated) getPage ();
						ModalContainer relationships = page.modalContainer;
						relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">Want to see people nominated?</h3>")).setEscapeModelStrings(false).setOutputMarkupId(true), new ModalSignUp("modal-content").setOutputMarkupId(true));
						relationships.update(target, "", true);
						return;
					}
					Authenticated page = (Authenticated) getPage ();
					ModalContainer relationships = page.modalContainer;
					relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">People nominated in this post</h3>")).setEscapeModelStrings(false), new OtherPeopleNominated ("modal-content", eid));
					relationships.update(target, "", true);
				}
			};
		} else {
			othersNominated = new WebMarkupContainer ("others");
			othersNominated.setVisible(false);
		}
		PlatformUser owner = errand.getOwner();
		Label action = null;
		LoadableDetachableModel<String> friendsNominated = new LoadableDetachableModel<String>() {

			@Override
			protected String load() {
				StringBuffer friendsLink = new StringBuffer();
				List<PlatformUser> friends;
				try {
					friends = controller.friendsWhoBroadcastedPost(loggedIn.getId(), eid, 0, 3);
					Integer count = friends.size();
					for (int i = 0; i < friends.size(); i++) {
						PlatformUser friend = friends.get(i);
						friendsLink.append(String.format(
							" <a href='%s'>%s</a> ", friend.getLink(),
							i ==  count - 1 ? (count > 1 ? " and " + friend.getName() : friend.getName()) : friend.getName() + ", "));
					}
					
					friendsLink.append(" broadcasted this ");
					
					} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return friendsLink.toString();
			}
		};
		action = new Label("action", friendsNominated);
		action.setEscapeModelStrings(false);
		add(action, othersNominated.setVisible(false));
		
	}

}
