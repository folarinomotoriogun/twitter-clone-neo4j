package com.erranda.prototype.mobile;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;

public class BaseMobile extends WebPage {
	
	@SpringBean
	Controller controller;
	
	PlatformUser loggedIn = UserSession.get().getPerson();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BaseMobile() {
		
	}

	public BaseMobile(PageParameters parameters) {
		super(parameters);
	}

}
