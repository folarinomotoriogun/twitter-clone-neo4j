package com.erranda.prototype.mobile;

import org.apache.wicket.RestartResponseAtInterceptPageException;

import com.erranda.prototype.UserSession;

public class HomeMobile extends BaseMobile {

	private static final long serialVersionUID = 1L;

	public HomeMobile() {
		if (UserSession.get().userLoggedIn())
			add(new InterestFeedMobile("feed"));
		else {
			throw new RestartResponseAtInterceptPageException(LoginMobile.class);
		}
	}

}
