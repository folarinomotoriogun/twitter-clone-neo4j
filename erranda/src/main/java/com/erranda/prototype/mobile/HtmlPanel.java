package com.erranda.prototype.mobile;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;

public class HtmlPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@SpringBean
	protected Controller controller;
	
	protected PlatformUser loggedIn = UserSession.get().getPerson();

	public HtmlPanel(String id) {
		super(id);
	}
	
	public HtmlPanel (String id, IModel<?> model) {
		super(id, model);
	}
	
	

}
