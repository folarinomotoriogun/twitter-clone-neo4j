package com.erranda.prototype.mobile;

import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostViewItem;
import com.erranda.prototype.web.ErrorPage;

public class InterestFeedMobile extends HtmlPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int skip;
	
	private int limit;
	
	private String appendableMarkup;
	
	private int lastItem;
	
	public InterestFeedMobile(String id) {
		super(id);
		
		skip = 0;
		
		limit = 20;
		
		WebMarkupContainer appendable = new WebMarkupContainer("appendable");
		appendable.setOutputMarkupId(true);
		appendableMarkup = appendable.getMarkupId();
		
		LoadableDetachableModel<List<PostViewItem>> posts = new LoadableDetachableModel<List<PostViewItem>>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected List<PostViewItem> load() {
				try {
					return controller.interestFeed(loggedIn.getId(), skip, limit);
				} catch (ControllerException e) {
					e.printStackTrace();
					throw new RestartResponseAtInterceptPageException(ErrorPage.class);
					
				}
			}
		};
		
		lastItem = posts.getObject().size();
		
		final ListView<PostViewItem> interestFeed = new ListView<PostViewItem>("post", posts) {
			
			@Override
			protected void populateItem(ListItem<PostViewItem> item) {
				PostViewItem viewItem = item.getModelObject();
				Errand errand = viewItem.post().getErrand();
				IModel<Errand> model = Model.of(errand);
				try {
					if (errand.getOwner() == null)
						errand.setOwner(controller.findPerson(errand.getOwnerId()));
					if (viewItem.relationship().equalsIgnoreCase("NOMINATED")) {
						item.add(new PostNominateMetaMobile("meta", model));
					} else if (viewItem.relationship().equalsIgnoreCase("BROADCASTED")) {
						item.add(new PostBroadcastMetaMobile("meta", model));
					} else {
						item.add(new PostOwnerNominateMetaMobile("meta", Model.of(viewItem.post())));
					}
					item.add(new ErrandMobile("item", model));
				} catch (ControllerException exception) {
					exception.printStackTrace();
					throw new RestartResponseAtInterceptPageException(ErrorPage.class);
				}
			}
		};
		
		AjaxLink<InterestFeedMobile> infiniteScroll = new AjaxLink<InterestFeedMobile>("infinite-scroll") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					skip = skip + limit;
					LoadableDetachableModel<List<PostViewItem>> posts = new LoadableDetachableModel<List<PostViewItem>>() {

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						protected List<PostViewItem> load() {
							try {
								return controller.interestFeed(loggedIn.getId(), skip, limit);
							} catch (ControllerException e) {
								e.printStackTrace();
								throw new RestartResponseAtInterceptPageException(ErrorPage.class);
								
							}
						}
					};
					
					if (posts.getObject().size() != limit) {
						String end = String.format("$('#%s').unbind(); $('#stream-end').show(); $('#stream-loader').hide()", this.getMarkupId());
						target.prependJavaScript(end);
					}
					
					List<PostViewItem> upNext = posts.getObject();
					
					for (PostViewItem viewItem: upNext) {
						lastItem++;
						Errand errand = viewItem.post().getErrand();
						ListItem<Errand> item = new ListItem<Errand>(new Integer( 
								lastItem).toString(), lastItem);
						Model<Errand> model = Model.of(errand);
						if (errand.getOwner() == null)
							errand.setOwner(controller.findPerson(errand.getOwnerId()));
						if (viewItem.relationship().equalsIgnoreCase("NOMINATED")) {
							item.add(new PostNominateMetaMobile("meta", model));
						} else if (viewItem.relationship().equalsIgnoreCase("BROADCASTED")) {
							item.add(new PostBroadcastMetaMobile("meta", model));
						} else {
							item.add(new PostOwnerNominateMetaMobile("meta", Model.of(viewItem.post())));
						}
						item.add(new ErrandMobile("item", model));
						item.setOutputMarkupId(true);
						interestFeed.add(item);
						target.prependJavaScript(String
								.format("var item=document.createElement('%s');item.class='stream-item stream-tweet'; item.id='%s';"
										+ "$('#%s').append(item);", "li",
										item.getMarkupId(), appendableMarkup));
						target.add(item);
						//Important to rebind events
						target.add(this);
					}
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		infiniteScroll.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$(window).scroll(function() {"
						+ "if($(window).scrollTop() + $(window).height() > $(document).height() /2) {"
						+ "$('#" + component.getMarkupId()
						+ "').trigger ('click');}});";
				String script2 = "$('#"
						+ component.getMarkupId()
						+ "').click(function(){$('#"
						+ component.getMarkupId()
						+ "').unbind('click');$('#stream-end').hide();$('#stream-loader').show();})";
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript(script2));
			}
		});
		add(appendable.add(interestFeed), infiniteScroll);
	}

}
