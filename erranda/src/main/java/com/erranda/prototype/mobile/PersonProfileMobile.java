package com.erranda.prototype.mobile;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.ErrorPage;

public class PersonProfileMobile extends BaseMobile {
	
	IModel<PlatformUser> user;


	public PersonProfileMobile(PageParameters parameters) {
		super(parameters);
		PlatformUser person = null;
		String username = parameters.get("username") != null ? parameters
				.get("username").toString().toLowerCase().trim() : null;
		if(username == null)
			throw new RestartResponseAtInterceptPageException(ErrorPage.class);
		final IModel<String> usernameModel = Model.of(username);
		user = new LoadableDetachableModel<PlatformUser>() {

			@Override
			protected PlatformUser load() {
				return controller.findUsername(usernameModel.getObject().toLowerCase());
			}
		};
		person = user.getObject();
		
		if (person != null && loggedIn != null) {
			boolean blocked;
			try {
				blocked = controller.isBlockedTwoWay(loggedIn.getId(), person.getId());
				if (blocked)
					throw new RestartResponseAtInterceptPageException(ErrorPage.class);
			} catch (ControllerException e) {
				e.printStackTrace();
				throw new RestartResponseAtInterceptPageException(ErrorPage.class);
			}
		} else {
			throw new RestartResponseAtInterceptPageException(ErrorPage.class);
		}

	}
	
	@Override
	public void onInitialize() {
		super.onInitialize();
		add(new ProfileOverviewMobile("overview", user));
		add(new PersonFeedMobile("timeline", user.getObject().getId()));
	}

}
