package com.erranda.prototype.mobile;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.web.component.TimeConverter;

public class PostOwnerNominateMetaMobile extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;
	
	PlatformUser session = UserSession.get().getPerson();
	
	WebMarkupContainer othersNominated = null;

	
	public PostOwnerNominateMetaMobile(String id, IModel<Post> model)
			throws ControllerException {
		super(id);
		Errand errand = model.getObject().getErrand();
		errand.setOwner(model.getObject().getOwner());
		if (errand.getAnonymous()) {
			setVisible(false);
			return;
		}
		final Long eid = errand.getId();
		Integer count = controller.countOfPeopleNominateByOwner(session.getId(), eid);
		if (count == 0) {
			this.setVisible(false);
			return;
		}
		
		PlatformUser nominated = model.getObject().getOwner();
		ExternalLink personLink = new ExternalLink("link",
				new PropertyModel<String>(nominated, "link"));
		Label name = new Label("name", new PropertyModel<String>(nominated, "name"));
		if (count >= 1) {
			othersNominated = new ExternalLink("others", "/mobile/errand/nominated");
			Label othersLabel = new Label ("others-label", Model.of(count == 1 ? "1 person" : count + " people"));
			othersNominated.add(othersLabel);
		} else {
			othersNominated = new WebMarkupContainer ("others");
			Label othersLabel = new Label ("others-label");
			othersNominated.add(othersLabel);
			othersNominated.setVisible(false);
		}
		add(personLink.add(name), othersNominated);
		
		
		
	}

}
