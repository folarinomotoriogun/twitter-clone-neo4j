package com.erranda.prototype.mobile;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.web.Authenticated;
import com.erranda.prototype.web.ModalContainer;
import com.erranda.prototype.web.ModalSignUp;
import com.erranda.prototype.web.OtherPeopleNominated;
import com.erranda.prototype.web.component.ModalAjaxLink;
import com.erranda.prototype.web.component.TimeConverter;

public class PostNominatePersonMetaMobile extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;
	
	PlatformUser session = UserSession.get().getPerson();
	
	WebMarkupContainer othersNominated = null;

	
	public PostNominatePersonMetaMobile(String id, IModel<Errand> model, Long pid)
			throws ControllerException {
		super(id);
		Errand errand = model.getObject();
		if (errand.getAnonymous()) {
			setVisible(false);
			return;
		}
		final Long eid = errand.getId();
		Integer count = controller.countOfPeopleNominate(eid);
		if (count == 0) {
			this.setVisible(false);
			return;
		}
		PlatformUser nominated = controller.findPerson(pid);
		ExternalLink personLink = new ExternalLink("link",
				new PropertyModel<String>(nominated, "link"));
		Label name = new Label("name", new PropertyModel<String>(nominated, "name"));
		if (count > 1) {
			othersNominated = new ModalAjaxLink<String> ("others") {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void onClick(AjaxRequestTarget target) {
					if (UserSession.get().userNotLoggedIn()) {
						Authenticated page = (Authenticated) getPage ();
						ModalContainer relationships = page.modalContainer;
						relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">Want to see people nominated?</h3>")).setEscapeModelStrings(false).setOutputMarkupId(true), new ModalSignUp("modal-content").setOutputMarkupId(true));
						relationships.update(target, "", true);
						return;
					}
					Authenticated page = (Authenticated) getPage ();
					ModalContainer relationships = page.modalContainer;
					relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">People nominated in this errand</h3>")).setEscapeModelStrings(false), new OtherPeopleNominated ("modal-content", eid));
					relationships.update(target, "", true);
				}
				
			};
		} else {
			othersNominated = new WebMarkupContainer ("others");
			othersNominated.setVisible(false);
		}
		PlatformUser owner = controller.findPerson(errand.getOwnerId());
		Label action = null;
		if (!errand.getAnonymous())
			action = new Label("action", Model.of(String.format(
					" %s nominated in <a href='%s'>%s's</a> post", count > 1 ? "were" : "was", owner.getLink(),
					owner.getName())));
		else
			action = new Label("action", Model.of(String
					.format(" %s nominated in this post", count > 1 ? "were" : "was")));
		action.setEscapeModelStrings(false);
		add(personLink.add(name), action, othersNominated);
		
		
		
	}

}
