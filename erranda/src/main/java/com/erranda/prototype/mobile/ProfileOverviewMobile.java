package com.erranda.prototype.mobile;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;

import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.ErrorPage;
import com.erranda.prototype.web.component.HtmlDecorator;
import com.erranda.prototype.web.component.StaticImage;

public class ProfileOverviewMobile extends HtmlPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProfileOverviewMobile(String id, final IModel<PlatformUser> model) {
		super(id, model);
		StaticImage image = new StaticImage("image", new PropertyModel<String>(model.getObject(), "thumbNail"));
		Label name = new Label("name", new PropertyModel<String>(model.getObject(), "name"));
		Label username = new Label("username", new PropertyModel<String>(model.getObject(), "username"));
		Label bio = new Label("bio", new PropertyModel<String>(model.getObject(), "bio"));
		Label location = new Label("location", new PropertyModel<String>(model.getObject(), "localityName"));
		ExternalLink following = new ExternalLink("following", Model.of("/mobile/following/" + model.getObject().getUsername()));
		ExternalLink followers = new ExternalLink("followers", Model.of("/mobile/followers/" + model.getObject().getUsername()));
		ExternalLink friends = new ExternalLink("friends", Model.of("/mobile/friends/" + model.getObject().getUsername()));
		ExternalLink timeline = new ExternalLink("timeline", Model.of("/mobile/profile/" + model.getObject().getUsername()));
		Label countFollowing = new Label("count-following", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				// TODO Auto-generated method stub
				try {
					return HtmlDecorator.countReadable(controller.countFollowers(loggedIn.getId(), model.getObject().getId()));
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RestartResponseAtInterceptPageException(ErrorPage.class);
				}
			}
		});
		Label countFollowers = new Label("count-followers", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				// TODO Auto-generated method stub
				try {
					return HtmlDecorator.countReadable(controller.countFollowers(loggedIn.getId(), model.getObject().getId()));
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RestartResponseAtInterceptPageException(ErrorPage.class);
				}
			}
		});
		Label countFriends = new Label("count-friends", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				// TODO Auto-generated method stub
				try {
					return HtmlDecorator.countReadable(controller.countFriends(model.getObject().getId()));
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RestartResponseAtInterceptPageException(ErrorPage.class);
				}
			}
		});
		add(image, name, username, bio, location, followers.add(countFollowers),following.add(countFollowing), friends.add(countFriends), timeline);
	}

}
