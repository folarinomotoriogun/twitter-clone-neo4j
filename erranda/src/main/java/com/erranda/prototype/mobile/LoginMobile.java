package com.erranda.prototype.mobile;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.cookies.CookieUtils;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.Home;
import com.erranda.prototype.web.MobileHome;
import com.erranda.prototype.web.component.StatelessAjaxButton;

/**
 * Login page
 * 
 * @author kloe and Folarin
 * 
 */
public class LoginMobile extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8991168083149150709L;
	private String emailModel;
	private String passwordModel;

	@SpringBean
	Controller controller;

	public LoginMobile() {
		if (UserSession.get().userLoggedIn()) {
			//throw new RestartResponseAtInterceptPageException (MobileIndex.class);
		}
		
		StatelessForm<Void> loginForm = new StatelessForm<Void>("signin-form");
		add(loginForm);

		final FeedbackPanel feedback = new FeedbackPanel("signin-feedback");
		feedback.setOutputMarkupId(true);
		feedback.setMarkupId("signin-feedback");
		add(feedback);

		final TextField<String> email = new TextField<String>("signin-email",
				new PropertyModel<String>(this, "emailModel"));
		email.setLabel(new ResourceModel("label.email"));
		loginForm.add(email);

		final PasswordTextField password = new PasswordTextField("signin-password",
				new PropertyModel<String>(this, "passwordModel"));
		password.setLabel(new ResourceModel("label.password"));
		password.setRequired(false);
		loginForm.add(password);
		
		final StatelessAjaxButton submit = new StatelessAjaxButton("signin-button", loginForm) {

			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				try {
					PlatformUser user = controller.login(emailModel, passwordModel);
					if (user == null)
						throw new ControllerException("Account not found");
					if (UserSession.get().isTemporary())
						UserSession.get().bind();
					UserSession.get().login(user);
					CookieUtils util = new CookieUtils();
					util.getSettings().setMaxAge(Integer.MAX_VALUE);
					util.save(Config.REMEMBER_ME_COOKIE,
					user.getVerificationKey());
					target.add(feedback);
					setResponsePage(HomeMobile.class);
				}  catch (ControllerException e) {
					error(e.getMessage());
					target.add(feedback);
				} finally {
					String script = String
							.format("$('#%s').html('Log In');",
									this.getMarkupId());
					target.appendJavaScript(script);
				}
				
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(feedback);
				String script = String
						.format("$('#%s').html('Log In');",
								this.getMarkupId());
				target.appendJavaScript(script);
			}
		};
		submit.setMarkupId("signin-button");
		submit.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').click(function () {$('#%s').html('Logging In...');})",
								component.getMarkupId(), component.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		
		password.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13) {$('#%s').click();} })});",
								password.getMarkupId(),
								submit.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
				String script1 = String
						.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13) {$('#%s').click();} })});",
								email.getMarkupId(),
								submit.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script1));
			}
		});

		loginForm.add(submit);
	}
}
