package com.erranda.prototype.mobile;

import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.PlatformUser;

public class NavAuthenticatedMobile extends Panel {
	
	PlatformUser session = UserSession.get().getPerson();
	
	public NavAuthenticatedMobile(String id) {
		super(id);
		// Home Messages People Me
		ExternalLink  home = new ExternalLink("home", Model.of("/mobile"));
		ExternalLink messages = new ExternalLink("messages", Model.of("/mobile/messages"));
		ExternalLink activity = new ExternalLink("activity", Model.of("/mobile/activity"));
		ExternalLink profile = new ExternalLink("profile", Model.of(String.format("/mobile/profile/%s", session.getUsername())));
		add(home,activity,messages, profile);
	}

	public NavAuthenticatedMobile(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
