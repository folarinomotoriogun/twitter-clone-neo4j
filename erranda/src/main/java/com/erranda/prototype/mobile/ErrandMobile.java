package com.erranda.prototype.mobile;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.web.component.StaticImage;
import com.erranda.prototype.web.component.TimeConverter;

public class ErrandMobile extends Panel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;

	public ErrandMobile(String id, final IModel<Errand> iModel) throws ControllerException {
		super(id, iModel);
		if (iModel == null)
			return;
		Errand errand =  iModel.getObject();
		PlatformUser owner = errand.getOwner();
		if(owner == null)
			iModel.getObject().setOwner(controller.findPerson(errand.getOwnerId()));
		Label name = new Label("name", new PropertyModel<String>(iModel.getObject().getOwner(), "name"));
		Label username = new Label("username", new PropertyModel<String>(owner, "username"));
		StaticImage image = new StaticImage("image", new PropertyModel<String>(owner, "thumbNail"));
		Label details = new Label("details", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				return parse(iModel.getObject());
			}
		});
		Label time = new Label("time", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				return TimeConverter.timeAgo(iModel.getObject().getAdded());
			}
		});
		add(name,username, image, details.setEscapeModelStrings(false), time);
	}
	
	private String parse (Errand errand) {
		if (errand == null || errand.getDetails() == null)
			return "";
		String details = errand.getDetails();
		
		String[] words = details.split(" ");
		
		for (String word: words) {
			if (word.indexOf("#") == 0 && word.lastIndexOf("#") == 0 && word.length() > 1) {
				details = details.replace(word, String.format("<a href='%s'>%s</a>", "/mobile/posts/search/" + word.split("#")[1], word));
			}
			else if (word.indexOf("@") == 0) {
				String username = word.split("@")[1];
				PlatformUser person = controller.findUsername(username.toLowerCase());
				if (person != null)
					details = details.replace("@" + username, String.format("<a href='%s'>@%s</a>", "/mobile" + person.getLink(), username));
			}
		}
		
		if (details.length() > 350) {
			String yeah = details.substring(0, 350);
			yeah = yeah.substring(0, yeah.lastIndexOf(" ")) + String.format("...<a href='%s'>More</a>", "/mobile" + errand.getErrandLink());
			details = yeah;
		} 
		

		return 	details.replaceAll("(\r\n|\n)", "<br />");
	}

}
