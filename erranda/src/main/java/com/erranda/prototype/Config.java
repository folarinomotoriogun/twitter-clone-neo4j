package com.erranda.prototype;

import java.util.Arrays;
import java.util.List;

public class Config {

	public static final String name = "Erranda";

	public static final int REMEMBER_ME_DURATION_IN_SECONDS = 1 * 24 * 60 * 60;

	public static final String REMEMBER_ME_COOKIE = "errandrUserKey";

	// public static final String IMAGES =
	// "C:/Users/Folarin/git/erranda/erranda/target/images";

	public static final String IMAGES = "/erranda/images";

	public static final String DOMAIN_NAME = "http://fb.erranda.com";

	
	 
	public static final String PLATFORM_TYPE_PERSON = "PERSON";
	
	public static final String PLATFORM_TYPE_PAGE = "PAGE";

	public static final String WICKET_DEPLOYMENT = "deployment";
	
	// public static final String WICKET_DEPLOYMENT = "development";

	//public static final String DOMAIN_NAME = "http://test.erranda.com";

	public static final String DATABASE_ENDPOINT = "http://localhost:7474/db/data";

	public static final String DATABASE_USERNAME = "neo4j";

	public static final String DATABASE_PASSWORD = "3c0a0a6ea165ce3b6f7edd269e22a94d";

	public static final String PROFILE_NAME_LINK = "/profile";

	public static final String GROUP_NAME_LINK = DOMAIN_NAME + "/group";
	
	public static final String FOLLOWING_LINK = DOMAIN_NAME + "/mobile/following";
	
	public static final String FOLLOWERS_LINK = DOMAIN_NAME + "/mobile/following";

	public static final String PLACE_NAME_LINK = DOMAIN_NAME + "/place";

	public static final String ERRAND_NAME_LINK = "/errand";
	
	public static final String PEOPLE_SEARCH_LINK = DOMAIN_NAME + "/people/search";
	
	public static final String POST_SEARCH_LINK = DOMAIN_NAME + "/posts/search";
	
	public static final String PAGES_SEARCH_LINK = DOMAIN_NAME + "/pages/search";

	public static final String DEACTIVATE_LINK = DOMAIN_NAME + "/deactivate";

	public static final String CONFIRM_LINK = DOMAIN_NAME
			+ "/confirm-registeration";
	
	public static final String PRIVACY_PUBLIC = "PUBLIC";
	
	public static final String PRIVACY_LOCAL = "LOCAL";
	
	public static final String PRIVACY_FRIENDS = "FRIENDS";
	
	public static final String PRIVACY_PRIVATE = "PRIVATE";

	public static final String SET_PASSWORD_LINK = DOMAIN_NAME
			+ "/account/reset-password";

	public static final String FORGOT_PASSWORD_LINK = DOMAIN_NAME
			+ "/account/forgot-password";

	public static final List<String> ERRANDTYPES = Arrays.asList("Challenge",
			"Holiday Shopping", "Local Shopping", "Handyman", "Cleaning",
			"Delivery", "Assistance", "Question");

	public static final String EMAILER_USERNAME = "notifications@erranda.com";

	public static final String EMAILER_PASSWORD = "xETIrqR7";

	public static final String EMAILER_SMTP_AUTH = "true";

	public static final String EMAILER_STARTTLS_ENABLE = "true";

	public static final String EMAILER_SMTP_HOST = "smtp.erranda.com";

	public static final String EMAILER_SMTP_PORT = "587";

	/*
	 * public static final String EMAILER_USERNAME = null;
	 * 
	 * public static final String EMAILER_PASSWORD =
	 * PropertiesInstaller.get("mailer.password");
	 * 
	 * public static final String EMAILER_SMTP_AUTH =
	 * PropertiesInstaller.get("mailer.auth");
	 * 
	 * public static final String EMAILER_STARTTLS_ENABLE =
	 * PropertiesInstaller.get("mailer.ttls");
	 * 
	 * public static final String EMAILER_SMTP_HOST =
	 * PropertiesInstaller.get("mailer.host");
	 * 
	 * public static final String EMAILER_SMTP_PORT =
	 * PropertiesInstaller.get("mailer.port");
	 * 
	 * public static final String SUPER_ADMIN_PASSWORD =
	 * PropertiesInstaller.get("password");
	 * 
	 * public static final String SUPER_ADMIN_EMAIL =
	 * PropertiesInstaller.get("email");
	 * 
	 * public static final String SUPER_ADMIN_REAL_NAME =
	 * PropertiesInstaller.get("name");
	 * 
	 * public static final String SECURITY_BUNDLE_LOCATION =
	 * PropertiesInstaller.get("security.bundle");
	 */

}