package com.erranda.prototype;

import org.apache.wicket.core.request.mapper.MountedMapper;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.component.IRequestablePage;

public class UsernameMountUrlMapper extends MountedMapper {

	public UsernameMountUrlMapper(String mountPath,
			Class<? extends IRequestablePage> pageClass) {
		super(mountPath, pageClass);
	}

	@Override
	protected boolean urlStartsWithMountedSegments(Url url) {
		super.urlStartsWithMountedSegments(url);
		if (url == null || url.getSegments() == null || url.getPath() == null)
			return false;
		return url.getSegments().size() == 1 && !url.getPath().equals("favicon.ico") && !url.getPath().equals("oops") && !url.getPath().equals("Index") && !url.getPath().equalsIgnoreCase("mobile");
	}

}
