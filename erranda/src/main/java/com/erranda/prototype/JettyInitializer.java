package com.erranda.prototype;

import java.util.concurrent.TimeUnit;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.annotation.Configuration;


@Configuration
public class JettyInitializer implements EmbeddedServletContainerCustomizer {

	@Override
	public void customize(ConfigurableEmbeddedServletContainer arg0) {
		arg0.setPort(80);
		if (Config.WICKET_DEPLOYMENT.equalsIgnoreCase("deployment"))
			arg0.setSessionTimeout(5, TimeUnit.MINUTES);
		else {
			arg0.setSessionTimeout(24, TimeUnit.HOURS);
		}
	}
	

}
