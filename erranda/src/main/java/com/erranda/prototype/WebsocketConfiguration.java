package com.erranda.prototype;

import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.wicket.protocol.http.WicketFilter;
import org.cometd.bayeux.server.BayeuxServer;
import org.cometd.server.BayeuxServerImpl;
import org.cometd.server.CometdServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.context.embedded.jetty.JettyEmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.jetty.JettyServerCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
//@EnableAutoConfiguration
public class WebsocketConfiguration {
	
	private static Logger logger = LoggerFactory.getLogger(WebsocketConfiguration.class);
	
	@Bean
	public JettyEmbeddedServletContainerFactory servletContainerFactory() {
		JettyEmbeddedServletContainerFactory factory = new JettyEmbeddedServletContainerFactory();
		
		factory.addServerCustomizers(new JettyServerCustomizer(){
			@Override
			public void customize(Server server) {
				try {
		    		WebSocketServerContainerInitializer.configureContext((WebAppContext)server.getHandler());
				} catch (ServletException e) {
					logger.error(e.getMessage());
				}
			}			
		});		
		return factory;
	}
	
	@Bean
	public ServletRegistrationBean servletRegistrationBean(){
		CometdServlet cometdServlet = new CometdServlet();
		return new ServletRegistrationBean(cometdServlet,"/cometd/*");
	}
	
	@Bean
	public FilterRegistrationBean filterRegistration() {
		FilterRegistrationBean filter = new FilterRegistrationBean();
		Filter crossOriginFilter = new WicketFilter();
		filter.setFilter(crossOriginFilter);
		filter.setAsyncSupported(true);
		filter.setName("cross-origin");
		filter.addUrlPatterns("/cometd/*","/chat/template/*");
		return filter;
	}
	
	@Bean
	public ServletContextInitializer initializer() {
	    return new ServletContextInitializer() {
	        @Override
	        public void onStartup(ServletContext servletContext) throws ServletException {
	            servletContext.setAttribute(BayeuxServer.ATTRIBUTE, bayeuxServer(servletContext));
	        }
	    };
	}
	
	@Bean
    public BayeuxServer bayeuxServer(ServletContext servletContext)
    {
		BayeuxServerImpl bean = new BayeuxServerImpl();
        bean.addExtension(new org.cometd.server.ext.AcknowledgedMessagesExtension());
        bean.setOption(ServletContext.class.getName(), servletContext);
        bean.setOption("ws.cometdURLMapping", "/cometd/*"); 
        return bean;
    }
	
}