package com.erranda.prototype;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;

public class UserSession extends AuthenticatedWebSession {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1713951920830505354L;

	private PlatformUser user;

	public transient Controller controller;
	
	private Long userId;

	public UserSession(Request request) {
		super(request);
	}

	public PlatformUser getPerson() {
		return user;
	}
	
	public void login (PlatformUser user){
		this.user = user;
		this.userId = user.getId();
	}

	public void setUser(PlatformUser user) {
		this.user = user;
	}

	public boolean userLoggedIn() {
		return user != null && !user.getEmail().equals("public@public.com");
	}

	public boolean userNotLoggedIn() {
		return user == null || user.getEmail().equals("public@public.com");
	}

	public static UserSession get() {
		return (UserSession) WebSession.get();
	}

	public void logout() {
		((App) App.get()).unregisterMyNodes(userId);
		((App) App.get()).unregisterMyNodes(user.getId());
		user = null;
		userId = null;
	}

	@Override
	public boolean authenticate(String arg0, String arg1) {
		return user != null;
	}

	@Override
	public Roles getRoles() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPerson(PlatformUser user) {
		this.user = user;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}