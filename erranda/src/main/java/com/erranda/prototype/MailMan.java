package com.erranda.prototype;

import static com.erranda.prototype.Config.EMAILER_PASSWORD;
import static com.erranda.prototype.Config.EMAILER_SMTP_AUTH;
import static com.erranda.prototype.Config.EMAILER_SMTP_HOST;
import static com.erranda.prototype.Config.EMAILER_SMTP_PORT;
import static com.erranda.prototype.Config.EMAILER_STARTTLS_ENABLE;
import static com.erranda.prototype.Config.EMAILER_USERNAME;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailMan {

	public void sendMessage(final String subject, final String address,
			final String content) throws MessagingException {
		Runnable r = new Runnable() {

			@Override
			public void run() {
				Message message = new MimeMessage(getSession());

				try {
					message.addRecipient(javax.mail.Message.RecipientType.TO,
							new InternetAddress(address));
					if (Config.EMAILER_USERNAME == null)
						throw new MessagingException("Email not setup");
					message.addFrom(new InternetAddress[] { new InternetAddress(
							EMAILER_USERNAME, "Erranda") });
					message.setSubject(subject);
					message.setContent(content, "text/html");
					Transport.send(message);
				} catch (AddressException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};
		Thread thread = new Thread(r);
		thread.start();

	}

	private Session getSession() {
		Authenticator authenticator = new Authenticator();

		Properties properties = new Properties();
		properties.setProperty("mail.smtp.submitter", authenticator
				.getPasswordAuthentication().getUserName());
		properties.setProperty("mail.smtp.auth", EMAILER_SMTP_AUTH);
		properties.setProperty("mail.smtp.starttls.enable",
				EMAILER_STARTTLS_ENABLE);
		properties.setProperty("mail.smtp.host", EMAILER_SMTP_HOST);
		properties.setProperty("mail.smtp.port", EMAILER_SMTP_PORT);

		return Session.getInstance(properties, authenticator);
	}

	private class Authenticator extends javax.mail.Authenticator {
		private PasswordAuthentication authentication;

		public Authenticator() {
			String username = EMAILER_USERNAME;
			String password = EMAILER_PASSWORD;
			authentication = new PasswordAuthentication(username, password);
		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return authentication;
		}
	}
}