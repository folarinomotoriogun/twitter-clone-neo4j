package com.erranda.prototype.web.component;

import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.util.time.Duration;

public class KeepAliveBehavior extends AbstractAjaxTimerBehavior {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public KeepAliveBehavior() {
		super(Duration.minutes(1));
	}

	@Override
	protected void onTimer(AjaxRequestTarget target) {
		target.focusComponent(null);
	}
}