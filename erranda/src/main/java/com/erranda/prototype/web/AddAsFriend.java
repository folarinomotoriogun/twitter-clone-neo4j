package com.erranda.prototype.web;

import java.util.List;

import javax.mail.MessagingException;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.Config;
import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.ConfirmingGlobalAjaxLink;
import com.erranda.prototype.web.component.ModalAjaxLink;
import com.erranda.prototype.web.mailer.EmailNewAction;

public class AddAsFriend extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	PlatformUser session = UserSession.get().getPerson();

	Long rid;

	Label label;

	public AddAsFriend(final String id, Long pid,
			final WebMarkupContainer container) {
		super(id);
		this.rid = pid;
		if (session.getType().equals(Config.PLATFORM_TYPE_PAGE))
			setVisible(false);
		final IndicatingAjaxLink<String> addFriend = new IndicatingAjaxLink<String>(
				"add-friend") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Long sid = UserSession.get().getPerson().getId();
				try {
					controller.sendFriendRequest(sid, rid);
					sendPushNotificationMessage (rid);
					((UserActionContainer) container).replaceWindow(new FriendRequestSent(id, rid,
							container));
					((UserActionContainer) container).update(target, "");
					String html = new String(ComponentRenderer.renderPage(
							new PageProvider(new EmailNewAction(session,
									session.getName() + " " + session.getUsername()
											+ " wants be on your network.",
									Config.DOMAIN_NAME + session.getLink()))).toString());
					MailMan man = new MailMan();
					man.sendMessage(
							session.getName() + " @" + session.getUsername()
									+ " sent you a friend request", controller
									.findPerson(rid).getEmail(), html);
				} catch (ControllerException e) {
					throw new RuntimeException(e.getMessage());
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		
		add(addFriend);
	}
	
	private void sendPushNotificationMessage (Long rid) throws ControllerException {
		List<Long> people = controller.friendRequestNotificationList(session.getId(), rid);
		for (Long person : people) {
			IPushNode node = ((App) App.get()).getFriendRequestPanel(person);
			if (node != null) {
				TimerPushService.get().publish(node, "");
			}
		}
	}

}
