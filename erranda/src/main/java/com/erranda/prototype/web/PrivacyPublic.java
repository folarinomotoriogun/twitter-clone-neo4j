/**
 * 
 */
package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * @author Folarin
 *
 */
public class PrivacyPublic extends Panel {

	/**
	 * @param id
	 */
	public PrivacyPublic(String id, final ListItem<String> item,
			final String buttonId, final String inputField) {
		super(id);

		add(new Behavior() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String html = htmlData();
				String script = String
						.format("$('#%s').bind('click', function(){$('#%s').html('%s'); $('#%s').val('%s')});",
								item.getMarkupId(), buttonId, html, inputField,
								"PUBLIC");
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
	}

	public static String htmlData() {
		return "<span class=\"fa fa-globe privacy-icon\"></span><span class=\"privacy-text\">Public</span>";
	}

	/**
	 * @param id
	 * @param model
	 */
	public PrivacyPublic(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
