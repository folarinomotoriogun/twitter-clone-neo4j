package com.erranda.prototype.web;

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.springframework.beans.factory.annotation.Autowired;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class SearchResultsProvider implements IDataProvider<PlatformUser> {

	@Autowired
	Controller controller;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private int removed = 0;
	
	private PlatformUser session = UserSession.get().getPerson();

	public SearchResultsProvider(String name, Controller controller) {
		this.controller = controller;
		this.name = name;
	}

	@Override
	public void detach() {
		name = null;
	}

	@Override
	public Iterator<? extends PlatformUser> iterator(long arg0, long arg1) {
		List<PlatformUser> persons = controller.searchPeople(session.getId(), name, (int) arg0,
				(int) arg1);
		return persons.iterator();
	}

	@Override
	public IModel<PlatformUser> model(PlatformUser arg0) {
		final Long id = arg0.getId();
		LoadableDetachableModel<PlatformUser> model = new LoadableDetachableModel<PlatformUser>() {

			private static final long serialVersionUID = 1L;

			@Override
			protected PlatformUser load() {
				try {
					return controller.findPerson(id);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
			}

		};
		return model;
	}

	@Override
	public long size() {
		try {
			return controller.searchSize(name) - removed;
		} catch (ControllerException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

}
