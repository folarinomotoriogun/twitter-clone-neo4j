package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.wicket.Component;import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostViewItem;
import com.erranda.prototype.web.component.AutogrowBehavior;

public class PrivacySwitch extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;
	
	UserSession session = UserSession.get();
	
	PlatformUser currentUser = session.getPerson();
	
	String chosen;
	
	public PrivacySwitch(String id, final IModel<Errand> model, final ListItem<PostViewItem> item) throws ControllerException {
		super(id);
		chosen = model.getObject().getPrivacy ();
		// Convert privacy settings back to keys
		chosen = chosen.equals("PUBLIC") ? "Public" : (chosen.equals("FRIENDS") ? "Friends" : (chosen.equals("PRIVATE") ? "Private" : ""));
		Set<String> privacy = controller.getPrivacySettings(currentUser).keySet();
		List<String> privacySettings = new ArrayList<String> ();
		privacySettings.addAll(privacy);
		
		Form<String> form = new Form<String> ("switch-privacy-form") ;
		final TextArea<String> detailsField = new TextArea<String>("details", new PropertyModel<String>(model.getObject(), "detailsRaw"));
		detailsField.setOutputMarkupId(true);
		detailsField.add(new AutogrowBehavior());
		
		final DropDownChoice<String> selector = new DropDownChoice<String> ("switch-privacy-select", new PropertyModel<String>(this, "chosen"), privacySettings);
		selector.setOutputMarkupId(true);
		selector.add (new Behavior () {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component component, IHeaderResponse response) {
				//String changeSubmit = String.format("$('#%s').change (function () { $('#%s').click (); $('#%s').css('display', 'block').fadeIn();});", component.getMarkupId(), button.getMarkupId(), staticFeedback.getMarkupId());
				//response.render(OnDomReadyHeaderItem.forScript(changeSubmit));
			}
		});
		
		final IndicatingAjaxButton button = new IndicatingAjaxButton ("submit", form) {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit (AjaxRequestTarget target, Form<?> form) {
				try {
					String details = model.getObject().getDetailsRaw();
					if (details != null && !details.trim().equals("")) {
						controller.updatePostDetails(currentUser.getId(), model.getObject().getId(), details);
						String errorScript = String.format("$('#%s').popover ({title: 'Success', content: '%s', placement: 'bottom'}).popover('show');", this.getMarkupId() , "Post details updated");
						target.appendJavaScript(errorScript);
					}
					controller.switchPrivacy(currentUser.getId(), model.getObject().getId(), chosen);
					String errorScript = String.format("$('#%s').popover ({title: 'Success', content: '%s', placement: 'bottom'}).popover('show');", this.getMarkupId() , "Post details updated");
					target.appendJavaScript(errorScript);
					if (item != null)
						target.add(item);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					String errorScript = String.format("$('#%s').popover ({title: 'Error', content: '%s', placement: 'left'}).popover('show');", this.getMarkupId() , e.getMessage());
					target.appendJavaScript(errorScript);
				}
			} 
		};
		
		final WebMarkupContainer mentionBox = new WebMarkupContainer ("mention-box");
		mentionBox.setOutputMarkupId(true);
		final WebMarkupContainer mentionResults = new WebMarkupContainer ("mention-results");
		mentionResults.setOutputMarkupId(true);
		form.add(mentionBox.add(mentionResults));
		detailsField.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String inputId = detailsField.getMarkupId();
				String script = "$('#" + inputId + "').bind ('keyup', function () {" +
                   		 "nominateAutocomplete ('" + inputId + "', '" + mentionBox.getMarkupId() + "','" + mentionResults.getMarkupId() + "');" +
					"});";
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		

		add (form.add(detailsField, selector, button));
		
	}

}
