package com.erranda.prototype.web;

import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;

public class UsernameValidator implements IValidator<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Controller personRepo;

	public UsernameValidator(Controller controller) {
		this.personRepo = controller;
	}

	@Override
	public void validate(IValidatable<String> validatable) {
		PlatformUser person = personRepo.findUsername(validatable
				.getValue());
		if (person != null)
			error(validatable, "username-taken");
	}

	private void error(IValidatable<String> validatable, String errorKey) {
		ValidationError error = new ValidationError();
		error.addKey(getClass().getSimpleName() + "." + errorKey);
		validatable.error(error);
	}

}
