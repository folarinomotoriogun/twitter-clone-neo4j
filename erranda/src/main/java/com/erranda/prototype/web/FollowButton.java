package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

/**
 * @author FolarinOmotoriogun
 *
 */
public class FollowButton extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	PlatformUser session = UserSession.get().getPerson();

	AjaxLink<?> followLink;

	AjaxLink<?> unfollowLink;
	
	AjaxLink<?> followRequestLink;

	public FollowButton(String id, final IModel<PlatformUser> model)
			throws ControllerException {
		super(id, model);

		followLink = new AjaxLink<Object>("follow-link") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.followUser(session.getId(), model.getObject()
							.getId());
				} catch (ControllerException e) {
					String script = String.format(
							"$('#%s').hide();$('#%s').fadeIn();",
							unfollowLink.getMarkupId(), this.getMarkupId());
					target.appendJavaScript(script);
				}
			}
		};

		unfollowLink = new AjaxLink<Object>("unfollow-link") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.unfollowUser(session.getId(), model.getObject()
							.getId());
				} catch (ControllerException e) {
					String script = String.format(
							"$('#%s').hide();$('#%s').fadeIn();",
							followLink.getMarkupId(), this.getMarkupId());
					target.appendJavaScript(script);
				}
			}
		};
		
		followRequestLink = new AjaxLink<Object>("follow-request-link") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.sendFollowRequest(session.getId(), model.getObject()
							.getId());
				} catch (ControllerException e) {
					// 
				}
			}
		};
		
		final Label unfollowLabel = new Label("label", Model.of("Following"));
		unfollowLink.add(unfollowLabel);
		unfollowLabel.setOutputMarkupId(true);
		
		final Label followRequestLabel = new Label("label", Model.of("Follow Back Request"));
		followRequestLink.add(followRequestLabel);
		followRequestLabel.setOutputMarkupId(true);
		
		add (new Behavior() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			@Override
			public void renderHead( Component component, IHeaderResponse response) {
				String script = String.format("$('#%s').hover (function () {$('#%s').html ('Unfollow')}, function () {$('#%s').html ('Following')});" , unfollowLink.getMarkupId(), unfollowLabel.getMarkupId(), unfollowLabel.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
				String script2 = String
						.format("$('#%s').click(function () {$(this).hide(); $('#%s').fadeIn();}); ",
								followLink.getMarkupId(), unfollowLink.getMarkupId());
				String script3 = String
						.format("$('#%s').click(function () {$(this).hide(); $('#%s').fadeIn();}); ",
								unfollowLink.getMarkupId(), followLink.getMarkupId());
				String script4 = String
						.format("$('#%s').click(function () {$('#%s').fadeOut(); $('#%s').html('Sent Follow Back request'); $('#%s').fadeIn();}); ",
								followRequestLink.getMarkupId(), followRequestLabel.getMarkupId(), followRequestLabel.getMarkupId(), followRequestLabel.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script4));
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript(script2));
				response.render(OnDomReadyHeaderItem.forScript(script3));
			}
		});

		
		if (controller.isFollowingUser(session.getId(), model.getObject()
				.getId()))
			followLink.add(AttributeModifier.append("style", "display:none"));
		else {
			unfollowLink
					.add(AttributeModifier.append("style", "display:none"));
		}
		if (controller.isFollowingUser(model.getObject().getId(), session.getId()))
			followRequestLink.setVisible(false);
		add(followLink, unfollowLink, followRequestLink);
	}

}
