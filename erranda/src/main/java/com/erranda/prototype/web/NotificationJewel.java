package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.AbstractPushEventHandler;
import org.wicketstuff.push.IPushEventContext;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.cometd.CometdPushNode;
import org.wicketstuff.push.cometd.CometdPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;

public class NotificationJewel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	private Controller controller;

	public Integer count = 0;

	Label countLabel;

	public NotificationJewel(String id, final Long pid)
			throws ControllerException {
		super(id);

		countLabel = new Label("count", new LoadableDetachableModel<Integer>() {

			private static final long serialVersionUID = 1L;

			@Override
			protected Integer load() {
				try {
					count = controller.unseenNotificationsCount(pid);
				} catch (ControllerException e) {
					e.printStackTrace();
					throwException(e);
				}
				return count;
			}

		}) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(IHeaderResponse response) {
				super.renderHead(response);
				response.render(OnDomReadyHeaderItem
						.forScript("if ($('#notification-jewel-count').html() != 0){$('#notification-jewel').css ('display', 'block')}"));
			}
		};

		setOutputMarkupId(true);
		add(countLabel);
		
		
	}
	
	@Override
	public void onInitialize () {
		super.onInitialize();
	}

	private void throwException(ControllerException e) throws RuntimeException {
		throw new RuntimeException(e.getMessage());
	}

}
