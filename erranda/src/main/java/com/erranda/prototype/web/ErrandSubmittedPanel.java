package com.erranda.prototype.web;

import org.apache.wicket.markup.html.panel.Panel;

public class ErrandSubmittedPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrandSubmittedPanel(String id) {
		super(id);
	}

}
