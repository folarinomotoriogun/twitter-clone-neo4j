package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.AbstractPushEventHandler;
import org.wicketstuff.push.IPushEventContext;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.GlobalAjaxLink;
import com.erranda.prototype.web.component.ModalAjaxLink;

public class NavAuthenticated extends Panel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	PlatformUser session = UserSession.get().getPerson();
	
	@SpringBean
	Controller controller;

	public NavAuthenticated(String id, final Authenticated page) {
		super(id);
		HiddenField<Long> t = new HiddenField<Long>("user-id",
				new PropertyModel<Long>(session, "id"));
		add(t);
		final NewMessageNotificationCount jewel = new NewMessageNotificationCount(
				"new-message-count");
		add(jewel);
		
		final IPushNode<String> pushNode =  TimerPushService.get().installNode(jewel,
				new AbstractPushEventHandler<String>()
				{
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public void onEvent(final AjaxRequestTarget target, final String event,
						final IPushNode<String> node, final IPushEventContext<String> ctx)
					{
						target.add(jewel);
					}
		});
		((App) App.get()).registerMessagePanel(UserSession.get().getPerson().getId(), pushNode);
		
		final GlobalAjaxLink<String> messagesLink = new GlobalAjaxLink<String>(
				"messages-link") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					Integer count = controller.countConversation(session.getId());
					Label header = new Label("modal-header",
							Model.of(String.format("<h3 style=\"padding-left: 20px;font-weight: bold;\" >Conversations %s</h3>", count > 0 ? "(" + count + ")" : "")));
					header.setEscapeModelStrings(false);
					page.modalContainer.replaceWindow(header, new ConversationsPanel(
							"modal-content", page.modalContainer, page.cp));
					page.modalContainer.update(target, "", false);
					controller.markAllNewMessages(UserSession.get().getPerson()
							.getId());
					target.appendJavaScript("$('#main-modal-ajax-indicator').hide()");
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};

		messagesLink.add(new Behavior() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').click(function () {$('#new-message-notification-jewel').hide(); $('#main-modal-ajax').fadeIn('slow'); $('#show-modal-link').click();});",
								messagesLink.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));

			}
		});

		FriendRequests requests = new FriendRequests(
				"friend-requests-container");
		
		add(new Notifications("notifications-container"), requests);

		ModalAjaxLink<String> updateProfile = new ModalAjaxLink<String>(
				"update-profile-link") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Label header = new Label("modal-header",
						"<h3 style=\"padding-left: 20px;font-weight: bold;\" >Account settings</h3>");
				header.setEscapeModelStrings(false);
				page.modalContainer.replaceWindow(header, new AccountSettings(
						"modal-content"));
				page.modalContainer.update(target, "", true);
			}

		};
		add(updateProfile);
		
		final IPushNode<String> pushNodeModal =  TimerPushService.get().installNode(page.modalContainer,
				new AbstractPushEventHandler<String>()
				{
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public void onEvent(final AjaxRequestTarget target, final String event,
						final IPushNode<String> node, final IPushEventContext<String> ctx)
					{
						target.add(page.modalContainer.body);
					}
		});
		
		((App) App.get()).registerInstantMessagePanel(UserSession.get().getPerson().getId(), pushNodeModal);

		ModalAjaxLink<String> createPage = new ModalAjaxLink<String>(
				"create-business-page-link") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Label header = new Label("modal-header",
						"<h3 style=\"padding-left: 20px;font-weight: bold;\" >New Errand Page</h3>");
				header.setEscapeModelStrings(false);
				page.modalContainer.replaceWindow(header, new AddPageInfo(
						"modal-content"));
				page.modalContainer.update(target, "", true);
			}

		};
		add(createPage);
		if (!session.getType().equals(Config.PLATFORM_TYPE_PERSON))
			createPage.setVisible(false);

		ModalAjaxLink<String> switchAccountLink = new ModalAjaxLink<String>(
				"switch-account-link") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Label header = new Label(
						"modal-header",
						"<h3 style=\"padding-left: 20px;font-weight: bold;\" >Switch to another account</h3>");
				header.setEscapeModelStrings(false);
				try {
					page.modalContainer.replaceWindow(header, new AccountSwitch(
							"modal-content"));
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				page.modalContainer.update(target, "", true);
			}

		};
		try {
			if (controller.switchAccounts(UserSession.get().getUserId()).size() == 1)
				switchAccountLink.setVisible(false);
		} catch (ControllerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		add(switchAccountLink);

		BookmarkablePageLink<Logout> logout = new BookmarkablePageLink<Logout>(
				"logout", Logout.class);
		add(logout);

		ExternalLink userProfile = new ExternalLink("user-profile",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						return UserSession.get().getPerson().getLink();
					}

				});
		add(userProfile);
		Label fullName = new Label("fullname",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						return UserSession.get().getPerson().getName();
					}

				});
		Label userName = new Label("username",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						return UserSession.get().getPerson().getUsername();
					}
				});
		userProfile.add(fullName, userName);

		WebMarkupContainer iconImage = new WebMarkupContainer("icon-image");
		iconImage.add(AttributeModifier.replace("src",
				new LoadableDetachableModel<Object>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = -1446646754517223593L;

					@Override
					protected Object load() {
						// TODO Auto-generated method stub
						try {
							return controller.findPerson(
									UserSession.get().getPerson().getId())
									.getThumbNail();
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return "";
						}
					}

				}));
		add(messagesLink, iconImage);
	}

}
