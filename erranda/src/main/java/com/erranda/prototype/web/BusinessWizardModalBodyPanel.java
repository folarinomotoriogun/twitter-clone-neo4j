package com.erranda.prototype.web;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class BusinessWizardModalBodyPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3647395653190336618L;

	public BusinessWizardModalBodyPanel(String id, BusinessWizardContainer container, final Long pid) {
		super(id);
		add(new BusinessWizardWelcomePanel("modal-content", container, pid));
	}

	public BusinessWizardModalBodyPanel(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
