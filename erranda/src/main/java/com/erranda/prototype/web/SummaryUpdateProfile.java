package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;
import com.erranda.prototype.web.component.DisablingIndicatingAjaxButton;

public class SummaryUpdateProfile extends Panel {

	@SpringBean
	Controller controller;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SummaryUpdateProfile(String id) {
		super(id);

		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);

		final PlatformUser person = UserSession.get().getPerson();

		final Form<String> form = new Form<String>("form");

		TextField<String> username = new TextField<String>("username",
				new PropertyModel<String>(person, "username"));
		form.add(username);

		TextArea<String> bio = new TextArea<String>("bio",
				new PropertyModel<String>(person, "bio"));
		form.add(bio);

		TextField<String> email = new TextField<String>("email",
				new PropertyModel<String>(person, "email"));
		email.add(AttributeModifier.replace("disabled", "disabled"));
		form.add(email);
		
		TextField<String> location = new TextField<String>("user-location",
				new PropertyModel<String>(person, "localityName"));
		form.add(location);
		
		email.add(AttributeModifier.replace("disabled", "disabled"));
		form.add(email);


		form.add(feedback);

		form.add(new DisablingIndicatingAjaxButton("submit", form) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				try {
					controller.updateProfile(person);
					String errorScript = String.format("$('#%s').popover ({title: 'Success', content: '%s', placement: 'right'}).popover('show');", this.getMarkupId() , "Your profile is updated with new info");
					target.appendJavaScript(errorScript);
				} catch (ControllerException e) {
					String errorScript = String.format("$('#%s').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show');", this.getMarkupId() , e.getMessage());
					target.appendJavaScript(errorScript);
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				String errorScript = String.format("$('#%s').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show');", this.getMarkupId() , "Something went wrong with your request");
				target.appendJavaScript(errorScript);
			}
		});
		add(form);
	}

}
