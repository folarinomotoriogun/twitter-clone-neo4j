package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.context.annotation.Configuration;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class BusinessWizardFinishPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public BusinessWizardFinishPanel(String id, final BusinessWizardContainer container, final Long pid) {
		super(id);
		
		PlatformUser person = null;
		try {
			person = controller.findPerson(pid);
		} catch (ControllerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ExternalLink businessPageLink = new ExternalLink ("business-page-link", Model.of(person.getLink()));
		Label label = new Label ("label", Model.of(Config.DOMAIN_NAME + person.getLink()));
		add (businessPageLink.add(label));
		
		add(new ShareButton ("share-page", Config.DOMAIN_NAME + person.getLink()));
	}

}
