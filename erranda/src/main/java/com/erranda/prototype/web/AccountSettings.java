package com.erranda.prototype.web;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;

public class AccountSettings extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccountSettings(String id) {
		super(id);
		add(new UpdateProfile("profile"));
		add(new UpdatePassword("password"));
		add(new UpdateLocation("location"));
		BookmarkablePageLink<Deactivate> deactivate = new BookmarkablePageLink<Deactivate>(
				"deactivate-link", Deactivate.class);
		add(deactivate);
		BookmarkablePageLink<Deactivate> blockList = new BookmarkablePageLink<Deactivate>(
				"block-list", BlockList.class);
		add(blockList);
	}

}
