package com.erranda.prototype.web;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;

public class SearchResult extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public SearchResult(String id, final IModel<PlatformUser> iModel) {
		super(id);
		
		Label name = new Label("name", new LoadableDetachableModel<Object>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected Object load() {
				PlatformUser person = iModel.getObject();
				String template = String
							.format("<a class='account-group js-user-profile-link' href='%s'>"
									+ "<img class='avatar js-action-profile-avatar' src='%s'>"
									+ "<strong class='fullname js-action-profile-name'>%s</strong>"
									+ "<span class='username js-action-profile-name'>@%s</span></a>",
									person.getLink(),
									person.getThumbNail(),
									person.getName(), person.getUsername());
					return template;
			}

		});
		name.setEscapeModelStrings(false);
		add(name);

		Label bio = new Label("bio", new LoadableDetachableModel<Object>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected Object load() {
				PlatformUser person = iModel.getObject();
				return person.getBio();
				
			}

		});
		bio.setEscapeModelStrings(false);
		add(bio);
		if (UserSession.get().userLoggedIn())
			add(new MiniUserActions("user-actions", iModel));
		else {
			add(new WebMarkupContainer("user-actions"));
		}
	}

}
