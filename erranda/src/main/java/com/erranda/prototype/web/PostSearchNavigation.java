package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.Config;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;

public class PostSearchNavigation extends Panel {

	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public PostSearchNavigation (String id, final IModel<String> model) {
		super(id, model);

		final WebMarkupContainer peopleSearchLink = new WebMarkupContainer(
				"people-search-link");
		peopleSearchLink.add(new Behavior() {

			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response ) {
				String script = String.format("$('#%s').click (function () { document.location = '%s'})", component.getMarkupId(), Config.PEOPLE_SEARCH_LINK + "/" + model.getObject());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}

		});
		peopleSearchLink.add(AttributeModifier.append("class",
				"global-ajax-action"));
		peopleSearchLink.setOutputMarkupId(true);

		final WebMarkupContainer postsSearchLink = new WebMarkupContainer(
				"posts-search-link");
		postsSearchLink.add(new Behavior() {

			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response ) {
				String script = String.format("$('#%s').click (function () { document.location = '%s'})", component.getMarkupId(), Config.POST_SEARCH_LINK + "/" + model.getObject());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}

		});
		postsSearchLink.add(AttributeModifier.append("class",
				"active global-ajax-action"));
		postsSearchLink.setOutputMarkupId(true);

		add(postsSearchLink, peopleSearchLink);
	}

}
