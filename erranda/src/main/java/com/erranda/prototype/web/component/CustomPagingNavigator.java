package com.erranda.prototype.web.component;

import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigator;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.IPagingLabelProvider;

public class CustomPagingNavigator extends AjaxPagingNavigator
{
   public CustomPagingNavigator(final String id, final IPageable pageable)
	{
		this(id, pageable, null);
	}

	public CustomPagingNavigator(final String id, final IPageable pageable,
		final IPagingLabelProvider labelProvider)
	{
		super(id,pageable,labelProvider);
	}

}