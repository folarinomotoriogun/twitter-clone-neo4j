package com.erranda.prototype.web;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;

public class PersonNewsFeedHeader extends Panel {

	@SpringBean
	Controller controller;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersonNewsFeedHeader(String id, IModel<PlatformUser> person) {
		super(id);
		add(new Label("header", Model.of(person.getObject().getName()
				+ "'s Timeline")));

		UserActions relationship = new UserActions("relationship", person);
		add(relationship);
	}

}
