package com.erranda.prototype.web.component;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;

public class AutogrowBehavior extends Behavior {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void renderHead (Component component, IHeaderResponse response) {
		component.setOutputMarkupId(true);
		String script = String.format("$('#%s').autogrow()", component.getMarkupId());
		response.render(OnDomReadyHeaderItem.forScript(script));
	}
}
