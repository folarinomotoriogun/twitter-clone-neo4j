/*
 * package com.erranda.prototype.web;
 * 
 * import java.util.Collections; import java.util.Date; import java.util.List;
 * 
 * import org.apache.wicket.ajax.AjaxRequestTarget; import
 * org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton; import
 * org.apache.wicket.markup.html.WebMarkupContainer; import
 * org.apache.wicket.markup.html.basic.Label; import
 * org.apache.wicket.markup.html.form.Form; import
 * org.apache.wicket.markup.html.form.TextArea; import
 * org.apache.wicket.markup.html.list.ListItem; import
 * org.apache.wicket.markup.html.list.ListView; import
 * org.apache.wicket.markup.html.panel.Panel; import
 * org.apache.wicket.model.LoadableDetachableModel; import
 * org.apache.wicket.model.PropertyModel; import
 * org.apache.wicket.spring.injection.annot.SpringBean; import
 * org.joda.time.DateTime; import org.joda.time.format.DateTimeFormat; import
 * org.joda.time.format.DateTimeFormatter; import
 * org.springframework.data.domain.PageRequest;
 * 
 * import com.erranda.prototype.UserSession; import
 * com.erranda.prototype.domain.Message; import
 * com.erranda.prototype.domain.Person; import
 * com.erranda.prototype.domain.Thankyou; import
 * com.erranda.prototype.domain.ThankyouRepository; import
 * com.erranda.prototype.domain.repo.PersonRepository;
 * 
 * public class ThanksPanel extends Panel {
 * 
 * private static final long serialVersionUID = 1L;
 * 
 * @SpringBean PersonRepository personRepo;
 * 
 * @SpringBean ThankyouRepository thankyouRepo;
 * 
 * public String listId;
 * 
 * public ThanksPanel(String id, final Long pid) { super(id); final
 * WebMarkupContainer thanksContainer = new WebMarkupContainer(
 * "container-thanks-list"); thanksContainer.setOutputMarkupId(true);
 * 
 * ListView<Thankyou> messages = new ListView<Thankyou>("thanks-list", new
 * LoadableDetachableModel<List<Thankyou>>() {
 * 
 * private static final long serialVersionUID = 1L;
 * 
 * @Override protected List<Thankyou> load() { Person person =
 * personRepo.findOne(pid); List<Thankyou> thankyous =
 * thankyouRepo.findByReceiver(person, new PageRequest(0, 10));
 * Collections.sort(thankyous, Collections.reverseOrder()); return thankyous; }
 * }) {
 * 
 * private static final long serialVersionUID = 1L;
 * 
 * @Override protected void populateItem(ListItem<Thankyou> item) { Thankyou
 * thisItem = item.getModelObject(); final Long tId = thisItem.getId(); // image
 * name summary date Label name = new Label("sender-name", new
 * LoadableDetachableModel<String>() {
 * 
 * private static final long serialVersionUID = 1L;
 * 
 * @Override protected String load() { return
 * thankyouRepo.findOne(tId).getSender().getName(); }
 * 
 * });
 * 
 * Label messageSummary = new Label("message-summary", new
 * LoadableDetachableModel<String>() {
 * 
 * private static final long serialVersionUID = 1L;
 * 
 * @Override protected String load() { return
 * thankyouRepo.findOne(tId).getData(); }
 * 
 * });
 * 
 * Label date = new Label("message-date", new LoadableDetachableModel<String>()
 * {
 * 
 * private static final long serialVersionUID = 1L;
 * 
 * @Override protected String load() { DateTimeFormatter out =
 * DateTimeFormat.forPattern("EEE, MMM d"); DateTime date = new
 * DateTime(thankyouRepo.findOne(tId).getSentDate()); return out.print(date); }
 * });
 * 
 * item.add(name, messageSummary, date); }
 * 
 * };
 * 
 * final Thankyou reply = new Thankyou(); Form<Message> replyForm = new
 * Form<Message>("reply-form"); replyForm.add(new TextArea<String>("reply-box",
 * new PropertyModel<String>(reply, "data"))); replyForm.add(new
 * IndicatingAjaxButton("send-button", replyForm) { private static final long
 * serialVersionUID = -6415555183396288060L;
 * 
 * @Override protected void onSubmit(AjaxRequestTarget target, Form<?>
 * OfferForm) { Person sender = UserSession.get().getPerson(); Person receiver =
 * personRepo.findOne(pid); reply.setReceiver(receiver);
 * reply.setSender(sender); reply.setSentDate(new Date()); DateTimeFormatter out
 * = DateTimeFormat.forPattern("EEE, MMM d"); DateTime date = new DateTime(new
 * Date());
 * 
 * StringBuffer success = new StringBuffer(); success.append(
 * "<li><div class='DMInboxItem' data-thread-id='1056558601-1056558601' data-last-message-id='546783055726534656'><div class='DMInboxItem-avatar'>"
 * + "<a class='js-action-profile js-user-profile-link' href='/" +
 * sender.getUsername() +
 * " data-user-id='1056558601'><div class='DMConversationAvatar DMConversationAvatar--1'><span class='DMConversationAvatar-container'><img class='DMConversationAvatar-image' src='https://pbs.twimg.com/profile_images/502026696652881920/P1g24fzW_normal.jpeg' alt=''></span></div></a></div><div class='DMInboxItem-title'><b class='fullname'>"
 * + sender.getName() +
 * "</b></div><p class='DMInboxItem-snippet js-tweet-text '>" + (reply.getData()
 * != null ? reply.getData() : "") +
 * "</p><div class='DMInboxItem-timestamp'><small class='time'><span class='_timestamp' data-aria-label-part='last' data-time='1419198213' data-long-form='true' data-include-sec='true'>"
 * + out.print(date) + "</span></small></div></div></li>");
 * target.prependJavaScript("$(\"#" + thanksContainer.getMarkupId() +
 * "\").append(\"" + success + "\")"); } });
 * 
 * thanksContainer.add(messages); add(thanksContainer, replyForm); listId =
 * thanksContainer.getMarkupId(); }
 * 
 * }
 */