package com.erranda.prototype.web.component;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.attributes.AjaxRequestAttributes;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.Form;
import org.wicketstuff.stateless.StatelessAjaxFormSubmitBehavior;

public class DisablingAjaxButton extends GlobalAjaxButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DisablingAjaxButton(String id, Form form) {
		super(id, form);
	}

	@Override
	public void onInitialize() {
		super.onInitialize();
		add(new AjaxEventBehavior("click") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$('#" + DisablingAjaxButton.this.getMarkupId()
						+ "').bind('click', function(){"
						+ "$(this).attr('disabled', 'disabled')});";
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript("initAjax()"));

			}

			@Override
			protected void onEvent(AjaxRequestTarget target) {

			}

		});
	}

	@Override
	protected StatelessAjaxFormSubmitBehavior newAjaxFormSubmitBehavior(String event) {
		return new StatelessAjaxFormSubmitBehavior(event) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				DisablingAjaxButton.this.onSubmit(target,
						DisablingAjaxButton.this.getForm());
				clear(target);
			}

			@Override
			protected void onAfterSubmit(AjaxRequestTarget target) {
				DisablingAjaxButton.this.onAfterSubmit(target,
						DisablingAjaxButton.this.getForm());
				target.appendJavaScript(String.format("$(\"#%s\").removeAttr(\"disabled\")", DisablingAjaxButton.this.getMarkupId()));
				clear(target);
			}

			@Override
			protected void onError(AjaxRequestTarget target) {
				DisablingAjaxButton.this.onError(target,
						DisablingAjaxButton.this.getForm());
				target.appendJavaScript(String.format("$(\"#%s\").removeAttr(\"disabled\")", DisablingAjaxButton.this.getMarkupId()));
				clear(target);
			}

			@Override
			protected void updateAjaxAttributes(AjaxRequestAttributes attributes) {
				super.updateAjaxAttributes(attributes);
				DisablingAjaxButton.this.updateAjaxAttributes(attributes);
			}
		};
	}

	private void clear(AjaxRequestTarget target) {
		target.appendJavaScript("$(\"#global-ajax-indicator\").css('display', 'none')");
		target.appendJavaScript("$(\"#global-logo\").css('display', 'block')");
		target.appendJavaScript("initAjax()");
		String script = "$('#" + DisablingAjaxButton.this.getMarkupId()
				+ "').removeAttr('disabled')";
		target.appendJavaScript(script);

	}

}
