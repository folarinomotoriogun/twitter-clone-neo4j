package com.erranda.prototype.web;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.ClientProperties;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Commented;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.ModalAjaxLink;
import com.erranda.prototype.web.component.TimeConverter;

public class CommentItem extends Panel {

	@SpringBean
	Controller controller;
	

	PlatformUser session = UserSession.get().getPerson();
	
	Label kudosLabel;
	
	AjaxLink<?> minusKudosLink = null;

	private static final long serialVersionUID = 1L;

	public CommentItem(String id, final IModel<Commented> model, ClientProperties cp,
			final String parentMarkup) throws ControllerException {
		super(id, model);
		final WebMarkupContainer container = new WebMarkupContainer("container");
		container.setOutputMarkupId(true);
		
		Commented object = model.getObject();
		final Long cid = object.getId();
		Label name = new Label("sender-name", Model.of(anonymizePerson(object)));
		Label messageSummary = new Label("message-summary", Model.of(parse(object)));
		// Label messageSummary = new Label("message-summary", new
		// PropertyModel<Comment>(object, "message"));
		Label date = new Label("message-date", Model.of(TimeConverter
				.convertTime(object.getCtime(), cp).split("#")[0]));
		add(container.add(name.setEscapeModelStrings(false), messageSummary.setEscapeModelStrings(false), date));

		final AjaxLink remove = new AjaxLink("remove") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.removeComment(cid);
					target.appendJavaScript(String.format("$('#%s').slideUp()",
							parentMarkup));
				} catch (ControllerException e) {
					e.printStackTrace();
					Authenticated page = (Authenticated) getPage();
					ErrorModalContainer feedback = page.errorModalContainer;
					feedback.show(
							new Label("modal-header", Model.of("Error")),
							new Label(
									"modal-content",
									Model.of("There was an error processing your request. Please try again.")));
					feedback.update(target, "", true);
					return;
				}
			}

		};
		remove.setVisible(false);
		container.add(new Behavior () {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public void renderHead (Component c, IHeaderResponse response) {
				String script = String.format("$('#%s').hover(function () { $('#%s').fadeIn ()}, function () { $('#%s').fadeOut ()});", container.getMarkupId(), remove.getMarkupId(), remove.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		

		// Data ownership security
		if (object.getSender().equals(UserSession.get().getPerson())
				|| UserSession.get().getPerson().getId()
						.equals(object.getErrand().getOwnerId()))
			remove.setVisible(true);
		container.add(remove);
		
		final AjaxLink<?> kudosLink = new AjaxLink<Object> ("kudos-link") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.commentKudos(cid, session.getId());
					 sendPushNotificationKudos (cid);
				} catch (ControllerException e) {
					String script = String
							.format("$('#%s').hide();$('#%s').fadeIn();",
									minusKudosLink.getMarkupId(),this.getMarkupId());
					target.appendJavaScript(script);
				}
			}
		};
		
		minusKudosLink = new AjaxLink<Object> ("minus-kudos-link") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.minusKudos(cid, model.getObject().getId());
				} catch (ControllerException e) {
					String script = String
							.format(" $('#%s').hide();$('#%s').fadeIn();",
									kudosLink.getMarkupId(), this.getMarkupId());
					target.appendJavaScript(script);
				}
			}
		};
		
		
		
		
		
		final ModalAjaxLink<?> peopleThatGaveKudos = new ModalAjaxLink<Object> ("people-that-gave-kudos") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Authenticated page = (Authenticated) getPage ();
				ModalContainer relationships = page.modalContainer;
				relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">People who gave this Kudos</h3>")).setEscapeModelStrings(false), new CommentKudosByPeople ("modal-content", cid));
				relationships.update(target, "", true);
			}
			
		};
		Integer kudosCount = controller.commentKudosCount(cid);
		kudosLabel = new Label ("label", Model.of(kudosCount));
		kudosLabel.setOutputMarkupId(true);
		kudosLabel.add(AttributeModifier.replace("data-kudos-count", kudosCount));
		
		if (controller.isCommentKudos(session.getId(), cid))
			kudosLink.add(AttributeModifier.replace("style", "display:none"));
		else
			minusKudosLink.add(AttributeModifier.replace("style", "display:none"));
		if (kudosCount == 0) {
			peopleThatGaveKudos.add(AttributeModifier.replace("style", "display:none"));
		}
		
		add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').click(function () {$(this).hide(); $('#%s').fadeIn(); var kudosLabel = '%s'; var count = parseInt ($('#' + kudosLabel).attr('data-kudos-count')); count = count - 1; $('#' + kudosLabel).html (count); $('#' + kudosLabel).attr('data-kudos-count', count); if (count == 0) { $('#%s').hide()};});  ",
								minusKudosLink.getMarkupId(), kudosLink.getMarkupId(), kudosLabel.getMarkupId(), peopleThatGaveKudos.getMarkupId());
				String script2 = String
						.format("$('#%s').click(function () {$(this).hide(); $('#%s').fadeIn(); var kudosLabel = '%s'; var count = parseInt ($('#' + kudosLabel).attr('data-kudos-count')); count = count + 1; $('#' + kudosLabel).html (count); $('#' + kudosLabel).attr('data-kudos-count', count); if (count > 0) { $('#%s').fadeIn()};});  ",
								kudosLink.getMarkupId(), minusKudosLink.getMarkupId(), kudosLabel.getMarkupId(), peopleThatGaveKudos.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript(script2));
			}
		});
		container.add(peopleThatGaveKudos.add(kudosLabel), kudosLink, minusKudosLink);
	}

	private String anonymizePerson(Commented post) {
		String userUrl = (post.getAnonymous() ? "" : " href='"
				+ Config.PROFILE_NAME_LINK + "/"
				+ post.getSender().getUsername() + "'");
		String name = (post.getAnonymous() ? "Anonymous" : (post.getSender()
				.getName()));
		String username = (post.getAnonymous() ? "Hidden" : post.getSender()
				.getUsername());
		PlatformUser person = post.getSender();
		String src = (post.getAnonymous() ? new PlatformUser().getProfilePictureUrl()
				: person.getThumbNail());

		String userdetails = "<a class='account-group js-account-group js-action-profile js-user-profile-link js-nav' "
				+ userUrl
				+ ">"
				+ "<strong class='fullname js-action-profile-name'>"
				+ name
				+ "</strong><span class='username js-username'><s>@"
				+ username
				+ "</s></span><div class='DMInboxItem-avatar'>"
				+ "<img class='avatar js-action-profile-avatar size32' src='"
				+ src + "'></div></a>";
		return userdetails;
	}
	
	private String parse (Commented comment) {
		if (comment == null || comment.getMessage() == null)
			return "";
		String details = comment.getMessage();
		char [] letters = details.toCharArray();
		StringBuffer current = new StringBuffer ();
		Boolean appendNext = false;
		for (int i = 0 ; i < letters.length; i++) {
			char c = letters[i];
			if (c == '@') {
				appendNext = true;
			} else if (c == ' ') {
				current.append(c);
				appendNext = false;
			} else if (c != ' ' && c != '@' && appendNext) {
				current.append(c);
			} 
		}
		
		String[] usernames = current.toString().split(" ");
		
		for (String username : usernames) {
				PlatformUser person = controller.findUsername(username.toLowerCase());
				if (person != null)
					details = details.replace("@" + username, String.format("<a href='%s'>@%s</a>", person.getLink(), username));
		}
		return details;
	}
	
	private void sendPushNotificationKudos (Long cid) throws ControllerException {
		List<Long> people = controller.commentKudosNotificationList(cid);
		for (Long person : people) {
			IPushNode node = ((App) App.get()).getNotificationPanel(person);
			if (node != null)
				TimerPushService.get().publish(node, "");
		}
	}

}
