package com.erranda.prototype.web.component;

import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.IModel;

public class KeepAliveTextArea extends TextArea<Void> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public KeepAliveTextArea(String id) {
		super(id);
		add(new KeepAliveBehavior());
	}

	public KeepAliveTextArea(String id, IModel<Void> model) {
		super(id, model);
		add(new KeepAliveBehavior());
	}
}