package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.PlatformUser;

public class DirectoryPage extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DirectoryPage() {
		
	}

	@Override
	public void onInitialize() {
		super.onInitialize();
		Long start = System.currentTimeMillis();
		Double startD = start.doubleValue();

		add(new Directory("people"));

		Long end = System.currentTimeMillis();
		Double endD = end.doubleValue();
		p((endD - startD) / 1000);
	}

}
