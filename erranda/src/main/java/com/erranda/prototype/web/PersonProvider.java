package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.Collection;

import org.wicketstuff.select2.Response;
import org.wicketstuff.select2.TextChoiceProvider;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;

public class PersonProvider extends TextChoiceProvider<PlatformUser> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Controller controller;
	
	Long pid;

	public PersonProvider(Controller controller, Long pid) {
		this.controller = controller;
		this.pid = pid;
	}

	@Override
	protected String getDisplayText(PlatformUser arg0) {
		return arg0.getName() + " @" + arg0.getUsername();
	}

	@Override
	protected Object getId(PlatformUser arg0) {
		return arg0.getId();
	}

	@Override
	public void query(String term, int page, Response<PlatformUser> response) {
		response.addAll(controller.searchFriends(pid, term));

	}

	@Override
	public Collection<PlatformUser> toChoices(Collection<String> arg0) {

		ArrayList<PlatformUser> nids = new ArrayList<PlatformUser>();
		for (String id : arg0) {
			try {
				nids.add(controller.findPerson(Long.parseLong(id)));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return nids;
	}
}
