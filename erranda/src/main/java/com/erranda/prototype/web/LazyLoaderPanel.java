package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.Panel;

public class LazyLoaderPanel extends Panel {

	Component c;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LazyLoaderPanel(String id, Component c) {
		super(id);
		this.c = c;
		add(new AjaxLoaderPanel("content"));
		setOutputMarkupId(true);
	}

	public void update(AjaxRequestTarget target) {
		removeAll();
		add(c);
		target.add(this);
	}

}
