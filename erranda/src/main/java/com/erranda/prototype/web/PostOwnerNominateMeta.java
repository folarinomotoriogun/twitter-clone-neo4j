package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.web.component.ModalAjaxLink;
import com.erranda.prototype.web.component.TimeConverter;

public class PostOwnerNominateMeta extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;
	
	PlatformUser session = UserSession.get().getPerson();
	
	WebMarkupContainer othersNominated = null;

	
	public PostOwnerNominateMeta(String id, IModel<Post> model)
			throws ControllerException {
		super(id);
		Errand errand = model.getObject().getErrand();
		if (errand.getAnonymous()) {
			setVisible(false);
			return;
		}
		final Long eid = errand.getId();
		Integer count = controller.countOfPeopleNominateByOwner(session.getId(), eid);
		if (count == 0) {
			this.setVisible(false);
			return;
		}
		
		PlatformUser nominated = model.getObject().getOwner();
		ExternalLink personLink = new ExternalLink("link",
				new PropertyModel<String>(nominated, "link"));
		Label name = new Label("name", new PropertyModel<String>(nominated, "name"));
		if (count >= 1) {
			othersNominated = new ModalAjaxLink<String> ("others") {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void onClick(AjaxRequestTarget target) {
					if (UserSession.get().userNotLoggedIn()) {
						Authenticated page = (Authenticated) getPage ();
						ModalContainer relationships = page.modalContainer;
						relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">Want to see people mentioned?</h3>")).setEscapeModelStrings(false).setOutputMarkupId(true), new ModalSignUp("modal-content").setOutputMarkupId(true));
						relationships.update(target, "", true);
						return;
					}
					Authenticated page = (Authenticated) getPage ();
					ModalContainer relationships = page.modalContainer;
					relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">People mentioned in this post</h3>")).setEscapeModelStrings(false), new OtherPeopleNominatedByOwner ("modal-content", eid));
					relationships.update(target, "", true);
				}
			};
			Label othersLabel = new Label ("others-label", Model.of(count == 1 ? "1 person" : count + " people"));
			othersNominated.add(othersLabel);
		} else {
			othersNominated = new WebMarkupContainer ("others");
			Label othersLabel = new Label ("others-label");
			othersNominated.add(othersLabel);
			othersNominated.setVisible(false);
		}
		Label time = new Label("time", Model.of(TimeConverter.timeAgo(errand.getAdded())));
		add(time);
		add(personLink.add(name), othersNominated);		
	}

}
