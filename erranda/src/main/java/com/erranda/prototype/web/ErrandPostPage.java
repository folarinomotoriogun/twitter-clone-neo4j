package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.Errand;

public class ErrandPostPage extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public ErrandPostPage() {
		Url url = (RequestCycle.get().getRequest()).getUrl();
		String id = RequestCycle.get().getUrlRenderer().renderRelativeUrl(url);
		id = id.replace("./", "");
		id = id.split("\\?")[0];
		PageParameters p = new PageParameters();
		p.add("pid", id);
		throw new RestartResponseAtInterceptPageException(new ErrandPostPage(p));
	}

	public ErrandPostPage(PageParameters parameters) {
		super();
		String postId = parameters.get("pid") != null ? parameters.get("pid")
				.toString() : null;
		
		RuntimeException exp = new RestartResponseAtInterceptPageException(
				ErrorPage.class);

		try {
			final Long pid = Long.parseLong(postId);
			
			LoadableDetachableModel<Errand> model = new LoadableDetachableModel<Errand> (){

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				protected Errand load() {
					try {
						Errand errand = controller.findErrand(pid);
						return errand;
					} catch (ControllerException e) {
						return null;
					}
				}
				
			};
			
			if (model.getObject() == null)
				throw exp;
			add(new ErrandSingleDetailed("this-post", model, this, initClientProperties(), null, null));
			add(new Proposals("proposals", pid).setVisible(false));
		} catch (ControllerException e) {
			e.printStackTrace();
			throw exp;
		} catch (NumberFormatException e) {
			return;
		}

	}

}
