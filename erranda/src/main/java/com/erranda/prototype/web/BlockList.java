package com.erranda.prototype.web;

import java.util.Collections;
import java.util.List;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class BlockList extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	ListView<PlatformUser> resultsList;

	public BlockList() {
		super();
		WebMarkupContainer container = new WebMarkupContainer("items-container");
		container.setOutputMarkupId(true);

		resultsList = new ListView<PlatformUser>("result",
				new LoadableDetachableModel<List<PlatformUser>>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<PlatformUser> load() {
						try {
							return controller.myBlockList(UserSession.get()
									.getPerson().getId());
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return Collections.emptyList();
						}
					}

				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<PlatformUser> item) {
				Long pid = item.getModel().getObject().getId();
				item.add(new SearchResult("this", item.getModel()));
			}

		};
		resultsList.setOutputMarkupId(true);

		add(container.add(resultsList));

		addFooter(resultsList);
	}

	private void addFooter(ListView<PlatformUser> newsFeedList) {
		int size = newsFeedList.getList().size();
		Label emptyTimeLine = new Label("empty");
		add(emptyTimeLine);

		if (size == 0) {
			emptyTimeLine
					.setDefaultModel(new LoadableDetachableModel<Object>() {

						private static final long serialVersionUID = 1L;

						@Override
						protected Object load() {
							return "You haven't blocked anyone";
						}

					});
		} else {
			emptyTimeLine.setDefaultModel(Model.of(""));
		}
		add(emptyTimeLine);
	}

}
