package com.erranda.prototype.web;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.wicketstuff.push.AbstractPushEventHandler;
import org.wicketstuff.push.IPushEventContext;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.Message;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.mailer.EmailNewAction;

public class MessagesPanel extends Panel {

	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public String listId;

	public int skip = 0;

	public int limit = 10;
	
	private int cursor;

	@SpringBean
	Neo4jTemplate template;

	PlatformUser session = UserSession.get().getPerson();

	public MessagesPanel(String id, Conversation c) {
		super(id);
		final Long cid = c.getId();

		
		final WebMarkupContainer messagesContainer = new WebMarkupContainer(
				"container-messages-list");
		messagesContainer.setOutputMarkupId(true);

		final ListView<Message> messages = new ListView<Message>("messages-list",
				new LoadableDetachableModel<List<Message>>() {

					private static final long serialVersionUID = 1L;

					@Override
					protected List<Message> load() {
						List<Message> messages;
						try {
							messages = controller.messages(cid, skip, limit);
							Collections.sort(messages);
							cursor = messages.size();
							return messages;
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}

					}
				}) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Message> item) {
				// image name summary date
				item.add(new MessageItem("this", item.getModel()));
			}
		};

		add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				// Scroll to bottom each time
				String script = String.format("$('#%s').scrollTop($('#%s')[0].scrollHeight);$('#%s').attr('style', 'cursor:auto');", messagesContainer.getMarkupId(),messagesContainer.getMarkupId(), messagesContainer.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));

			}

		});

		final Message reply = new Message();
		final Form<Message> replyForm = new Form<Message>("reply-form");
		final TextArea<String> replyField = new TextArea<String>("reply-box",
				new PropertyModel<String>(reply, "payloadRaw"));
		replyForm.add(replyField.setRequired(true));
		final AjaxButton submit = new AjaxButton ("send-button", replyForm) {
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> OfferForm) {
				Conversation c;
				try {
					c = controller.findOneConversation(cid);
				} catch (ControllerException e) {
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
				PlatformUser sender = UserSession.get().getPerson();
				reply.setCid(cid);
				reply.setId(null);
				reply.setReceiver(MessageHeader.withWho(c));
				reply.setSender(sender);
				reply.setCtime(new Date());
				try {
					controller.sendMessage(sender.getId(), MessageHeader
							.withWho(c).getId(), reply.getPayloadRaw());
					sendPushNotificationMessage(MessageHeader
							.withWho(c).getId());
					
					cursor++;
					ListItem<Message> item = new ListItem<Message>(cursor,
							Model.of(reply));
					item.setOutputMarkupId(true);
					messages.add(item);
					target.prependJavaScript(String
							.format("var item=document.createElement('%s');item.class='original-tweet-container'; item.id='%s';"
									+ "$('#%s').append(item);", "li",
									item.getMarkupId(), messagesContainer.getMarkupId()));
					// Line dependency
					item.add(new MessageItem("this", Model.of(reply)));
					target.add(item);
					target.appendJavaScript("$(\"#"
							+ messagesContainer.getMarkupId()
							+ "\").scrollTop($(\"#"
							+ messagesContainer.getMarkupId()
							+ "\")[0].scrollHeight);");
					target.appendJavaScript("$(\"#message-reply-box\").removeAttr(\"readonly\")");
					target.appendJavaScript("$(\"#message-reply-box\").val('')");
					PlatformUser person = MessageHeader.withWho(c);

					String html = new String(ComponentRenderer.renderPage(
							new PageProvider(new EmailNewAction(person, "\""
									+ reply.getPayloadRaw() + "\"",
									"http://www.erranda.com/home"))).toString());
					MailMan man = new MailMan();
					man.sendMessage(
							"New message from " + session.getName(),
							MessageHeader.withWho(c).getEmail(), html);
					replyField.detach();
				} catch (ControllerException e) {
					error(e.getMessage());
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			
		};
		
		replyForm.add(submit);

		add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13 && $('#%s').val().length > 0) {$('#%s').click(); return false; } })});",
								replyField.getMarkupId(), replyField.getMarkupId(),
								submit.getMarkupId());
				String script2 = String
						.format("$('#%s').click(function () {$('#%s').attr ('readonly', 'readonly')}); $('#%s').autogrow();",
								submit.getMarkupId(), replyField.getMarkupId(),
								replyField.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript(script2));
			}
		});


		messagesContainer.add(messages);
		add(messagesContainer, replyForm);
		listId = messagesContainer.getMarkupId();
	}
	
	private void sendPushNotificationMessage (Long rid) throws ControllerException {
		List<Long> people = controller.messageNotificationList(session.getId(), rid);
		for (Long person : people) {
			IPushNode node = ((App) App.get()).getMessagePanel(person);
			IPushNode instant = ((App) App.get()).getInstantMessagePanel(person);
			if (node != null) {
				TimerPushService.get().publish(node, "");
				TimerPushService.get().publish(instant, "");
			}
		}
	}
	
	

}
