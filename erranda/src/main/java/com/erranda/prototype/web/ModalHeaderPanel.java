package com.erranda.prototype.web;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class ModalHeaderPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4469892228838616215L;

	public ModalHeaderPanel(String id) {
		super(id);
		add(new Label("modal-header"));
	}

	public ModalHeaderPanel(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
