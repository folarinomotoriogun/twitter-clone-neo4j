package com.erranda.prototype.web;

import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.InvalidEmailException;
import com.erranda.prototype.domain.InvalidFirstNameException;
import com.erranda.prototype.domain.InvalidLastNameException;
import com.erranda.prototype.domain.InvalidPasswordException;
import com.erranda.prototype.domain.InvalidUsernameException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.StatelessIndicatingAjaxButton;
import com.erranda.prototype.web.mailer.EmailWelcome;

/**
 * Wicket form element for the person model
 * 
 * @author Folarin
 *
 */
public class SignUpForm extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@SpringBean
	Controller controller;

	public SignUpForm(String id) {
		super(id);
		final PlatformUser person = new PlatformUser();
		
		StatelessForm<PlatformUser> signup = new StatelessForm<PlatformUser>("signup");

		// FeedbackPanel
		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);
		feedback.setMarkupId("signup-feedback");
		signup.add(feedback);

		final TextField<String> fname = new TextField<String>("fname",
				new PropertyModel<String>(person, "firstName"));
		fname.setOutputMarkupId(true);
		
		final TextField<String> lname = new TextField<String>("lname",
				new PropertyModel<String>(person, "lastName"));
		lname.setOutputMarkupId(true);
		
		TextField<String> email = new TextField<String>("email",
				new PropertyModel<String>(person, "email"));
		email.setOutputMarkupId(true);
		email.setLabel(new ResourceModel("label.email"));
		
		final TextField<String> username = new TextField<String>("username",
				new PropertyModel<String>(person, "usernameRaw"));
		username.setOutputMarkupId(true);
		username.setLabel(new ResourceModel("label.username"));

		final TextField<String> password = new PasswordTextField("password",
				new PropertyModel<String>(person, "password"));
		password.setOutputMarkupId(true);
		password.setRequired(false);
		
		final HiddenField<String> location = new HiddenField <String> ("user-location", new PropertyModel<String>(person, "localityName"));
		
		final StatelessIndicatingAjaxButton submit = new StatelessIndicatingAjaxButton("submit", signup) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				if (target == null)
					return;
				try {
					person.setName(person.getFirstName() + " "
							+ person.getLastName());
					PlatformUser session = controller.signUp(person);
					if(UserSession.get().isTemporary())
						UserSession.get().bind();
					UserSession.get().setPerson(session);
					UserSession.get().setUserId(session.getId());
					String html = new String(ComponentRenderer.renderPage(
							new PageProvider(new EmailWelcome(person)))
							.toString());
					MailMan man = new MailMan();
					man.sendMessage("Welcome to Erranda - Confirm Account",
							session.getEmail(), html);
					throw new RestartResponseAtInterceptPageException(Home.class);
					

				} catch (ControllerException e) {
					if (e instanceof InvalidFirstNameException) {
						String errorScript = String.format("$('#%s').popover('destroy').popover ({title: '', content: '%s', placement: 'left'}).popover('show');", "signup-firstname" , e.getMessage());
						target.appendJavaScript(errorScript);
					} else if (e instanceof InvalidLastNameException) {
						String errorScript = String.format("$('#%s').popover('destroy').popover ({title: '', content: '%s', placement: 'left'}).popover('show');", "signup-lastname" , e.getMessage());
						target.appendJavaScript(errorScript);
					}  else if (e instanceof InvalidUsernameException) {
						String errorScript = String.format("$('#%s').popover('destroy').popover ({title: '', content: '%s', placement: 'left'}).popover('show');", "signup-username" , e.getMessage());
						target.appendJavaScript(errorScript);
					} else if (e instanceof InvalidPasswordException) {
						String errorScript = String.format("$('#%s').popover('destroy').popover ({title: '', content: '%s', placement: 'left'}).popover('show');", "signup-password" , e.getMessage());
						target.appendJavaScript(errorScript);
					} else if (e instanceof InvalidEmailException) {
						String errorScript = String.format("$('#%s').popover('destroy').popover ({title: '', content: '%s', placement: 'left'}).popover('show');", "signup-email" , e.getMessage());
						target.appendJavaScript(errorScript);
					}
					String script = String
							.format("$('#%s').html('Sign Up');",
									this.getMarkupId());
					target.appendJavaScript(script);
				} catch (MessagingException e) {
					error(e.getMessage());
					target.add(feedback);
					String errorScript = String.format("$('#%s').popover('destroy').popover ({title: '', content: '%s', placement: 'left'}).popover('show');", this.getMarkupId() , e.getMessage());
					target.appendJavaScript(errorScript);
				}
				String script = String
						.format("$('#%s').html('Sign Up');",
								this.getMarkupId());
				target.appendJavaScript(script);
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(feedback);
			}
		};
		submit.setMarkupId("signup-button");
		submit.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').click(function () {$('#%s').html('Signing Up...');})",
								component.getMarkupId(), component.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		
		List<TextField<String>> cs = Arrays.asList(fname, lname, username, password, email);
		for (TextField<String> t : cs) {
			t.add(new Behavior() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void renderHead(Component component, IHeaderResponse response) {
					String script = String
							.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13) {$('#%s').click();} })});",
									component.getMarkupId(),
									submit.getMarkupId());
					
					response.render(OnDomReadyHeaderItem.forScript(script));
				}
			});
		}
		
		signup.add(submit);

		signup.add(fname, lname, email, password, username, location);

		add(signup.setOutputMarkupId(true));
	}

}
