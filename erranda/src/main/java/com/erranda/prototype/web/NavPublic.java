package com.erranda.prototype.web;

import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;

public class NavPublic extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NavPublic(String id) {
		super(id);
		
		BookmarkablePageLink<SignUp> signUp = new BookmarkablePageLink<SignUp>(
				"sign-up", SignUp.class);
		add(signUp);
		
		BookmarkablePageLink<Login> logIn = new BookmarkablePageLink<Login>(
				"log-in", Login.class);
		add(logIn);
	}

}
