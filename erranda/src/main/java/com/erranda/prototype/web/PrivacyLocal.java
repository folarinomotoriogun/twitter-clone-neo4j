/**
 * 
 */
package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import com.erranda.prototype.UserSession;

/**
 * @author Folarin
 *
 */
public class PrivacyLocal extends Panel {

	/**
	 * @param id
	 */
	public PrivacyLocal(String id, final ListItem<String> item,
			final String buttonId, final String inputField) {
		super(id);
		setOutputMarkupId(true);

		add(new Label("localty", new PropertyModel<String>(UserSession.get()
				.getPerson(), "localityName")));
		add(new Behavior() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String html = htmlData();
				String script = String
						.format("$('#%s').bind('click', function(){$('#%s').html('%s'); $('#%s').val('%s')});",
								item.getMarkupId(), buttonId, html, inputField,
								"LOCAL");
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
	}

	public static String htmlData() {
		return String
				.format("<span class=\"fa fa-map-marker privacy-icon\"></span><span class=\"privacy-text\">%s</span>",
						UserSession.get().getPerson().getLocalityName());
	}

	/**
	 * @param id
	 * @param model
	 */
	public PrivacyLocal(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
