package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.ConfirmingGlobalAjaxLink;
import com.erranda.prototype.web.component.ModalAjaxLink;

public class FriendsAction extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	Long rid;

	private Label label;

	private PlatformUser session = UserSession.get().getPerson();

	public FriendsAction(final String id, Long pid,
			final WebMarkupContainer container) {
		super(id);
		this.rid = pid;
		setOutputMarkupId(true);

		
		final IndicatingAjaxLink<String> unfriend = new IndicatingAjaxLink<String>(
				"unfriend-person") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Long sid = UserSession.get().getPerson().getId();
				try {
					controller.removeFriend(sid, rid);
					((UserActionContainer) container)
							.replaceWindow(new AddAsFriend(id, rid, container));
					((UserActionContainer) container).update(target, "");
				} catch (ControllerException e) {
					throw new RuntimeException(e.getMessage());
				}
			}
		};

		final ConfirmingGlobalAjaxLink<String> block = new ConfirmingGlobalAjaxLink<String>(
				"block", "Continue?") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Long sid = UserSession.get().getPerson().getId();
				try {
					if (!controller.isBlocked(sid, rid))
						controller.blockPerson(sid, rid);
					else
						controller.unblockPerson(sid, rid);
					throw new RestartResponseAtInterceptPageException(getPage().getPageClass());
				} catch (ControllerException e) {
					throw new RuntimeException(e.getMessage());
				}
			}
		};

		label = new Label("label", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				try {
					return controller.isBlocked(session.getId(), rid) ? "Unblock"
							: "Block";
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "";
				}
			}

		});
		label.setOutputMarkupId(true);

		add(unfriend, block.add(label));

	}

}
