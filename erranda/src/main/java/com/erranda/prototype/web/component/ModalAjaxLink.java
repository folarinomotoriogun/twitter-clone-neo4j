package com.erranda.prototype.web.component;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;

public class ModalAjaxLink<T> extends AjaxLink<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ModalAjaxLink(String id) {
		super(id);
		
		add(new Behavior() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').click(function () {$('#modal-body-panel').html(''); $('#main-modal-ajax-indicator').fadeIn('slow'); if ($('#bigModal.in').length == 0) {$('#show-modal-link').click();} });",
								getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});


	}

	@Override
	public void onClick(AjaxRequestTarget arg0) {
		// TODO Auto-generated method stub
		
	}

}
