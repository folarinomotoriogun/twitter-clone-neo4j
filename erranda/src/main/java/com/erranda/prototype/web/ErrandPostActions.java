package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostViewItem;
import com.erranda.prototype.web.component.ConfirmingGlobalAjaxLink;
import com.erranda.prototype.web.component.JqueryActions;
import com.erranda.prototype.web.component.ModalAjaxLink;

public class ErrandPostActions extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;

	public ErrandPostActions(String id, final Long ownerid, final Long eid,
			final IModel<Errand> iModel, final ListItem<PostViewItem> item) {
		super(id);
		Label meta = new Label("meta");
		Errand post = iModel.getObject();
		String type = post.getPrivacy().toUpperCase();
		if (type.equals("FRIENDS")) {
			meta.add(AttributeModifier.replace("class", "fa fa-users"));	
		} else if (type.equals("LOCAL")) {
			meta.add(AttributeModifier.replace("class", "fa fa-map-marker"));
		} else if (type.equals("PUBLIC")) {
			meta.add(AttributeModifier.replace("class", "fa fa-globe"));
		} else if (type.equals("PRIVATE")) {
			meta.add(AttributeModifier.replace("class", "fa fa-lock"));
		}
		meta.add(AttributeModifier.replace("title", post.getMetaInfo()));
		add(meta);
		
		ExternalLink detailsLink =  new ExternalLink ("details-link", Model.of(post.getErrandLink()));
		detailsLink.add (AttributeModifier.replace("target", "_blank"));
		add (detailsLink);
		
		ConfirmingGlobalAjaxLink<Void> deleteLink = new ConfirmingGlobalAjaxLink<Void>(
				"delete-link", "Are you sure?") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.deleteErrand(eid);
					if (item == null)
						throw new RestartResponseAtInterceptPageException (Home.class);
					JqueryActions.slideHide(item, target);
				} catch (ControllerException e) {
					// show modal error;
					e.printStackTrace();
				}
			}
		};

		WebMarkupContainer reportLink = new ModalAjaxLink<String>(
				"report-link") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Authenticated page = (Authenticated) getPage();
				Label header = new Label(
						"modal-header",
						"<h3 class=\"modal-title report-title\" id=\"block-or-report-dialog-header\">Block or report</h3>");
				header.setEscapeModelStrings(false);
				page.modalContainer.replaceWindow(header, new BlockReportForm(
						"modal-content", eid, ownerid, item));
				page.modalContainer.update(target, "", true);
			}

		};
		
		if (UserSession.get().userNotLoggedIn()){
			reportLink = new WebMarkupContainer("report-link");
			reportLink.setVisible(false);
		}
		
		ModalAjaxLink<String> edit = new ModalAjaxLink<String>(
				"edit-link") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {	
			try {
				Authenticated page = (Authenticated) getPage();
				Label header = new Label(
						"modal-header",
						"<h3 class=\"modal-title\">Edit Post</h3>");
				header.setEscapeModelStrings(false);	
				page.modalContainer.replaceWindow(header, new PrivacySwitch(
							"modal-content", Model.of(iModel.getObject()),item));
				page.modalContainer.update(target, "", true);	
			} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};

		PlatformUser person = UserSession.get().getPerson();
		if (person.getId().equals(ownerid) && UserSession.get().userLoggedIn()) {
			add(deleteLink, edit.setVisible(false));
			WebMarkupContainer dummy = new WebMarkupContainer ("report-link");
			add(dummy);
			dummy.setVisible(false);
		} else {
			WebMarkupContainer deleteLinkDummy = new WebMarkupContainer(
					"delete-link");
			WebMarkupContainer editDummy = new WebMarkupContainer(
					"edit-link");
			add(editDummy.setVisible(false), deleteLinkDummy.setVisible(false));
			add(reportLink);
		}
	}

}
