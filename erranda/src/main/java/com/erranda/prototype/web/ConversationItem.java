package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.Strings;

import com.erranda.prototype.App;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.Message;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.TimeConverter;

public class ConversationItem extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	Authenticated page;

	ModalContainer modalContainer;

	public ConversationItem(String id, final IModel<Conversation> thisItem,
			final ConversationsPanel panel, Authenticated parent) {
		super(id);
		page = parent;

		modalContainer = page.modalContainer;
		// image name summary date
		Label name = new Label("sender-name",
				new LoadableDetachableModel<String>() {

					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						Conversation c = thisItem.getObject();
						String name = withWho(c).getName();
						if (c.getLastMessage() != null
								&& !c.getLastMessage().getIsRead()
								&& !c.getLastMessage()
										.getSid()
										.equals(UserSession.get()
												.getPerson().getId())) {
							name = name + " (Unread)";
						}
						return name + (((App) App.get()).getInstantMessagePanel(withWho(c).getId()) != null ? " <span class='fa fa-circle' style='margin-left: 10px;color: #5ABB69;'></span>" : "");
					}

				});
		
		name.setEscapeModelStrings(false);
		Label replyMessage = new Label("message-reply"); 
		
		Label messageSummary = new Label("message-summary",
				new LoadableDetachableModel<String>() {

					/**			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						StringBuffer message = new StringBuffer();
						Message model;
						model = thisItem.getObject().getLastMessage();
						if (model != null
								&& model.getSid().equals(
										UserSession.get().getPerson()
												.getId())) {
								message.append(" <span class=\"fa fa-mail-forward\"></span>");
						} else 
							message.append(" <span class=\"fa fa-mail-reply\"></span>");
							if (model.getPayload().length() > 120)
								message.append(Strings.escapeMarkup(model.getPayload()).toString()
										.substring(0, 120) + "...");
							else
								message.append(Strings.escapeMarkup(model.getPayload()).toString());
						return message.toString();

					}

		});
		messageSummary.setEscapeModelStrings(false);
		Message model = thisItem.getObject().getLastMessage();

		Label date = new Label("message-date",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						return TimeConverter.convertTime(
								thisItem.getObject()
										.getLastMessage().getCtime(),
								page.cp).split("#")[0];
					}

				});

		add(name, messageSummary, date);

		WebMarkupContainer iconImage = new WebMarkupContainer("icon-image");
		iconImage.add(AttributeModifier.replace("src",
				new LoadableDetachableModel<Object>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = -1446646754517223593L;

					@Override
					protected Object load() {
						return withWho(thisItem.getObject())
								.getThumbNail();
					}

				}));
		add(iconImage);
		
		messageSummary.add(new Behavior() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component c, IHeaderResponse response) {
				String script = String.format("$('#%s').emotions()", c.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
	}

	public static PlatformUser withWho(Conversation c) {
		PlatformUser current = UserSession.get().getPerson();
		if (!c.getReceiver().equals(current))
			return c.getReceiver();
		else
			return c.getSender();
	}

}
