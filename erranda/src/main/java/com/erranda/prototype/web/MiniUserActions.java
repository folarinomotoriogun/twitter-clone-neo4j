/**
 * 
 */
package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;

/**
 * @author Folarin
 *
 */
public class MiniUserActions extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	PlatformUserRepository personRepo;

	@SpringBean
	Controller controller;

	WebMarkupContainer container;

	private Long rid;

	public MiniUserActions(String id, IModel<PlatformUser> person) {
		super(id);
		this.rid = person.getObject().getId();
		PlatformUser user = UserSession.get().getPerson();
		if (user.getId().equals(rid))
			setVisible(false);
		addUserActions(person);
	}

	private void addUserActions(IModel<PlatformUser> person) {
		Long sid = UserSession.get().getPerson().getId();
		try {
			Boolean pending = controller.isPendingFriendRequest(sid, rid);
			Boolean friends = controller.areWeFriends(sid, rid);
			
			AjaxLink<String> notFriendsAction = new AjaxLink<String>(
					"not-friends-link") {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void onClick(AjaxRequestTarget target) {
					PlatformUser sender = UserSession.get().getPerson();
					try {
						controller.sendFriendRequest(sender.getId(), rid);
					} catch (ControllerException e) {
						error(e.getMessage());
					}
				}
			};
			
			ExternalLink friendsAction = new ExternalLink("friends-action-link", new PropertyModel<String>(person.getObject(), "link"));
			final ExternalLink requestSentAction = new ExternalLink("pending-action-link", new PropertyModel<String>(person.getObject(), "link"));
			add(friendsAction, requestSentAction, notFriendsAction);
			
			notFriendsAction.add(new Behavior () {
				
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void renderHead (Component c, IHeaderResponse response) {
					String javaScript = String.format("$('#%s').click (function () {$(this).hide(); $('#%s').fadeIn();});", c.getMarkupId(), requestSentAction.getMarkupId()); 
					response.render(OnDomReadyHeaderItem.forScript(javaScript));
				}
			});
			Boolean isPerson = person.getObject().getType().equals(Config.PLATFORM_TYPE_PERSON);
			Boolean sessionIsPerson = UserSession.get().getPerson().getType().equals(Config.PLATFORM_TYPE_PERSON);
			if (pending && isPerson && sessionIsPerson) {
				requestSentAction.add(AttributeModifier.replace("style", "display:inline-block"));
			} else if (friends && isPerson && sessionIsPerson) {
				friendsAction.add(AttributeModifier.replace("style", "display:inline-block"));
			} else if (isPerson && sessionIsPerson) {
				notFriendsAction.add(AttributeModifier.replace("style", "display:inline-block"));
			} 
			
			if (!isPerson) {
				add(new MiniFollowButton("mini-follow-action", person));
			} else if (!sessionIsPerson) {
				add(new MiniFollowButton("mini-follow-action", person));
			} else {
				add(new WebMarkupContainer ("mini-follow-action"));
			}
			
		} catch (ControllerException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
