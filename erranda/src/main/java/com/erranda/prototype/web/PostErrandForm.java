package com.erranda.prototype.web;

import static com.erranda.prototype.Config.IMAGES;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.ClientProperties;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.file.File;
import org.apache.wicket.validation.validator.UrlValidator;
import org.wicketstuff.select2.Select2MultiChoice;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostViewItem;
import com.erranda.prototype.web.component.AutogrowBehavior;
import com.erranda.prototype.web.component.DisablingAjaxButton;
import com.erranda.prototype.web.component.ImageUtils;
import com.erranda.prototype.web.component.JqueryActions;
import com.fasterxml.jackson.databind.cfg.ConfigFeature;

public class PostErrandForm extends Panel {

	@SpringBean
	Controller controller;

	private int cursor;

	List<PlatformUser> nodes = new LinkedList<PlatformUser>();
	
	PlatformUser session = UserSession.get().getPerson();  
	
	Long targetId = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("rawtypes")
	public PostErrandForm(String id, final ListView<PostViewItem> newsFeedList,
			final ClientProperties cp, final String listViewMarkupId, final Long targetId) throws ControllerException {
		super(id);
		this.targetId = targetId;
		PlatformUser person = UserSession.get().getPerson();
		PlatformUser targetPage = null;
		if (targetId != null) {
			targetPage = controller.findPerson(targetId);
			Boolean isFriends = controller.areWeFriends(session.getId(), targetId);
			Boolean isOwner = UserSession.get().getPerson().getId().equals(targetId);
			if (!isFriends && !isOwner && !targetPage.getType().equals(Config.PLATFORM_TYPE_PAGE))
				setVisible(false);
				
		}
		
		final Errand errand = new Errand();
		errand.setPrivacy(person.getPreferredPrivacyOption());
		errand.setErrandLocation(person.getLocalityName());
		errand.setOwner(UserSession.get().getPerson());
		
		final Form<String> errandForm = new Form<String>("errand");
		TextField<String> errandLocation = new TextField<String>(
				"errand-location", new PropertyModel<String>(errand,
						"errandLocation"));
		errandForm.add(errandLocation);

		TextArea<String> details = new TextArea<String>("details",
				new PropertyModel<String>(errand, "details"));
		details.add(new AutogrowBehavior());
		errandForm.add(details);
		if (targetPage != null)
			details.add(AttributeModifier.replace("placeholder", "Nominate with @" + targetPage.getUsername()));

		TextField<String> errandLink = new TextField<String>("errand-link",
				new PropertyModel<String>(errand, "link"));
		errandForm.add(errandLink);

		TextField<String> pictureUrl = new TextField<String>("pictureUrl",
				new PropertyModel<String>(errand, "pictureUrl"));
		errandForm.add(pictureUrl);
		UrlValidator u = new UrlValidator();
		pictureUrl.add(u);

		CheckBox anonymous = new CheckBox("anonymous",
				new PropertyModel<Boolean>(errand, "anonymous"));
		errandForm.add(anonymous);

		final Select2MultiChoice<PlatformUser> nodes = new Select2MultiChoice<PlatformUser>(
				"tags", new PropertyModel<Collection<PlatformUser>>(this, "nodes"),
				new PersonProvider(controller,UserSession.get().getPerson().getId()));
		errandForm.add(nodes);
		nodes.add(new Behavior() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$('#s2id_" + nodes.getMarkupId()
						+ "').css('display', 'none');";
				response.render(OnDomReadyHeaderItem.forScript(script));
				String script2 = String
						.format("$('#%s').change(function(){if ($('#%s .select2-search-choice').length > 0) {$('#tag-friend-button').css('color', '#0084B4')} else {$('#tag-friend-button').css('color', '#999')} ;});",
								nodes.getMarkupId(),
								"s2id_" + nodes.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script2));
			}

		});

		final FileUploadField picture = new FileUploadField("photo");
		errandForm.add(picture);

		errandForm.add(new DisablingAjaxButton("submit", errandForm) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				errand.setId(null);
				if (errand.getDetails() == null) {
					Authenticated page = (Authenticated) getPage();
					ErrorModalContainer feedback = page.errorModalContainer;
					feedback.show(
							new Label("modal-header", Model.of("Errand is empty")),
							new Label("modal-content", Model
									.of("Your errand seems to be empty.")));
					feedback.update(target, "", true);
					return;
				} else if (!session.getVerified() && Config.WICKET_DEPLOYMENT.equals("deployment")) {
					Authenticated page = (Authenticated) getPage();
					ErrorModalContainer feedback = page.errorModalContainer;
					feedback.show(
							new Label("modal-header", Model.of("Security Check")),
							new ErrorEmailNotVerified("modal-content", page.introductionContainer));
					feedback.update(target, "", true);
					return;
				}
					

				try {
					PlatformUser person = UserSession.get().getPerson();
					
					FileUpload upload = picture.getFileUpload();
					
					if (upload != null) {
						try {
							String fileName = UUID.randomUUID()
									+ "-"
									+ upload.getClientFileName().substring(
											upload.getClientFileName()
													.lastIndexOf("."));
							File file = new File(IMAGES + "/" + fileName);
							upload.writeTo(file);
							errand.setImageWidth(ImageUtils.resizeImg(file, 600));
							errand.setImageHeight(ImageUtils.imageHeight(file));
							errand.setPictureUrl("/wicket/resource/org.apache.wicket.Application/images?id="
									+ fileName);
							errand.setIsExternalUrl(false);
							
						} catch (IOException e) {
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if (errand.getPictureUrl() != null) {
						errand.setIsExternalUrl(true);
					}
					errand.setOwner(person);
					errand.setAdded(new Date());
					if (targetId != null)
						PostErrandForm.this.nodes.add(controller.findPerson(targetId));
					errand.setId(controller.postErrand(errand, person.getId(), PostErrandForm.this.nodes)
							.getId());
					target.appendJavaScript("$('#" + nodes.getMarkupId()
							+ "').select2('val', '');");
					target.appendJavaScript("resetErrandForm()");
					cursor--;
					ListItem<PostViewItem> item = new ListItem<PostViewItem>(new Integer(
							cursor).toString(), cursor);
					
					final Long pid = errand.getId();
					
					LoadableDetachableModel<Errand> model = new LoadableDetachableModel<Errand> (){

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						protected Errand load() {
							try {
								Errand errand = controller.findErrand(pid);
								return errand;
							} catch (ControllerException e) {
								return null;
							}
						}
						
					};
					if (targetId != null) {
						item.add(new PostNominatePersonMeta ("meta", model, targetId));
					} else {
						Post post = new Post(session, errand);
						item.add(new PostOwnerNominateMeta ("meta", Model.of(post)));
					}
					ErrandSingle single = new ErrandSingle("this-post", model, (Authenticated) getPage(), cp, null, item);
					item.add(single);
					item.setOutputMarkupId(true);
					newsFeedList.add(item);
					target.prependJavaScript(String
							.format("var item=document.createElement('%s');item.style.display = 'none'; item.class='original-tweet-container'; item.id='%s';"
									+ "$('#%s').prepend(item);", "li",
									item.getMarkupId(), listViewMarkupId));
					JqueryActions.fadeIn(item, target);
					JqueryActions.highlight(item, target);
					target.add(item);
					UserSession.get().getPerson()
							.setPreferredPrivacyOption(errand.getPrivacy());
					if (errand.getPrivacy().equals("PRIVATE") && person.getFirstPrivatePost()) {
						person.setFirstPrivatePost(false);
						Authenticated page = (Authenticated) getPage();
						ErrorModalContainer feedback = page.errorModalContainer;
						feedback.show(
								new Label("modal-header", Model.of("About posts shared privately")),
								new Label("modal-content", Model
										.of("This post is visible only to you and those who you choose to nominate using @Username.")));
						feedback.update(target, "", true);
						return;
					} else if (errand.getPrivacy().equals("PUBLIC") && person.getFirstLocalPost()) {
						person.setFirstLocalPost(false);
						Authenticated page = (Authenticated) getPage();
						ErrorModalContainer feedback = page.errorModalContainer;
						feedback.show(
								new Label("modal-header", Model.of("About posts shared with public")),
								new Label("modal-content", Model
										.of("Your public posts will be visible to followers, friends and may appear in search results.")));
						feedback.update(target, "", true);
						return;
					} else if (errand.getPrivacy().equals("FRIENDS") && person.getFirstFriendPost()) {
						person.setFirstFriendPost(false);
						Authenticated page = (Authenticated) getPage();
						ErrorModalContainer feedback = page.errorModalContainer;
						feedback.show(
								new Label("modal-header", Model.of("About posts shared with friends")),
								new Label("modal-content", Model
										.of("This post will appear on the news feed of friends. Friends of nominated friends with @Username will also see this post.")));
						feedback.update(target, "", true);
						return;
					}

					errand.setDetails(null);
				} catch (ControllerException e1) {
					error(e1.getMessage());
					e1.printStackTrace();
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				Authenticated page = (Authenticated) getPage();
				ErrorModalContainer feedback = page.errorModalContainer;
				feedback.show(
						new Label("modal-header", Model.of("Error")),
						new Label("modal-content", Model
								.of("There was an error with your request")));
				feedback.update(target, "", true);
			}
		});

		WebMarkupContainer iconImage = new WebMarkupContainer("icon-image");
		iconImage.add(AttributeModifier.replace("src",
				new LoadableDetachableModel<Object>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = -1446646754517223593L;

					@Override
					protected Object load() {
						try {
							return controller.findPerson(UserSession.get().getPerson().getId())
									.getThumbNail();
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return "";
						}
					}

				}));
		errandForm.add(iconImage);
		final TextField<String> privacy = new TextField<String>("privacy",
				new PropertyModel<String>(errand, "privacy"));
		errandForm.add(privacy);
		privacy.setOutputMarkupId(true);

		Label selectedPrivacy = new Label("selected-privacy");

		if (UserSession.get().getPerson().getPreferredPrivacyOption()
				.equals("FRIENDS")) {
			selectedPrivacy.setDefaultModel(new LoadableDetachableModel() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				protected Object load() {
					return PrivacyFriends.htmlData();
				}

			});
		} else if (UserSession.get().getPerson().getPreferredPrivacyOption()
				.equals("LOCAL")) {
			selectedPrivacy.setDefaultModel(new LoadableDetachableModel() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				protected Object load() {
					// TODO Auto-generated method stub
					return PrivacyLocal.htmlData();
				}

			});
		} else if (UserSession.get().getPerson().getPreferredPrivacyOption()
				.equals("PRIVATE")) {
			selectedPrivacy.setDefaultModel(new LoadableDetachableModel() {

				private static final long serialVersionUID = -5683885231919248961L;

				@Override
				protected Object load() {
					return PrivacyPrivate.htmlData();
				}

			});
		} else if (UserSession.get().getPerson().getPreferredPrivacyOption()
				.equals("PUBLIC")) {
			selectedPrivacy.setDefaultModel(new LoadableDetachableModel() {

				private static final long serialVersionUID = -5683885231919248961L;

				@Override
				protected Object load() {
					return PrivacyPublic.htmlData();
				}

			});
		}
		errandForm.add(selectedPrivacy.setEscapeModelStrings(false));

		ListView<String> privacyOptions = new ListView<String>(
				"privacy-options", new LoadableDetachableModel<List<String>>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<String> load() {
						try {
							Map<String, String> privacy = controller
									.getPrivacySettings(UserSession.get()
											.getPerson());
							List<String> keys = new ArrayList<String>(privacy
									.keySet());
							return keys;
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						return null;
					}

				}) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<String> item) {
				String object = item.getModelObject();
				String inputField = privacy.getMarkupId();
				String buttonId = "selected-privacy";
				if (object.equals("Public")) {
					item.add(new PrivacyPublic("this", item, buttonId,
							inputField));
				} else if (object.equals(UserSession.get().getPerson()
						.getLocalityName())) {
					item.add(new PrivacyLocal("this", item, buttonId,
							inputField));
				} else if (object.equals("Friends")) {
					item.add(new PrivacyFriends("this", item, buttonId,
							inputField));
				} else if (object.equals("Private")) {
					item.add(new PrivacyPrivate("this", item, buttonId,
							inputField));
				}
			}

		};
		errandForm.add(privacyOptions);
		add(errandForm);
	}

	public PostErrandForm(String id, IModel<?> model) {
		super(id, model);
	}

}
