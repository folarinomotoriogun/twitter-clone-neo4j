package com.erranda.prototype.web;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class ModalSignUp extends Panel {

	public ModalSignUp(String id) {
		super(id);
		add(new SignUpForm("sign-up"));
	}

	public ModalSignUp(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
