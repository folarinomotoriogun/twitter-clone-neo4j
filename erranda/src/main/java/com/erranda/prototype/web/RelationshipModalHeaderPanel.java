package com.erranda.prototype.web;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class RelationshipModalHeaderPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4469892228838616215L;

	public RelationshipModalHeaderPanel(String id) {
		super(id);
		add(new Label("modal-header"));
	}

	public RelationshipModalHeaderPanel(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
