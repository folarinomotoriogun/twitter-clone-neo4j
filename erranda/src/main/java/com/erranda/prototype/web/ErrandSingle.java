package com.erranda.prototype.web;

import java.util.List;

import javax.mail.MessagingException;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.ClientProperties;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.Strings;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.Config;
import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostViewItem;
import com.erranda.prototype.web.component.ModalAjaxLink;
import com.erranda.prototype.web.component.TimeConverter;
import com.erranda.prototype.web.mailer.EmailNewAction;

public class ErrandSingle extends Panel {

	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	ErrandActionContainer actionContainer;

	CommentErrand commentErrand;

	Label commentErrandLabel;
	
	Label pinErrandLabel;

	TimeConverter tc;

	PlatformUser session = UserSession.get().getPerson();
	
	Label kudosLabel;
	
	AjaxLink<?> minusKudosLink = null;
	
	Long pid = null;
	
	

	public ErrandSingle(String id, final IModel<Errand> iModel, Authenticated page,
			final ClientProperties cp, WebMarkupContainer meta, final ListItem<PostViewItem> item) {
		super(id);
		setOutputMarkupId(true);
		setOutputMarkupPlaceholderTag(true);
		Errand post = iModel.getObject();
		
		pid = post.getId();
		try {
			Label time = new Label("time",
							new LoadableDetachableModel<String>() {

								/**
						 * 
						 */
								private static final long serialVersionUID = 1L;

								@Override
								protected String load() {
									String[] time = TimeConverter
											.convertTime(iModel.getObject().getAdded(),
													cp).split("#");
									String timeShort = "<a style=\"float:right\" title = '"
											+ time[1]
											+ "'"
											+ " class=\"tweet-timestamp js-permalink js-nav\"><span class=\"_timestamp js-short-timestamp js-relative-timestamp\">"
											+ TimeConverter.timeAgo(iModel.getObject()
													.getAdded())
											+ "</span></a>";
									return timeShort;

								}

							});

					time.setEscapeModelStrings(false);

					Label userdetails = new Label("name",
							new LoadableDetachableModel<String>() {

								/**
						 * 
						 */
								private static final long serialVersionUID = 1L;

								@Override
								protected String load() {
									try {
										return anonymizePerson(iModel.getObject());
									} catch (ControllerException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
										return "";
									}

								}

							});
					userdetails.setEscapeModelStrings(false);

					final Label errandDetails = new Label("errand-details",
							new LoadableDetachableModel<String>() {

								/**
						 * 
						 */
								private static final long serialVersionUID = 1L;

								@Override
								protected String load() {
									Errand post = iModel.getObject();
									String errandDetails = "<div class=\"errand-details-container\">"
											+ "<span class=\"line\"><p class=\"errand-details\">"
											+ parse(post)
											+ "</p></span>" + "</div>";
									if (post.getLink() != null)
										errandDetails = errandDetails
												+ "<div style=\"clear:both; -ms-word-break: break-all;\"><a href=\""
												+ post.getLink()
												+ "\" class=\"twitter-timeline-link\"><b>"
												+ post.getLink()
												+ "</b></a></div>";
									return errandDetails;

								}

							});
					errandDetails.setEscapeModelStrings(false);
					errandDetails.setOutputMarkupId(true);

					Label errandImage = new Label("image",
							new LoadableDetachableModel<Object>() {

								/**
						 * 
						 */
								private static final long serialVersionUID = 1L;

								@Override
								protected Object load() {
									Errand post = iModel.getObject();
									String str = "";
									str = "<a target=\"_blank\" href=\""
											+ (post.getLink() != null ? post
													.getLink()
													: Config.ERRAND_NAME_LINK
															+ "/"
															+ post.getId())
											+ "\" class=\"media media-thumbnail twitter-timeline-link is-preview \"><div class=\" is-preview\"><img src=\""
											+ post.getPictureUrl()
											+ "\" class=\"js-media-img-placeholder lazy-load\" /></div></a>";
									return str;
								}

							});
					errandImage.setEscapeModelStrings(false);
					if (post.getPictureUrl() == null) {
						errandImage.setDefaultModel(Model.of(""));
						errandImage.setVisible(false);
					}

				
					if (commentErrandLabel == null) {
						commentErrandLabel = new Label("label",
								new LoadableDetachableModel<Object>() {

									private static final long serialVersionUID = 1L;

									@Override
									protected Object load() {
										Integer count;
										try {
											count = controller
													.commentCountByErrand(iModel.getObject().getId());
											return "Comment"
													+ (count == 0 ? "" : " ("
															+ count + ")");
										} catch (ControllerException e) {
											e.printStackTrace();
											throw new RestartResponseAtInterceptPageException(
													Home.class);
										}

									}

								});
					}

					IndicatingAjaxLink<Void> commentLink = new IndicatingAjaxLink<Void>(
							"comment-link") {
						private static final long serialVersionUID = 1L;

						@Override
						public void onClick(AjaxRequestTarget target) {
							if (UserSession.get().userNotLoggedIn()) {
								Authenticated page = (Authenticated) getPage ();
								ErrorModalContainer relationships = page.errorModalContainer;
								relationships.show(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">Want to Comment on this?</h3>")).setEscapeModelStrings(false), new ModalSignUp("modal-content"));
								relationships.update(target, "", true);
								return;
							}
							if (commentErrand == null) {
								try {
									commentErrand = new CommentErrand(
											"content", iModel.getObject().getId(), commentErrandLabel,
											cp);
								} catch (ControllerException e) {
									e.printStackTrace();
									throw new RuntimeException(e.getMessage());
								}
							}
							actionContainer.replaceWindow(commentErrand);
							actionContainer.update(target, "");
						}
					};
					commentLink.add(commentErrandLabel.setOutputMarkupId(true));

					pinErrandLabel = new Label("label",
							new LoadableDetachableModel<Object>() {

								/**
						 * 
						 */
								private static final long serialVersionUID = 1L;

								@Override
								protected Object load() {
									Integer count = controller.recommendCount(iModel.getObject().getId());
									return "Broadcast"
											+ (count == 0 ? "" : String.format(
													" (%d)", count));
								}

							});
					pinErrandLabel.setOutputMarkupId(true);

					final AjaxLink<Void> pinLink = new AjaxLink<Void>(
							"pin-link") {
						private static final long serialVersionUID = 1L;

						@Override
						public void onClick(AjaxRequestTarget target) {
							try {
								controller.recommendPost(session.getId(), iModel.getObject().getId(), "");
								sendPushNotificationBroadcast(pid);
								Authenticated page = (Authenticated) getPage();
								ErrorModalContainer feedback = page.errorModalContainer;
								feedback.show(
										new Label("modal-header", Model
												.of("First broadcast")),
										new Label(
												"modal-content",
												Model.of("Your friends list will see this post in their feed")));
								if (session.getFirstBroadcast()) {
									feedback.update(target, "", true);
									session.setFirstBroadcast(false);
								}
								Errand errand = iModel.getObject();
								PlatformUser owner = controller.findPerson(errand.getOwnerId());
								String html = new String(
										ComponentRenderer
												.renderPage(
														new PageProvider(
																new EmailNewAction(
																		session,
																		session.getName() + " @" + session.getUsername()
																				+ " broadcasted your post",
																		errand.getErrandLink())))
												.toString());
								MailMan man = new MailMan();
								if (!session.getId()
										.equals(errand.getOwnerId()))
									man.sendMessage(session.getName() + " @" + session.getUsername()
											+ " broadcasted your post", owner.getEmail(), html);
								return;
							} catch (ControllerException e) {
								Authenticated page = (Authenticated) getPage();
								ErrorModalContainer feedback = page.errorModalContainer;
								feedback.show(
										new Label("modal-header", Model
												.of("Something went wrong")),
										new Label(
												"modal-content",
												Model.of(e.getMessage())));
								feedback.update(target, "", true);
								e.printStackTrace();
							} catch (MessagingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					};
					pinLink.add(pinErrandLabel.setOutputMarkupId(true));
					if (post.getPrivacy().equals("PRIVATE") || UserSession.get().userNotLoggedIn())
						pinLink.setVisible(false);
						
					if (commentErrand == null)
						
						commentErrand = new CommentErrand("content", iModel.getObject().getId(),
								commentErrandLabel, cp);
					actionContainer = new ErrandActionContainer(
							"errand-action-container", new WebMarkupContainer("content"));
					WebMarkupContainer more = new ErrandPostActions(
							"more-actions", post.getOwnerId(), post.getId(),
							iModel, item);
					
					final AjaxLink<?> kudosLink = new AjaxLink<Object> ("kudos-link") {

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public void onClick(AjaxRequestTarget target) {
							if (UserSession.get().userNotLoggedIn()) {
								Authenticated page = (Authenticated) getPage ();
								ErrorModalContainer relationships = page.errorModalContainer;
								relationships.show(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">Want to give +1 Kudos?</h3>")).setEscapeModelStrings(false).setOutputMarkupId(true), new ModalSignUp("modal-content").setOutputMarkupId(true));
								relationships.update(target, "", true);
							} else {
								try {
									controller.kudos(pid, session.getId());
									sendPushNotificationKudos (pid);
								} catch (ControllerException e) {
									String script = String
											.format("$('#%s').hide();$('#%s').fadeIn();",
													minusKudosLink.getMarkupId(),this.getMarkupId());
									target.appendJavaScript(script);
									Authenticated page = (Authenticated) getPage();
									ErrorModalContainer feedback = page.errorModalContainer;
									feedback.show(
											new Label("modal-header", Model
													.of("Something went wrong")),
											new Label(
													"modal-content",
													Model.of(e.getMessage())));
									feedback.update(target, "", true);
									e.printStackTrace();
								}
							}
						}
					};
					
					minusKudosLink = new AjaxLink<Object> ("minus-kudos-link") {

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public void onClick(AjaxRequestTarget target) {
							try {
								controller.minusKudos(pid, session.getId());
							} catch (ControllerException e) {
								String script = String
										.format(" $('#%s').hide();$('#%s').fadeIn();",
												kudosLink.getMarkupId(), this.getMarkupId());
								target.appendJavaScript(script);
							}
						}
					};
					
					
					
					
					
					final ModalAjaxLink<?> peopleThatGaveKudos = new ModalAjaxLink<Object> ("people-that-gave-kudos") {

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public void onClick(AjaxRequestTarget target) {
							if (UserSession.get().userNotLoggedIn()) {
								Authenticated page = (Authenticated) getPage ();
								ModalContainer relationships = page.modalContainer;
								relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">Want to see who gave +1 Kudos?</h3>")).setEscapeModelStrings(false).setOutputMarkupId(true), new ModalSignUp("modal-content").setOutputMarkupId(true));
								relationships.update(target, "", true);
								return;
							}
							Authenticated page = (Authenticated) getPage ();
							ModalContainer relationships = page.modalContainer;
							relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">People who gave this Kudos</h3>")).setEscapeModelStrings(false), new KudosByPeople ("modal-content", pid));
							relationships.update(target, "", true);
						}
						
					};
					Integer kudosCount = controller.kudosCount(pid);
					kudosLabel = new Label ("label", Model.of(kudosCount));
					kudosLabel.setOutputMarkupId(true);
					kudosLabel.add(AttributeModifier.replace("data-kudos-count", kudosCount));
					
					if (controller.isKudos(pid, session.getId()))
						kudosLink.add(AttributeModifier.replace("style", "display:none"));
					else
						minusKudosLink.add(AttributeModifier.replace("style", "display:none"));
					if (kudosCount == 0) {
						peopleThatGaveKudos.add(AttributeModifier.replace("style", "display:none"));
					}
					
					if(UserSession.get().userLoggedIn()) {
						add(new Behavior() {
							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							@Override
							public void renderHead(Component component, IHeaderResponse response) {
								String script = String
										.format("$('#%s').click(function () {$(this).hide(); $('#%s').fadeIn(); var kudosLabel = '%s'; var count = parseInt ($('#' + kudosLabel).attr('data-kudos-count')); count = count - 1; $('#' + kudosLabel).html (count); $('#' + kudosLabel).attr('data-kudos-count', count); if (count == 0) { $('#%s').hide()};});  ",
												minusKudosLink.getMarkupId(), kudosLink.getMarkupId(), kudosLabel.getMarkupId(), peopleThatGaveKudos.getMarkupId());
								String script2 = String
										.format("$('#%s').click(function () {$(this).hide(); $('#%s').fadeIn(); var kudosLabel = '%s'; var count = parseInt ($('#' + kudosLabel).attr('data-kudos-count')); count = count + 1; $('#' + kudosLabel).html (count); $('#' + kudosLabel).attr('data-kudos-count', count); if (count > 0) { $('#%s').fadeIn()};});  ",
												kudosLink.getMarkupId(), minusKudosLink.getMarkupId(), kudosLabel.getMarkupId(), peopleThatGaveKudos.getMarkupId());
								String script3 = String.format("$('#%s').click(function () {$('#%s').hide(); $('#%s').html('Broadcasted'); $('#%s').fadeIn()});", pinLink.getMarkupId(), pinErrandLabel.getMarkupId(),pinErrandLabel.getMarkupId(),pinErrandLabel.getMarkupId());
								response.render(OnDomReadyHeaderItem.forScript(script));
								response.render(OnDomReadyHeaderItem.forScript(script2));
								response.render(OnDomReadyHeaderItem.forScript(script3));
							}
						});
					} else {
						
					}
					add(peopleThatGaveKudos.add(kudosLabel), kudosLink, minusKudosLink, more, actionContainer, userdetails, time,
							errandDetails, errandImage, 
							commentLink, pinLink);
		} catch (ControllerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new RuntimeException(e1.getMessage());
		}

	}

	private String anonymizePerson(Errand post) throws ControllerException {
		post.setOwner(controller.findPerson(post.getOwnerId()));
		String userUrl = (post.getAnonymous() ? "" : "href='"
				+ Config.PROFILE_NAME_LINK + "/"
				+ post.getOwner().getUsername() + "'");
		String name = (post.getAnonymous() ? "Anonymous" : post.getOwner()
				.getName());
		String username = (post.getAnonymous() ? "Hidden" : post.getOwner()
				.getUsername());
		PlatformUser person = new PlatformUser();
		String src = (post.getAnonymous() ? person.getThumbNail()
				: post.getOwner().getThumbNail());

		String userdetails = "<a class=\"account-group js-account-group js-action-profile js-user-profile-link js-nav\""
				+ userUrl
				+ "><img class=\"avatar js-action-profile-avatar\" src=\""
				+ src
				+ "\">"
				+ "<strong class=\"fullname js-action-profile-name\">"
				+ name
				+ "</strong>"
				+ "<span class='js-username username'><s>@"
				+ username
				+ "</s></a><b></span>";
		return userdetails;
	}
	
	private String parse (Errand errand) {
		if (errand == null || errand.getDetails() == null)
			return "";
		String details = errand.getDetails();
		
		String[] words = details.split("\\r?\\n?\\s");
		
		for (String word: words) {
			if (word.indexOf("#") == 0 && word.lastIndexOf("#") == 0 && word.length() > 1) {
				details = details.replace(word, String.format("<a href='%s'>%s</a>", "/posts/search/" + word.split("#")[1], word));
			}
			else if (word.indexOf("@") == 0) {
				String username = word.split("@")[1];
				PlatformUser person = controller.findUsername(username.toLowerCase());
				if (person != null)
					details = details.replace("@" + username, String.format("<a href='%s'>@%s</a>", person.getLink(), username));
			}
		}
		
		if (details.length() > 350) {
			String yeah = details.substring(0, 350);
			yeah = yeah.substring(0, yeah.lastIndexOf(" ")) + String.format("...<a href='%s'>See more</a>", errand.getErrandLink());
			details = yeah;
		} 
		

		return 	details.replaceAll("(\r\n|\n)", "<br />");
	}
	
	private void sendPushNotificationKudos (Long eid) throws ControllerException {
		List<Long> people = controller.kudosNotificationList(session.getId(), eid);
		for (Long person : people) {
			IPushNode node = ((App) App.get()).getNotificationPanel(person);
			if (node != null)
				TimerPushService.get().publish(node, "");
		}
	}
	
	private void sendPushNotificationBroadcast (Long eid) throws ControllerException {
		List<Long> people = controller.kudosNotificationList(session.getId(), eid);
		for (Long person : people) {
			IPushNode node = ((App) App.get()).getNotificationPanel(person);
			if (node != null)
				TimerPushService.get().publish(node, "");
		}
	}
	

}
