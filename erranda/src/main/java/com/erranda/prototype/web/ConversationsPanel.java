package com.erranda.prototype.web;

import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.protocol.http.ClientProperties;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.PlatformUser;

public class ConversationsPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;

	int skip = 0;

	int limit = 100;

	Long pid = UserSession.get().getPerson().getId();

	public ConversationsPanel(String id, final ModalContainer modalContainer,
			final ClientProperties cp) {
		super(id);

		ListView<Conversation> conversations = new ListView<Conversation>(
				"conversation-list",
				new LoadableDetachableModel<List<Conversation>>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<Conversation> load() {
						// TODO Auto-generated method stub
						try {
							List<Conversation> c = controller.inbox(pid, skip,
									limit);
							return c;
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}
					}
				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			
			private PlatformUser session = UserSession.get().getPerson();

			@Override
			protected void populateItem(ListItem<Conversation> item) {
				final Conversation thisItem = item.getModelObject();
				final Long cId = thisItem.getId();
				// If I have an unread message mark it.
				if (thisItem.getLastMessage() != null) {
					if (!thisItem.getLastMessage().getIsRead() && !thisItem.getLastMessage().getSid().equals(session.getId()))
						item.add(AttributeModifier.append("class", "is-unread"));
					item.add(new ConversationItem("this", item.getModel(),
							ConversationsPanel.this,
							((Authenticated) getPage())));
					item.add(new AjaxEventBehavior("click") {

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						protected void onEvent(AjaxRequestTarget target) {
							try {
								controller.markAllMessagesAsRead(cId,
										UserSession.get().getPerson().getId());
							} catch (ControllerException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								throw new RuntimeException(e.getMessage());
							}
							MessagesPanel mp = new MessagesPanel(
									"modal-content", thisItem);
							
							modalContainer.replaceWindow(new MessageHeader(
									"modal-header", cId), mp);
							modalContainer.update(target, "", false);
						}

					});
					item.add(AttributeModifier.append("class",
							"modal-ajax-action"));
				} else {
					item.add(new WebMarkupContainer("this"));
				}

			}

		};
		add(conversations);
	}
}
