package com.erranda.prototype.web.component;

import java.util.Date;
import java.util.TimeZone;

import org.apache.wicket.protocol.http.ClientProperties;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.github.kevinsawicki.timeago.TimeAgo;

public class TimeConverter {

	public static String convertTime(Date time, ClientProperties cp) {
		if (cp != null) {
			/*TimeZone client = cp.getTimeZone();
			DateTimeZone toTimeZone = DateTimeZone.forTimeZone(client);
			DateTime dateTime2 = new DateTime(time, toTimeZone);
			DateTimeFormatter outputFormatterLong = DateTimeFormat.forPattern(
					"EEE, d MMM yyyy - HH:mm:ss").withZone(toTimeZone);*/
			return timeAgo(time) + "#" + " ";//outputFormatterLong.print(dateTime2);
		} else
			return time.toString();
	}

	public static String timeAgo(Date d) {
		TimeAgo time = new TimeAgo();
		String timeago = time.timeAgo(d); // returns "15 minutes ago"
		return timeago;
	}
}
