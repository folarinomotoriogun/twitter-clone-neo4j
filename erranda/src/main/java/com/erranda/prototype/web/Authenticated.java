package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.ClientProperties;
import org.apache.wicket.protocol.http.request.WebClientInfo;
import org.apache.wicket.request.http.WebResponse;
import org.apache.wicket.settings.RequestCycleSettings;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.AbstractPushEventHandler;
import org.wicketstuff.push.IPushEventContext;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.ErrandRepository;
import com.erranda.prototype.domain.repo.PlatformUserRepository;
import com.erranda.prototype.mobile.LoginMobile;
import com.erranda.prototype.web.component.GlobalAjaxLink;
import com.erranda.prototype.web.component.ModalAjaxLink;

public class Authenticated extends WebPage {

	@SpringBean
	ErrandRepository errandRepo;

	@SpringBean
	PlatformUserRepository personRepo;

	@SpringBean
	private Controller controller;

	// Change to private later
	public ModalContainer modalContainer;

	public ErrorModalContainer errorModalContainer;
	
	public Label title;

	public IntroductionContainer introductionContainer;

	public RelationshipModalContainer relationshipContainer;

	public ClientProperties cp;

	public String email;

	public PlatformUser session = null;
	
	WebMarkupContainer nav = new WebMarkupContainer("nav");

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Authenticated() {

		if (UserSession.get().isTemporary()) {
			UserSession.get().bind();
		}
		if (UserSession.get().userLoggedIn()) 
			session = UserSession.get().getPerson();
		else {
			throw new RestartResponseAtInterceptPageException(Login.class);
			// session = controller.getGuest();
			// UserSession.get().login(session);
		}
		title = new Label("title", Model.of("Erranda"));
		add(title);
	}

	public ClientProperties initClientProperties() {
		RequestCycleSettings requestCycleSettings = getApplication()
				.getRequestCycleSettings();
		boolean gatherExtendedBrowserInfo = requestCycleSettings
				.getGatherExtendedBrowserInfo();
		ClientProperties properties = null;
		try {
			requestCycleSettings.setGatherExtendedBrowserInfo(false);
			WebClientInfo clientInfo = (WebClientInfo) getSession()
					.getClientInfo();
			properties = clientInfo.getProperties();
		} finally {
			requestCycleSettings
					.setGatherExtendedBrowserInfo(gatherExtendedBrowserInfo);
		}
		return properties;
	}

	public ClientProperties getClientProperties() {
		return cp;
	}

	@Override
	public void configureResponse(WebResponse response) {
		super.configureResponse(response);/*
										 * response.setHeader("Cache-Control",
										 * "no-cache, max-age=0,must-revalidate, no-store"
										 * );
										 */
	}

	@Override
	public void onInitialize() {
		super.onInitialize();
		modalContainer = new ModalContainer("global-modal-container");
		modalContainer.setOutputMarkupId(true);
		errorModalContainer = new ErrorModalContainer("error-modal-container");
		errorModalContainer.setOutputMarkupId(true);
		add(errorModalContainer);
		add(modalContainer);
		session = UserSession.get().getPerson();
		cp = initClientProperties();
		// add(new KeepAliveTextArea("heart-beat"));
		introductionContainer = new IntroductionContainer("intro");
		if (session.getShowWelcome()
				&& session.getType().equals(Config.PLATFORM_TYPE_PERSON)) {
			introductionContainer.add(new Behavior() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void renderHead(Component component,
						IHeaderResponse response) {
					String script = "$('#show-introduction-modal-link').click();";
					response.render(OnDomReadyHeaderItem.forScript(script));
				}
			});
		} else {
			introductionContainer.setVisible(false);
		}
		add(introductionContainer);
		if (!session.getEmail().equals("public@public.com")) {
			nav = new NavAuthenticated("nav", this);
		} else {
			nav = new NavPublic("nav");
		}
		add(nav);
	}

	public void p(Object o) {
		System.out.println(o);
	}
}
