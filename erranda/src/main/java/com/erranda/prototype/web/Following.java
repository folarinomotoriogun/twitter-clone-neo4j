package com.erranda.prototype.web;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.GridView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.CustomPagingNavigator;


public class Following extends Panel {

	@SpringBean
	Controller controller;
	
	private Integer size;
	
	PlatformUser session = UserSession.get().getPerson();

	/**
	 * 
	 */
	private static final long serialVersionUID = -12393148652085950L;

	public Following(String id, final IModel<PlatformUser> model) {
		super(id, model);
		PlatformUser person = model.getObject();
		try {
			size = controller.countFollowing(session.getId(), person.getId());
		} catch (ControllerException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		Label none = new Label("none", new LoadableDetachableModel<String>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			

			@Override
			protected String load() {
				if (size == 0)
					return model.getObject().getName()
							+ " has not followed anyone yet";
				return "";
			}
		});
		add(none);
		
		WebMarkupContainer listContainer = new WebMarkupContainer ("list-container");
		listContainer.setOutputMarkupId(true);
		
		GridView<PlatformUser> view = new GridView<PlatformUser>("view",
				new FollowingProvider(person.getId(), controller)) {

			/**
			 * 
			 */
			private static final long serialVersionUID = -6083515869767395482L;

			@Override
			protected void populateEmptyItem(Item<PlatformUser> item) {
				item.add(new Label("friend-card"));
			}

			@Override
			protected void populateItem(Item<PlatformUser> item) {
				try {
					item.add(new MiniContactCard("friend-card", item.getModel()));
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		view.setColumns(2);
		view.setRows(5);
		CustomPagingNavigator nav = new CustomPagingNavigator("navigator", view);
		if (size == 0 || size < 10)
			nav.setVisible(false);
		view.setOutputMarkupId(true);
		add(listContainer.add(view, nav));
	}

}
