package com.erranda.prototype.web;

import java.util.Arrays;
import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.FriendRequest;
import com.erranda.prototype.web.component.TimeConverter;

public class FriendRequestItem extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2118544312808255200L;

	@SpringBean
	Controller controller;

	public FriendRequestItem(String id, FriendRequest fr,
			final String itemMarkup) {
		super(id);
		this.setOutputMarkupId(true);
		
		final Long rid = fr.getSender().getId();
		
		final Label feedback = new Label ("feedback");
		add (feedback.setOutputMarkupId(true));
		
		final Long frid = fr.getId();
		Label details = new Label("details",
				new LoadableDetachableModel<String>() {

					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						FriendRequest object;
						try {
							object = controller.findFriendRequest(frid);
							StringBuffer details = new StringBuffer("");
							details.append("<div class=\"DMInboxItem-timestamp\"><b><small class=\"time\"><span class=\"_timestamp\">");
							String[] time = TimeConverter.convertTime(
									object.getCtime(),
									((Authenticated) getPage()).cp).split("#");
							String timeShort = "<a style=\"float:right\" title = '"
									+ time[1]
									+ "'"
									+ " href=\"#\" class=\"tweet-timestamp js-permalink js-nav js-tooltip\"><span class=\"_timestamp js-short-timestamp js-relative-timestamp\">"
									+ time[0] + "</span></a>";
							details.append(timeShort);
							details.append("</span></small></b></div><div class=\"DMInboxItem-title\"><strong class=\"fullname js-action-profile-name\">");
							String userdetails = "<a class=\"account-group js-account-group js-action-profile js-user-profile-link js-nav\" href=\""
									+ object.getSender().getLink()
									+ "\">"
									+ "<strong class=\"fullname js-action-profile-name\">"
									+ object.getSender().getName()
									+ "</strong></a>";
							details.append(userdetails);
							details.append("</strong><span><div class=\"DMInboxItem-avatar\"><img class=\"avatar js-action-profile-avatar\" src=\"");
							details.append(object.getSender()
									.getThumbNail());
							details.append("\"></div></div>");
							return details.toString();
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						return "";
					}

				});
		final WebMarkupContainer buttons = new WebMarkupContainer ("buttons");
		buttons.setOutputMarkupId(true);
		
		final IndicatingAjaxLink<String> acceptRequest = new IndicatingAjaxLink<String>(
				"accept-request") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.acceptFriendRequest(frid);
					sendPushNotificationMessage(rid);
					feedback.setDefaultModel(Model.of("Request confirmed"));
					target.add(feedback);
					String script = "$('#" + buttons.getMarkupId() + "').fadeOut();";
					String script2 = "$('#" + feedback.getMarkupId() + "').fadeIn();";
					target.appendJavaScript(script);
					target.appendJavaScript(script2);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};

		final IndicatingAjaxLink<String> rejectRequest = new IndicatingAjaxLink<String>(
				"reject-request") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.rejectFriendRequest(frid);
					feedback.setDefaultModel(Model.of("Request deleted"));
					target.add(feedback);
					String script = "$('#" + buttons.getMarkupId() + "').fadeOut();";
					String script2 = "$('#" + feedback.getMarkupId() + "').fadeIn();";
					target.appendJavaScript(script);
					target.appendJavaScript(script2);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		
		add(details.setEscapeModelStrings(false), buttons.add(acceptRequest, rejectRequest));
	}
	
	private void sendPushNotificationMessage (Long rid) throws ControllerException {
		List<Long> people = Arrays.asList(rid);
		for (Long person : people) {
			IPushNode node = ((App) App.get()).getNotificationPanel(person);
			if (node != null)
				TimerPushService.get().publish(node, "");
		}
	}

	public FriendRequestItem(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
