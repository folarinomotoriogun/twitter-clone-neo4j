package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.Panel;

public class UserActionContainer extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3624180352885273032L;

	public UserActionContainer(String id) {
		super(id);
		this.setOutputMarkupId(true);
	}

	public void update(AjaxRequestTarget target, String script) {
		target.add(this);
		target.appendJavaScript(script);
	}

	public void replaceWindow(Component bodyC) {
		this.removeAll();
		add(bodyC);
	}

}
