/**
 * 
 */
package com.erranda.prototype.web;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * @author Folarin
 *
 */
public class SearchForm extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1478104892036835075L;

	/**
	 * @param id
	 */
	public SearchForm(String id) {
		super(id);

	}

	/**
	 * @param id
	 * @param model
	 */
	public SearchForm(String id, IModel<?> model) {
		super(id, model);

	}

}
