package com.erranda.prototype.web;

import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Proposal;

public class Proposals extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;

	public Proposals(String id, final Long eid) {
		super(id);
		ListView<Proposal> proposals = new ListView<Proposal>("proposals",
				new LoadableDetachableModel<List<Proposal>>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<Proposal> load() {
						try {
							return controller.getProposalByErrand(eid, 0, 100);
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}
					}

				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Proposal> item) {
				try {
					item.add(new ProposalSingle("item", item.getModelObject()
							.getId()));
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
			}
		};

		Label proposalCount = new Label("proposal-count",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						try {
							Integer count = controller
									.proposeCountByErrand(eid);
							if (count == 0 || count > 1)
								return count + " proposals";
							else if (count == 1)
								return count + " proposal";
							return "Proposals";
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}
					}

				});
		add(proposals, proposalCount);

	}
}
