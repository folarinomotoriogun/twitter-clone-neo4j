package com.erranda.prototype.web;

import static com.erranda.prototype.Config.IMAGES;

import java.io.IOException;
import java.util.UUID;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.form.upload.UploadProgressBar;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.file.File;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.DisablingIndicatingAjaxButton;
import com.erranda.prototype.web.component.ImageUtils;

public class ProfilePhotoUploader extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer x;

	private Integer y;

	private Integer w;

	private Integer h;

	private Boolean current = true;

	@SpringBean
	Controller controller;

	public ProfilePhotoUploader(String id) {
		super(id);
		Form<Void> form = new Form<Void>("upload-form");

		HiddenField<Integer> xFld = new HiddenField<Integer>("x",
				new PropertyModel<Integer>(this, "x"));
		HiddenField<Integer> yFld = new HiddenField<Integer>("y",
				new PropertyModel<Integer>(this, "y"));
		HiddenField<Integer> wFld = new HiddenField<Integer>("w",
				new PropertyModel<Integer>(this, "w"));
		HiddenField<Integer> hFld = new HiddenField<Integer>("h",
				new PropertyModel<Integer>(this, "h"));
		HiddenField<Boolean> currentFld = new HiddenField<Boolean>(
				"edit-current-photo", new PropertyModel<Boolean>(this,
						"current"));
		
		final FileUploadField photo = new FileUploadField("photoField");
		UploadProgressBar progress = new UploadProgressBar("progress",
				form, photo);
		form.add(progress);
		DisablingIndicatingAjaxButton submit = new DisablingIndicatingAjaxButton(
				"submit", form) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				PlatformUser person = UserSession.get().getPerson();
				try {
					FileUpload upload = photo.getFileUpload();
					String fileNameLocalUrl = "";
					String fileName = "";
					String thumbNail = "";
					String thumbNailLocalUrl = "";
					if (current) {
						fileNameLocalUrl = IMAGES + "/"
								+ person.getProfilePictureUrl().split("=")[1];
						thumbNailLocalUrl = IMAGES + "/"
								+ person.getThumbNail().split("=")[1];
					} else if (upload != null) {
						String rand = UUID.randomUUID()
								+ "-";
						String ext = upload.getClientFileName().substring(
								upload.getClientFileName().lastIndexOf(
										"."));
						fileName = rand + ext;
						fileNameLocalUrl = IMAGES + "/" + fileName;
						
						thumbNail = rand + "thumbNail" + ext;
						thumbNailLocalUrl = IMAGES + "/" + thumbNail;
						
						File file = new File(fileNameLocalUrl);
						upload.writeTo(file);
						
						String url = "/wicket/resource/org.apache.wicket.Application/images?id="
								+ fileName;
						String thumbNailUrl = "/wicket/resource/org.apache.wicket.Application/images?id="
								+ thumbNail;
						UserSession.get().setPerson(controller.updateProfilePicture(person.getId(), url, thumbNailUrl));
					}
					File file = new File(fileNameLocalUrl);
					ImageUtils.cropImage(file, thumbNailLocalUrl, x, y, w, h);
					PageParameters params = new PageParameters();
					params.set("username", person.getUsername());
					setResponsePage(PersonProfile.class, params);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		form.add(xFld.setRequired(true), yFld.setRequired(true),
				wFld.setRequired(true), hFld.setRequired(true), photo,
				currentFld.setRequired(true), submit);

		IndicatingAjaxLink remove = new IndicatingAjaxLink("remove") {

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					PlatformUser person = UserSession.get().getPerson();
					controller.removeProfilePicture(person.getId());
					PageParameters params = new PageParameters();
					params.set("username", person.getUsername());
					throw new RestartResponseAtInterceptPageException(
							PersonProfile.class, params);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		add(form, remove);
	}

	public ProfilePhotoUploader(String id, IModel<?> model) {
		super(id, model);

	}

}
