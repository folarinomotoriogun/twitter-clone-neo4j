package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Notification;
import com.erranda.prototype.domain.PlatformUser;

public class NotificationsList extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public NotificationsList(String id) {
		super(id);

		ListView<Notification> notifications = new ListView<Notification>(
				"notifications",
				new LoadableDetachableModel<List<Notification>>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<Notification> load() {
						PlatformUser person = UserSession.get().getPerson();
						List<Notification> notifications = new ArrayList<Notification>();
						try {
							notifications.addAll(controller.getMyNotifications(
									person.getId(), 0, 10));
						} catch (ControllerException e) {
							// TODO Auto-generated catch block Use global error
							// dialogue
							e.printStackTrace();
						}
						return notifications;
					}

				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Notification> item) {
				Notification n = item.getModel().getObject();
				item.add(new NotificationMessage("details-item", n));
				item.add(AttributeModifier.replace("onclick", "window.open('"
						+ n.getLink() + "')"));
			}

		};
		add(notifications);
	}

}
