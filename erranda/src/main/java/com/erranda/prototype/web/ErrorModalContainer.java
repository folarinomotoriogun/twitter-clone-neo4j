package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.Panel;

public class ErrorModalContainer extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3624180352885273032L;

	public ErrorModalHeaderPanel header;

	public ErrorModalBodyPanel body;

	public ErrorModalContainer(String id) {
		super(id);
		header = new ErrorModalHeaderPanel("error-modal-header");
		header.setOutputMarkupId(true);
		body = new ErrorModalBodyPanel("error-modal-body");
		body.setOutputMarkupId(true);
		add(header);
		add(body);
	}

	public void update(AjaxRequestTarget target, String script, boolean show) {
		target.prependJavaScript("$('#modal-ajax').hide();");
		target.add(this);
		if (show)
			target.appendJavaScript("$('#show-error-modal-link').click();"
					+ script);
	}

	public void show(Component headerC, Component bodyC) {
		header.removeAll();
		header.add(headerC);
		body.removeAll();
		body.add(bodyC);
	}

}
