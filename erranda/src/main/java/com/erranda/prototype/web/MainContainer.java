package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class MainContainer extends Panel {

	private static final long serialVersionUID = 1L;

	public MainContainer(String id) {
		super(id);
	}

	public MainContainer(String id, IModel<?> model) {
		super(id, model);
	}

	public void replaceContent(Component component, AjaxRequestTarget target) {
		replaceContent(component);
		target.add(this);
	}

	public void replaceContent(Component component) {
		this.removeAll();
		this.add(component);
	}

}
