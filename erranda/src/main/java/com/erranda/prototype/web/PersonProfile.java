package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import scala.collection.script.Script;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.mobile.PersonProfileMobile;

public class PersonProfile extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	MainContainer container;

	NewsFeedPanel newsFeed;

	Friends friends;
	
	Following following;
	
	Followers followers;

	Long pid = null;
	
	WebMarkupContainer wizard = new WebMarkupContainer ("setup-page");
	
	String type = null;
	
	Boolean isPageMan = false;

	/**
	 * Homepage link, messages
	 */
	public PersonProfile() {
		Url url = (RequestCycle.get().getRequest()).getUrl();
		String id = RequestCycle.get().getUrlRenderer().renderRelativeUrl(url);
		id = id.replace("./", "");
		id = id.split("\\?")[0];
		PageParameters p = new PageParameters();
		p.add("username", id);
		throw new RestartResponseAtInterceptPageException(new PersonProfile(p));
	}

	private ProfileSummaryCard summary = null;

	private PersonProfileNavigation navigation = null;

	public PersonProfile(PageParameters parameters) {
		Url url = (RequestCycle.get().getRequest()).getUrl();
		String id = RequestCycle.get().getUrlRenderer().renderRelativeUrl(url);
		id = id.replace("./", "").toLowerCase();
		PlatformUser person = null;
		String username = parameters.get("username") != null ? parameters
				.get("username").toString().toLowerCase().trim() : null;
		if (username != null && (person = controller.findUsername(username.toLowerCase())) != null) {
			boolean blocked;
			try {
				blocked = controller.isBlockedTwoWay(UserSession.get().getPerson().getId(), person.getId());
				if (blocked)
					throw new RestartResponseAtInterceptPageException(ErrorPage.class);
				else {
					pid = person.getId();
					type = person.getType();
					isPageMan = controller.canManagePage(session.getId(), pid);
					if (cp != null && cp.getBrowserWidth() != 0 && cp.getBrowserWidth() < 1100)
						throw new RestartResponseAtInterceptPageException(PersonProfileMobile.class, parameters);
					initPanels(person,id);
				}
			} catch (ControllerException e) {
				e.printStackTrace();
				throw new RestartResponseAtInterceptPageException(ErrorPage.class);
			}
		} else {
			throw new RestartResponseAtInterceptPageException(ErrorPage.class);
		}

	}

	public void initPanels(PlatformUser person, String primary) throws ControllerException {
		if (pid == null)
			pid = UserSession.get().getPerson().getId();
		
		title.setDefaultModel(Model.of(person.getName() + " @"  + person.getUsername() + " | Erranda"));
		final LoadableDetachableModel<PlatformUser> model = new LoadableDetachableModel<PlatformUser>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected PlatformUser load() {
				try {
					return controller.findPerson(pid);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RestartResponseAtInterceptPageException(ErrorPage.class);
				}
			}
			
		};
		container = new MainContainer("content-container");
		container.setOutputMarkupId(true);
		
		summary = new ProfileSummaryCard("profile-summary", pid, model);
		friends = new Friends("content-container", pid);
		followers = new Followers("content-container", model);
		following = new Following("content-container", model);
		newsFeed = new NewsFeedPanel(
				"content-container", pid, modalContainer, "person", this);
		if (primary.equals("friends")) {
			container.replaceContent(friends);
			navigation = new PersonProfileNavigation("profile-nav", this, model, "friends");		
		} else if (primary.equals("following")) {
			container.replaceContent(following);
			navigation = new PersonProfileNavigation("profile-nav", this, model, "following");
		} else if (primary.equals("followers")) {
			container.replaceContent(followers);
			navigation = new PersonProfileNavigation("profile-nav", this, model, "followers");
		} else {
			primary = "timeline";
			container.replaceContent(newsFeed);
			navigation = new PersonProfileNavigation("profile-nav", this, model, "timeline");
		}
		newsFeed.setOutputMarkupId(true);
		friends.setOutputMarkupId(true);
		followers.setOutputMarkupId(true);
		following.setOutputMarkupId(true);
		if (type.equals(Config.PLATFORM_TYPE_PAGE) && isPageMan && person.getShowWelcome()) {
			wizard = new BusinessWizardContainer ("setup-page", person.getId());
			wizard.add(new Behavior() {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void renderHead(Component component,
						IHeaderResponse response) {
					String script = "$('#setup-page-modal-link').click();";
					response.render(OnDomReadyHeaderItem.forScript(script));
				}
			});
		}
		
		add(summary, navigation, container, wizard);
		if (session.getEmail().equals("public@public.com")) {
			add(new WebMarkupContainer("user-actions"));
		} else {
			add( new UserActions("user-actions", new LoadableDetachableModel <PlatformUser> () {

					@Override
					protected PlatformUser load() {
						try {
							return controller.findPerson(pid);
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RestartResponseAtInterceptPageException (ErrorPage.class);
						}
					}
					
				}));
		}
		add (new Behavior() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component component, IHeaderResponse response) {
				String script = String.format("if ($(window).width() <= 1100) {document.location = '/%s';}", "mobile/profile/" + model.getObject().getUsername());
				response.render(JavaScriptHeaderItem.forScript(script, "redirect"));
			}
		});
	}

}
