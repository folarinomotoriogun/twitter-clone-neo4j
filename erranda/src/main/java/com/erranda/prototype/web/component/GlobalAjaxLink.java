package com.erranda.prototype.web.component;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;

public abstract class GlobalAjaxLink<T> extends AjaxLink<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GlobalAjaxLink(String id) {
		super(id);
		add(AttributeModifier.append("class", "global-ajax-action"));
		add (new AjaxEventBehavior ("click") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onEvent(AjaxRequestTarget target) {
				onClick(target);
				target.appendJavaScript("$(\"#global-ajax-indicator\").css('display', 'none')");
				target.appendJavaScript("$(\"#global-logo\").css('display', 'block')");
				target.appendJavaScript("initAjax()");
			}
			
		});
	}

}
