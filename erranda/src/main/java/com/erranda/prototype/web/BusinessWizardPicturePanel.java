package com.erranda.prototype.web;

import static com.erranda.prototype.Config.IMAGES;

import java.io.IOException;
import java.util.UUID;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.form.upload.UploadProgressBar;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.file.File;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.ImageUtils;

public class BusinessWizardPicturePanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer x;

	private Integer y;

	private Integer w;

	private Integer h;

	private Boolean current = false;

	FileUploadField photo = null;

	@SpringBean
	Controller controller;

	public BusinessWizardPicturePanel(String id, final BusinessWizardContainer container, final Long pid) {
		super(id);
		Form<Void> form = new Form<Void>("upload-form");

		HiddenField<Integer> xFld = new HiddenField<Integer>("x",
				new PropertyModel<Integer>(this, "x"));
		HiddenField<Integer> yFld = new HiddenField<Integer>("y",
				new PropertyModel<Integer>(this, "y"));
		HiddenField<Integer> wFld = new HiddenField<Integer>("w",
				new PropertyModel<Integer>(this, "w"));
		HiddenField<Integer> hFld = new HiddenField<Integer>("h",
				new PropertyModel<Integer>(this, "h"));
		HiddenField<Boolean> currentFld = new HiddenField<Boolean>(
				"edit-current-photo", new PropertyModel<Boolean>(this,
						"current"));
		photo = new FileUploadField("photoField");
		UploadProgressBar progress = new UploadProgressBar("progress",
				form, photo);
		form.add(progress);
		
		IndicatingAjaxButton submit = new IndicatingAjaxButton("submit", form) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				try {
					PlatformUser person = controller.findPerson(pid);
					FileUpload upload = photo.getFileUpload();
					String fileNameLocalUrl = "";
					String fileName = "";
					String thumbNail = "";
					String thumbNailLocalUrl = "";
					
					if (upload != null) {
						String rand = UUID.randomUUID()
								+ "-";
						String ext = upload.getClientFileName().substring(
								upload.getClientFileName().lastIndexOf(
										"."));
						fileName = rand + ext;
						fileNameLocalUrl = IMAGES + "/" + fileName;
						thumbNail = rand + "thumbNail" + ext;
						thumbNailLocalUrl = IMAGES + "/" + thumbNail;
						
						File file = new File(fileNameLocalUrl);
						upload.writeTo(file);
						
						String url = "/wicket/resource/org.apache.wicket.Application/images?id="
								+ fileName;
						String thumbNailUrl = "/wicket/resource/org.apache.wicket.Application/images?id="
								+ thumbNail;
						ImageUtils.cropImage(file, thumbNailLocalUrl, x, y, w, h);
						UserSession.get().setPerson(controller.updateProfilePicture(person.getId(), url, thumbNailUrl));
					}
					
					Label label = new Label("modal-header", Model.of(String
							.format("Your page looks star studded", person.getName().split(" ")[0])));
					BusinessWizardFinishPanel finish = new BusinessWizardFinishPanel (
							"modal-content", container, pid);
					container.show(label, finish);
					container.update(target, "hideCropper()", false);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		form.add(xFld.setRequired(true), yFld.setRequired(true),
				wFld.setRequired(true), hFld.setRequired(true), photo,
				currentFld.setRequired(true), submit);

		add(form);
		add (new IndicatingAjaxLink<Object> ("skip") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Label label = new Label("modal-header", Model.of(String
						.format("You are done, %s", UserSession.get()
								.getPerson().getName().split(" ")[0])));
				BusinessWizardFinishPanel finish = new BusinessWizardFinishPanel (
						"modal-content", container, pid);
				container.show(label, finish);
				container.update(target, "hideCropper()", false);
			}
			
		});
	}

}
