/**
 * 
 */
package com.erranda.prototype.web;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Notification;
import com.erranda.prototype.web.component.TimeConverter;

/**
 * @author Folarin
 *
 */
public class NotificationMessage extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;

	public NotificationMessage(String id, Notification notification) {
		super(id);
		final Long nid = notification.getId();

		Label details = new Label("details",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						Notification n;
						try {
							n = controller.findOneNotification(nid);
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return "";
						}
						StringBuffer details = new StringBuffer("");
						details.append("<div class=\"DMInboxItem-title\"><div class=\"DMInboxItem\" style=\"box-sizing: border-box;  cursor: pointer;  display: block;min-height: 20px;  padding: 15px;  padding-left: 73px;  position: relative;color: rgb(129, 129, 129);font-weight: bold;font-size: small;\">"
								+ "<p class=\"js-action-profile-name\">");
						details.append(n.getDetails());
						details.append("</p><div class=\"NotificationTimestamp\"><b><small class=\"time\"><span class=\"_timestamp\">");
						details.append(TimeConverter.convertTime(n.getCtime(),
								((Authenticated) getPage()).cp).split("#")[0]);
						details.append("</span></small></b></div><span><div class=\"DMInboxItem-avatar\"><img class=\"avatar js-action-profile-avatar size32\" src=\"");
						details.append(n.getPictureLink());
						details.append("\"></div></div></div>");
						return details.toString();
					}

				});
		add(details.setEscapeModelStrings(false));
	}

}
