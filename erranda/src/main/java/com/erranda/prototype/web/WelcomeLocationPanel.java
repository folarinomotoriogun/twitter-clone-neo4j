package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.DisablingIndicatingAjaxButton;

public class WelcomeLocationPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public WelcomeLocationPanel(String id, final IntroductionContainer container) {
		super(id);
		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);

		final PlatformUser person = UserSession.get().getPerson();

		final Form<String> form = new Form<String>("form");

		TextField<String> currentLocation = new TextField<String>(
				"current-location", new PropertyModel<String>(person,
						"localityName"));
		form.add(currentLocation);
		form.add(feedback);

		form.add(new DisablingIndicatingAjaxButton("submit", form) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				PlatformUser session = UserSession.get().getPerson();
				person.setId(session.getId());
				try {
					controller.updateLocation(person.getId(),
							person.getLocalityName());
					Label label = new Label("modal-header", Model
							.of("Add a profile photo (3/3)"));
					WelcomePicturePanel photo = new WelcomePicturePanel(
							"modal-content", container);
					container.show(label, photo);
					container.update(target, "", false);
				} catch (ControllerException e) {
					form.error("Could not complete your request");
					target.add(feedback);
					e.printStackTrace();
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(feedback);
			}
		});

		add(form);

	}

}
