package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.select2.Select2MultiChoice;

import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.DisablingAjaxButton;
import com.erranda.prototype.web.mailer.EmailNewAction;

public class NominatePerson extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	String message;

	List<PlatformUser> nodes = new ArrayList<PlatformUser>();

	PlatformUser session = UserSession.get().getPerson();

	public NominatePerson(String id, final Long pid, final Label label) {
		super(id);
		final Form<String> tagged = new Form<String>("people");
		final Select2MultiChoice<PlatformUser> nodesComponent = new Select2MultiChoice<PlatformUser>(
				"tags", new PropertyModel<Collection<PlatformUser>>(this, "nodes"),
				new PersonProvider(controller,UserSession.get().getPerson().getId()));
		nodesComponent.getSettings().setMinimumInputLength(0);
		tagged.add(nodesComponent);

		TextArea<String> textArea = new TextArea<String>("broadcast-message",
				new PropertyModel<String>(this, "message"));
		tagged.add(textArea);
		tagged.add(new DisablingAjaxButton("submit", tagged) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				try {
					controller.nominatePerson(nodes, pid, UserSession.get()
							.getPerson().getId(), message);
					Authenticated page = (Authenticated) getPage();
					ErrorModalContainer feedback = page.errorModalContainer;
					feedback.show(
							new Label("modal-header", Model
									.of("Your first time Nominating a friend")),
							new Label(
									"modal-content",
									Model.of("Nominating a friend is a way of recommending them for an Errand. The errand can be accepted, broadcasted or nominated by your friend.")));
					if (session.getFirstNomination()) {
						feedback.update(target, "", true);
						session.setFirstNomination(false);
					}
					Errand errand = controller.findErrand(pid);
					String html = new String(ComponentRenderer.renderPage(
							new PageProvider(new EmailNewAction(session,
									session.getName() + " @"
											+ session.getUsername()
											+ " nominated you in this post",
									errand.getErrandLink()))).toString());
					MailMan man = new MailMan();
					for (PlatformUser person : nodes) {
						man.sendMessage(
								session.getName() + " @"
										+ session.getUsername()
										+ " nominated you", person.getEmail(),
								html);
					}
					nodes.clear();
				} catch (ControllerException e) {
					e.printStackTrace();
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				target.add(label);
				target.appendJavaScript("$('#" + nodesComponent.getMarkupId()
						+ "').select2('val', '');");

			}
		});
		Errand errand = new Errand();
		errand.setId(pid);
		add(tagged.add(new ShareButton("share", errand.getErrandLink()).setVisible(false)));add (new Behavior () {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').slideDown();",
								tagged.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));

			}
			
		});
	}
}
