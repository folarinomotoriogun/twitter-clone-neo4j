package com.erranda.prototype.web;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class IntroductionModalBodyPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3647395653190336618L;

	public IntroductionModalBodyPanel(String id, IntroductionContainer container) {
		super(id);
		add(new WelcomePanel("modal-content", container));
	}

	public IntroductionModalBodyPanel(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
