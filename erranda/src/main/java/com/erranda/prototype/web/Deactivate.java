package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.DisablingAjaxButton;

public class Deactivate extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	private Controller controller;
	
	private PlatformUser session = UserSession.get().getPerson();

	public Deactivate() {
		Form<String> form = new Form<String>("deactivate-form");
		PlatformUser person = UserSession.get().getPerson();
		try {
			person = controller.forgotPassword(UserSession.get().getPerson().getId());
		} catch (ControllerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		ExternalLink reset = new ExternalLink("change-password",
				Model.of(Config.SET_PASSWORD_LINK + "/"
						+person.getForgotPassword()));
		
		final DisablingAjaxButton deactivate = new DisablingAjaxButton(
				"deactivate", form) {

			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit(AjaxRequestTarget target, Form<?> form) {
				try {
					controller.deactivateAccount(UserSession.get().getPerson());
					setResponsePage(Home.class);
					throw new RestartResponseAtInterceptPageException(
							Logout.class);
				} catch (ControllerException e) {
					error("Sorry we could not deactivate your account at this time");
				}
			}
		};
		
		Label name = new Label("name", new PropertyModel<String>(session, "name"));
		add( name);

		IndicatingAjaxLink<String> cancel = new IndicatingAjaxLink<String>(
				"cancel") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				throw new RestartResponseAtInterceptPageException(Home.class);
			}
		};
		form.add(deactivate, cancel);
		add(form,reset);
	}

}
