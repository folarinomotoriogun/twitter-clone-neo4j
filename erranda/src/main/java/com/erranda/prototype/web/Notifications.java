package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.AbstractPushEventHandler;
import org.wicketstuff.push.IPushEventContext;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.cometd.CometdPushNode;
import org.wicketstuff.push.cometd.CometdPushService;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;

public class Notifications extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	NotificationJewel njewel;

	LazyLoaderPanel notifications;

	public Notifications(String id) {
		super(id);

		notifications = new LazyLoaderPanel("notifications-panel",
				new NotificationsList("content"));

		try {
			njewel = new NotificationJewel("notification-count", UserSession
					.get().getPerson().getId());
			
			final IPushNode<String> pushNode =  TimerPushService.get().installNode(njewel,
					new AbstractPushEventHandler<String>()
					{
						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						@Override
						public void onEvent(final AjaxRequestTarget target, final String event,
							final IPushNode<String> node, final IPushEventContext<String> ctx)
						{
							target.add(njewel);
						}
			});
			((App) App.get()).registerNotificationPanel(UserSession.get().getPerson().getId(), pushNode);
		} catch (ControllerException e1) {
			throw new RuntimeException(e1.getMessage());
		}

		final AjaxLink<String> notificationsLink = new AjaxLink<String>(
				"notifications-link") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.markAllNotificationAsSeen(UserSession.get()
							.getPerson().getId());
					target.add(njewel);
					notifications.update(target);

				} catch (ControllerException e) {
					// TODO Auto-generated catch block //Global error handler
					e.printStackTrace();
				}
			}

		};
		notificationsLink.add(new Behavior() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$('#" + notificationsLink.getMarkupId()
						+ "').bind('click', function(){" + "$('#"
						+ njewel.getMarkupId() + "').hide();"
						+ "$('#notifications-dropdown').click();" + "});";
				response.render(OnDomReadyHeaderItem.forScript(script));
			}

		});
		add(notifications, njewel, notificationsLink);
	}

}
