/**
 * 
 */
package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;
import com.erranda.prototype.web.component.ModalAjaxLink;

/**
 * @author Folarin
 *
 */
public class UserActions extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	PlatformUserRepository personRepo;

	@SpringBean
	Controller controller;

	WebMarkupContainer container;

	private Long rid;

	public UserActions(String id, IModel<PlatformUser> person) {
		super(id);
		this.rid = person.getObject().getId();
		PlatformUser user = UserSession.get().getPerson();
		if (user.getId().equals(rid))
			setVisible(false);
		addUserActions(person);
	}

	private void addUserActions(IModel<PlatformUser> person) {
		Long sid = UserSession.get().getPerson().getId();
		try {
			Boolean fr = controller.isPendingFriendRequest(sid, rid);
			Boolean rf = controller.isPendingFriendRequest(rid, sid);
			Boolean f = controller.areWeFriends(sid, rid);
			container = new UserActionContainer("action-container");
			if (rf && person.getObject().getType().equals(Config.PLATFORM_TYPE_PERSON))
				((UserActionContainer) container).replaceWindow(new AcceptRequest("action-body",
						rid, container));
			else if (!person.getObject().getType().equals(Config.PLATFORM_TYPE_PERSON)) {
				((UserActionContainer) container).replaceWindow(new WebMarkupContainer("action-body"));
			} else if (fr && person.getObject().getType().equals(Config.PLATFORM_TYPE_PERSON)) {
				((UserActionContainer) container).replaceWindow(new FriendRequestSent("action-body",
						rid, container));
			} else if (f && person.getObject().getType().equals(Config.PLATFORM_TYPE_PERSON)) {
				((UserActionContainer) container).replaceWindow(new FriendsAction("action-body", rid,
						container));
			} else if (person.getObject().getType().equals(Config.PLATFORM_TYPE_PERSON))
				((UserActionContainer) container).replaceWindow(new AddAsFriend("action-body", rid,
						container));
			ModalAjaxLink<String> message = new ModalAjaxLink<String>(
					"message-person") {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void onClick(AjaxRequestTarget target) {
					PlatformUser sender = UserSession.get().getPerson();
					Conversation c;
					try {
						c = controller.getConversation(sender.getId(), rid);
						ModalContainer modalContainer = ((Authenticated) getPage()).modalContainer;
						MessagesPanel mp = new MessagesPanel("modal-content", c);
						modalContainer.replaceWindow(new MessageHeader(
								"modal-header", c.getId()), mp);
						String script = "setTimeout(function(){$('#" + mp.listId
								+ "').scrollTop($('#" + mp.listId
								+ "')[0].scrollHeight);},1000)";
						modalContainer.update(target, script, true);
					} catch (ControllerException e) {
						error(e.getMessage());
					}
				}
			};
			if (f)
				add(new WebMarkupContainer ("follow-button"));
			else {
				add(new FollowButton("follow-button", person));
			}
			add(container, message);
		} catch (ControllerException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
