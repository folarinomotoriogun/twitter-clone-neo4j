package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Proposal;
import com.erranda.prototype.web.component.GlobalAjaxLink;

/**
 * @author Folarin
 *
 */
public class ConfirmProposal extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;

	/**
	 * @param id
	 * @param model
	 */
	public ConfirmProposal(String id, final Long pid) {
		super(id);
		final Label details = new Label("details",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {

						try {
							Proposal post = controller.findOneProposal(pid);
							String details = "";
							if (post.getConfirmed())
								details = "<span class=\"fa fa-check fa-4x\" style=\"color:#44B449\"></span>";
							else
								details = "<span class=\"fa fa-check fa-4x\" style=\"color: #BEBCBC\"></span>";
							return details;
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}
					}

				});
		details.setEscapeModelStrings(false);
		details.setOutputMarkupId(true);

		final GlobalAjaxLink<Void> confirmLink = new GlobalAjaxLink<Void>(
				"confirm-link") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					Proposal p = controller.findOneProposal(pid);
					if (p.getRid()
							.equals(UserSession.get().getPerson().getId())) {
						controller.confirmProposal(pid, "");
						target.add(details);
					} else {
						// Show error

					}
				} catch (ControllerException e) {
					throw new RuntimeException(e.getMessage());
				}
			}
		};
		try {
			Proposal p = controller.findOneProposal(pid);
			if (!p.getRid().equals(UserSession.get().getPerson().getId())) {
				confirmLink.add(AttributeModifier.append("class",
						"disabled-link"));
			}
		} catch (ControllerException e) {
			throw new RuntimeException(e.getMessage());
		}
		confirmLink.add(details);
		confirmLink.add(AttributeModifier.replace("title", "Mark as helpful"));
		add(confirmLink);
	}

}
