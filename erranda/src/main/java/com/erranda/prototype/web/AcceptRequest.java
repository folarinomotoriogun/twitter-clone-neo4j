package com.erranda.prototype.web;

import java.util.Arrays;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.Config;
import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.ConfirmingGlobalAjaxLink;
import com.erranda.prototype.web.component.ModalAjaxLink;
import com.erranda.prototype.web.mailer.EmailNewAction;

public class AcceptRequest extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	PlatformUser session = UserSession.get().getPerson();

	Long rid;

	Label label;

	public AcceptRequest(final String id, Long pid,
			final WebMarkupContainer container) {
		super(id);
		this.rid = pid;
		if (session.getType().equals(Config.PLATFORM_TYPE_PAGE))
			setVisible(false);
		final IndicatingAjaxLink<String> addFriend = new IndicatingAjaxLink<String>(
				"add-friend") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Long sid = UserSession.get().getPerson().getId();
				try {
					controller.acceptFriendRequest(sid, rid);
					sendPushNotificationMessage(rid);
					((UserActionContainer) container).replaceWindow(new FriendsAction(id, rid,
							container));
					((UserActionContainer) container).update(target, "");
				} catch (ControllerException e) {
					throw new RuntimeException(e.getMessage());
				} 
			}
		};

		
		add(addFriend);
	}
	
	private void sendPushNotificationMessage (Long rid) throws ControllerException {
		List<Long> people = Arrays.asList(rid);
		for (Long person : people) {
			IPushNode node = ((App) App.get()).getNotificationPanel(person);
			if (node != null)
				TimerPushService.get().publish(node, "");
		}
	}

}
