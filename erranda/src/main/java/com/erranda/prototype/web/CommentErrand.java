package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.ClientProperties;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.cometd.CometdPushNode;
import org.wicketstuff.push.cometd.CometdPushService;
import org.wicketstuff.push.timer.TimerPushService;

import ch.qos.logback.core.pattern.parser.Node;

import com.erranda.prototype.App;
import com.erranda.prototype.Config;
import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Commented;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.DisablingAjaxButton;
import com.erranda.prototype.web.mailer.EmailNewAction;

public class CommentErrand extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	private Integer cursor = 0;

	PlatformUser session = UserSession.get().getPerson();

	String comment;
	
	public CommentErrand(String id, final Long eid, final Label label,
			final ClientProperties cp) throws ControllerException {
		super(id);
		label.setOutputMarkupId(true);

		WebMarkupContainer commentsContainer = new WebMarkupContainer(
				"comments-list");
		add(commentsContainer);
		commentsContainer.setOutputMarkupId(true);
		final String markupid = commentsContainer.getMarkupId();

		final ListView<Commented> commentsRepeater = new ListView<Commented>(
				"repeater", new LoadableDetachableModel<List<Commented>>() {

					private static final long serialVersionUID = 1L;

					@Override
					protected List<Commented> load() {
						List<Commented> comments = new ArrayList<Commented>();
						try {
							for (Commented c : controller
									.getComments(session.getId(), eid, 0, 100))
								comments.add(c);
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return comments;
					}

				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Commented> item) {
				cursor++;
				item.setOutputMarkupId(true);
				try {
					item.add(new CommentItem("this", item.getModel(), cp, item
							.getMarkupId()));
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		final Commented commentFormModel = new Commented();
		final Form<Commented> commentForm = new Form<Commented>("comment-form");
		commentForm.setOutputMarkupId(true);
		CheckBox anonymous = new CheckBox("anonymous",
				new PropertyModel<Boolean>(commentFormModel, "anonymous"));
		commentForm.add(anonymous);

		CheckBox enterHandlerToggle = new CheckBox("enter-to-send");
		// commentForm.add(enterHandlerToggle);

		final TextArea<String> message = new TextArea<String>("comment-box",
				new PropertyModel<String>(this, "comment"));
		message.setRequired(true);
		message.setOutputMarkupId(true);
		commentForm.add(message);
		
		final WebMarkupContainer mentionBox = new WebMarkupContainer ("mention-box");
		mentionBox.setOutputMarkupId(true);
		final WebMarkupContainer mentionResults = new WebMarkupContainer ("mention-results");
		mentionResults.setOutputMarkupId(true);
		commentForm.add(mentionBox.add(mentionResults));
		
		message.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String inputId = message.getMarkupId();
				String script = "$('#" + inputId + "').bind ('keyup', function () {" +
                   		 "nominateAutocomplete ('" + inputId + "', '" + mentionBox.getMarkupId() + "','" + mentionResults.getMarkupId() + "');" +
					"});";
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		
		final DisablingAjaxButton submit = new DisablingAjaxButton(
				"comment-button", commentForm) {

			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target,
					Form<?> commentForm) {
				if (comment.length() > 0) {
					PlatformUser sender = UserSession.get().getPerson();
					commentFormModel.setSender(sender);
					commentFormModel.setCtime(new Date());
					try {
						Commented c = controller.comment(sender.getId(), eid,
								comment,
								commentFormModel.getAnonymous());
						Errand errand = controller.findErrand(eid);
						sendPushNotifications(errand.getId());
						// So ajax update can contain comment id.
						commentFormModel.setId(c.getId());
						commentFormModel.setMessage(comment);
						commentFormModel.setErrand(errand);
						// Important anonymity protectors in place
						String html = new String(
								ComponentRenderer
										.renderPage(
												new PageProvider(
														new EmailNewAction(
																commentFormModel
																		.getAnonymous() ? new PlatformUser()
																		: session,
																"\""
																		+ commentFormModel
																				.getMessage()
																		+ "\"",
																		Config.DOMAIN_NAME + errand.getErrandLink())))
										.toString());
						MailMan man = new MailMan();
						man.sendMessage(anonymize(commentFormModel)
								+ " commented on your post", errand.getOwner()
								.getEmail(), html);
						for (PlatformUser person : controller.othersThatCommented(
								eid, session.getId())) {
							man.sendMessage(anonymize(commentFormModel)
									+ " also commented on this post",
									person.getEmail(), html);
						}
						cursor++;
						ListItem<Commented> item = new ListItem<Commented>(cursor,
								Model.of(commentFormModel));
						item.setOutputMarkupId(true);
						commentsRepeater.add(item);
						target.prependJavaScript(String
								.format("var item=document.createElement('%s');item.class='original-tweet-container'; item.id='%s';"
										+ "$('#%s').append(item);", "li",
										item.getMarkupId(), markupid));
						// Line dependency
						item.add(new CommentItem("this",
								Model.of(commentFormModel), cp, item.getMarkupId()));
						target.add(item);
						target.appendJavaScript("$(\"#" + markupid
								+ "\").scrollTop($(\"#" + markupid
								+ "\")[0].scrollHeight);");
						target.appendJavaScript("$(\"#" + message.getMarkupId()
								+ "\").val('')");
						target.appendJavaScript("$(\"#" + message.getMarkupId()
								+ "\").removeAttr(\"readonly\")");

						unset(commentFormModel);
					} catch (ControllerException e) {
						error("Could not process your request");
						e.printStackTrace();
						throw new RuntimeException(e.getMessage());
					} catch (MessagingException e) {
						e.printStackTrace();
					}
					
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.appendJavaScript("$(\"#" + message.getMarkupId()
						+ "\").removeAttr(\"readonly\")");
			}
		};
		submit.add(AttributeModifier.replace("class", "visuallyHidden"));
		commentForm.add(submit);
		commentForm.add(commentsContainer.add(commentsRepeater));
		add(commentForm);

		add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13 && $('#%s').val().length > 0) {$('#%s').click(); return false; } })});",
								message.getMarkupId(), message.getMarkupId(),
								submit.getMarkupId());
				String script2 = String
						.format("$('#%s').click(function () {$('#%s').attr ('readonly', 'readonly')}); $('#%s').autogrow();",
								submit.getMarkupId(), message.getMarkupId(),
								message.getMarkupId());
				String script3 = String
						.format("$('#%s').slideDown();",
								commentForm.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script3));
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript(script2));
			}
		});

	}

	private String anonymize(Commented post) {
		if (post.getAnonymous())
			return "Anonymous";
		else
			return post.getSender().getName() + " @"
					+ post.getSender().getUsername();
	}

	private void unset(Commented comment) {
		comment.setCtime(null);
		comment.setErrand(null);
		comment.setMessage(null);
		comment.setSender(null);
	}

	public Integer getCursor() {
		return cursor;
	}

	public void setCursor(Integer cursor) {
		this.cursor = cursor;
	}
	
	private void sendPushNotifications (Long eid) throws ControllerException {
		List<Long> people = controller.commentNotificationList(session.getId(), eid);
		for (Long person : people) {
			IPushNode node = ((App) App.get()).getNotificationPanel(person);
			if (node != null)
				TimerPushService.get().publish(node, "");
		}
	}

}
