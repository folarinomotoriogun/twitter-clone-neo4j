package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.select2.Select2MultiChoice;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Page;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;
import com.erranda.prototype.web.component.DisablingIndicatingAjaxButton;
import com.google.common.base.Strings;

public class AddPageInfo extends Panel {

	@SpringBean
	Controller controller;

	@SpringBean
	PlatformUserRepository pageRepo;
	
	List<PlatformUser> team = new ArrayList<PlatformUser>();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AddPageInfo(String id) {
		super(id);

		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);

		final Page page = new Page ();

		final Form<String> form = new Form<String>("form");
		
		TextField<String> name = new TextField<String>("page-name",
				new PropertyModel<String>(page, "nameRaw"));
		form.add(name);

		TextField<String> username = new TextField<String>("username",
				new PropertyModel<String>(page, "usernameRaw"));
		//username.add(new UsernameValidator(pageRepo));
		form.add(username);

		TextArea<String> bio = new TextArea<String>("bio",
				new PropertyModel<String>(page, "bio"));
		form.add(bio);

		TextField<String> location = new TextField<String>("user-location",
		new PropertyModel<String>(page, "localityName"));
		
		TextField<String> phoneNumber = new TextField<String>("phone-number",
				new PropertyModel<String>(page, "phoneNumber"));
		
		final Select2MultiChoice<PlatformUser> nodes = new Select2MultiChoice<PlatformUser>(
				"tags", new PropertyModel<Collection<PlatformUser>>(this, "team"),
				new PersonProvider(controller,UserSession.get().getPerson().getId()));
		form.add(nodes, phoneNumber);
		
		form.add(location);
		form.add(name);

		form.add(feedback);

		form.add(new DisablingIndicatingAjaxButton("submit", form) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				PlatformUser session = UserSession.get().getPerson();
				try {
					Page result = controller.createPage(session.getId(), page, team);
					PageParameters params = new PageParameters ();
					params.set("username", result.getUsername ());
					throw new RestartResponseAtInterceptPageException (PersonProfile.class, params);
				} catch (ControllerException e) {
					String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show');", this.getMarkupId() , e.getMessage());
					target.appendJavaScript(errorScript);
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show');", this.getMarkupId() , "Something is wrong with your request");
				target.appendJavaScript(errorScript);
			}
		});
		add(form);
	}

}
