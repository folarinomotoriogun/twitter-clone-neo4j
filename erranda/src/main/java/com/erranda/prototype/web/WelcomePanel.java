package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class WelcomePanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@SpringBean
	Controller controller;

	public WelcomePanel(String id, final IntroductionContainer container) {
		super(id);
	
		AjaxLink<?> next = new AjaxLink<Object>("next") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Label label = new Label("modal-header", Model
						.of("Add a profile photo"));
				WelcomePicturePanel photo = new WelcomePicturePanel(
						"modal-content", container);
				container.show(label, photo);
				container.update(target, "", false);
			}
		};
		add(next.setOutputMarkupId(true));
	}
	
	@Override
	public void onInitialize() {
		super.onInitialize();
		PlatformUser person = UserSession.get().getPerson();
		person.setShowWelcome(false);
		try {
			person = (PlatformUser) controller.save(person);
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
