package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.PostNominate;
import com.erranda.prototype.web.component.ModalAjaxLink;

public class PostDefaultMeta extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;
	
	PlatformUser session = UserSession.get().getPerson();
	
	WebMarkupContainer othersNominated = null;

	
	public PostDefaultMeta(String id, IModel<Errand> model)
			throws ControllerException {
		super(id);
		Errand errand = model.getObject();
		final Long eid = errand.getId();
		Integer count = controller.countOfPeopleNominate(eid);
		if (count == 0) {
			this.setVisible(false);
			return;
		}
		
		PostNominate post = controller.findPrimaryNominated(session.getId(), eid);
		PlatformUser nominated = post.getOwner();
		ExternalLink personLink = new ExternalLink("link",
				new PropertyModel<String>(nominated, "link"));
		StringBuffer meta = new StringBuffer ();
		meta.append(count);
		meta.append(count == 1 ? " person nominated " : " people nominated");
		Label details = new Label("label", Model.of(meta.toString()));
		if (count >= 1) {
			othersNominated = new ModalAjaxLink<String> ("others") {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void onClick(AjaxRequestTarget target) {
					Authenticated page = (Authenticated) getPage ();
					ModalContainer relationships = page.modalContainer;
					relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">People nominated in this post</h3>")).setEscapeModelStrings(false), new OtherPeopleNominated ("modal-content", eid));
					relationships.update(target, "", true);
				}
				
			};
			othersNominated.add(details);
		} else {
			othersNominated = new WebMarkupContainer ("others");
			othersNominated.add(new WebMarkupContainer("label"));
			othersNominated.setVisible(false);
		}
		add(othersNominated);
		
		
		
	}

}
