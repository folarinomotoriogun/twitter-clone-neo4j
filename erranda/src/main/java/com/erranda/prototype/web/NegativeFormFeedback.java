package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

public class NegativeFormFeedback extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NegativeFormFeedback(String id) {
		super(id);
	}

	public void show(AjaxRequestTarget target, String message) {
		removeAll();
		add(new Label("message", Model.of(message)));
	}

}
