package com.erranda.prototype.web;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class RelationshipModalBodyPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3647395653190336618L;

	public RelationshipModalBodyPanel(String id) {
		super(id);
		add(new Label("modal-content"));
		// TODO Auto-generated constructor stub
	}

	public RelationshipModalBodyPanel(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
