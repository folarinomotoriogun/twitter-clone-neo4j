package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class BusinessWizardWelcomePanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@SpringBean
	Controller controller;

	public BusinessWizardWelcomePanel(String id, final BusinessWizardContainer container, final Long pid) {
		super(id);
		
		
		try {
			PlatformUser person = controller.findPerson(pid);
			person.setShowWelcome(false);
			person = (PlatformUser) controller.save(person);
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		AjaxLink<?> next = new AjaxLink<Object>("next") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Label label = new Label("modal-header",
						Model.of("Add a photo"));
				BusinessWizardPicturePanel pictureMan = new BusinessWizardPicturePanel(
						"modal-content", container, pid);
				container.show(label, pictureMan);
				container.update(target, "", false);
			}
		};
		add(next.setOutputMarkupId(true));
	}

}
