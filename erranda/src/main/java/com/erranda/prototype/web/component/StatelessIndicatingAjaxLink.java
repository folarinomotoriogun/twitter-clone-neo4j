package com.erranda.prototype.web.component;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.IAjaxIndicatorAware;
import org.apache.wicket.extensions.ajax.markup.html.AjaxIndicatorAppender;
import org.wicketstuff.stateless.StatelessAjaxFallbackLink;

public class StatelessIndicatingAjaxLink<T> extends StatelessAjaxFallbackLink<T> implements IAjaxIndicatorAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StatelessIndicatingAjaxLink(String id) {
		super(id);
	}
	
	private final AjaxIndicatorAppender indicatorAppender = new AjaxIndicatorAppender() {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public boolean getStatelessHint(Component component) {
			return true;
		}
	};
	
	@Override
	protected void onInitialize() {
		super.onInitialize();
		add(indicatorAppender);
	}

	@Override
	public String getAjaxIndicatorMarkupId() {
		return indicatorAppender.getMarkupId();
	}

	@Override
	public void onClick(AjaxRequestTarget arg0) {
		// TODO Auto-generated method stub
		
	}

}
