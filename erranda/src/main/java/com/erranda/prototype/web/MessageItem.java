package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.Strings;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.Message;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.TimeConverter;

public class MessageItem extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	Authenticated page;

	ModalContainer modalContainer;

	public MessageItem(String id, final IModel<Message> item) {
		super(id);
		Label name = new Label("sender-name", new PropertyModel<String>(item.getObject().getSender(), "name"));
		Label messageSummary = new Label("message-summary", new PropertyModel<String>(item.getObject(), "payload"));
		// messageSummary.setEscapeModelStrings(false);
		Label date = new Label("message-date", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				// TODO Auto-generated method stub
				return TimeConverter.convertTime(
						item.getObject().getCtime(),
						((Authenticated) getPage()).cp)
						.split("#")[0];
			}
		});
		WebMarkupContainer iconImage = new WebMarkupContainer(
				"icon-image");
		iconImage.add(AttributeModifier.replace("src",new PropertyModel<String>(item.getObject().getSender(), "thumbNail")));
		add(iconImage);
		add(name, messageSummary, date);
		
		messageSummary.add(new Behavior() {
		
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component c, IHeaderResponse response) {
				String script = String.format("$('#%s').emotions()", c.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
	}

	public static PlatformUser withWho(Conversation c) {
		PlatformUser current = UserSession.get().getPerson();
		if (!c.getReceiver().equals(current))
			return c.getReceiver();
		else
			return c.getSender();
	}

}
