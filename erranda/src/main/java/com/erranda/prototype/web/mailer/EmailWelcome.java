package com.erranda.prototype.web.mailer;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.model.Model;

import com.erranda.prototype.Config;
import com.erranda.prototype.domain.PlatformUser;

public class EmailWelcome extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmailWelcome() {
	}

	public EmailWelcome(PlatformUser person) {
		ExternalLink profile = new ExternalLink("confirmLink",
				Model.of(Config.CONFIRM_LINK + "/"
						+ person.getVerificationKey()));
		Label name = new Label("name", Model.of(person.getName().split(" ")[0]
				+ ", Welome to Erranda"));
		add(profile, name);
	}

}
