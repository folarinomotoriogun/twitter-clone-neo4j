package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class AccountSwitch extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;
	
	UserSession session = UserSession.get();
	
	String currentUser = session.getPerson().getUsername();
	
	public AccountSwitch(String id) throws ControllerException {
		super(id);
		List<PlatformUser> accounts = controller.switchAccounts(session.getUserId());
		List<String> usernames = new ArrayList<String> ();
		for (PlatformUser user : accounts) {
			usernames.add (user.getUsername());
		}
		Form<String> form = new Form<String> ("switch-account-form") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onSubmit () {
				PlatformUser user = controller.findUsername(currentUser);
				if (user == null)
					throw new RestartResponseAtInterceptPageException (ErrorPage.class);
				else {
					session.setPerson(user);
					PageParameters params = new PageParameters ();
					params.set("username", user.getUsername());
					throw new RestartResponseAtInterceptPageException (PersonProfile.class, params);
				}
			}
		};
		
		final WebMarkupContainer staticFeedback = new WebMarkupContainer ("static-feedback");
		staticFeedback.setOutputMarkupId(true);
		
		final Button button = new Button ("submit");
		final DropDownChoice<String> selector = new DropDownChoice<String> ("switch-account-select", new PropertyModel<String>(this, "currentUser"), usernames);
		selector.add (new Behavior () {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component component, IHeaderResponse response) {
				String changeSubmit = String.format("$('#%s').change (function () { $('#%s').click (); $('#%s').css('display', 'block').fadeIn();});", component.getMarkupId(), button.getMarkupId(), staticFeedback.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(changeSubmit));
			}
		});

		add (form.add(selector, button, staticFeedback));
		
	}

}
