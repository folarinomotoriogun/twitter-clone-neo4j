package com.erranda.prototype.web.component;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;

public class ImageUtils {

	public ImageUtils() {
		// TODO Auto-generated constructor stub
	}

	public static void cropImage(File file, String thumbNailLocalUrl, int x, int y, int x2, int y2)
			throws IOException {
		BufferedImage img = ImageIO.read(file);
		BufferedImage newImg = cropImage(img, new Rectangle(x, y, (x2 - x), (y2 - y)));
		if (newImg.getWidth() > 400)
			newImg = Scalr.resize(newImg, 400);
		saveImage(newImg, file);
		File thumbNail = new File (thumbNailLocalUrl);
		BufferedImage tn = Scalr.resize(newImg, 50);
		saveImage (tn, thumbNail);
	}

	private static BufferedImage cropImage(BufferedImage src, Rectangle rect) {
		BufferedImage dest = src.getSubimage(rect.x, rect.y, rect.width,
				rect.height);
		return dest;
	}

	private static File saveImage(BufferedImage image, File file)
			throws IOException {
		ImageIO.write(image, "png", file);
		return file;
	}
	
	public static int resizeImg (File image, int size) throws IOException {
		BufferedImage img = ImageIO.read(image);
		if (img.getWidth() > size) {
			img = Scalr.resize(img, size);
			saveImage (img, image);
		}
		return img.getWidth(); 
	}
	
	public static int imageHeight (File image) throws IOException {
		BufferedImage img = ImageIO.read(image);
		return img.getHeight();
	}
}
