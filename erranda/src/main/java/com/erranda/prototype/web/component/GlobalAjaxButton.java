package com.erranda.prototype.web.component;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.attributes.AjaxRequestAttributes;
import org.apache.wicket.markup.html.form.Form;
import org.wicketstuff.stateless.StatelessAjaxFormSubmitBehavior;

public abstract class GlobalAjaxButton extends StatelessAjaxButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GlobalAjaxButton(String id, Form form) {
		super(id, form);
		add(AttributeModifier.append("class", "global-ajax-action"));
	}

	@Override
	protected StatelessAjaxFormSubmitBehavior newAjaxFormSubmitBehavior(String event) {
		return new StatelessAjaxFormSubmitBehavior(event) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				GlobalAjaxButton.this.onSubmit(target,
						GlobalAjaxButton.this.getForm());
			}

			@Override
			protected void onAfterSubmit(AjaxRequestTarget target) {
				GlobalAjaxButton.this.onAfterSubmit(target,
						GlobalAjaxButton.this.getForm());
				clear(target);
			}

			@Override
			protected void onError(AjaxRequestTarget target) {
				GlobalAjaxButton.this.onError(target,
						GlobalAjaxButton.this.getForm());
				clear(target);
			}

			@Override
			protected void updateAjaxAttributes(AjaxRequestAttributes attributes) {
				super.updateAjaxAttributes(attributes);
				GlobalAjaxButton.this.updateAjaxAttributes(attributes);
			}

			public boolean getDefaultProcessing() {
				return GlobalAjaxButton.this.getDefaultFormProcessing();
			}
		};
	}

	private void clear(AjaxRequestTarget target) {
		target.appendJavaScript("$(\"#global-ajax-indicator\").css('display', 'none')");
		target.appendJavaScript("$(\"#global-logo\").css('display', 'block')");
		target.appendJavaScript("initAjax()");
	}

}
