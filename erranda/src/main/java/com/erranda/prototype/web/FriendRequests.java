package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.push.AbstractPushEventHandler;
import org.wicketstuff.push.IPushEventContext;
import org.wicketstuff.push.IPushNode;
import org.wicketstuff.push.timer.TimerPushService;

import com.erranda.prototype.App;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;

public class FriendRequests extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;

	LazyLoaderPanel lazy;

	int skip = 0;
	int limit = 100;

	public FriendRequests(String id) {
		super(id);

		final FriendRequestNotificationCount jewel = new FriendRequestNotificationCount(
				"friend-request-count");
		
		final IPushNode<String> pushNode =  TimerPushService.get().installNode(jewel,
				new AbstractPushEventHandler<String>()
				{
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					public void onEvent(final AjaxRequestTarget target, final String event,
						final IPushNode<String> node, final IPushEventContext<String> ctx)
					{
						target.add(jewel);
					}
		});
		
		((App) App.get()).registerFriendRequestPanel(UserSession.get().getPerson().getId(), pushNode);

		final AjaxLink<String> notificationsLink = new AjaxLink<String>(
				"friend-requests-link") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					controller.resetUnseenFriendRequestCount(UserSession.get()
							.getPerson().getId());
					target.add(jewel);
					lazy.update(target);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block //Global error handler
					e.printStackTrace();
				}
			}

		};

		notificationsLink.add(new Behavior() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$('#" + notificationsLink.getMarkupId()
						+ "').bind('click', function(){" + "$('#"
						+ jewel.getMarkupId() + "').hide();" + "});";
				response.render(OnDomReadyHeaderItem.forScript(script));

			}

		});

		lazy = new LazyLoaderPanel("lazy-friend-requests",
				new FriendRequestsList("content"));
		add(jewel, notificationsLink, lazy);
	}

}
