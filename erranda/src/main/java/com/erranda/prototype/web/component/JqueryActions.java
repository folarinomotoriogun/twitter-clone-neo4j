package com.erranda.prototype.web.component;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;

public class JqueryActions {

	public JqueryActions() {
		// TODO Auto-generated constructor stub
	}

	public static void slideHide(Component c, AjaxRequestTarget target) {
		String script = String.format(" $('#%s').slideUp();", c.getMarkupId(),
				c.getMarkupId());
		target.appendJavaScript(script);
	}

	public static void slideOpen(Component c, AjaxRequestTarget target) {
		String script = String.format(" $('#%s').slideDown();",
				c.getMarkupId(), c.getMarkupId());
		target.prependJavaScript(script);
	}

	public static void fadeIn(Component c, AjaxRequestTarget target) {
		String script = String.format(" $('#%s').fadeIn('slow');",
				c.getMarkupId());
		target.appendJavaScript(script);
	}
	
	public static void highlight (Component c, AjaxRequestTarget target) {
		String script = String.format(" $('#%s').effect('highlight', 'slow');",
				c.getMarkupId());
		target.appendJavaScript(script);
	}

}
