package com.erranda.prototype.web;

import java.util.List;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class MutualFriends extends Panel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PlatformUser session = UserSession.get().getPerson();
	
	@SpringBean
	Controller controller;
	
	int skip = 0;
	
	int limit = 10;
	
	int infiniteSkip = 0;
	
	int infiniteLimit = 4;

	private String listViewMarkupId;
	
	private Integer cursor = 0;

	public MutualFriends(String id, final Long rid) {
		super(id);
		
		LoadableDetachableModel<List<PlatformUser>> kudosPeople = new LoadableDetachableModel<List<PlatformUser>> () {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected List<PlatformUser> load() {
				try {
					PlatformUser session = UserSession.get().getPerson();
					return controller.mutualFriends(session.getId(), rid, infiniteSkip, infiniteLimit);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
			
		};
		
		WebMarkupContainer container = new WebMarkupContainer("items-container");
		container.setOutputMarkupId(true);
		listViewMarkupId = container.getMarkupId();
		
		final ListView<PlatformUser> people = new ListView<PlatformUser> ("people", kudosPeople) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<PlatformUser> item) {
				item.add(new SearchResult ("this", item.getModel()));
			}
		};
		people.setOutputMarkupId(true);
		
		final AjaxLink<PlatformUser> moreButton = new AjaxLink<PlatformUser>("more-items") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
				infiniteSkip = infiniteSkip + infiniteLimit;
				List<PlatformUser> results = people.getModel().getObject();
				if (cursor == 0)
					cursor = results.size();
				List<PlatformUser> moreFeed = controller.mutualFriends(session.getId(), rid, infiniteSkip, infiniteLimit);
				for (int i = 0; i < moreFeed.size(); i++) {
					cursor++;
					ListItem<PlatformUser> item = new ListItem<PlatformUser>(new Integer(
							cursor).toString(), cursor);
					item.add(new SearchResult("this", Model.of(moreFeed.get(i))));
					item.setOutputMarkupId(true);
					people.add(item);
					target.prependJavaScript(String
							.format("var item=document.createElement('%s');item.class='original-tweet-container'; item.id='%s';"
									+ "$('#%s').append(item);", "li",
									item.getMarkupId(), listViewMarkupId));
					target.add(item);
				}
				if (controller.mutualFriends(session.getId(), rid, infiniteSkip + infiniteLimit, 1).size() == 0) {
					String script3 = "$('#" + this.getMarkupId()
							+ "').unbind('click');";
					target.appendJavaScript(script3);
					String script4 = "$('#relatioship-ajax-loader').hide();";
					target.appendJavaScript(script4);
				} else {
					String script3 = "$('#relatioship-ajax-loader').show();";
					target.appendJavaScript(script3);
				}
				target.add(this);
				} catch (ControllerException e) {
					e.printStackTrace();
				}
			}

		};
		
		add (moreButton, container.add(people));
	}

}
