package com.erranda.prototype.web.component;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.attributes.AjaxRequestAttributes;
import org.apache.wicket.ajax.form.AjaxFormSubmitBehavior;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.Form;

public class DisablingIndicatingAjaxButton extends IndicatingAjaxButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DisablingIndicatingAjaxButton(String id, Form form) {
		super(id, form);
	}

	@Override
	public void onInitialize() {
		super.onInitialize();
		add(new AjaxEventBehavior("click") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$('#"
						+ DisablingIndicatingAjaxButton.this.getMarkupId()
						+ "').bind('click', function(){"
						+ "$(this).attr('disabled', 'disabled')});";
				response.render(OnDomReadyHeaderItem.forScript(script));

			}

			@Override
			protected void onEvent(AjaxRequestTarget target) {

			}

		});
	}

	@Override
	protected AjaxFormSubmitBehavior newAjaxFormSubmitBehavior(String event) {
		return new AjaxFormSubmitBehavior(getForm(), event) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit(AjaxRequestTarget target) {
				DisablingIndicatingAjaxButton.this.onSubmit(target,
						DisablingIndicatingAjaxButton.this.getForm());
				clear(target);
			}

			@Override
			protected void onAfterSubmit(AjaxRequestTarget target) {
				DisablingIndicatingAjaxButton.this.onAfterSubmit(target,
						DisablingIndicatingAjaxButton.this.getForm());
				clear(target);
			}

			@Override
			protected void onError(AjaxRequestTarget target) {
				DisablingIndicatingAjaxButton.this.onError(target,
						DisablingIndicatingAjaxButton.this.getForm());
				clear(target);
			}

			@Override
			protected void updateAjaxAttributes(AjaxRequestAttributes attributes) {
				super.updateAjaxAttributes(attributes);
				DisablingIndicatingAjaxButton.this
						.updateAjaxAttributes(attributes);
			}

			@Override
			public boolean getDefaultProcessing() {
				return DisablingIndicatingAjaxButton.this
						.getDefaultFormProcessing();
			}
		};
	}

	private void clear(AjaxRequestTarget target) {
		String script = "$('#"
				+ DisablingIndicatingAjaxButton.this.getMarkupId()
				+ "').removeAttr('disabled')";
		target.appendJavaScript(script);

	}

}
