package com.erranda.prototype.web.mailer;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.model.Model;

import com.erranda.prototype.Config;
import com.erranda.prototype.domain.PlatformUser;

public class EmailForgotPassword extends WebPage {

	private static final long serialVersionUID = 1L;

	public EmailForgotPassword() {
	}

	public EmailForgotPassword(PlatformUser person) {
		ExternalLink profile = new ExternalLink("confirmLink",
				Model.of(Config.SET_PASSWORD_LINK + "/"
						+ person.getForgotPassword()));
		Label name = new Label("name", Model.of(person.getName().split(" ")[0]
				+ ", you requested to change your password?"));
		add(profile, name);
	}
}
