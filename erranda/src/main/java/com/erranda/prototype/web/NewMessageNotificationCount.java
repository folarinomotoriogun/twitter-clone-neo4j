package com.erranda.prototype.web;

import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;

public class NewMessageNotificationCount extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	private Controller controller;

	public Integer count = 0;

	Label countLabel;

	public NewMessageNotificationCount(String id) {
		super(id);
		this.setOutputMarkupId(true);
		countLabel = new Label("count", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				try {
					return controller.newMessagesCount(
							UserSession.get().getPerson().getId()).toString();
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return "0";
			}

		}) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(IHeaderResponse response) {
				super.renderHead(response);
				response.render(OnDomReadyHeaderItem
						.forScript("if ($('#new-message-notification-jewel-count').html() != 0){$('#new-message-notification-jewel').css ('display', 'block')}"));
			}
		};

		setOutputMarkupId(true);
		add(countLabel);
	}

}
