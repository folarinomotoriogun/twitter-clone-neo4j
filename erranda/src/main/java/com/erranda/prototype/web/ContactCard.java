package com.erranda.prototype.web;

import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;

public class ContactCard extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	PlatformUserRepository personRepo;

	public ContactCard(String id, final Long pid) {
		super(id);
		ExternalLink nameLink = new ExternalLink("user-name-link",
				(new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						PlatformUser person = personRepo.findOne(pid);
						return "\\" + person.getUsername();
					}
				}), (new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						PlatformUser person = personRepo.findOne(pid);
						return person.getName();
					}
				}));

		ExternalLink usernameLink = new ExternalLink("user-name-link-username",
				(new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						PlatformUser person = personRepo.findOne(pid);
						return "\\" + person.getUsername();
					}
				}), (new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						PlatformUser person = personRepo.findOne(pid);
						return "@<span class=\"u-linkComplex-target\">"
								+ person.getUsername() + "</span>";
					}
				}));

		ExternalLink errandLink = new ExternalLink("errand-link",
				(new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						PlatformUser person = personRepo.findOne(pid);
						return "\\" + person.getUsername();
					}
				}), (new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						return "<span class=\"DashboardProfileCard-statLabel u-block\">Errands</span><span class=\"DashboardProfileCard-statValue\">"
								+ personRepo.countErrands(pid) + "</span>";
					}
				}));

		ExternalLink followingLink = new ExternalLink("following-link",
				(new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						PlatformUser person = personRepo.findOne(pid);
						return "\\" + person.getUsername() + "\following";
					}
				}), (new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						return "<span class=\"DashboardProfileCard-statLabel u-block\">Following</span><span class=\"DashboardProfileCard-statValue\">"
								+ personRepo.countFollowing(pid) + "</span>";
					}
				}));

		ExternalLink followersLink = new ExternalLink("followers-link",
				(new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						PlatformUser person = personRepo.findOne(pid);
						return "\\" + person.getUsername() + "\followers";
					}
				}), (new Model<String>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					public String getObject() {
						return "<span class=\"DashboardProfileCard-statLabel u-block\">Followers</span><span class=\"DashboardProfileCard-statValue\">"
								+ personRepo.countFollowers(pid) + "</span>";
					}
				}));

		add(nameLink.setEscapeModelStrings(false));
		add(usernameLink.setEscapeModelStrings(false));
		add(errandLink.setEscapeModelStrings(false));
		add(followersLink.setEscapeModelStrings(false));
		add(followingLink.setEscapeModelStrings(false));
	}

	public ContactCard(String id, IModel<?> model) {
		super(id, model);
	}

}
