package com.erranda.prototype.web;

import java.util.Collections;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.protocol.http.ClientProperties;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.ErrandControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostViewItem;

public class NewsFeedPanel extends Panel {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private Controller controller;

	private int skip;

	private int limit;

	private String listViewMarkupId = null;

	private Integer cursor;

	private Long pid;

	ListView<PostViewItem> newsFeedList = null;

	private String type;

	private PlatformUser session = UserSession.get().getPerson();

	final ModalContainer modalContainer;

	final Authenticated page;

	@Override
	public void onInitialize() {
		super.onInitialize();

		skip = 0;
		limit = 20;
		cursor = 0;
		
		new LoadableDetachableModel<PlatformUser>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected PlatformUser load() {
				try {
					return controller.findPerson(pid);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return new PlatformUser();
				}
			}

		};
		final ClientProperties cp = page.cp;
		WebMarkupContainer container = new WebMarkupContainer("items-container");
		container.setOutputMarkupId(true);

		listViewMarkupId = container.getMarkupId();
		
		newsFeedList = new ListView<PostViewItem>("repeater",
				new LoadableDetachableModel<List<PostViewItem>>() {
					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<PostViewItem> load() {
						try {
							PlatformUser person = controller.findPerson(pid);
							if (type.equals("all"))
								return controller.interestFeed(person.getId(),
										skip, limit);
							else if (type.equals("person")) {
									return controller.personFeed(
											person.getId(), session.getId(),
											skip, limit);
							}

						} catch (ErrandControllerException e) {
							error(e.getMessage());
							e.printStackTrace();
						} catch (ControllerException e) {
							error(e.getMessage());
							e.printStackTrace();
							throw new RuntimeException();
						}
						return Collections.emptyList();
					}
				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<PostViewItem> item) {
				PostViewItem viewItem = item.getModelObject();
				Post post = item.getModelObject().post();
				Errand errand = post.getErrand();
				item.setOutputMarkupId(true);
				IModel<Errand> model = Model.of(errand);
				WebMarkupContainer meta = null;
				try {
					if (viewItem.relationship().equals("NOMINATED")  && !type.equalsIgnoreCase("person")) {
						item.add(meta = new PostNominateMeta ("meta", model));
					}
					else if (viewItem.relationship().equals("NOMINATED")  && type.equalsIgnoreCase("person")) {
						item.add(meta = new PostNominatePersonMeta("meta", model, NewsFeedPanel.this.pid));
					}
					else if (viewItem.relationship().equals("BROADCASTED")) {
						item.add(meta = new PostBroadcastMeta("meta", model));
					}
					else {
						item.add(meta = new PostOwnerNominateMeta ("meta", Model.of(post)));
					}
					item.add(new ErrandSingle("this-post", model, page, cp, meta, item));
				} catch (ControllerException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		};
		newsFeedList.setOutputMarkupId(true);
		try {
		if (page instanceof Home
				|| (page instanceof PersonProfile && UserSession.get()
						.getPerson().getId().equals(pid))) {
			add(new PostErrandForm("header", newsFeedList, cp,
					listViewMarkupId, null));		
		} else {
			add(new PostErrandForm("header", newsFeedList, cp,
					listViewMarkupId, pid));
		}
		} catch (ControllerException e) {
			e.printStackTrace();
			throw new RestartResponseAtInterceptPageException (ErrorPage.class);
		}
		if (newsFeedList.getModel() != null && newsFeedList.getModel().getObject().size() == 0)
			newsFeedList.setVisible(false);

		final AjaxLink<PlatformUser> moreButton = new AjaxLink<PlatformUser>("more-items") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				List<PostViewItem> errands = newsFeedList.getModel().getObject();
				if (cursor == 0)
					cursor = errands.size();
				try {
					List<PostViewItem> moreFeed = null;
					if (type.equals("all")) {
						skip = skip + limit;
						moreFeed = controller.interestFeed(pid, skip, limit);
						if (moreFeed.size() != limit) {
							String script2 = "$('#footer-logo').css('display', 'block');";
							String script4 = "$('#more-item-loader').css('display', 'none');";
							String script3 = "$('#" + this.getMarkupId()
									+ "').unbind();";
							String script5 = "$('#stream-end').html('That\\'s all we got')";
							target.appendJavaScript(script2);
							target.appendJavaScript(script3);
							target.appendJavaScript(script4);
							target.appendJavaScript(script5);
						}
					} else {
							skip = skip + limit;
							moreFeed = controller.personFeed(pid,
									session.getId(), skip, limit);
							if (moreFeed.size() != limit) {
								String script2 = "$('#footer-logo').css('display', 'block');";
								String script4 = "$('#more-item-loader').css('display', 'none');";
								String script3 = "$('#" + this.getMarkupId()
										+ "').unbind();";
								target.appendJavaScript(script2);
								target.appendJavaScript(script3);
								target.appendJavaScript(script4);
							}
						}

					for (int i = 0; i < moreFeed.size(); i++) {
						cursor++;
						cursor++;
						
						ListItem<PostViewItem> item = new ListItem<PostViewItem>(new Integer(
								cursor).toString(), cursor);
						PostViewItem viewItem = moreFeed.get(i);
						Post post = viewItem.post();
						Errand errand = viewItem.post().getErrand();
						WebMarkupContainer meta = null;
						Model<Errand> model = Model.of(errand);
						if (viewItem.relationship().equals("NOMINATED")  && !type.equalsIgnoreCase("person")) {
							item.add(meta = new PostNominateMeta ("meta", model));
						}
						else if (viewItem.relationship().equals("NOMINATED")  && type.equalsIgnoreCase("person")) {
							item.add(meta = new PostNominatePersonMeta("meta", model, NewsFeedPanel.this.pid));
						}
						else if (viewItem.relationship().equals("BROADCASTED")) {
							item.add(meta = new PostBroadcastMeta("meta", model));
						}
						else {
							item.add(meta = new PostOwnerNominateMeta ("meta", Model.of(post)));
						}
						item.add(new ErrandSingle("this-post", model, page, cp, meta, item));
						item.setOutputMarkupId(true);
						newsFeedList.add(item);
						target.prependJavaScript(String
								.format("var item=document.createElement('%s');item.class='original-tweet-container'; item.id='%s';"
										+ "$('#%s').append(item);", "li",
										item.getMarkupId(), listViewMarkupId));
						target.add(item);
					}

					target.add(this);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
			}

		};

		moreButton.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$(window).scroll(function() {"
						+ "if($(window).scrollTop() + $(window).height() > $(document).height() / 2) {"
						+ "$('#" + component.getMarkupId()
						+ "').trigger ('click');}});";
				String script2 = "$('#"
						+ component.getMarkupId()
						+ "').click(function(){$('#"
						+ component.getMarkupId()
						+ "').unbind('click');$('#footer-logo').css('display', 'none');$('#more-item-loader').css('display', 'block');})";
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript(script2));
			}
		});

		add(container.add(newsFeedList), moreButton);
		addFooter(newsFeedList, type);

	}

	public NewsFeedPanel(String id, Long pid,
			final ModalContainer modalContainer, final String type,
			final Authenticated page) {
		super(id);
		this.pid = pid;
		this.modalContainer = modalContainer;
		this.type = type;
		this.page = page;
	}

	private void addFooter(ListView<PostViewItem> newsFeedList2, final String type) {
		int size = newsFeedList2.getList().size();
		Label emptyTimeLine = new Label("empty");
		add(emptyTimeLine);

		if (size == 0) {
			emptyTimeLine
					.setDefaultModel(new LoadableDetachableModel<Object>() {

						/**
			 * 
			 */
						private static final long serialVersionUID = 1L;

						@Override
						protected Object load() {
							if (!type.equals("person")) {
								return "";
							} else if (pid != UserSession.get().getPerson()
									.getId()) {
								return "";
							} else
								return "";
						}

					});
		} else {
			emptyTimeLine.setDefaultModel(Model.of(""));
		}
		add(emptyTimeLine);
	}

	public void p(Object o) {
		System.out.println(o);
	}

}
