package com.erranda.prototype.web;

import java.util.List;

import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.FriendRequest;

public class FriendRequestsList extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;

	int skip = 0;
	int limit = 100;

	public FriendRequestsList(String id) {
		super(id);

		ListView<FriendRequest> friendRequests = new ListView<FriendRequest>(
				"friend-requests",
				new LoadableDetachableModel<List<FriendRequest>>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<FriendRequest> load() {

						try {
							List<FriendRequest> friendRequests = controller
									.getFriendRequestsByPerson(UserSession
											.get().getPerson().getId(), skip,
											limit);
							return friendRequests;
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}

					}

				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<FriendRequest> item) {
				item.setOutputMarkupId(true);
				item.add(new FriendRequestItem("item", item.getModelObject(),
						item.getMarkupId()));
			}

		};
		add(friendRequests);
	}

}
