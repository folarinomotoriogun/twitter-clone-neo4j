package com.erranda.prototype.web;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Proposal;
import com.erranda.prototype.web.component.NameScramble;
import com.erranda.prototype.web.component.TimeConverter;

public class ProposalSingle extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1061015651699631884L;
	@SpringBean
	Controller controller;

	public ProposalSingle(String id, final Long pid) throws ControllerException {
		super(id);
		Label name = new Label("name", new LoadableDetachableModel<String>() {

			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				// Important if user uses the back button and the cache is empty
				Proposal p;
				try {
					p = controller.findOneProposal(pid);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
				return anonymizePerson(p);
			}

		});
		name.setEscapeModelStrings(false);

		Label time = new Label("time", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				Proposal post;
				try {
					post = controller.findOneProposal(pid);
				} catch (ControllerException e) {
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
				String[] time = TimeConverter.convertTime(post.getCtime(),
						((Authenticated) getPage()).cp).split("#");
				String timeShort = "<a style=\"float:right\" title = '"
						+ time[1]
						+ "'"
						+ " href=\"#\" class=\"tweet-timestamp js-permalink js-nav js-tooltip\"><span class=\"_timestamp js-short-timestamp js-relative-timestamp\">"
						+ time[0] + "</span></a>";
				return timeShort;
			}

		});

		time.setEscapeModelStrings(false);

		final Label details = new Label("details",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						Proposal post;
						try {
							post = controller.findOneProposal(pid);
							String details = "<div class=\"errand-details-container\">"
									+ "<span class=\"line\"><strong class=\"errand-details\">"
									+ post.getMessage()
									+ "</strong></span>"
									+ "</div>";
							return details;
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return "";

					}

				});
		details.setEscapeModelStrings(false);
		add(details, name, time);
		add(new ConfirmProposal("confirm-proposal-panel", pid));
	}

	private String anonymizePerson(Proposal post) {
		PlatformUser session = UserSession.get().getPerson();
		PlatformUser postOwner = post.getPerson();
		String userUrl = "";
		String name = "";
		String src = "";
		if (session.equals(post.getPerson())
				|| session.equals(post.getReceiver())) {
			userUrl = Config.PROFILE_NAME_LINK + "/"
					+ post.getPerson().getUsername();
			name = (session.equals(postOwner) ? "You" : postOwner.getName());
			src = postOwner.getProfilePictureUrl();
		} else {
			userUrl = "#";
			name = NameScramble.scramble(postOwner.getName());
			src = new PlatformUser().getProfilePictureUrl();
		}

		String userdetails = "<a class='account-group js-account-group js-action-profile js-user-profile-link js-nav' href='"
				+ userUrl
				+ "'>"
				+ "<strong class='fullname js-action-profile-name'>"
				+ name
				+ "</strong><span><s>@</s><b><p class='errand-tag'>"
				+ postOwner.getLocalityName()
				+ "</p></b></span><div class='DMInboxItem-avatar' style='margin-left: 25px'>"
				+ "<img class='avatar js-action-profile-avatar' src='"
				+ src
				+ "'></div></a>";
		return userdetails;
	}
}
