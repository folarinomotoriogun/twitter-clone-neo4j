package com.erranda.prototype.web.component;

public abstract class HtmlDecorator {
	
	public static String countReadable (Integer count) {
		Double val = count * 1.00;
		if (count > 1000 && count < 1000000) {
			Double newVal = (val / 1000.00); 
			return newVal.toString().substring(0, 3) + "K";
		} else if (count > 1000000) {
			Double newVal = (val / 1000000.00); 
			return newVal.toString().substring(0, 3) + "M";
		} else if (count > 1000000000) {
			Double newVal = (val / 1000000000.00); 
			return newVal.toString().substring(0, 3) + "B";
		}
		return count.toString();
	}

}
