package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.DisablingIndicatingAjaxButton;

public class UpdateLocation extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public UpdateLocation(String id) {
		super(id);
		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);

		final PlatformUser person = UserSession.get().getPerson();

		final Form<String> form = new Form<String>("form");

		TextField<String> currentLocation = new TextField<String>(
				"current-location", new PropertyModel<String>(person,
						"localityName"));
		form.add(currentLocation);
		form.add(feedback);

		form.add(new DisablingIndicatingAjaxButton("submit", form) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				PlatformUser session = UserSession.get().getPerson();
				person.setId(session.getId());
				try {
					controller.updateLocation(person.getId(),
							person.getLocalityName());
					String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Success', content: '%s', placement: 'right'}).popover('show'); setTimeout (function () {$('#%s').popover('hide')}, 2000);", this.getMarkupId() , "Your location has been updated to " + person.getLocalityName(), this.getMarkupId());
					target.appendJavaScript(errorScript);
				} catch (ControllerException e) {
					String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show');", this.getMarkupId() , e.getMessage());
					target.appendJavaScript(errorScript);
					e.printStackTrace();
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(feedback);
			}
		});

		add(form);
	}

}
