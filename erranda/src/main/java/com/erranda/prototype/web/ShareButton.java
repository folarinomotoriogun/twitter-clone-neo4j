package com.erranda.prototype.web;

import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class ShareButton extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ShareButton(String id, String url) {
		super(id);
		ExternalLink facebook = new ExternalLink("facebook",
				Model.of(encodeFacebook(url)));
		ExternalLink linkedin = new ExternalLink("linkedin",
				Model.of(encodeLinkedIn(url)));
		ExternalLink googleplus = new ExternalLink("gplus",
				Model.of(encodeGooglePlus(url)));
		ExternalLink twitter = new ExternalLink("twitter",
				Model.of(encodeTwitter(url)));
		add(facebook, linkedin, googleplus, twitter);
	}

	public ShareButton(String id, IModel<?> model) {
		super(id, model);

	}

	private String encodeFacebook(String url) {
		return "https://www.facebook.com/sharer/sharer.php?u=" + url;
	}

	private String encodeLinkedIn(String url) {
		return "http://www.linkedin.com/shareArticle?mini=true&url=" + url;
	}

	private String encodeTwitter(String url) {
		return "http://twitter.com/home?status=" + url;
	}

	private String encodeGooglePlus(String url) {
		return "https://plus.google.com/share?url=" + url;
	}

	

}
