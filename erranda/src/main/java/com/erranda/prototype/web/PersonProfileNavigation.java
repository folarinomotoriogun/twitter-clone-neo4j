package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.HtmlDecorator;

public class PersonProfileNavigation extends Panel {

	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;
	
	PlatformUser session = UserSession.get().getPerson();

	public PersonProfileNavigation(String id, final PersonProfile page, final IModel<PlatformUser> model, String panelId) {
		super(id);

		final WebMarkupContainer errandsLink = new WebMarkupContainer(
				"errands-link");
		errandsLink.add(new Behavior() {

			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component c, IHeaderResponse response) {
				String script = String.format("$('#%s').click (function () {document.location = '%s'});", c.getMarkupId(), Config.PROFILE_NAME_LINK + "/" + model.getObject().getUsername());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		errandsLink.add(AttributeModifier.append("class",
				"global-ajax-action"));
		errandsLink.setOutputMarkupId(true);

		final WebMarkupContainer friendsLink = new WebMarkupContainer(
				"friends-link");
		friendsLink.add(new Behavior() {

			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component c, IHeaderResponse response) {
				String script = String.format("$('#%s').click (function () {document.location = '%s/friends'});", c.getMarkupId(), Config.PROFILE_NAME_LINK + "/" + model.getObject().getUsername());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		final WebMarkupContainer followingLink = new WebMarkupContainer(
				"following-link");
		followingLink.add(new Behavior() {

			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component c, IHeaderResponse response) {
				String script = String.format("$('#%s').click (function () {document.location = '%s/following'});", c.getMarkupId(), Config.PROFILE_NAME_LINK + "/" +  model.getObject().getUsername());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		
		final WebMarkupContainer followersLink = new WebMarkupContainer(
				"followers-link");
		followersLink.add(new Behavior() {

			/**
					 * 
					 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component c, IHeaderResponse response) {
				String script = String.format("$('#%s').click (function () {document.location = '%s/followers'});", c.getMarkupId(), Config.PROFILE_NAME_LINK + "/" + model.getObject().getUsername());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		
		if (panelId.equals("friends"))
			friendsLink
			.add(AttributeModifier.append("class", "active"));
		else if (panelId.equals("timeline")) {
			errandsLink
			.add(AttributeModifier.append("class", "active"));
		}else if (panelId.equals("followers")) {
			followersLink
			.add(AttributeModifier.append("class", "active"));
		}else if (panelId.equals("following")) {
			followingLink
			.add(AttributeModifier.append("class", "active"));
		}
		friendsLink
				.add(AttributeModifier.append("class", ""));

		Label friendsLabel = new Label("friends-label",
				new LoadableDetachableModel<Object>() {

					private static final long serialVersionUID = 1L;

					@Override
					protected Object load() {
						Integer count;
						try {
							count = controller.countFriends(page.pid);
							return "Friends "
									+ (count == 0 ? "" : "(" + HtmlDecorator.countReadable(count) + ")");
						} catch (ControllerException e) {
							e.printStackTrace();
							throw new RestartResponseAtInterceptPageException(
									Home.class);
						}

					}

				});
		Label followersLabel = new Label("label",
				new LoadableDetachableModel<Object>() {

					private static final long serialVersionUID = 1L;

					@Override
					protected Object load() {
						Integer count;
						try {
							count = controller.countFollowers(session.getId(), page.pid);
							return "Followers "
									+ (count == 0 ? "" : "(" + HtmlDecorator.countReadable(count) + ")");
						} catch (ControllerException e) {
							e.printStackTrace();
							throw new RestartResponseAtInterceptPageException(
									Home.class);
						}

					}

				});
		Label followingLabel = new Label("label",
				new LoadableDetachableModel<Object>() {

					private static final long serialVersionUID = 1L;

					@Override
					protected Object load() {
						Integer count;
						try {
							count = controller.countFollowing(session.getId(), page.pid);
							return "Following "
									+ (count == 0 ? "" : "(" + HtmlDecorator.countReadable(count) + ")");
						} catch (ControllerException e) {
							e.printStackTrace();
							throw new RestartResponseAtInterceptPageException(
									Home.class);
						}

					}

				});
		if (!model.getObject().getType().equals(Config.PLATFORM_TYPE_PERSON))
			add(new WebMarkupContainer("friends-link").setVisible(false));
		else {
			add(friendsLink.add(friendsLabel));
		}
		add(errandsLink, followingLink.add(followingLabel), followersLink.add(followersLabel));
	}

	public PersonProfileNavigation(String id, IModel<?> model) {
		super(id, model);
	}


}
