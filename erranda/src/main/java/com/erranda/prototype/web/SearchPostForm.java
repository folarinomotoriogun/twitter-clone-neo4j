package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

public class SearchPostForm extends Panel {

	public SearchPostForm(String id, String currentKey) {
		super(id);
		final WebMarkupContainer current = new WebMarkupContainer("current");
		current.add(AttributeModifier.replace("name", "query"));
		current.add(AttributeModifier.replace("value", currentKey));
		final Button submit1 = new Button("submit");
		current.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13) {$('#%s').click();} })});",
								current.getMarkupId(),
								submit1.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		submit1.add(AttributeModifier.remove("name"));
		add(current, submit1);
	}

}
