package com.erranda.prototype.web.mailer;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.model.Model;

import com.erranda.prototype.Config;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.StaticImage;

public class EmailNewAction extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmailNewAction(PlatformUser sender, String action, String reply) {
		StaticImage image = new StaticImage("image", Model.of(Config.DOMAIN_NAME + "" + sender
				.getProfilePictureUrl()));
		Label details = new Label("details", action);
		details.setEscapeModelStrings(false);
		ExternalLink replyAction = new ExternalLink("reply", Model.of(reply));
		add(image, details, replyAction);
	}

}
