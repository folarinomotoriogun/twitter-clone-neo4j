package com.erranda.prototype.web;

import java.util.Arrays;
import java.util.List;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostViewItem;
import com.erranda.prototype.web.component.DisablingAjaxButton;
import com.erranda.prototype.web.component.JqueryActions;

@SuppressWarnings("unused")
public class BlockReportForm extends Panel {

	@SpringBean
	Controller controller;

	private static final long serialVersionUID = 1L;

	private static final List<String> TYPES = Arrays.asList(new String[] {
			"This post is annoying", "This post is spam",
			"Contains private information", "Shows abuse" });

	private String selected = "This post is annoying";

	private Boolean shouldBlockOwner;

	private String details;

	public BlockReportForm(final String id, final Long eid, final Long ownerid,
			final ListItem<PostViewItem> item) {
		super(id);

		RadioChoice<String> reportType = new RadioChoice<String>("report-type",
				new PropertyModel<String>(this, "selected"), TYPES);
		reportType.setVisible(false);

		final CheckBox blockOwner = new CheckBox("block-owner",
				new PropertyModel<Boolean>(this, "shouldBlockOwner"));

		Label ownerName = new Label("owner-name",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						try {
							if (controller.findErrand(eid).getAnonymous())
								return "Post Owner";
							return controller.findPerson(ownerid).getName();
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}
					}

				});

		TextArea<String> detailsArea = new TextArea<String>("details",
				new PropertyModel<String>(this, "details"));

		final Form<String> blockForm = new Form<String>("report-form");
		blockForm.add(new DisablingAjaxButton("submit", blockForm) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				Long pid = UserSession.get().getPerson().getId();
				try {
					if (shouldBlockOwner) {
						controller.blockPerson(pid, ownerid);
					}
					controller.reportPost(pid, eid, selected, details);
					if (item != null) {
						JqueryActions.slideHide(item, target);
						target.appendJavaScript("$('#bigModal').modal('hide');");
					}
					else
						throw new RestartResponseAtInterceptPageException (Home.class);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		add(blockForm);
		blockForm.add(reportType, blockOwner, ownerName, detailsArea);

	}
}