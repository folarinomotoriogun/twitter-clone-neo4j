package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.SuggestionObject;
import com.erranda.prototype.web.component.JqueryActions;
import com.erranda.prototype.web.component.StaticImage;

public class Suggestion extends Panel {

	private static final long serialVersionUID = 1L;
	
	@SpringBean
	Controller controller;
	
	PlatformUser session = UserSession.get().getPerson();

	public Suggestion(String id, final IModel<SuggestionObject> model, final ListItem<SuggestionObject> item) {
		super(id, model);
		PlatformUser person = model.getObject().person;
		Label name = new Label("name",
				new PropertyModel<String>(person, "name"));
		Label username = new Label("username", new PropertyModel<String>(
				person, "username"));
		Label socialMetaData = new Label("metadata", new PropertyModel<String>(
				model.getObject(), "metaData"));
		ExternalLink link = new ExternalLink("link", new PropertyModel<String>(
				person, "link"));
		StaticImage thumbNail = new StaticImage("thumbNail",
				new PropertyModel<String>(person, "thumbNail"));
		MiniUserActions socialAction = new MiniUserActions("social-action",
				Model.of(model.getObject().person));
		add(link.add(name, username, thumbNail), socialMetaData, socialAction);
		
		final AjaxLink <String> dontRecommend = new AjaxLink <String> ("hide") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				controller.dontRecommendPerson(session.getId(), model.getObject().person.getId());
				JqueryActions.slideHide(item, target);
			}
		};
		add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				// Scroll to bottom each time
				String script = String.format("$('#%s').click(function () {$('#%s').slideUp()});", dontRecommend.getMarkupId(), item.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));

			}

		});
		
		add (dontRecommend);
	}
}
