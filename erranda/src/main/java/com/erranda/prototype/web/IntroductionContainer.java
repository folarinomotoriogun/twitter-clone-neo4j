package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.Panel;

public class IntroductionContainer extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3624180352885273032L;

	public IntroductionModalHeaderPanel header;

	public IntroductionModalBodyPanel body;

	public IntroductionContainer(String id) {
		super(id);
		header = new IntroductionModalHeaderPanel("header");
		header.setOutputMarkupId(true);
		body = new IntroductionModalBodyPanel("body", this);
		body.setOutputMarkupId(true);
		add(header);
		add(body);
	}

	public void update(AjaxRequestTarget target, String script, boolean show) {
		target.prependJavaScript("$('#modal-ajax').hide();");
		target.add(header);
		target.add(body);
		if (show)
			target.appendJavaScript("$('#show-introduction-modal-link').click();"
					+ script);
	}

	public void show(Component headerC, Component bodyC) {
		header.removeAll();
		header.add(headerC);
		body.removeAll();
		body.add(bodyC);
	}

}
