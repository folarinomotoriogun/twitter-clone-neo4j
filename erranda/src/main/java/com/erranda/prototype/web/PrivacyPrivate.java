/**
 * 
 */
package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * @author Folarin
 *
 */
public class PrivacyPrivate extends Panel {

	/**
	 * @param id
	 */
	public PrivacyPrivate(String id, final ListItem<String> item,
			final String buttonId, final String inputField) {
		super(id);
		setOutputMarkupId(true);
		add(new Behavior() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String html = htmlData();
				String script = String
						.format("$('#%s').bind('click', function(){$('#%s').html('%s'); $('#%s').val('%s')});",
								item.getMarkupId(), buttonId, html, inputField,
								"PRIVATE");
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
	}

	public static String htmlData() {
		return "<span class=\"fa fa-lock privacy-icon\"></span><span class=\"privacy-text\">Private (+)</span>";
	}

	/**
	 * @param id
	 * @param model
	 */
	public PrivacyPrivate(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
