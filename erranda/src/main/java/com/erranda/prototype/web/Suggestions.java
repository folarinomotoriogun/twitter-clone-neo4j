package com.erranda.prototype.web;

import java.util.List;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.SuggestionObject;

public class Suggestions extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	int skip = 0;
	int limit = 10;

	public Suggestions(String id) {
		super(id);

		ListView<SuggestionObject> suggestions = new ListView<SuggestionObject>(
				"suggested",
				new LoadableDetachableModel<List<SuggestionObject>>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<SuggestionObject> load() {
						PlatformUser person = UserSession.get().getPerson();
						try {
							return controller.friendSuggestions(person.getId(),
									skip, limit);
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}

					}

				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<SuggestionObject> item) {
				item.setOutputMarkupId(true);
				item.add(new Suggestion("this", item.getModel(),item));
			}

		};

		WebMarkupContainer infiniteScroll = new WebMarkupContainer(
				"infinite-scroll");
		add(infiniteScroll.add(suggestions));
	}

}
