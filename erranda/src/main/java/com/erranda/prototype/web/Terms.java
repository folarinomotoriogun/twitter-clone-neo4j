package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.cookies.CookieUtils;
import org.apache.wicket.validation.validator.StringValidator;

import com.erranda.prototype.Config;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.StatelessIndicatingAjaxButton;


public class Terms extends WebPage {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9148290804119211346L;
	

	@SpringBean
	Controller controller;

	private boolean rememberMe = true;

	public Terms() {
		final PlatformUser person = new PlatformUser();
		final FeedbackPanel lfd = new FeedbackPanel("login-fd");
		lfd.setOutputMarkupId(true);
		lfd.setMarkupId("login-feedback");
		
		StatelessForm<PlatformUser> login = new StatelessForm<PlatformUser>("login");
		login.add(lfd);
		
		final TextField<String> emailLogin = new TextField<String>("email",
				new PropertyModel<String>(person, "email"));
		emailLogin.setOutputMarkupId(true);
		emailLogin.setLabel(new ResourceModel("label.email"));
		emailLogin.setRequired(true);
		
		final TextField<String> passwordLogin = new PasswordTextField("password",
				new PropertyModel<String>(person, "password"));
		passwordLogin.setOutputMarkupId(true);
		passwordLogin.add(StringValidator.minimumLength(6));
		
		login.add(new CheckBox("rememberMe", new PropertyModel<Boolean>(this,
				"rememberMe")).setRequired(true));
		passwordLogin.setLabel(new ResourceModel("label.password"));
		final StatelessIndicatingAjaxButton submit1 = new StatelessIndicatingAjaxButton("submit", login) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				if (target == null)
					return;

				try {
					person.setName(person.getFirstName() + " "
							+ person.getLastName());
					PlatformUser session = controller.login(person.getEmail(), person.getPassword());
					if (session == null)
						throw new ControllerException("Account not found");
					UserSession.get().login(session);
					if (rememberMe) {CookieUtils util = new CookieUtils();
						util.getSettings().setMaxAge(Integer.MAX_VALUE);
						util.save(Config.REMEMBER_ME_COOKIE,
								session.getVerificationKey());
					}
					setResponsePage(Home.class);
				} catch (ControllerException e) {
					error(e.getMessage());
					target.add(lfd);
					String script = String
							.format("$('#%s').html('Log In');",
									this.getMarkupId());
					target.appendJavaScript(script);
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(lfd);
				String script = String
						.format("$('#%s').html('Log In');",
								this.getMarkupId());
				target.appendJavaScript(script);
			}
		};
		submit1.setMarkupId("login-button");
		submit1.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').click(function () {$('#%s').html('Logging In...');})",
								component.getMarkupId(), component.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
		});
		passwordLogin.add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13) {$('#%s').click();} })});",
								passwordLogin.getMarkupId(),
								submit1.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
				String script1 = String
						.format("$(document).ready(function() {$('#%s').keydown(function(event) {if (event.keyCode == 13) {$('#%s').click();} })});",
								emailLogin.getMarkupId(),
								submit1.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script1));
			}
		});
		login.add(submit1);
		login.add(emailLogin, passwordLogin);

		BookmarkablePageLink<PasswordReset> resetPassword = new BookmarkablePageLink<PasswordReset>(
				"forgot-password", PasswordReset.class);
		login.add(resetPassword);

		add(login.setOutputMarkupId(true));
	}

}
