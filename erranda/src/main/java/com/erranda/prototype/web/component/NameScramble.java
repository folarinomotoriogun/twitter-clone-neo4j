package com.erranda.prototype.web.component;

public class NameScramble {

	public static String scramble(String name) {

		if (name != null) {
			char[] chars = new char[5];
			for (int i = 0; i < chars.length; i++) {
				if (i == 0)
					chars[i] = name.charAt(0);
				else if (i == 4)
					chars[i] = name.charAt(name.length() - 1);
				else
					chars[i] = '*';
			}
			return new String(chars);
		}
		return "";

	}

}
