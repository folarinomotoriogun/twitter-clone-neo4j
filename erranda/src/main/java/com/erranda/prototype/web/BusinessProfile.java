package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.StringValue;

import com.erranda.prototype.domain.Page;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;

public class BusinessProfile extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Long businessId;
	
	@SpringBean
	Controller controller;
	
	public BusinessProfile (PageParameters parameters) {
		// Convert business name to business id or show error page
		// Panels under this page will be business profile navigation pane, business summary, timeline, followers, people
		if (parameters == null || parameters.get("businessName") == null)
			throw new RestartResponseAtInterceptPageException (ErrorPage.class);
		StringValue businessName = parameters.get("businessName");
		if (businessName == null)
			throw new RestartResponseAtInterceptPageException (ErrorPage.class);
		try {
			Page business = controller.findPageByUniqueName(businessName.toString());
			if (business == null)
				throw new RestartResponseAtInterceptPageException (ErrorPage.class);
			// Web logic
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RestartResponseAtInterceptPageException (ErrorPage.class);
		}
			
			
	}

}
