package com.erranda.prototype.web;

import java.util.Collections;
import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.neo4j.cypher.internal.compiler.v1_9.commands.expressions.Collect;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;

public class PostSearchResults extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	private int initSkip = 0;

	private int initLimit = 20;
	
	private PlatformUser session = UserSession.get().getPerson();

	private int skip = initSkip;

	private int limit = 4;

	private String listViewMarkupId = null;

	private Integer cursor = 0;

	ListView<Errand> resultsList;

	String query;

	public PostSearchResults() {
		throw new RestartResponseAtInterceptPageException(SearchHome.class);
	}

	public PostSearchResults(PageParameters parameters) {
		super();
		try {
			query = parameters.get("query") != null ? parameters.get("query")
					.toString().trim() : null;
		} catch (Exception e) {
			throw new RestartResponseAtInterceptPageException(SearchHome.class);
		}
		Label header = null;
		if (query == null || query.equals("")) {
			throw new RestartResponseAtInterceptPageException(SearchHome.class);
		} else {
			header = new Label("results-header",
					new LoadableDetachableModel<Object>() {

						/**
				 * 
				 */
						private static final long serialVersionUID = 1L;

						@Override
						protected Object load() {
							return query;
						}

					});
		}
		add(header);

		WebMarkupContainer container = new WebMarkupContainer("items-container");
		container.setOutputMarkupId(true);
		listViewMarkupId = container.getMarkupId();

		resultsList = new ListView<Errand>("result",
				new LoadableDetachableModel<List<Errand>>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<Errand> load() {
						try {
							return controller.findPostsByKeyword(session.getId(), query, initSkip,
									initLimit);
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return Collections.emptyList();
						}
					}

				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Errand> item) {
				PlatformUser owner = item.getModel().getObject().getOwner();
				item.getModel().getObject().setOwner(owner);
				Post post = new Post(owner, item.getModelObject());
				item.add(new ErrandSingle("this-post", item.getModel(), (Authenticated) getWebPage(), cp, null, null));
			}

		};
		resultsList.setOutputMarkupId(true);
		add (new PostSearchNavigation("post-nav", Model.of(query)));
		final AjaxLink<Errand> moreButton = new AjaxLink<Errand>("more-items") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				skip = skip + limit;
				List<Errand> results = resultsList.getModel().getObject();
				if (cursor == 0)
					cursor = results.size();
				List<Errand> moreFeed;
				try {
					moreFeed = controller.findPostsByKeyword(session.getId(), query, skip,
							limit);
					for (int i = 0; i < moreFeed.size(); i++) {
						cursor++;
						ListItem<Errand> item = new ListItem<Errand>(new Integer(
								cursor).toString(), cursor);
						item.add(new ErrandSingle("this-post", Model.of(moreFeed.get(i)), (Authenticated) getWebPage(), cp, null, null));
						item.setOutputMarkupId(true);
						resultsList.add(item);
						target.prependJavaScript(String
								.format("var item=document.createElement('%s');item.class='original-tweet-container'; item.id='%s';"
										+ "$('#%s').append(item);", "li",
										item.getMarkupId(), listViewMarkupId));
						target.add(item);
					}
					if (controller.findPostsByKeyword(session.getId(),query, skip + limit, 1).size() == 0) {
						String script2 = "$('#footer-logo').css('display', 'block');";
						String script4 = "$('#more-item-loader').css('display', 'none');";
						String script3 = "$('#" + this.getMarkupId()
								+ "').unbind('click');";
						String script5 = "$('#stream-end').html('That\\'s all we got')";
						target.appendJavaScript(script2);
						target.appendJavaScript(script3);
						target.appendJavaScript(script4);
						target.appendJavaScript(script5);
					}
					;
					target.add(this);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}

		};

		add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {

			}
		});

		moreButton.add(new Behavior() {
			/**
			 * lkoi90i
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$(window).scroll(function() {"
						+ "if($(window).scrollTop() + $(window).height() > $(document).height() - 500) {"
						+ "$('#" + moreButton.getMarkupId()
						+ "').trigger ('click');}});";
				String script2 = "$('#"
						+ moreButton.getMarkupId()
						+ "').click(function(){$('#"
						+ moreButton.getMarkupId()
						+ "').unbind('click');$('#footer-logo').css('display', 'none');$('#more-item-loader').css('display', 'block');})";
				String script3 = String.format("$('.errand-details').highlight('%s');", query);
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript(script2));
				response.render(OnDomReadyHeaderItem.forScript(script3));
			}
		});
		add(new SearchPostForm("search", query));
		add(container.add(resultsList), moreButton);

		addFooter(resultsList);
	}

	private void addFooter(ListView<Errand> newsFeedList) {
		int size = newsFeedList.getList().size();
		Label emptyTimeLine = new Label("empty");
		add(emptyTimeLine);

		if (size == 0) {
			emptyTimeLine
					.setDefaultModel(new LoadableDetachableModel<Object>() {

						private static final long serialVersionUID = 1L;

						@Override
						protected Object load() {
							return "Erranda could not find posts containing " +  query;
						}

					});
		} else {
			emptyTimeLine.setDefaultModel(Model.of(""));
		}
		add(emptyTimeLine);
	}
}
