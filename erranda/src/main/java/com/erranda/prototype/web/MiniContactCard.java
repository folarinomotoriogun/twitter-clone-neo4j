package com.erranda.prototype.web;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.HtmlDecorator;
import com.erranda.prototype.web.component.ModalAjaxLink;
import com.erranda.prototype.web.component.StaticImage;

public class MiniContactCard extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;
	
	WebMarkupContainer mutualFriends;
	
	WebMarkupContainer mutualFriendsContainer;
	
	Label mutualFriendsLabel;
	
	PlatformUser session = UserSession.get().getPerson();

	public MiniContactCard(String id, final IModel<PlatformUser> model) throws ControllerException {
		super(id, model);
		mutualFriendsContainer = new WebMarkupContainer("mutual-friends-container");
		Label kudosCount = new Label("kudos-count", new LoadableDetachableModel<String>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected String load() {
				try {
					return HtmlDecorator.countReadable(controller.myKudosCount(model.getObject().getId()));
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return "0";
				}
			}
		
		});
		add(kudosCount);
		ExternalLink nameLink = new ExternalLink ("person-name-link", new PropertyModel<String>(model.getObject(), "link"));
		Label name = new Label("person-name", new PropertyModel<String>(model.getObject(), "name"));
		add(nameLink.add(name));
		ExternalLink link = new ExternalLink("link",new PropertyModel<String>(model.getObject(), "link"));
		Label username = new Label("username",new PropertyModel<String>(model.getObject(), "username"));
		add(link.add(username));
		WebMarkupContainer phoneLinkContainer = new WebMarkupContainer("phone-link-container");
		ExternalLink phoneLink = new ExternalLink("phone-link",new PropertyModel<String>(model.getObject(), ""));
		phoneLink.add(AttributeModifier.replace("href", "tel:" + model.getObject().getPhoneNumber()));
		Label phoneName = new Label("phone-label",new PropertyModel<String>(model.getObject(), "phoneNumber"));
		add(phoneLinkContainer.add(phoneLink.add(phoneName)));
		if (model.getObject().getPhoneNumber() == null)
			phoneLinkContainer.setVisible(false);
		Label bio = new Label("person-bio",new PropertyModel<String>(model.getObject(), "bio"));
		bio.setEscapeModelStrings(false);
		add(bio);
		Label location = new Label("person-location",new PropertyModel<String>(model.getObject(), "localityName"));
		location.setEscapeModelStrings(false);
		add(location);
		final Integer mutualFriendsCount = controller.countMutualFriends(session.getId(), model.getObject().getId());
		if (mutualFriendsCount == 0) {
			mutualFriends = new WebMarkupContainer("mutual-friends");
			mutualFriendsLabel = new Label("mutual-friends-label");
			mutualFriendsContainer.setVisible(false);
		}
		else {
			mutualFriends = new ModalAjaxLink<String>("mutual-friends") {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void onClick(AjaxRequestTarget target) {
					Authenticated page = (Authenticated) getPage ();
					ModalContainer relationships = page.modalContainer;
					relationships.replaceWindow(new Label ("modal-header", Model.of("<h3 style=\"padding-left: 20px;font-weight: bold;\">Mutual friends</h3>")).setEscapeModelStrings(false), new MutualFriends("modal-content", model.getObject().getId()));
					relationships.update(target, "", true);
					
				}
			};
		
			mutualFriendsLabel = new Label("mutual-friends-label", new LoadableDetachableModel<String>() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				protected String load() {
					return mutualFriendsCount == 1 ? " 1 mutual friend" : mutualFriendsCount + " mutual friends";
				}
			});
			mutualFriends.add(mutualFriendsLabel);
		}
		add(mutualFriendsContainer.add(mutualFriends.add(mutualFriendsLabel)));

		Label joinDate = new Label("joined-date",
				new LoadableDetachableModel<Object>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected Object load() {
						return getJoinDate(model.getObject());
					}

				});
		add(joinDate);
		ExternalLink imageLink = new ExternalLink("profile-picture-link",new PropertyModel<String>(model.getObject(), "link"));
		StaticImage profilePicture = new StaticImage ("profile-picture", new PropertyModel<String>(model.getObject(), "thumbNail"));
		profilePicture.setEscapeModelStrings(false);
		add(imageLink.add(profilePicture));
		if (UserSession.get().userLoggedIn()) {
			add(new MiniUserActions("user-actions", model));
		} else {
			add(new WebMarkupContainer("user-actions"));
		}
		
	}
	
	private String getJoinDate (PlatformUser user) {
		DateTimeFormatter out = DateTimeFormat
				.forPattern("MMMM YYYY");
		DateTime date;
		date = new DateTime(user.getJoinDate());
		return "Joined " + out.print(date);

	}

}
