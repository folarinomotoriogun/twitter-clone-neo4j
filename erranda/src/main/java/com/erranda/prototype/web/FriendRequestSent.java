package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.ConfirmingGlobalAjaxLink;
import com.erranda.prototype.web.component.GlobalAjaxLink;
import com.erranda.prototype.web.component.ModalAjaxLink;

public class FriendRequestSent extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	Long rid;

	private Label label;

	private PlatformUser session = UserSession.get().getPerson();

	public FriendRequestSent(final String id, Long pid,
			final WebMarkupContainer container) {
		super(id);
		setOutputMarkupId(true);
		this.rid = pid;
		final GlobalAjaxLink<String> cancelRequest = new GlobalAjaxLink<String>(
				"cancel-request") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				Long sid = UserSession.get().getPerson().getId();
				controller.rejectFriendRequest(sid, rid);
				((UserActionContainer) container).replaceWindow(new AddAsFriend(id, rid, container));
				((UserActionContainer) container).update(target, "");
			}
		};

		
		add(cancelRequest);
	}

}
