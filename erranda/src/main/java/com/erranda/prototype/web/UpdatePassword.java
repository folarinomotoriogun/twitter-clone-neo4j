package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.geotools.styling.Fill;
import org.springframework.beans.factory.annotation.Required;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.DisablingIndicatingAjaxButton;

public class UpdatePassword extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;
	private String cPass;

	private String nPass;

	private String vPass;

	public UpdatePassword(String id) {
		super(id);
		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);

		final PlatformUser person = UserSession.get().getPerson();

		final Form<String> form = new Form<String>("form");

		final TextField<String> currentPassword = new PasswordTextField(
				"current-password", new PropertyModel<String>(this, "cPass"));
		form.add(currentPassword.setOutputMarkupId(true));
		currentPassword.setRequired(false);

		final TextField<String> newPassword = new PasswordTextField("new-password",
				new PropertyModel<String>(this, "nPass"));
		form.add(newPassword.setOutputMarkupId(true));
		newPassword.setRequired(false);

		final TextField<String> verifyPassword = new PasswordTextField(
				"verify-password", new PropertyModel<String>(this, "vPass"));
		form.add(verifyPassword.setOutputMarkupId(true));
		verifyPassword.setRequired(false);

		form.add(feedback);

		DisablingIndicatingAjaxButton submit = new DisablingIndicatingAjaxButton(
				"submit", form) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				PlatformUser session = UserSession.get().getPerson();
				person.setId(session.getId());
				try {
					if (cPass == null || vPass == null || nPass == null)
						throw new ControllerException("Fill Required fields");
					if (cPass.equals(person.getPassword())) {
						if (vPass.equals(nPass)) {
							controller.updatePassword(person.getId(), nPass);
							String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Success', content: '%s', placement: 'right'}).popover('show'); setTimeout (function () {$('#%s').popover('hide')}, 2000); $('#%s').val('');$('#%s').val('');$('#%s').val('');", this.getMarkupId() , "Your password has been updated", this.getMarkupId(), newPassword.getMarkupId(), verifyPassword.getMarkupId(), currentPassword.getMarkupId());
							target.appendJavaScript(errorScript);
							cPass = "";
							vPass = "";
							nPass = "";
						}
						else {
							String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show').blur($(this).popover('hide'));", this.getMarkupId() , "Your new password do not match each other");
							target.appendJavaScript(errorScript);
						}
					} else {
						String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show').blur($(this).popover('hide'));", this.getMarkupId() , "Your password does not match our records");
						target.appendJavaScript(errorScript);
					}

				} catch (ControllerException e) {
					String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show').blur($(this).popover('hide'));", this.getMarkupId() , e.getMessage());
					target.appendJavaScript(errorScript);
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				String errorScript = String.format("$('#%s').popover('destroy').popover ({title: 'Error', content: '%s', placement: 'right'}).popover('show').blur($(this).popover('hide'));", this.getMarkupId() , "Something went wrong");
				target.appendJavaScript(errorScript);
			}
		};
		BookmarkablePageLink<PasswordReset> resetPassword = new BookmarkablePageLink<PasswordReset>(
				"forgot-password", PasswordReset.class);
		form.add(resetPassword, submit);
		add(form);
	}

}
