package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Panel;

public class ErrandActionContainer extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3624180352885273032L;

	public ErrandActionContainer(String id, WebMarkupContainer webMarkupContainer) {
		super(id);
		this.setOutputMarkupId(true);
		add(webMarkupContainer);
	}

	public void update(AjaxRequestTarget target, String script) {
		if (target == null)
			return;
		target.add(this);
		target.appendJavaScript(script);
	}

	public void replaceWindow(Component bodyC) {
		this.removeAll();
		add(bodyC);
	}

}
