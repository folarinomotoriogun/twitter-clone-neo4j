package com.erranda.prototype.web;

import javax.mail.MessagingException;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.DisablingAjaxButton;
import com.erranda.prototype.web.mailer.EmailNewAction;

public class AcceptErrand extends Panel {

	private static final long serialVersionUID = 1L;

	String message;

	@SpringBean
	Controller controller;

	PlatformUser session = UserSession.get().getPerson();

	public AcceptErrand(String id, final Long eid, final Label label) {
		super(id);
		final Form<Void> messageForm = new Form<Void>("accept-form");
		messageForm.setOutputMarkupId(true);
		final TextArea<String> textArea = new TextArea<String>("message-box",
				new PropertyModel<String>(this, "message"));
		textArea.setRequired(true);
		textArea.setOutputMarkupId(true);

		messageForm.add(textArea);
		messageForm.add(new DisablingAjaxButton("send-message-button",
				messageForm) {
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target,
					Form<?> messageForm) {
				try {
					controller.acceptErrand(eid, UserSession.get().getPerson()
							.getId(), message);
					message = "";
					target.add(label, textArea);
					Authenticated page = (Authenticated) getPage();
					ErrorModalContainer feedback = page.errorModalContainer;
					feedback.show(
							new Label("modal-header", Model.of("Tip")),
							new Label(
									"modal-content",
									Model.of("When you accept an Errand a direct message is sent to start a direct conversation.")));
					if (session.getFirstNomination()) {
						feedback.update(target, "", true);
						session.setFirstNomination(false);
						controller.save(session);
					}
					Errand errand = controller.findErrand(eid);
					String html = new String(ComponentRenderer.renderPage(
							new PageProvider(new EmailNewAction(session,
									session.getName()
											+ "'s reply is in your inbox",
									errand.getErrandLink()))).toString());
					MailMan man = new MailMan();
					man.sendMessage(
							"Accepted! "
									+ session.getName() + " @"
									+ session.getUsername(), errand.getOwner()
									.getEmail(), html);
				} catch (ControllerException e) {
					messageForm.error(e.getMessage());
					e.printStackTrace();
					throw new RestartResponseAtInterceptPageException(
							Home.class);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {

			}
		});
		add(messageForm);
		add (new Behavior () {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = String
						.format("$('#%s').slideDown();",
								messageForm.getMarkupId());
				response.render(OnDomReadyHeaderItem.forScript(script));
			}
			
		});
	}
}
