package com.erranda.prototype.web;

import java.util.List;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.PlatformUser;

public class SearchResults extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	private int initSkip = 0;

	private int initLimit = 20;
	
	private PlatformUser session = UserSession.get().getPerson();

	private int skip = initSkip;

	private int limit = 4;

	private String listViewMarkupId = null;

	private Integer cursor = 0;

	ListView<PlatformUser> resultsList;

	String query;

	public SearchResults() {
		throw new RestartResponseAtInterceptPageException(SearchHome.class);
	}

	public SearchResults(PageParameters parameters) {
		super();
		try {
			query = parameters.get("query") != null ? parameters.get("query")
					.toString().trim() : null;
		} catch (Exception e) {
			throw new RestartResponseAtInterceptPageException(SearchHome.class);
		}
		Label header = null;
		if (query == null || query.equals("")) {
			throw new RestartResponseAtInterceptPageException(SearchHome.class);
		} else {
			header = new Label("results-header",
					new LoadableDetachableModel<Object>() {

						/**
				 * 
				 */
						private static final long serialVersionUID = 1L;

						@Override
						protected Object load() {
							return query;
						}

					});
		}
		add(header);

		WebMarkupContainer container = new WebMarkupContainer("items-container");
		container.setOutputMarkupId(true);
		listViewMarkupId = container.getMarkupId();

		resultsList = new ListView<PlatformUser>("result",
				new LoadableDetachableModel<List<PlatformUser>>() {

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					@Override
					protected List<PlatformUser> load() {
						return controller.searchPeople(session.getId(), query, initSkip,
								initLimit);
					}

				}) {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<PlatformUser> item) {
				Long pid = item.getModel().getObject().getId();
				item.add(new SearchResult("this", item.getModel()));
				
			}

		};
		resultsList.setOutputMarkupId(true);

		add(new Behavior() {
			/**
			 * lkoi90i
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script3 = String.format("$('.bio').highlight('%s');", query);
				response.render(OnDomReadyHeaderItem.forScript(script3));
			}
		});
		
		final AjaxLink<PlatformUser> moreButton = new AjaxLink<PlatformUser>("more-items") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				skip = skip + limit;
				List<PlatformUser> results = resultsList.getModel().getObject();
				if (cursor == 0)
					cursor = results.size();
				List<PlatformUser> moreFeed = controller.searchPeople(session.getId(), query, skip,
						limit);
				for (int i = 0; i < moreFeed.size(); i++) {
					cursor++;
					ListItem<PlatformUser> item = new ListItem<PlatformUser>(new Integer(
							cursor).toString(), cursor);
					item.add(new SearchResult("this", Model.of(moreFeed.get(i))));
					item.setOutputMarkupId(true);
					resultsList.add(item);
					target.prependJavaScript(String
							.format("var item=document.createElement('%s');item.class='original-tweet-container'; item.id='%s';"
									+ "$('#%s').append(item);", "li",
									item.getMarkupId(), listViewMarkupId));
					target.add(item);
				}
				if (controller.searchPeople(session.getId(), query, skip + limit, 1).size() == 0) {
					String script2 = "$('#footer-logo').css('display', 'block');";
					String script4 = "$('#more-item-loader').css('display', 'none');";
					String script3 = "$('#" + this.getMarkupId()
							+ "').unbind('click');";
					String script5 = "$('#stream-end').html('That\\'s all we got')";
					target.appendJavaScript(script2);
					target.appendJavaScript(script3);
					target.appendJavaScript(script4);
					target.appendJavaScript(script5);
				}
				;
				target.add(this);
			}

		};

		add(new Behavior() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {

			}
		});

		moreButton.add(new Behavior() {
			/**
			 * lkoi90i
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead(Component component, IHeaderResponse response) {
				String script = "$(window).scroll(function() {"
						+ "if($(window).scrollTop() + $(window).height() > $(document).height() - 500) {"
						+ "$('#" + moreButton.getMarkupId()
						+ "').trigger ('click');}});";
				String script2 = "$('#"
						+ moreButton.getMarkupId()
						+ "').click(function(){$('#"
						+ moreButton.getMarkupId()
						+ "').unbind('click');$('#footer-logo').css('display', 'none');$('#more-item-loader').css('display', 'block');})";
				response.render(OnDomReadyHeaderItem.forScript(script));
				response.render(OnDomReadyHeaderItem.forScript(script2));
			}
		});
		title.setDefaultModel(Model.of("Search Results - " + query));
		add(new PeopleSearchNavigation("people-nav", Model.of(query)), new SearchPersonForm("form", query));

		add(container.add(resultsList), moreButton);

		addFooter(resultsList);
	}

	private void addFooter(ListView<PlatformUser> newsFeedList) {
		int size = newsFeedList.getList().size();
		Label emptyTimeLine = new Label("empty");
		add(emptyTimeLine);

		if (size == 0) {
			emptyTimeLine
					.setDefaultModel(new LoadableDetachableModel<Object>() {

						private static final long serialVersionUID = 1L;

						@Override
						protected Object load() {
							return "We could not find people by that name - that's all we know";
						}

					});
		} else {
			emptyTimeLine.setDefaultModel(Model.of(""));
		}
		add(emptyTimeLine);
	}

}
