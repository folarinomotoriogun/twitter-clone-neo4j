package com.erranda.prototype.web;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Post;
import com.erranda.prototype.domain.PostRecommend;
import com.erranda.prototype.web.component.TimeConverter;

public class PostPinMeta extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public PostPinMeta(String id, IModel<Errand> model)
			throws ControllerException {
		super(id, model);
		Errand errand = model.getObject();
		ExternalLink personLink = new ExternalLink("link",
				new PropertyModel<String>(model.getObject().getOwner(), "link"));
		Label name = new Label("name", new PropertyModel<String>(model
				.getObject().getOwner(), "name"));
		PlatformUser owner = errand.getOwner();
		Label action = null;
		if (!errand.getAnonymous())
			action = new Label("action", Model.of(String.format(
					" broadcasted <a href='%s'>%s's</a> post", owner.getLink(),
					owner.getName())));
		else
			action = new Label("action", Model.of(String
					.format(" broadcasted this post")));
		action.setEscapeModelStrings(false);
		add(personLink.add(name), action);
	}

}
