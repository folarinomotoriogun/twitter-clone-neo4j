package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.springframework.beans.factory.annotation.Autowired;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class FollowersProvider implements IDataProvider<PlatformUser> {

	@Autowired
	Controller controller;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long pid;
	
	PlatformUser session = UserSession.get().getPerson();

	public FollowersProvider(final Long pid, Controller controller) {
		this.controller = controller;
		this.pid = pid;
	}

	@Override
	public void detach() {
		// TODO Auto-generated method stub

	}

	@Override
	public Iterator<? extends PlatformUser> iterator(long arg0, long arg1) {
		try {
			List<PlatformUser> persons = controller.followers(session.getId(), pid, (int) arg0,
					(int) arg1);
			return persons.iterator();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public IModel<PlatformUser> model(PlatformUser arg0) {
		final Long id = arg0.getId();
		LoadableDetachableModel<PlatformUser> model = new LoadableDetachableModel<PlatformUser>() {

			private static final long serialVersionUID = 1L;

			@Override
			protected PlatformUser load() {
				try {
					return controller.findPerson(id);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
			}

		};
		return model;
	}

	@Override
	public long size() {
		try {
			return controller.countFollowers(session.getId(), pid);
		} catch (ControllerException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

}
