package com.erranda.prototype.web;

import javax.mail.MessagingException;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.EmailTextField;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.MailMan;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;
import com.erranda.prototype.web.component.StatelessIndicatingAjaxButton;
import com.erranda.prototype.web.mailer.EmailForgotPassword;

public class PasswordReset extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	PlatformUserRepository personRepo;

	@SpringBean
	Controller controller;

	String email;

	public PasswordReset() {
		final FeedbackPanel lfd = new FeedbackPanel("login-fd");
		lfd.setOutputMarkupId(true);
		lfd.setMarkupId("reset-feedback");

		final StatelessForm<PlatformUser> login = new StatelessForm<PlatformUser>(
				"begin-password-reset-form");
		login.add(lfd);
		
		TextField<String> emailLogin = new EmailTextField("email",
				new PropertyModel<String>(this, "email"));
		emailLogin.setOutputMarkupId(true);
		emailLogin.setLabel(new ResourceModel("label.email"));
		emailLogin.setRequired(true);
		login.add(emailLogin);

		login.add(new StatelessIndicatingAjaxButton("submit", login) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				PlatformUser person = null;
				if (email == null) {
					error("Account could not be located");
					target.add(lfd);
					return;
				} else
					email = email.toLowerCase().trim();
				person = personRepo.findByEmail(email);
				if (person == null) {
					error("Account could not be located");
					target.add(lfd);
					return;
				}
				try {
					person = controller.forgotPassword(person.getId());
					String html = new String(ComponentRenderer.renderPage(
							new PageProvider(new EmailForgotPassword(person)))
							.toString());
					MailMan man = new MailMan();
					man.sendMessage("Reset your Errandr Password",
							person.getEmail(), html);
					login.info("An email has been sent to your account. This may take a while to be visible");
					target.add(lfd);
				} catch (ControllerException e) {
					e.printStackTrace();
					error(e.getMessage());
					target.add(lfd);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(lfd);
			}
		});
		add(login);
	}

	public PasswordReset(IModel<?> model) {
		super(model);
		// TODO Auto-generated constructor stub
	}

	public PasswordReset(PageParameters parameters) {
		super(parameters);
		// TODO Auto-generated constructor stub
	}

}
