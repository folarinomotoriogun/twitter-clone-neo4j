package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class ConfirmRegisteration extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	Controller controller;

	public ConfirmRegisteration(PageParameters params) {
		super(params);
		String key = params.get("key") != null ? params.get("key").toString()
				: null;
		if (key == null) {
			throw new RestartResponseAtInterceptPageException(Home.class);
		}
		try {
			PlatformUser person = controller.verifyUser(key);
			UserSession.get().setPerson(person);
			throw new RestartResponseAtInterceptPageException(Home.class);
		} catch (ControllerException e) {
			e.printStackTrace();
			throw new RestartResponseAtInterceptPageException(Home.class);
		}
	}

}
