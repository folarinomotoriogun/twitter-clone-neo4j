package com.erranda.prototype.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.springframework.beans.factory.annotation.Autowired;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class FriendsProvider implements IDataProvider<PlatformUser> {

	@Autowired
	Controller personRepo;
	
	PlatformUser session = UserSession.get().getPerson();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long pid;

	private int removed = 0;

	public FriendsProvider(final Long pid, Controller controller) {
		personRepo = controller;
		this.pid = pid;
	}

	@Override
	public void detach() {
		// TODO Auto-generated method stub

	}

	@Override
	public Iterator<? extends PlatformUser> iterator(long arg0, long arg1) {
		try {
			List<PlatformUser> persons = personRepo.friends(session.getId(), pid, (int) arg0,
					(int) arg1);
			return persons.iterator();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public IModel<PlatformUser> model(PlatformUser arg0) {
		final Long id = arg0.getId();
		return Model.of(arg0);
	}

	@Override
	public long size() {
		try {
			return personRepo.countFriends(pid) - removed;
		} catch (ControllerException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

}
