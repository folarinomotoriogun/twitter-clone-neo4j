package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.Message;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;
import com.erranda.prototype.web.component.GlobalAjaxButton;

public class RecommendBox extends Panel {

	/**
	 * 
	 */

	private String contactInfo;

	@SpringBean
	private PlatformUserRepository personRepo;

	@SpringBean
	private Controller controller;

	private static final long serialVersionUID = 1L;

	public RecommendBox(String id, Errand errand) {
		super(id);

		Form<Void> recommendForm = new Form<Void>("recommend-box");
		RequiredTextField<String> contactInfoBox = new RequiredTextField<String>(
				"contact-information", new PropertyModel<String>(this,
						"contactInfo"));
		recommendForm.add(contactInfoBox);
		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		recommendForm.add(feedback);
		feedback.setOutputMarkupId(true);
		recommendForm.add(new GlobalAjaxButton("share-button", recommendForm) {

			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> OfferForm) {
				PlatformUser receiver = personRepo.findByEmail(contactInfo);
				if (receiver == null)
					receiver = personRepo.findByUsername(contactInfo);
				if (receiver == null)
					error("We couldn't find that person");
				else {
					Message message = new Message();
					message.setReceiver(receiver);
					Long sid = UserSession.get().getPerson().getId();
					Long rid = receiver.getId();
					message.setSender(UserSession.get().getPerson());
					message.setPayload("Hi " + receiver.getName() + ","
							+ "I'd like to recommend this errand to you");
					try {
						controller.sendMessage(sid, rid, message.getPayload());
					} catch (ControllerException e) {
						error(e.getMessage());
					}
				}
				target.add(feedback);
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(feedback);
			}

		});
		add(recommendForm);
	}

	public RecommendBox(String id, IModel<?> model) {
		super(id, model);
	}

}
