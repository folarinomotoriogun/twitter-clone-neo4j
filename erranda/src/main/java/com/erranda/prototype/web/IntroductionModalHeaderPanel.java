package com.erranda.prototype.web;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import com.erranda.prototype.UserSession;

public class IntroductionModalHeaderPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4469892228838616215L;

	public IntroductionModalHeaderPanel(String id) {
		super(id);
		add(new Label("modal-header", Model.of(String.format(
				"Welcome to Erranda", UserSession.get().getPerson()
						.getName().split(" ")[0]))));
	}

	public IntroductionModalHeaderPanel(String id, IModel<?> model) {
		super(id, model);
		// TODO Auto-generated constructor stub
	}

}
