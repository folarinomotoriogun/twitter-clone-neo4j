package com.erranda.prototype.web;

import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.repo.PlatformUserRepository;

public class PasswordResetConfirm extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SpringBean
	PlatformUserRepository personRepo;

	@SpringBean
	Controller controller;

	private String nPass;

	private String vPass;

	Long pid;

	public PasswordResetConfirm(PageParameters parameters) {
		super(parameters);
		String key = parameters.get("key") != null ? parameters.get("key")
				.toString() : null;
		if (key == null) {
			// Error page
			throw new RestartResponseAtInterceptPageException(Home.class);
		}
		PlatformUser person = personRepo.findByForgotPassword(key);
		if (person == null)
			throw new RestartResponseAtInterceptPageException(Home.class);

		pid = person.getId();
		final FeedbackPanel feedback = new FeedbackPanel("feedback");
		feedback.setOutputMarkupId(true);

		final Form<String> form = new Form<String>("form");
		Label name = new Label("name", Model.of(person.getName().split(" ")[0]));
		add(name);
		TextField<String> newPassword = new PasswordTextField("new-password",
				new PropertyModel<String>(this, "nPass"));
		form.add(newPassword.setOutputMarkupId(true));

		TextField<String> verifyPassword = new PasswordTextField(
				"verify-password", new PropertyModel<String>(this, "vPass"));
		form.add(verifyPassword.setOutputMarkupId(true));

		form.add(feedback);

		IndicatingAjaxButton submit = new IndicatingAjaxButton("submit", form) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6415555183396288060L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				try {

					if (nPass.equals(vPass)) {
						PlatformUser person = controller.findPerson(pid);
						controller.updatePassword(person.getId(), nPass);
						UserSession.get().setPerson(person);
						setResponsePage(Home.class);
					} else {
						form.error("Passwords do not match");
						target.add(feedback);
					}

				} catch (ControllerException e) {
					e.printStackTrace();
				}
			}

			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				target.add(feedback);
			}
		};

		form.add(verifyPassword, newPassword, submit);

		add(form);
	}

}
