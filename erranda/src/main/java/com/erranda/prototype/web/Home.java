package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.RestartResponseAtInterceptPageException;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.PlatformUser;

public class Home extends Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Home() {
		if (UserSession.get().userNotLoggedIn())
			throw new RestartResponseAtInterceptPageException(Index.class);
	}

	@Override
	public void onInitialize() {
		super.onInitialize();
		Long start = System.currentTimeMillis();
		Double startD = start.doubleValue();

		PlatformUser person = UserSession.get().getPerson();
		add(new NewsFeedPanel("news-feed", person.getId(), modalContainer,
				"all", this));
		add(new Suggestions("suggestions"));

		Long end = System.currentTimeMillis();
		Double endD = end.doubleValue();
		p((endD - startD) / 1000);
		add (new Behavior() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void renderHead (Component component, IHeaderResponse response) {
				String script = String.format("if ($(window).width() <= 1100) {document.location = '/%s';}", "mobile");
				response.render(JavaScriptHeaderItem.forScript(script, "redirect"));
			}
		});
	}

}
