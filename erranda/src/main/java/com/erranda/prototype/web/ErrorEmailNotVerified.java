package com.erranda.prototype.web;

import javax.mail.MessagingException;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.core.request.handler.PageProvider;
import org.apache.wicket.core.util.string.ComponentRenderer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

import com.erranda.prototype.MailMan;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.mailer.EmailWelcome;

public class ErrorEmailNotVerified extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	PlatformUser session = UserSession.get().getPerson();

	public ErrorEmailNotVerified(String id, final IntroductionContainer container) {
		super(id);
		
		final FeedbackPanel feedback = new FeedbackPanel("fb");
		feedback.setOutputMarkupId(true);

		AjaxLink<?> next = new AjaxLink<Object>("resend-email") {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				try {
					String html = new String(ComponentRenderer.renderPage(
							new PageProvider(new EmailWelcome(session)))
							.toString());
					MailMan man = new MailMan();
					man.sendMessage("Confirm your account on Erranda",
							session.getEmail(), html);
					info("Reset email has been sent to " + session.getEmail());
					target.add(feedback);
				} catch (MessagingException e) {
					info("We are sorry an error has occured, please try again");
					e.printStackTrace();
					target.add(feedback);
				}
			}
		};
		add(feedback, next.setOutputMarkupId(true));
	}

}
