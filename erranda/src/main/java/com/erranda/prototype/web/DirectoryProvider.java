package com.erranda.prototype.web;

import java.util.Iterator;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.springframework.beans.factory.annotation.Autowired;

import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.PlatformUser;

public class DirectoryProvider implements IDataProvider<PlatformUser> {

	@Autowired
	Controller personRepo;
	
	PlatformUser session = UserSession.get().getPerson();
	
	Integer size;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DirectoryProvider(Controller controller, Integer size) {
		personRepo = controller;
		this.size = size;
	}

	@Override
	public void detach() {
		// TODO Auto-generated method stub

	}

	@Override
	public Iterator<? extends PlatformUser> iterator(long arg0, long arg1) {
		return  personRepo.directory((int) arg0,
				(int) arg1).iterator();
	}

	@Override
	public IModel<PlatformUser> model(PlatformUser arg0) {
		final Long id = arg0.getId();
		return Model.of(arg0);
	}

	@Override
	public long size() {
			return size;
	}

}
