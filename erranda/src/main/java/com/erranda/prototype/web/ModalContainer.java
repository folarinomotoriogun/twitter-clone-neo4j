package com.erranda.prototype.web;

import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.panel.Panel;

public class ModalContainer extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3624180352885273032L;

	public ModalHeaderPanel header;

	public ModalBodyPanel body;

	public ModalContainer(String id) {
		super(id);
		header = new ModalHeaderPanel("panel-modal-header");
		header.setOutputMarkupId(true);
		body = new ModalBodyPanel("panel-modal-body");
		body.setOutputMarkupId(true);
		add(header);
		add(body);
	}

	public void update(AjaxRequestTarget target, String script, boolean show) {
		target.prependJavaScript("$('#main-modal-ajax-indicator').hide()");
		target.add(header);
		target.add(body);
		//if (show)
			//target.appendJavaScript("$('#show-modal-link').click();" + script);
	}

	public void replaceWindow(Component headerC, Component bodyC) {
		header.removeAll();
		header.add(headerC);
		body.removeAll();
		body.add(bodyC);
	}

}
