package com.erranda.prototype.web;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.erranda.prototype.App;
import com.erranda.prototype.UserSession;
import com.erranda.prototype.domain.Controller;
import com.erranda.prototype.domain.ControllerException;
import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.web.component.GlobalAjaxLink;

public class MessageHeader extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SpringBean
	Controller controller;
	PlatformUser session = UserSession.get().getPerson();


	public MessageHeader(String id, final Long cid) {
		super(id);

		GlobalAjaxLink<String> link = new GlobalAjaxLink<String>(
				"breadcrumb-messages-link") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				ModalContainer modalContainer = ((Authenticated) getPage()).modalContainer;
				Integer count;
				try {
					count = controller.countConversation(session.getId());
					Label header = new Label("modal-header",
							Model.of(String.format("<h3 style=\"padding-left: 20px;font-weight: bold;\" >Conversations %s</h3>", count > 0 ? "(" + count + ")" : "")));
					header.setEscapeModelStrings(false);
					modalContainer.replaceWindow(header, new ConversationsPanel(
							"modal-content", modalContainer,
							((Authenticated) getPage()).cp));
					modalContainer.update(target, "", false);
				} catch (ControllerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		};

		Label withWho = new Label("with-person-name",
				new LoadableDetachableModel<String>() {

					/**
			 * 
			 */
					private static final long serialVersionUID = 1L;

					@Override
					protected String load() {
						try {
							PlatformUser who = withWho(controller
									.findOneConversation(cid));
							if (!who.getStatus().equals("ACTIVE"))
								return " " + who.getName();
							return " " + "<a href='" + who.getLink() + "'>"
									+ who.getName() + "</a>" + (((App) App.get()).getInstantMessagePanel(who.getId()) != null ? " <span class='fa fa-circle' style='margin-left: 10px;color: #5ABB69;'></span>" : "");
						} catch (ControllerException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							throw new RuntimeException(e.getMessage());
						}
					}

				});

		add(link, withWho.setEscapeModelStrings(false));

	}

	public static PlatformUser withWho(Conversation c) {
		PlatformUser current = UserSession.get().getPerson();
		if (!c.getReceiver().equals(current))
			return c.getReceiver();
		else
			return c.getSender();
	}

}