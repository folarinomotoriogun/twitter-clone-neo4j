package com.erranda.prototype;

import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class PropertiesInstaller {

	public static void install() {
		String input = null;
		if (new File("application.properties").exists()) {
			input = PropertiesInstaller.ask(
					"Continue previous application context? (Y or N)",
					Arrays.asList("Y", "N", "y", "n")).toUpperCase();
			if (input.toUpperCase().equals("N")) {
				run();
			}
		} else {
			run();
		}
		verifyConfig();
	}

	private static void verifyConfig() {
		String s = PropertiesInstaller.get("domain");
		if (s == null)
			throw new RuntimeException(
					"Domain name not configured check application.properties file");
		try {
			s = PropertiesInstaller.get("min.port");
			Integer.parseInt(s);
			s = PropertiesInstaller.get("max.port");
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			throw new RuntimeException(
					"Port number not integer check application.properties file");
		}
		s = PropertiesInstaller.get("security.bundle");
		if (s == null)
			throw new RuntimeException(
					"Security bundle location not configured check application.properties file");
		if (!new File(s).exists())
			throw new RuntimeException(
					"Security bundle not in configured location " + s);

	}

	private static void run() {
		String input = null;
		Properties p = new Properties();
		OutputStream o = null;
		try {
			o = new FileOutputStream("application.properties");

			email(p);
			p.store(o, "Non default settings");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (o != null)
					o.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static void email(Properties p) {
		String input = null;
		input = ask(
				"Would you like to setup email \n-> Needed for team functionality? (Y or N)",
				Arrays.asList("Y", "N", "y", "n"));
		if (input.toUpperCase().equals("Y")) {
			input = ask("What's the email account to dispatch emails:");
			p.setProperty("mailer.email", input);
			input = ask("What's the email password?");
			p.setProperty("mailer.password", input);
			input = ask("What's the host smtp address?");
			p.setProperty("mailer.host", input);
			input = ask("SMTP Port Number?");
			p.setProperty("mailer.port", input);
			input = ask("Startttls enabled (true or false)?",
					Arrays.asList("true", "false"));
			p.setProperty("mailer.ttls", input);
			p.setProperty("mailer.auth", "true");
		}
	}

	private static String ask(String question) {
		System.out.println(question);
		Console console = System.console();
		String response = console.readLine("Enter input:");
		if (response != null && response.length() > 0)
			return response;
		else {
			System.out.println("\nError: Your response is required\n");
			ask(question);
		}

		return response;
	}

	private static String ask(String question, List<String> expected) {
		System.out.println(question);
		Console console = System.console();
		String response = console.readLine("Enter input:");
		if (expected.contains(response))
			return response;
		else {
			System.out.println("\nError: That input is not recognized\n");
			ask(question, expected);
		}

		return response;
	}

	public static String get(String property) {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("application.properties");
			prop.load(input);
			return (String) prop.get(property);
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
