package com.erranda.prototype.domain.repo;

import java.util.List;
import java.util.Set;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.erranda.prototype.domain.Conversation;
import com.erranda.prototype.domain.results.ConversationResult;
import com.erranda.prototype.domain.results.MessageResult;

public interface ConversationRepository extends GraphRepository<Conversation> {

	@Query(" match (n1:PlatformUser)-[r:CONVERSATION]-n2 "
			+ " WHERE id (n1) = {0} AND id (n2) = {1}"
			+ " WITH r, n1, n2"
			+ " optional match n1-[x:MESSAGE]-n2 "
			+ " where id (x) = r.lastMessageId set x.id = id(x) "
			+ " return r.ctime as ctime, r.sender as sender, r.receiver as receiver, x as lastMessage, id (r) as id"
			+ " LIMIT 1")
	ConversationResult ourConversation(Long sid, Long rid);

	@Query(" match (n1:PlatformUser)-[r:CONVERSATION]-n2 "
			+ " WHERE id (r) = {0}"
			+ " WITH r, n1, n2"
			+ " optional match n1-[x:MESSAGE]-n2 "
			+ " where id (x) = r.lastMessageId set x.id = id(x) "
			+ " return r.ctime as ctime, n1 as sender, n2 as receiver, x as lastMessage, id (r) as id"
			+ " LIMIT 1")
	ConversationResult findOneConversation(Long sid);

	@Query(" match (n1:PlatformUser)-[r:CONVERSATION]-n2 "
			+ " WHERE id (n1) = {0}"
			+ " WITH r, n1, n2"
			+ " optional match n1-[x:MESSAGE]-n2 "
			+ " where id (x) = r.lastMessageId set x.id = id(x) "
			+ " return r.ctime as ctime, n1 as sender, n2 as receiver, x as lastMessage, id (r) as id"
			+ " ORDER BY x.ctime DESC")
	List<ConversationResult> inbox(Long pid, int skip, int limit);

	@Query(" match (n1:PlatformUser)-[r:MESSAGE]->n2 "
			+ " where r.cid = {0}"
			+ " return r.isRead as isRead, r.payload as payload, r.cid as cid, r.ctime as ctime, n1 as sender, n2 as receiver,  id (r) as id"
			+ " ORDER BY r.ctime ASC SKIP {1} LIMIT {2}")
	Set<MessageResult> conversationMessages(Long cid, int skip, int limit);

	@Query(" match (n1:PlatformUser)-[r:MESSAGE]->n2 "
			+ " where id(r) = {0} SET r.id = id(r)"
			+ " return r.isRead as isRead, r.payload as payload, r.cid as cid, r.ctime as ctime, n1 as sender, n2 as receiver,  id (r) as id"
			+ " LIMIT 1")
	MessageResult findOneMessage(Long mid);

	@Query(" match (n1:PlatformUser)<-[r:MESSAGE]-n2 "
			+ " where id(n1) = {1} AND r.cid = {0}" + " SET r.isRead = true")
	void markMyMessagesReadInConversation(Long cid, Long pid);

	@Query(" 	match (n1:PlatformUser)<-[r:MESSAGE]-n2 "
			+ " where id(n1) = {1} AND r.cid = {0} AND r.isRead = false"
			+ " return r.isRead as isRead, r.payload as payload, r.cid as cid, r.ctime as ctime, r.sender as sender, r.receiver as receiver,  id (r) as id")
	List<MessageResult> unread(Long cid, Long pid);

}
