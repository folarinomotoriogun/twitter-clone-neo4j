package com.erranda.prototype.domain;

import java.util.Date;

import com.erranda.prototype.domain.results.NotificationResult;

public class Notification extends AbstractNode implements
		Comparable<Notification> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date ctime;

	private String type;

	private String details;

	private String link;

	private String pictureLink;

	public Notification() {
	}

	public Notification(NotificationResult result) {
		if (result != null) {
			this.setCtime(result.getCtime());
			this.setDetails(result.getDetails());
			this.setId(result.getId());
			this.setLink(result.getLink());
			this.setPictureLink(result.getProfilePictureLink());
		}
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public int compareTo(Notification another) {
		return ctime.compareTo(another.ctime);
	}

	public String getPictureLink() {
		return pictureLink;
	}

	public void setPictureLink(String pictureLink) {
		this.pictureLink = pictureLink;
	}
}
