/*
 * package com.erranda.prototype.domain;
 * 
 * import java.lang.reflect.Type; import java.util.ArrayList; import
 * java.util.Collections; import java.util.Date; import java.util.List;
 * 
 * import javax.ws.rs.core.MediaType;
 * 
 * import org.json.JSONArray; import org.json.JSONException; import
 * org.json.JSONObject;
 * 
 * import com.google.gson.Gson; import com.google.gson.GsonBuilder; import
 * com.google.gson.JsonDeserializationContext; import
 * com.google.gson.JsonDeserializer; import com.google.gson.JsonElement; import
 * com.google.gson.JsonParseException; import com.sun.jersey.api.client.Client;
 * import com.sun.jersey.api.client.ClientResponse; import
 * com.sun.jersey.api.client.WebResource; import
 * com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
 * 
 * public class Neo4jResultGenerator {
 * 
 * final String ENDPOINT;
 * 
 * final String USERNAME;
 * 
 * final String PASSWORD;
 * 
 * public Neo4jResultGenerator(String endpoint, String username, String
 * password) { ENDPOINT = endpoint; USERNAME = username; PASSWORD = password; }
 * 
 * public Integer integerQuery (String query) throws JSONException { String
 * object = callRest(query); return converterInteger(object); }
 * 
 * private Integer converterInteger(String object) throws JSONException {
 * JSONObject newObj = new JSONObject(object); JSONArray items =
 * newObj.getJSONArray("data"); for (int it = 0; it < items.length();) {
 * JSONArray items2 = items.getJSONArray(it); String val = items2.toString();
 * val = val.replace("[", "").replace("]", ""); return Integer.parseInt(val); }
 * return 0; }
 * 
 * public List<Errand> errandQuery(String query) { String object =
 * callRest(query); try { return converter(object); } catch (JSONException e) {
 * return Collections.emptyList(); }
 * 
 * 
 * p(object); GsonBuilder builder = new GsonBuilder();
 * builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { public
 * Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext
 * context) throws JsonParseException { return new
 * Date(json.getAsJsonPrimitive().getAsLong()); } }); Gson gson =
 * builder.create(); Type type = new TypeToken<ErrandQueryResult>() {
 * }.getType(); ErrandQueryResult result = gson.fromJson(object, type); return
 * result.getErrands();
 * 
 * }
 * 
 * public List<FriendRequest> friendRequestQuery (String query) { String object
 * = callRest (query); try { return converterFriendRequest(object); } catch
 * (JSONException e) { return Collections.emptyList(); } }
 * 
 * private List<FriendRequest> converterFriendRequest(String json) throws
 * JSONException { List<FriendRequest> objects = new ArrayList<FriendRequest>();
 * JSONObject newObj = new JSONObject(json); JSONArray items =
 * newObj.getJSONArray("data");
 * 
 * for (int it = 0; it < items.length(); it++) { JSONArray items2 =
 * items.getJSONArray(it); for (int it2 = 0; it2 < items2.length(); it2++) {
 * JSONObject obj = items2.getJSONObject(it2); GsonBuilder builder = new
 * GsonBuilder(); builder.registerTypeAdapter(Date.class, new
 * JsonDeserializer<Date>() { public Date deserialize(JsonElement json, Type
 * typeOfT, JsonDeserializationContext context) throws JsonParseException {
 * return new Date(json.getAsJsonPrimitive() .getAsLong()); } }); Gson gson =
 * builder.create(); String data = obj.get("data").toString(); FriendRequest
 * object = gson.fromJson(data, FriendRequest.class); if (object.ctime == null
 * && objects.size() > 0) { objects.get(objects.size() - 1).sender =
 * gson.fromJson(data, Person.class);
 * 
 * } else { objects.add(object); }
 * 
 * } } for (FriendRequest fr : objects) p (fr.getSender()); return objects; }
 * 
 * public List<Person> personQuery(String query) { String object =
 * callRest(query); try { return converterPerson(object); } catch (JSONException
 * e) { return Collections.emptyList(); }
 * 
 * 
 * p(object); GsonBuilder builder = new GsonBuilder();
 * builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { public
 * Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext
 * context) throws JsonParseException { return new
 * Date(json.getAsJsonPrimitive().getAsLong()); } }); Gson gson =
 * builder.create(); Type type = new TypeToken<ErrandQueryResult>() {
 * }.getType(); ErrandQueryResult result = gson.fromJson(object, type); return
 * result.getErrands();
 * 
 * }
 * 
 * private String callRest(String query) { final String cypherUrl = ENDPOINT +
 * "/cypher"; Client c = Client.create(); c.addFilter(new
 * HTTPBasicAuthFilter(USERNAME, PASSWORD)); WebResource resource =
 * c.resource(cypherUrl); String request = "{\"query\":\"" + query + "\"}";
 * ClientResponse response = resource.accept(MediaType.APPLICATION_JSON)
 * .type(MediaType.APPLICATION_JSON).entity(request)
 * .post(ClientResponse.class); String object =
 * response.getEntity(String.class); response.close(); return object; }
 * 
 * public List<Errand> converter(String json) throws JSONException {
 * List<Errand> errands = new ArrayList<Errand>(); JSONObject newObj = new
 * JSONObject(json); JSONArray items = newObj.getJSONArray("data"); for (int it
 * = 0; it < items.length(); it++) { JSONArray items2 = items.getJSONArray(it);
 * for (int it2 = 0; it2 < items2.length(); it2++) { JSONObject obj =
 * items2.getJSONObject(it2); GsonBuilder builder = new GsonBuilder();
 * builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { public
 * Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext
 * context) throws JsonParseException { return new
 * Date(json.getAsJsonPrimitive() .getAsLong()); } }); Gson gson =
 * builder.create(); String data = obj.get("data").toString(); Errand errand =
 * gson.fromJson(data, Errand.class); if (errands.size() > 0 && errand.getType()
 * == null) { errands.get(errands.size() - 1).setOwner( gson.fromJson(data,
 * Person.class)); } else { errands.add(errand); }
 * 
 * } } return errands; }
 * 
 * public List<Person> converterPerson(String json) throws JSONException {
 * List<Person> people = new ArrayList<Person>(); JSONObject newObj = new
 * JSONObject(json); JSONArray items = newObj.getJSONArray("data"); for (int it
 * = 0; it < items.length(); it++) { JSONArray items2 = items.getJSONArray(it);
 * for (int it2 = 0; it2 < items2.length(); it2++) { JSONObject obj =
 * items2.getJSONObject(it2); GsonBuilder builder = new GsonBuilder();
 * builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { public
 * Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext
 * context) throws JsonParseException { return new
 * Date(json.getAsJsonPrimitive() .getAsLong()); } }); Gson gson =
 * builder.create(); if (obj.get("data") == null) return people; String data =
 * obj.get("data").toString(); Person errand = gson.fromJson(data,
 * Person.class); people.add(errand);
 * 
 * } } for (Person er :people) p(er.getName()); return people; }
 * 
 * public void p(Object s) { System.out.println(s); } }
 */