package com.erranda.prototype.domain;

import org.springframework.data.neo4j.annotation.RelationshipEntity;

@RelationshipEntity(type = "RECOMMENDED")
public class PostRecommend extends Post {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String message;
	
	public void setMessage (String data) {
		message = data;
	}
	
	public String getMessage () {
		return message;
	}

}
