package com.erranda.prototype.domain;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.Transaction;
import org.springframework.context.ApplicationContext;
import org.springframework.data.neo4j.core.GraphDatabase;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.wicketstuff.rest.annotations.MethodMapping;
import org.wicketstuff.rest.annotations.parameters.PathParam;
import org.wicketstuff.rest.annotations.parameters.RequestBody;
import org.wicketstuff.rest.contenthandling.json.objserialdeserial.GsonObjectSerialDeserial;
import org.wicketstuff.rest.contenthandling.json.webserialdeserial.JsonWebSerialDeserial;
import org.wicketstuff.rest.resource.AbstractRestResource;
import org.wicketstuff.rest.utils.http.HttpMethod;

import com.erranda.prototype.Config;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class Ajax extends AbstractRestResource<JsonWebSerialDeserial> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Ajax(ApplicationContext context) {
		super(new JsonWebSerialDeserial(new GsonObjectSerialDeserial()));
		
		applicationContext = context;
		template = applicationContext.getBean(Neo4jTemplate.class);
		db = applicationContext.getBean(GraphDatabase.class);
	}

	/*
	 * Neo4jResultGenerator restService = new Neo4jResultGenerator(
	 * Config.DATABASE_ENDPOINT, Config.DATABASE_USERNAME,
	 * Config.DATABASE_PASSWORD);
	 */

	ApplicationContext applicationContext;

	Neo4jTemplate template;

	GraphDatabase db;

	@MethodMapping(value = "/persons", httpMethod = HttpMethod.POST)
	@Transactional
	public PlatformUser query() {
		PlatformUser person = new PlatformUser();
		person.setEmail("folarinomotoriogun@gmail.com");
		return person;
	}
	
	@Transactional
	@MethodMapping(value = "/app/secret", httpMethod = HttpMethod.POST)
	public Object query(@RequestBody Map<String, String> object) {
		Transaction t = db.beginTx();
		Object o = null;
		try {
			String pass = object.get("pass");
			String query = object.get("query");
			System.out.println (query);
			if (!pass.equals(Config.DATABASE_PASSWORD))
				throw new RuntimeException("Authorization error");
			else
				o = template.query(query, null);
			t.success();
		} finally {
			t.close();
		}
		JsonDeserializer<Date> deser = new JsonDeserializer<Date>() {
			  @Override
			  public Date deserialize(JsonElement json, Type typeOfT,
			       JsonDeserializationContext context) throws JsonParseException {
			    return json == null ? null : new Date(json.getAsLong());
			  }
		};
		Gson gson = new GsonBuilder()
		   .registerTypeAdapter(Date.class, deser).create();
		return gson.toJson(o);
	}

	@Transactional
	@MethodMapping(value = "/search/{id}/{name}", httpMethod = HttpMethod.GET)
	public String htmlSearch(@PathParam("id") Long id,
			@PathParam("name") String search) {
		if (search == null)
			return "";
		String name = new String(search); 
		name = name.replace("@", "");
		name = name.replace("\\", "");
		name = name.replace("'", "\\'");
		Transaction t = db.beginTx();
		try {
			String query = String
					.format("match (session:PlatformUser) where id (session) = %s match (person:PlatformUser) where (person.status = \"ACTIVE\") AND NOT (session-[:BLOCKED]-person) AND (person.name =~ '(?i).*%s.*' OR person.username =~ '(?i).*%s.*' OR person.bio =~ '(?i).*%s.*') WITH session, person OPTIONAL MATCH path=((session)-[:FRIENDS*..6]-(person)) WITH count(DISTINCT path) as socialStrength, person return person ORDER BY socialStrength DESC LIMIT 10",
							id, name, name, name);
			if (isValidEmailAddress(search))
				 query = String
					.format("match (session:PlatformUser) where id (session) = %s match (person:PlatformUser) where (person.status = \"ACTIVE\") AND NOT (session-[:BLOCKED]-person) AND person.email =~ '(?i).*%s.*'  WITH session, person OPTIONAL MATCH path=((session)-[:FRIENDS*..6]-(person)) WITH count(DISTINCT path) as socialStrength, person return person ORDER BY socialStrength DESC LIMIT 10",
							id, name);
			List<PlatformUser> people = new ArrayList<PlatformUser>();
			Iterator<PlatformUser> iteratorPerson = template.query(query, null)
					.to(PlatformUser.class).iterator();
			while (iteratorPerson.hasNext()) {
				people.add(iteratorPerson.next());
			}
			StringBuffer buffer = new StringBuffer();
			for (PlatformUser person : people) {
				String result = "<li role='presentation' class='typeahead-item typeahead-account-item js-selectable'>"
						+ "<a role='option' class='js-nav has-social-context' href='"
						+ person.getLink()
						+ "'>"
						+ "<img class='avatar size32' alt='' src='"
						+ person.getThumbNail()
						+ "'>"
						+ "<span class='typeahead-user-item-info'><span class='fullname'>"
						+ person.getName()
						+ "</span>"
						+ "</span></span><span class='username'><s>@</s><b>"
						+ person.getUsername() + "</b></span></span></a></li>";
				buffer.append(result);
			}
			if (people.size() > 0) {
				String result = "<li role='presentation' class='typeahead-item typeahead-account-item js-selectable'>"
						+ "<a role='option' class='js-nav has-social-context' href='"
						+ Config.PEOPLE_SEARCH_LINK
						+ "/"
						+ name
						+ "'>"
						+ "<span class='typeahead-user-item-info'><span class='small'>Find people with "
						+ name + "</span>" + "</span></span></span></a></li>";
				buffer.append(result);
			}
			String result = "<li role='presentation' class='typeahead-item typeahead-account-item js-selectable'>"
					+ "<a role='option' class='js-nav has-social-context' href='"
					+ Config.POST_SEARCH_LINK
					+ "/"
					+ name
					+ "'>"
					+ "<span class='typeahead-user-item-info'><span class='small'>Find posts with "
					+ name + "</span>" + "</span></span></span></a></li>";
			buffer.append(result);
			if (people.size() == 0) {
				String none = String
						.format("<li role='presentation' class='typeahead-item typeahead-account-item js-selectable'>No results for %s</li>",
								name);
				buffer.append(none);
			}
			
		
			t.success();
			return buffer.toString();
		} finally {
			t.close();
		}
	}
	
	private boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}

	@Transactional
	@MethodMapping(value = "/mention/{id}/{name}/{target}/{results}/{lastIndex}", httpMethod = HttpMethod.GET)
	public String mentionSearch(@PathParam("id") Long id,
			@PathParam("name") String name, @PathParam("target") String target,
			@PathParam("results") String results, @PathParam("lastIndex") Integer lastIndex) {
		if (name == null)
			return "";
		Transaction t = db.beginTx();
		try {
		String query = String
				.format("match (session:PlatformUser) where id (session) = %s match (person:PlatformUser) where (person.status = \"ACTIVE\") AND NOT (session-[:BLOCKED]-person) AND (person.name =~ '(?i).*%s.*' OR person.username =~ '(?i).*%s.*') WITH session, person OPTIONAL MATCH path=((session)-[:FRIENDS*..6]-(person)) WITH count(DISTINCT path) as socialStrength, person return person ORDER BY socialStrength DESC SKIP 0 LIMIT 5",
						id, name, name, name);
		List<PlatformUser> people = new ArrayList<PlatformUser>();
		Iterator<PlatformUser> iteratorPerson = template.query(query, null)
				.to(PlatformUser.class).iterator();
		while (iteratorPerson.hasNext()) {
			people.add(iteratorPerson.next());
		}
		StringBuffer buffer = new StringBuffer();
		for (PlatformUser person : people) {
			String result = "<li role='presentation' class='typeahead-item typeahead-account-item js-selectable'>"
					+ "<a role='option' class='js-nav has-social-context' href='javascript:;' onclick=\"replace ('"
					+ target
					+ "','"
					+ lastIndex
					+ "','@"
					+ name
					+ "','@"
					+ person.getUsername()
					+ " " 
					+ "'); $('#"
					+ results
					+ "').hide();$('#"
					+ target
					+ "').focus();\">"
					+ "<img class='avatar size32' alt='' src='"
					+ person.getThumbNail()
					+ "'>"
					+ "<span class='typeahead-user-item-info'><span class='fullname'>"
					+ person.getName()
					+ "</span>"
					+ "</span></span><span class='username'><s>@</s><b>"
					+ person.getUsername() + "</b></span></span></a></li>";
			buffer.append(result);
		}
		return buffer.toString();
		} finally {
			t.close();
		}
	}

	@Override
	protected void onInitialize(JsonWebSerialDeserial objSerialDeserial) {
		super.onInitialize(objSerialDeserial);
	}

}
