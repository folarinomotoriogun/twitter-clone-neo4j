package com.erranda.prototype.domain;

import java.io.Serializable;

import org.springframework.data.neo4j.annotation.RelationshipEntity;

@RelationshipEntity
public abstract class RelationshipObject extends Node implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */

}