package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type = "REPORTED")
public class Reported extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	PlatformUser reporter;

	@EndNode
	Errand errand;

	String reasonType;

	String details;

	Long eid;

	Long rid;

	Date ctime;

}