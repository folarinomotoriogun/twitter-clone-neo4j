package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;
import org.springframework.data.neo4j.annotation.StartNode;

import scala.util.regexp.Base.Meta;

@QueryResult
public class PostViewItem {
	
	public PostViewItem() {
	
	}

	@ResultColumn("r")
	Post post;
	
	@ResultColumn("relationship")
	String relationship;
	
	public String relationship () {
		return relationship;
	}

	public Post post() {
		return post;
	}
	
	public void setPost(Post post) {
		this.post = post;
	}
	
	public void setRelationship (String relationship) {
		this.relationship = relationship;
	}
	
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (post == null || obj == null || !getClass().equals(obj.getClass())) {
			return false;
		}
		return post.errand.id.equals(((PostViewItem) obj).post.errand.id);

	}
	
	
}
