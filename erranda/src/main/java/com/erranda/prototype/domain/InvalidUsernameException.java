package com.erranda.prototype.domain;

public class InvalidUsernameException extends PersonControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public InvalidUsernameException(String message) {
		super(message);
	}

}
