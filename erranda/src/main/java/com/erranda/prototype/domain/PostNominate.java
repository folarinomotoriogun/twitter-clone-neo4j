package com.erranda.prototype.domain;

import org.springframework.data.neo4j.annotation.RelationshipEntity;

@RelationshipEntity(type = "GOT_NOMINATED")
public class PostNominate extends Post {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	Boolean primary = false;
	
}
