package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.erranda.prototype.domain.results.FriendRequestResult;

@RelationshipEntity(type = "FRIEND_REQUEST")
public class FriendRequest extends Node {

	public FriendRequest() {
	}

	public FriendRequest(FriendRequestResult result) {
		id = result.getId();
		sender = result.getSender();
		receiver = result.getReceiver();
		ctime = result.getCtime();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	@Fetch
	public PlatformUser sender;

	@EndNode
	@Fetch
	public PlatformUser receiver;
	public Date ctime;

	public Date getCtime() {
		return ctime;
	}

	public PlatformUser getSender() {
		return sender;
	}

}