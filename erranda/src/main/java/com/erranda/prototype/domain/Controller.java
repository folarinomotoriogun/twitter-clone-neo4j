package com.erranda.prototype.domain;

import java.util.List;
import java.util.Map;

public interface Controller {

	Errand postErrand(Errand item2, Long ownerId, List<PlatformUser> nominated) throws ControllerException;

	void acceptFriendRequest(Long frId) throws ControllerException;

	void archiveErrand(Long eid) throws ControllerException;

	void blockPerson(Long sid, Long rid) throws ControllerException;

	void confirmProposal(Long proposalId, String reference)
			throws ControllerException;

	Commented comment(Long pid, Long eid, String message, Boolean anonymous)
			throws ControllerException;

	void deleteComment(Long cid) throws ControllerException;

	void deactivateAccount(PlatformUser person) throws ControllerException;

	void deleteErrand(Long eid) throws ControllerException;

	Errand findErrand(Long eid) throws ControllerException;

	PlatformUser findPerson(Long pid) throws ControllerException;

	Conversation getConversation(Long sid, Long rid) throws ControllerException;

	List<Proposal> getProposalByErrand(Long eid, int skip, int limit)
			throws ControllerException;

	List<Proposal> getProposalsByPerson(Long pid, int skip, int limit)
			throws ControllerException;

	List<PostViewItem> interestFeed(Long pid, int skip, int limit)
			throws ControllerException;
	

	PlatformUser login(String email, String password) throws ControllerException;

	void nominatePerson(List<PlatformUser> people, Long eid, Long personId,
			String data) throws ControllerException;

	List<PostViewItem> personFeed(Long pid, Long viewerId, int low, int upper)
			throws ControllerException;
	
	void postErrand(Errand... errands) throws ControllerException;

	void acceptErrand(Long eid, Long pid, String data)
			throws ControllerException;

	Proposal findOneProposal(Long pid) throws ControllerException;

	void removeComment(Long cid) throws ControllerException;

	void removeFriend(Long sid, Long rid) throws ControllerException;

	FriendRequest sendFriendRequest(Long senderId, Long receiverId)
			throws ControllerException;

	void sendMail(Long pid, String message) throws ControllerException;

	void sendMail(String email, String message) throws ControllerException;

	void sendMessage(Long sid, Long rid, String data)
			throws ControllerException;

	PlatformUser signUp(PlatformUser person) throws ControllerException;

	void unblockPerson(Long sid, Long rid) throws ControllerException;

	PlatformUser changePassword(Long pid, String currentPassword, String newPassword,
			String confirmPassword) throws ControllerException;

	PlatformUser updateProfile(PlatformUser person) throws ControllerException;

	void rejectProposal(Long proposalId) throws ControllerException;

	void rejectFriendRequest(Long frid) throws ControllerException;
	
	List<FriendRequest> getFriendRequestsByPerson(Long pid, int skip, int limit)
			throws ControllerException;

	List<Block> blockList(Long pid, int skip, int limit)
			throws ControllerException;

	Integer commentCountByErrand(Long eid) throws ControllerException;

	Integer nominationsCountByErrand(Long eid) throws ControllerException;

	Integer proposeCountByErrand(Long eid) throws ControllerException;

	Commented findOneComment(Long cid) throws ControllerException;

	List<PlatformUser> searchFriends(Long pid, String name);

	List<Notification> getMyNotifications(Long pid, int skip, int limit)
			throws ControllerException;

	Integer unseenNotificationsCount(Long pid) throws ControllerException;

	Notification findOneNotification(Long nid) throws ControllerException;

	void markAllNotificationAsSeen(Long pid) throws ControllerException;

	Integer countFriends(Long pid) throws ControllerException;

	Boolean areWeFriends(Long sid, Long rid) throws ControllerException;

	FriendRequest theFriendRequestISent(Long sid, Long rid)
			throws ControllerException;

	Boolean isPendingFriendRequest(Long sid, Long rid);

	List<Conversation> inbox(Long pid, int skip, int limit)
			throws ControllerException;

	List<Message> messages(Long cid, int skip, int limit)
			throws ControllerException;

	Conversation findOneConversation(Long cid) throws ControllerException;

	Message findOneMessage(Long mid) throws ControllerException;

	void markAllMessagesAsRead(Long cid, Long pid) throws ControllerException;

	Integer pointsCount(Long pid) throws ControllerException;

	void deleteProposal(Long pid) throws ControllerException;

	void reportPost(Long pid, Long eid, String type, String details)
			throws ControllerException;

	List<Long> twoWayBlockList(Long pid) throws ControllerException;

	Integer countInterestFeed(Long pid) throws ControllerException;

	void batchInsertErrand(List<Errand> errands) throws ControllerException;

	FriendRequest findFriendRequest(Long frid) throws ControllerException;

	Node save(Node object) throws ControllerException;

	void resetUnseenFriendRequestCount(Long pid) throws ControllerException;

	Integer searchSize(String name) throws ControllerException;

	PlatformUser updateProfilePicture(Long pid, String url, String thumbNail)
			throws ControllerException;

	PlatformUser updatePassword(Long pid, String newPass) throws ControllerException;

	void updateLocation(Long pid, String placeName) throws ControllerException;

	Map<String, String> getPrivacySettings(PlatformUser person)
			throws ControllerException;

	List<SuggestionObject> friendSuggestions(Long pid, int skip, int limit)
			throws ControllerException;

	List<PlatformUser> mutualFriends(Long sid, Long rid, int skip, int limit)
			throws ControllerException;

	Integer newMessagesCount(Long pid) throws ControllerException;
	
	Integer countMutualFriends (Long sid, Long rid) throws ControllerException;

	void markAllNewMessages(Long pid) throws ControllerException;

	Post findOnePost(Long pid) throws ControllerException;

	PlatformUser forgotPassword(Long pid) throws ControllerException;

	PlatformUser verifyUser(String key) throws ControllerException;

	PlatformUser removeProfilePicture(Long pid) throws ControllerException;

	List<PlatformUser> othersThatCommented(Long pid, Long senderId)
			throws ControllerException;

	List<PlatformUser> myBlockList(Long pid) throws ControllerException;

	Boolean isBlocked(Long sid, Long rid) throws ControllerException;

	void rejectFriendRequest(Long sid, Long rid);

	PlatformUser tokenLogin(String key) throws ControllerException;

	List<PlatformUser> searchPeople(Long sid, String name, int skip, int limit);

	Boolean isBlockedTwoWay(Long sid, Long rid) throws ControllerException;

	List<Commented> getComments(Long viewer, Long eid, int skip, int limit)
			throws ControllerException;
	
	PlatformUser savePerson (PlatformUser person) throws ControllerException;
	
	public void dontRecommendPerson (Long sid, Long rid);
	
	public PlatformUser findUsername (String username);

	Kudos kudos(Long eid, Long pid) throws ControllerException;

	void minusKudos(Long eid, Long pid) throws ControllerException;
	
	Integer kudosCount (Long eid) throws ControllerException;
	
	Boolean isKudos (Long eid, Long pid) throws ControllerException;
	
	List<PlatformUser> kudosGivePeople (Long eid, Long pid, int skip, int limit) throws ControllerException;

	void recommendPost(Long personId, Long postId, String message)
			throws ControllerException;

	Integer recommendCount(Long postId);
	
	Page findPageByUniqueName (String uniqueName) throws ControllerException;
	
	Boolean canManagePage (Long sessionId, Long pageId) throws ControllerException;
	
	Integer countOfPeopleNominate (Long postId) throws ControllerException;
	
	List<PlatformUser> peopleNominated(Long postId, Long sessionId)
			throws ControllerException;

	PostNominate findPrimaryNominated(Long sessionId, Long postId)
			throws ControllerException;
	
	List<PlatformUser> switchAccounts (Long pid) throws ControllerException;

	List<Errand> findPostsByKeyword(Long session, String details, int skip, int limit)  throws ControllerException;
	
	String findPostsByKeywordCount(Long session, String details)  throws ControllerException;
	
	String findAccountsByKeywordCount(Long session, String details)  throws ControllerException;
	
	Page findPage (Long sessionId,Long pageId) throws ControllerException;

	Page createPage(Long pid, Page page, List<PlatformUser> team)
			throws ControllerException; 
	
	void followUser (Long sid, Long rid) throws ControllerException;
	
	void unfollowUser (Long sid, Long rid) throws ControllerException;
	
	Boolean isFollowingUser (Long sid, Long rid) throws ControllerException;

	List<PlatformUser> commentKudosGivePeople(Long cid, Long sid, Integer skip,
			Integer limit) throws ControllerException;

	Boolean isCommentKudos(Long sid, Long cid) throws ControllerException;

	Integer commentKudosCount(Long cid) throws ControllerException;

	void minusCommentKudos(Long sid, Long cid) throws ControllerException;

	CommentKudos commentKudos(Long cid, Long pid) throws ControllerException;
	
	Integer myKudosCount (Long rid) throws ControllerException;

	List<PlatformUser> peopleNominatedByOwner(Long sessionId, Long postId, int skip, int limit)
			throws ControllerException;
	
	Integer countOfPeopleNominateByOwner (Long session, Long postId) throws ControllerException;
	
	Boolean areWeInSixthDegree (Long sid, Long rid);
	
	Integer countConversation (Long sid) throws ControllerException;

	void acceptFriendRequest(Long sid, Long rid) throws ControllerException;
	
	Boolean isUsernameTaken (String name) throws ControllerException;
	
	Boolean isEmailTaken (String name) throws ControllerException;
	
	void switchPrivacy (Long sid, Long eid, String privacy) throws ControllerException;
	
	Errand updatePostDetails (Long sid, Long eid, String details) throws ControllerException;
	
	List<Long> commentNotificationList (Long sid, Long eid) throws ControllerException;
	
	List<Long> commentKudosNotificationList (Long cid) throws ControllerException;
	
	List<Long> broadcastNotificationList (Long sid, Long eid) throws ControllerException;
	
	List<Long> kudosNotificationList (Long sid, Long eid) throws ControllerException;
	
	List<Long> friendRequestNotificationList (Long sid, Long rid) throws ControllerException;
	
	List<Long> messageNotificationList (Long sid, Long rid) throws ControllerException;
	
	List<PlatformUser> canManagePage (Long pageId) throws ControllerException;

	Page updatePage(Long id, Page page, List<PlatformUser> team, Long sessionId)
			throws ControllerException;
	
	void sendFollowRequest (Long sid, Long rid) throws ControllerException;
	
	Integer countFollowing(Long sid, Long rid) throws ControllerException;

	List<PlatformUser> following(Long sid, Long rid, int skip, int limit)
			throws ControllerException;
	
	Integer countFollowers(Long sid, Long rid) throws ControllerException;

	List<PlatformUser> followers (Long sid, Long rid, int skip, int limit)
			throws ControllerException;

	List<PlatformUser> friends(Long sid, Long pid, int skip, int limit)
			throws ControllerException;

	Boolean isNominatedByOwner(Long eid, Long pid);
	
	List<PlatformUser> friendsNominatedInPost (Long pid, Long eid) throws ControllerException;
	
	Integer countFriendsNominatedInPost (Long pid, Long eid) throws ControllerException;

	PlatformUser getGuest();
	
	List<PlatformUser> directory (int skip, int limit);

	Integer countDirectory();
	
	List<PlatformUser> friendsWhoBroadcastedPost(Long sid, Long eid, int skip, int limit) throws ControllerException;
	
	Integer countFriendsWhoBroadcastedPost(Long sid, Long eid) throws ControllerException;
}
