package com.erranda.prototype.domain;

public class Place extends NamedNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Double longitude;

	private Double latitude;

	public Place() {
	}

	public Place(String name, Double double1, Double double2) {
		setName(name);
		setLatitude(double1);
		setLongitude(double2);
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

}
