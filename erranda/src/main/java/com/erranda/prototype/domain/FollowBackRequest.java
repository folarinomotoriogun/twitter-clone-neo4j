package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.erranda.prototype.domain.results.FriendsResult;

@RelationshipEntity(type = "FOLLOW_BACK_REQUEST")
public class FollowBackRequest extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public FollowBackRequest() {
	}

	@StartNode
	@Fetch
	public PlatformUser sender;

	@EndNode
	@Fetch
	public PlatformUser receiver;

	public Date ctime;

}