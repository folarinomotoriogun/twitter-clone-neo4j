package com.erranda.prototype.domain.results;

import java.util.Date;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.erranda.prototype.domain.PlatformUser;

// Use interfaces and the Result Column annotation
@QueryResult
public interface ProposalResult {

	@ResultColumn("ctime")
	public Date getCtime();

	@ResultColumn("message")
	public String getMessage();

	@ResultColumn("eid")
	public Long getEid();

	@ResultColumn("person")
	public PlatformUser getPerson();

	@ResultColumn("receiver")
	public PlatformUser getReceiver();

	@ResultColumn("id")
	public Long getId();

	@ResultColumn("confirmed")
	public Boolean getConfirmed();

	@ResultColumn("sid")
	public Long getSid();

	@ResultColumn("rid")
	public Long getRid();
}
