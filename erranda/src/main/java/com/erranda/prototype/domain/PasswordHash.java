package com.erranda.prototype.domain;

import java.util.Date;

public class PasswordHash {

	private String value;

	private Date expires;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getExpires() {
		return expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

}
