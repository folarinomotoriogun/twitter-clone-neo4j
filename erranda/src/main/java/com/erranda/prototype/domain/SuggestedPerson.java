package com.erranda.prototype.domain;

public class SuggestedPerson {

	private PlatformUser person;

	private String text;

	private Integer mutualFriendsCount;

	public PlatformUser getPerson() {
		return person;
	}

	public void setPerson(PlatformUser person) {
		this.person = person;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getMutualFriendsCount() {
		return mutualFriendsCount;
	}

	public void setMutualFriendsCount(Integer mutualFriendsCount) {
		this.mutualFriendsCount = mutualFriendsCount;
	}

}
