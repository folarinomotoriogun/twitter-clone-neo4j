package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type = "POSTED")
public class Post extends RelationshipObject implements Comparable<Post> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	@Fetch
	PlatformUser owner;

	@EndNode
	@Fetch
	Errand errand;

	Date ctime = new Date ();
	
	String relationship;
	
	public Post (PlatformUser owner, Errand errand) {
		this.owner = owner;
		this.errand = errand;
	}
	
	public String relationship () {
		return relationship;
	}
	
	public Date getCtime() {
		return ctime;
	}

	public PlatformUser getOwner() {
		return owner;
	}

	public Errand getErrand() {
		return errand;
	}
	
	public void setErrand (Errand errand) {
		this.errand = errand;
	}
	
	public void setOwner (PlatformUser user) {
		owner = user;
	}

	public Post() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compareTo(Post post) {
		return ctime.compareTo(post.getCtime());
	}

}
