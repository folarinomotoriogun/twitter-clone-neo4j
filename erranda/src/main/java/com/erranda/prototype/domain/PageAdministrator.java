package com.erranda.prototype.domain;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity (type = "MANAGES_PAGE")
public class PageAdministrator extends RelationshipObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	private PlatformUser owner;
	
	@EndNode
	private Page business;
	
	private String title;
	
	public PlatformUser getOwner() {
		return owner;
	}

	public void setOwner(PlatformUser owner) {
		this.owner = owner;
	}

	public Page getPage() {
		return business;
	}

	public void setPage(Page business) {
		this.business = business;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
