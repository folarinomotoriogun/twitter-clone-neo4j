package com.erranda.prototype.domain.repo;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.erranda.prototype.domain.AbstractNode;
import com.erranda.prototype.domain.Commented;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.results.BlockResult;
import com.erranda.prototype.domain.results.CommentResult;
import com.erranda.prototype.domain.results.NominationResult;
import com.erranda.prototype.domain.results.NotificationResult;
import com.erranda.prototype.domain.results.ProposalResult;

public interface AbstractRepository extends GraphRepository<AbstractNode> {

	@Query(" match n-[r:NOTIFICATION]->person "
			+ " WHERE id (person) = {0}"
			+ " return n.ctime as ctime, n.details as details, n.link as link, n.pictureLink as pictureLink, id (n) as id, r.isRead as isRead"
			+ " ORDER BY n.ctime DESC")
	List<NotificationResult> myNotifications(Long pid, int skip, int limit);

	@Query(" match n-[r:NOTIFICATION]->person "
			+ " WHERE id (person) = {0} AND r.isRead = false"
			+ " return n.ctime as ctime, n.details as details, n.link as link, n.pictureLink as pictureLink, id (n) as id, r.isRead as isRead"
			+ " ORDER BY n.ctime DESC")
	List<NotificationResult> unseenNotifications(Long pid, int skip, int limit);

	@Query(" match n-[r:NOTIFICATION]->person "
			+ " WHERE id (person) = {0} AND r.isRead = false"
			+ " return count (r)")
	Integer unseenNotificationsCount(Long pid);

	@Query(" match (person:PlatformUser)<-[r:NEW_MESSAGE]-n "
			+ " WHERE id (person) = {0}" + " return count (n)")
	Integer newMessagesCount(Long pid);

	@Query(" match (person:PlatformUser)<-[r:NEW_MESSAGE]-n "
			+ " WHERE id (person) = {0}" + " delete n, r")
	Integer markNewMessages(Long pid);

	@Query(" optional match (person:PlatformUser)<-[r:NEW_MESSAGE]-(n:MessageNotification) "
			+ " WHERE n.cid = {0}" + " return n IS NOT NULL")
	Boolean conversationNotificationExists(Long cid);

	@Query(" match n-[r:NOTIFICATION]->person "
			+ " WHERE id (person) = {0} AND r.isRead = false"
			+ " SET r.isRead = true ")
	void markAllNotificationsAsRead(Long pid);

	@Query(" match n-[r:MESSAGE_NOTIFICATION]->person "
			+ " WHERE id (person) = {0} AND r.isRead = false"
			+ " SET r.isRead = true ")
	void markAllMessageNotificationsAsRead(Long pid);

	@Query(" match (sender:PlatformUser)-[r:COMMENTED]->(errand:Errand)"
			+ " WHERE id (errand) = {0} AND (id (sender) <> errand.ownerId) AND (id (sender) <> {1})"
			+ " return distinct sender")
	List<PlatformUser> othersThatCommentedOnPost(Long eid, Long senderId);

	@Query(" match (sender:PlatformUser)-[r:COMMENTED]->errand "
			+ " WHERE id (errand) = {0}"
			+ " return sender, errand, r.ctime as ctime, r.message as message, id (r) as id, r.anonymous as anonymous"
			+ " ORDER BY r.ctime ASC")
	List<CommentResult> comments(Long eid, int skip, int limit);

	@Query(" match (sender:PlatformUser)-[r:COMMENTED]->errand "
			+ " WHERE id (r) = {0}"
			+ " return sender, errand, r.ctime as ctime, r.message as message, id (r) as id, r.anonymous as anonymous")
	CommentResult findComments(Long eid);

	@Query(" match (sender:PlatformUser)-[r:COMMENTED]->errand " + " WHERE id (r) = {0}"
			+ " return r")
	Commented findComment(Long eid);

	@Query("  match a-[r]-b " + "  WHERE id (r) = {0} " + "  delete r ")
	void deleteRelationship(Long rid);

	@Query(" match (person:PlatformUser)-[r:PROPOSED]->receiver "
			+ " WHERE r.eid = {0}"
			+ " return id (person) as sid, id (receiver) as rid, r.eid as eid, r.confirmed as confirmed, person, receiver, r.ctime as ctime, r.message as message, id (r) as id"
			+ " ORDER BY r.ctime DESC")
	List<ProposalResult> proposalsByErrand(Long id, int skip, int limit);

	@Query(" match (person:PlatformUser)-[r:PROPOSED]->receiver "
			+ " WHERE id(r) = {0}"
			+ " return id (person) as sid, id (receiver) as rid, r.eid as eid, r.confirmed as confirmed, person, receiver, r.ctime as ctime, r.message as message, id (r) as id"
			+ " LIMIT 1")
	ProposalResult findOneProposal(Long rid);

	@Query(" match (person:PlatformUser)-[r:PROPOSED]->receiver "
			+ " WHERE id(receiver) = {0}"
			+ " return id (person) as sid, id (receiver) as rid, r.eid as eid, r.confirmed as confirmed, person, receiver, r.ctime as ctime, r.message as message, id (r) as id"
			+ " ORDER BY r.ctime DESC")
	List<ProposalResult> proposalsByPerson(Long id, int skip, int limit);

	@Query(" match (sender:PlatformUser)-[r:BLOCKED]->receiver " + " WHERE id (sender) = {0}"
			+ " return sender, receiver, r.ctime as ctime, id (r) as id"
			+ " ORDER BY r.ctime DESC")
	List<BlockResult> blockList(Long id, int skip, int limit);

	@Query(" match (sender:PlatformUser)-[r:BLOCKED]->receiver " + " WHERE id (sender) = {0}"
			+ " return sender, receiver, r.ctime as ctime, id (r) as id"
			+ " ORDER BY r.ctime DESC")
	List<BlockResult> blockList(Long id);

	@Query(" optional match (sender:PlatformUser)-[r:BLOCKED]->receiver "
			+ " WHERE id (sender) = {0} AND id (receiver) = {1} "
			+ " return sender, receiver, r.ctime as ctime, id (r) as id"
			+ " LIMIT 1")
	BlockResult blockedRelationship(Long sid, Long rid);

	@Query(" match nominator-[r:NOMINATED]->nominated "
			+ " WHERE id (nominated) = {0} WITH nominator, nominated, r MATCH (person:PlatformUser)-[:POSTED]->errand WHERE id (errand) = r.eid "
			+ " return nominator, nominated, r.message as message, errand, r.ctime as ctime, id (r) as id")
	List<NominationResult> findNominationsByPerson(Long pid, int skip, int limit);

}
