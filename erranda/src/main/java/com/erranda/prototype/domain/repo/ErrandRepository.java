package com.erranda.prototype.domain.repo;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.SpatialRepository;
import org.springframework.transaction.annotation.Transactional;

import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.Proposal;
import com.erranda.prototype.domain.results.ErrandResult;

@Transactional
public interface ErrandRepository extends GraphRepository<Errand>,
		SpatialRepository<Errand> {

	@Query(" match (person:PlatformUser)-[:FOLLOWS]->leader "
			+ " where person = node({0}) " + " with leader "
			+ " match errand<-[p:POSTED]-leader " + " return errand "
			+ " ORDER BY errand.added DESC")
	List<Errand> followingFeed(PlatformUser person, Pageable pageable);

	@Query(" start person=node({0}) "
			+ " match (person:PlatformUser)-[:MEMBER_OF]->group " + " with group "
			+ " match errand-[:SHARED_WITH_GROUP]->group" + " return errand "
			+ " ORDER BY errand.added DESC")
	List<Errand> groupFeed(PlatformUser person, Pageable pageable);

	@Query(" match (person:PlatformUser)-[r:REPLIED]->errand "
			+ " where id (errand) = {0}" + " return count(r) ")
	Integer replies(Long errand);

	@Query(" match (person:PlatformUser)-[r:NOMINATION]->nominated "
			+ " where r.eid = {0}" + " return count(r) ")
	Integer nominations(Long pid);

	@Query(" match (person:PlatformUser)-[r:COMMENTED]->errand "
			+ " where id (errand) = {0}" + " return count(r) ")
	Integer commentsCount(Long eid);

	@Query("START n=node:ErrandLocation('withinDistance:[-2.80, 56.34, 100.00]') RETURN n SKIP 0 LIMIT 10")
	List<Errand> localFeed(String longitude, String latitude);

	@Query(" match (person:PlatformUser)-[r:PROPOSED]->errand "
			+ " where id (errand) = {0}" + " return r ")
	List<Proposal> findProposalsByErrand(Long eid);

	@Query(" match (person:PlatformUser)-[r:PROPOSED]->receiver "
			+ " where r.eid = {0}" + " return count(r) ")
	Integer acceptErrandCount(Long eid);

	@Query(" match (errand:Errand)-[r:NOMINATED]->person "
			+ " where id (errand) = {0}" + " return count(r) ")
	Integer nominatedErrandCount(Long eid);

	@Query(" match (person:PlatformUser)-[r:PROPOSED]->errand "
			+ " where errand.ownerId = {0}" + " return r ")
	List<Proposal> findProposalsByOwner(Long pid);

	@Query(" OPTIONAL match (person:PlatformUser)-[r:PROPOSED]->errand "
			+ " where id (person) = sid AND id (errand) = {0}" + " return r ")
	Proposal findBySenderAndErrand(Long sid, Long eid);

	@Query(" match  (person:PlatformUser)<-[r:POINTS]-errand "
			+ " where id (person) = {0}" + " return SUM (r.points) ")
	Integer confirmedPoints(Long pid);

	@Query(" match  (person:PlatformUser)<-[r:POINTS]-errand "
			+ " where r.prid = {0}" + " delete r")
	void deleteConfirmedPoint(Long pid);

	@Query(" OPTIONAL MATCH (person:PlatformUser)<-[z:PROPOSED]-b "
			+ "where z.eid = {0} " + "WITH z "
			+ "match ()-[r]-errand where id(errand) = {0} "
			+ "delete z, r, errand")
	void deleteErrand(Long eid);

	@Query("OPTIONAL MATCH (person:PlatformUser)<-[y:POINTS]-errand "
			+ "where y.prid = {0} " + "DELETE y " + "WITH errand "
			+ "MATCH (person:PlatformUser)<-[z:PROPOSED]-b " + "where id(z) = {0} "
			+ "delete z")
	void deleteProposal(Long prid);

	@Query(" match (person:PlatformUser)-[r:POSTED]->errand where id(errand) = {0} return errand, person")
	ErrandResult findOneErrand(Long pid);

}
