package com.erranda.prototype.domain;

public class PersonConflictException extends PersonControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public PersonConflictException(String message) {
		super(message);
	}

}
