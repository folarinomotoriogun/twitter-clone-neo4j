package com.erranda.prototype.domain.repo;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.erranda.prototype.domain.Page;

public interface PageRepository extends GraphRepository<Page> {

}
