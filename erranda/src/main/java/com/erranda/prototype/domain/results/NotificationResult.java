package com.erranda.prototype.domain.results;

import java.util.Date;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

@QueryResult
public interface NotificationResult {

	@ResultColumn("isread")
	Boolean getIsRead();

	@ResultColumn("details")
	String getDetails();

	@ResultColumn("link")
	String getLink();

	@ResultColumn("pictureLink")
	String getProfilePictureLink();

	@ResultColumn("ctime")
	Date getCtime();

	@ResultColumn("id")
	Long getId();

}