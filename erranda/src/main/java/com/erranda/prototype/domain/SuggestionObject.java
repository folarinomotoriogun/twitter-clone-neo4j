package com.erranda.prototype.domain;

import java.io.Serializable;

import com.erranda.prototype.domain.results.SuggestionResult;

public class SuggestionObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PlatformUser person;

	public String metaData;

	public SuggestionObject() {
	}

	public SuggestionObject(SuggestionResult result) {
		person = result.getPerson();
	}

	public PlatformUser getPerson() {
		return person;
	}

	public String getMetaData() {
		return metaData;
	}

}
