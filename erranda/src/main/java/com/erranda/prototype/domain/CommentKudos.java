package com.erranda.prototype.domain;

import java.util.Date;

public class CommentKudos extends AbstractNode {

	private static final long serialVersionUID = 1L;

	Long sid;
	
	Long cid;

	Date ctime;
	
	public Long getPerson () {
		return sid;
	}
	
	public Long getComment () {
		return cid;
	}

}