package com.erranda.prototype.domain;

import java.util.Date;

import org.apache.wicket.util.string.Strings;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.erranda.prototype.domain.results.MessageResult;

@RelationshipEntity(type = "MESSAGE")
public class Message extends Node implements Comparable<Message> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date ctime = new Date();

	private Boolean isRead = false;

	@StartNode
	private PlatformUser sender;

	@EndNode
	private PlatformUser receiver;

	public String payload = "";

	private Long sid;

	private Long rid;

	private Long cid;

	public Message(PlatformUser sender, PlatformUser receiver, String string) {
		setSender(sender);
		setReceiver(receiver);
		setPayload(string);
	}

	public Message() {
	}

	public Message(MessageResult r) {
		payload = r.getPayLoad();
		ctime = r.getCtime();
		sender = r.getSender();
		receiver = r.getReceiver();
		sid = sender.getId();
		rid = receiver.getId();
		cid = r.getCid();
		id = r.getId();
		isRead = r.getIsRead();
	}

	public String getPayload() {
		// if (payload != null)
			// payload  = Strings.escapeMarkup(payload).toString();
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}
	
	public String getPayloadRaw() {
		return payload;
	}

	public void setPayloadRaw(String payload) {
		this.payload = payload;
	}

	public PlatformUser getReceiver() {
		return receiver;
	}

	public void setReceiver(PlatformUser receiver) {
		this.receiver = receiver;
	}

	public PlatformUser getSender() {
		return sender;
	}

	public void setSender(PlatformUser sender) {
		this.sender = sender;
	}

	@Override
	public int compareTo(Message another) {
		return ctime.compareTo(another.ctime);
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	public Long getRid() {
		return rid;
	}

	public void setRid(Long rid) {
		this.rid = rid;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}
}
