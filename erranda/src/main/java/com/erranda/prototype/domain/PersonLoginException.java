package com.erranda.prototype.domain;

public class PersonLoginException extends PersonControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public PersonLoginException(String message) {
		super(message);
	}

}
