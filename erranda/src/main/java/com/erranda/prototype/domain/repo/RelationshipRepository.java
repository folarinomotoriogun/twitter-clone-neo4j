package com.erranda.prototype.domain.repo;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.erranda.prototype.domain.FriendRequest;

public interface RelationshipRepository extends GraphRepository<FriendRequest> {

}
