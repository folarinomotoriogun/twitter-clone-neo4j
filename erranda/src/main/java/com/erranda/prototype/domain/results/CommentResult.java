package com.erranda.prototype.domain.results;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;

// Use interfaces and the Result Column annotation
@QueryResult
public interface CommentResult extends Serializable {

	@ResultColumn("ctime")
	public Date getCtime();

	@ResultColumn("message")
	public String getMessage();

	@ResultColumn("errand")
	public Errand getErrand();

	@ResultColumn("sender")
	public PlatformUser getSender();

	@ResultColumn("anonymous")
	public Boolean getAnonymous();

	@ResultColumn("id")
	public Long getId();
}
