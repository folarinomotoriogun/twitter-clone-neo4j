package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.erranda.prototype.domain.results.BlockResult;

@RelationshipEntity(type = "BLOCKED")
public class Block extends RelationshipObject {

	public Block() {
	}

	public Block(BlockResult block) {
		sender = block.getSender();
		receiver = block.getReceiver();
		ctime = block.getCtime();

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	PlatformUser sender;

	@EndNode
	Node receiver;

	Date ctime;

}