package com.erranda.prototype.domain;

public class InvalidFirstNameException extends PersonControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public InvalidFirstNameException(String message) {
		super(message);
	}

}
