package com.erranda.prototype.domain;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.support.index.IndexType;

public class ErrandLocal extends Errand {

	
	public ErrandLocal() {
		// TODO Auto-generated constructor stub
	}

	public ErrandLocal(Errand errand) {
		setAdded(errand.getAdded());

		setDateAddedLong(errand.getDateAddedLong());

		setDetails(errand.getDetails());

		setLink(errand.getLink());

		setAnonymous(errand.getAnonymous());

		setPictureUrl(errand.getPictureUrl());

		setIsExternalUrl(errand.getIsExternalUrl());

		setDeliveryLocation(errand.getDeliveryLocation());

		setStatus(errand.getStatus());

		setErrandLocation(errand.getErrandLocation());

		setOwnerId(errand.getOwnerId());

		setNomCount(errand.getNomCount());

		setRepliesCount(errand.getRepliesCount());

		setExpiryDate(errand.getExpiryDate());

		setBounty(errand.getBounty());

		setShareCount(errand.getShareCount());

		setOwner(errand.getOwner());

		setPrivacy(errand.getPrivacy());
	}

}
