package com.erranda.prototype.domain;

import java.io.Serializable;

import org.springframework.data.neo4j.annotation.GraphId;

public abstract class Node implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@GraphId
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}

		if (id == null || obj == null || !getClass().equals(obj.getClass())) {
			return false;
		}
		return id.equals(((Node) obj).id);

	}

	@Override
	public int hashCode() {
		return id == null ? 0 : id.hashCode();
	}
}