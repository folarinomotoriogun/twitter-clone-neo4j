package com.erranda.prototype.domain.results;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.erranda.prototype.domain.PlatformUser;

@QueryResult
public interface SuggestionResult extends Serializable {

	@ResultColumn("person")
	PlatformUser getPerson();

	@ResultColumn("mutualFriendsCount")
	Integer getMutualFriendsCount();

	@ResultColumn("mutualFriendIds")
	List<Long> getMutualFriendIds();

}
