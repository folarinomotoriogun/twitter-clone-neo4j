package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type = "POINTS")
public class ErrandPoint extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	Errand errand;

	@EndNode
	PlatformUser receiver;

	Long eid;

	Long rid;

	Long prid;

	Integer points;

	Date ctime;

}