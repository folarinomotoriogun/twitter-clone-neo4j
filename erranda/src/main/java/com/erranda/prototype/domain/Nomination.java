package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type = "NOMINATED")
public class Nomination extends RelationshipObject {
	
	public Nomination() {
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;

	@StartNode
	PlatformUser nominator;

	@EndNode
	PlatformUser nominated;

	Long eid;

	Date ctime;

}