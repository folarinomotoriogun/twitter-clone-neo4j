package com.erranda.prototype.domain;

import java.io.IOException;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;
import com.google.code.geocoder.model.GeocoderStatus;
import com.google.code.geocoder.model.LatLng;

public class GoogleGeocoder {

	public GoogleGeocoder() {
		// TODO Auto-generated constructor stub
	}

	public static Double[] performGeoCoding(String location) {
		if (location == null)
			return null;

		Geocoder geocoder = new Geocoder();
		GeocoderRequest geocoderRequest = new GeocoderRequestBuilder()
				.setAddress(location) // location
				.setLanguage("en") // language
				.getGeocoderRequest();
		GeocodeResponse geocoderResponse;

		try {
			geocoderResponse = geocoder.geocode(geocoderRequest);
			if (geocoderResponse.getStatus() == GeocoderStatus.OK
					& !geocoderResponse.getResults().isEmpty()) {
				GeocoderResult geocoderResult = geocoderResponse.getResults()
						.iterator().next();
				LatLng latitudeLongitude = geocoderResult.getGeometry()
						.getLocation();
				Double[] coords = new Double[2];
				coords[0] = latitudeLongitude.getLat().doubleValue();
				coords[1] = latitudeLongitude.getLng().doubleValue();
				return coords;
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return null;
	}

}
