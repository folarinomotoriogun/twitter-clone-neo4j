package com.erranda.prototype.domain;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.StringUtils;

public class DomainValidator<T extends Object> {

	private Validator validator;

	public boolean valid(T object) {
		if (object == null)
			return false;
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		Set<ConstraintViolation<T>> constraintViolations = validator
				.validate(object);
		return (constraintViolations.size() == 0);
	}

	public static boolean isNull(Object o) {
		return o == null;
	}

	public static void assertNotNull(Object o, ControllerException e)
			throws ControllerException {
		if (o == null)
			throw e;
	}
	
	public static void validateFirstName (String name) throws InvalidFirstNameException {
		try {
			validateName("First name", name);
		} catch (ControllerException e) {
			throw new InvalidFirstNameException(e.getMessage());
		}
	}
	
	public static void validateLastName (String name) throws InvalidLastNameException {
		try {
			validateName("Last name", name);
		} catch (ControllerException e) {
			throw new InvalidLastNameException(e.getMessage());
		}
	}
	
	public static void validateEmail (String email, Controller controller) throws InvalidEmailException {
		if (email == null || email == "")
			throw new InvalidEmailException("Email is empty");
		if (!isValidEmailAddress (email))
			throw new InvalidEmailException("Email is not valid");
		try {
			if (controller.isEmailTaken(email))
				throw new InvalidEmailException(email + " is registered");
		} catch (ControllerException e) {
			throw new InvalidEmailException(e.getMessage());
		}
	}
	
	private static void p(Object...o) {
		for (Object obj : o)
			System.out.println(obj);
	}

	
	public static void validatePassword (String password) throws InvalidPasswordException {
		if (password == null || password.equals(""))
			throw new InvalidPasswordException("Password is empty");
		if (password.length() < 6)
			throw new InvalidPasswordException("Password must be at least 6 characters");
	}
	
	public static void validateUsername (String username, Controller controller) throws InvalidUsernameException {
		if (username == null || username.equals(""))
			throw new InvalidUsernameException("Username is empty");
		String reservedString = ":/?#[]@!$&'/)*+,;=\\";
		char[] reserved = reservedString.toCharArray();
		for (char reservedWord : reserved) {
			if (username.indexOf(reservedWord) != -1)
				throw new InvalidUsernameException ("Username cannot contain reserved symbols like :/?#[]@!$&'\\)*+,;="); 
		}
		if (username.split(" ").length > 1) {
			throw new InvalidUsernameException("Username cannot contain space");
		}
		if (username.length() > 40)
			throw new InvalidUsernameException("Username must be less than 40 characters");
		if (controller == null)
			throw new InvalidUsernameException("Erranda is down we are working on it");
		try {
			if (controller.isUsernameTaken(username))
				throw new InvalidUsernameException(username + " is taken");
		} catch (ControllerException e) {
			throw new InvalidUsernameException(e.getMessage());
		}
	}
	
	
	
	
	private static void validateName (String label, String name) throws ControllerException {
		if (name == null || name == "")
			throw new ControllerException(label + " is empty");
		if (!StringUtils.isAlpha(name))
			throw new ControllerException(label + " should contain only letters a-z");
		if (name.length() > 20)
			throw new ControllerException(label + " cannot be more than 20 characters long");
	}
	
	private static boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}
}
