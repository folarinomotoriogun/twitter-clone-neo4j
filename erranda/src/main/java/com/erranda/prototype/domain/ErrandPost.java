package com.erranda.prototype.domain;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type = "POSTED")
public class ErrandPost extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	PlatformUser owner;

	@EndNode
	Errand errand;

}
