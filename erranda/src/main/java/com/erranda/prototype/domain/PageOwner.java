package com.erranda.prototype.domain;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity (type = "OWNS_PAGE")
public class PageOwner extends RelationshipObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	private PlatformUser owner;
	
	@EndNode
	private Page business;
	
	private Boolean isPageCreator;
	
	private Boolean isPageAdministrator;

	public PlatformUser getOwner() {
		return owner;
	}

	public void setOwner(PlatformUser owner) {
		this.owner = owner;
	}

	public Page getPage() {
		return business;
	}

	public void setPage(Page business) {
		this.business = business;
	}

	public Boolean getIsPageCreator() {
		return isPageCreator;
	}

	public void setIsPageCreator(Boolean isPageCreator) {
		this.isPageCreator = isPageCreator;
	}

	public Boolean getIsPageAdministrator() {
		return isPageAdministrator;
	}

	public void setIsPageAdministrator(Boolean isPageAdministrator) {
		this.isPageAdministrator = isPageAdministrator;
	}
	
}
