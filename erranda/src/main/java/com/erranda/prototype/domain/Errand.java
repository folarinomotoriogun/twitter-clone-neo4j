package com.erranda.prototype.domain;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.apache.wicket.util.string.Strings;
import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphProperty;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.annotation.RelatedToVia;
import org.springframework.data.neo4j.support.index.IndexType;

import com.erranda.prototype.Config;

@NodeEntity
public class Errand extends AbstractNode implements Comparable<Errand> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3281994984579896628L;
	
	protected Date added;

	private long dateAddedLong;
	
	public Date timeline;

	@NotNull
	@Indexed (indexType = IndexType.FULLTEXT, indexName = "ErrandDetails")
	private String details;

	private String link;

	private Boolean anonymous = false;

	private String pictureUrl;

	private Boolean isExternalUrl;

	private String deliveryLocation;

	private String status;

	private String errandLocation;

	private Long ownerId;

	private Integer nomCount = 0;

	private String metaInfo;
	
	private String meta;

	private Integer repliesCount = 0;

	@GraphProperty(propertyType = Long.class)
	private Long expiryDate;

	private Integer bounty = 0;

	private Integer shareCount = 0;

	// @NotNull
	@Fetch
	@RelatedToVia (type="POSTED")
	private Post post;
	
	private Integer kudosCount = 0;

	@RelatedTo(type = "FULFILLED", direction = Direction.INCOMING)
	private PlatformUser fulfilled;

	private String privacy;
	
	private Integer imageWidth;
	
	private Integer imageHeight;
	
	

	public Errand() {
	}

	public void close() {
		status = "CLOSED";
	}

	public void fulfilled() {
		status = "FULFILLED";
	}

	public void open() {
		status = "OPEN";
	}

	public void expired() {
		status = "EXPIRED";
	}

	public Date getAdded() {
		return added;
	}

	public void setAdded(Date added) {
		setDateAddedLong(added.getTime());
		this.added = added;
	}

	public String getPictureUrl() {
		if (pictureUrl != null)
			this.pictureUrl = Strings.escapeMarkup(pictureUrl).toString();
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public PlatformUser getOwner() {
		if (post == null)
			post = new Post();
		return post.getOwner();
	}

	public void setOwner(PlatformUser owner) {
		if (post == null)
			post = new Post();
		this.post.setOwner(owner);
	}
	
	public void setPost (Post post) {
		this.post = post;
	}

	public String getLink() {
		if (link != null)
			link  = Strings.escapeMarkup(details).toString();
		return link;
	}

	public void setLink(String link) {
		if (link != null && !link.contains("http") && !link.contains("https")) {
			this.link = "http://" + this.link;
		}
	}

	@Override
	public int compareTo(Errand post) {
		return added.compareTo(post.getAdded());
	}

	public String getDetails() {
		if (details == null)
			return details;
		return Strings.escapeMarkup(details).toString();
	}
	
	public String getDetailsRaw () {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
	public void setDetailsRaw(String details) {
		this.details = details;
	}

	public Boolean getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(Boolean anonymous) {
		this.anonymous = anonymous;
	}

	@Override
	public String toString() {
		return details + " " + added;
	}

	public Boolean getIsExternalUrl() {
		return isExternalUrl;
	}

	public void setIsExternalUrl(Boolean isExternalUrl) {
		this.isExternalUrl = isExternalUrl;
	}

	public Long getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Long expiryDate) {
		this.expiryDate = expiryDate;
	}

	public void empty() {
		this.setAdded(null);
		this.setAnonymous(null);
		this.setDetails(null);
		this.setExpiryDate(null);
		this.setLink(null);
		this.setOwner(null);
		this.setPictureUrl(null);
	}

	public String getDeliveryLocation() {
		return deliveryLocation;
	}

	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}

	public String getErrandLink() {
		return Config.ERRAND_NAME_LINK + "/" + id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public PlatformUser getFulfilled() {
		return fulfilled;
	}

	public void setFulfilled(PlatformUser fulfilled) {
		this.fulfilled = fulfilled;
	}

	public long getDateAddedLong() {
		return dateAddedLong;
	}

	public void setDateAddedLong(long dateAddedLong) {
		this.dateAddedLong = dateAddedLong;
	}

	public String getErrandLocation() {
		return errandLocation;
	}

	public void setErrandLocation(String errandLocation) {
		this.errandLocation = errandLocation;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public Integer getNomCount() {
		return nomCount;
	}

	public void setNomCount(Integer nomCount) {
		this.nomCount = nomCount;
	}

	public Integer getRepliesCount() {
		return repliesCount;
	}

	public void setRepliesCount(Integer repliesCount) {
		this.repliesCount = repliesCount;
	}

	public Integer getBounty() {
		return bounty;
	}

	public void setBounty(Integer bounty) {
		this.bounty = bounty;
	}

	public Integer getShareCount() {
		return shareCount;
	}

	public void setShareCount(Integer shareCount) {
		this.shareCount = shareCount;
	}

	public String getPrivacy() {
		return privacy;
	}

	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}

	public String getMetaInfo() {
		return metaInfo;
	}

	public void setMetaInfo(String metaInfo) {
		this.metaInfo = metaInfo;
	}

	public Integer getImageWidth() {
		return imageWidth;
	}

	public void setImageWidth(Integer imageWidth) {
		this.imageWidth = imageWidth;
	}

	public Integer getImageHeight() {
		return imageHeight;
	}

	public void setImageHeight(Integer imageHeight) {
		this.imageHeight = imageHeight;
	}

	public Integer getKudosCount() {
		return kudosCount.intValue();
	}

	public void setKudosCount(Integer kudosCount) {
		this.kudosCount = kudosCount;
	}
	
	
	
	@Indexed(indexType = IndexType.POINT, indexName = "ErrandLocation")
	private String wkt;

	public void setLocation(float lat, float lon) {
		this.wkt = String.format("POINT( %.2f %.2f )", lat, lon);
	}

	public void setLocation(double lat, double lon) {
		this.wkt = String.format("POINT( %.2f %.2f )", lat, lon);
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}


}
