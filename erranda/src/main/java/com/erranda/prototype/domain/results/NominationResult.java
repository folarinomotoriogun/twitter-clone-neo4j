package com.erranda.prototype.domain.results;

import java.util.Date;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;

@QueryResult
public interface NominationResult {

	@ResultColumn("nominator")
	PlatformUser getNominator();

	@ResultColumn("nominated")
	PlatformUser getNominated();

	@ResultColumn("errand")
	Errand getErrand();

	@ResultColumn("message")
	String getMessage();

	@ResultColumn("ctime")
	Date getCtime();

	@ResultColumn("id")
	Long getId();

}