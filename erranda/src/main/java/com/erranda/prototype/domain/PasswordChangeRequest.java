package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.StartNode;

public class PasswordChangeRequest extends RelationshipObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	PlatformUser person;

	@EndNode
	PasswordHash hash;

	Date requestDate;

}
