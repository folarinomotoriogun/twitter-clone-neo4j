package com.erranda.prototype.domain;

import java.util.Date;

import org.apache.wicket.util.string.Strings;
import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.erranda.prototype.domain.results.CommentResult;

@RelationshipEntity(type = "COMMENTED")
public class Commented extends RelationshipObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private @StartNode @Fetch PlatformUser sender;

	private @EndNode @Fetch Errand errand;

	private Date ctime;

	private String message;

	private Boolean anonymous;
	
	private String photoUrl;

	public Commented() {

	}
	
	public Commented(CommentResult comment) {
		setCtime(comment.getCtime());
		setErrand(comment.getErrand());
		setMessage(comment.getMessage());
		setSender(comment.getSender());
		setId(comment.getId());
		setAnonymous(comment.getAnonymous());
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public String getMessage() {
		if (message != null)
			return Strings.escapeMarkup(message).toString();
		else
			return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Errand getErrand() {
		return errand;
	}

	public void setErrand(Errand errand) {
		this.errand = errand;
	}

	public PlatformUser getSender() {
		return sender;
	}

	public void setSender(PlatformUser sender) {
		this.sender = sender;
	}

	public Boolean getAnonymous() {
		return anonymous;
	}

	public void setAnonymous(Boolean anonymous) {
		this.anonymous = anonymous;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}
}
