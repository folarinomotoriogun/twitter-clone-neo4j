package com.erranda.prototype.domain.results;

import java.util.Date;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.erranda.prototype.domain.PlatformUser;

@QueryResult
public interface BlockResult {

	@ResultColumn("sender")
	PlatformUser getSender();

	@ResultColumn("receiver")
	PlatformUser getReceiver();

	@ResultColumn("ctime")
	Date getCtime();

	@ResultColumn("id")
	public Long getId();
}