package com.erranda.prototype.domain;

public class InvalidLastNameException extends PersonControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public InvalidLastNameException(String message) {
		super(message);
	}

}
