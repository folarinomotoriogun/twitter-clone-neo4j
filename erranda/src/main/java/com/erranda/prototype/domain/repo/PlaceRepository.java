package com.erranda.prototype.domain.repo;

import java.util.List;

import org.springframework.data.neo4j.repository.GraphRepository;

import com.erranda.prototype.domain.Place;

public interface PlaceRepository extends GraphRepository<Place> {
	List<Place> findByNameIgnoreCase(String name);
}
