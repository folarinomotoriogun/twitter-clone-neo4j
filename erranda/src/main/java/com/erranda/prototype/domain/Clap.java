package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type = "CLAP")
class Clap extends RelationshipObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@StartNode
	PlatformUser sender;

	@EndNode
	PlatformUser receiver;

	String reference;

	Date date;

}