package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.erranda.prototype.domain.results.FriendsResult;

@RelationshipEntity(type = "DONT_RECOMMEND")
public class DontRecommend extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DontRecommend() {
	}

	public DontRecommend(FriendsResult result) {
		sender = result.getSender();
		receiver = result.getReceiver();
		receiver.setFriendsCount(result.getReceiverFriendsCount());
		ctime = result.getCtime();
		id = result.getId();
	}

	@StartNode
	public PlatformUser sender;

	@EndNode
	public PlatformUser receiver;

	public Date ctime;

}