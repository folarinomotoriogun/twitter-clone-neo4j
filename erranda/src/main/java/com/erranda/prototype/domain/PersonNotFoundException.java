package com.erranda.prototype.domain;

public class PersonNotFoundException extends PersonControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public PersonNotFoundException(String message) {
		super(message);
	}

}
