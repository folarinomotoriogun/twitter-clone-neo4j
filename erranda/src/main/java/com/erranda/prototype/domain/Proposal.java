package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.erranda.prototype.domain.results.ProposalResult;

@RelationshipEntity(type = "PROPOSED")
public class Proposal extends RelationshipObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private @StartNode PlatformUser sender;

	private @EndNode PlatformUser receiver;

	private Long sid;

	private Long rid;

	private Long eid;

	private Boolean confirmed;

	private String message;

	private Date ctime;

	public Proposal() {
	}

	public Proposal(ProposalResult result) {
		setPerson(result.getPerson());
		setEid(result.getEid());
		setConfirmed(result.getConfirmed());
		setMessage(result.getMessage());
		setCtime(result.getCtime());
		setId(result.getId());
		setReceiver(result.getReceiver());
		setSid(result.getSid());
		setRid(result.getRid());
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}

	public PlatformUser getPerson() {
		return sender;
	}

	public void setPerson(PlatformUser person) {
		this.sender = person;
	}

	public Long getEid() {
		return eid;
	}

	public void setEid(Long eid) {
		this.eid = eid;
	}

	public PlatformUser getReceiver() {
		return receiver;
	}

	public void setReceiver(PlatformUser receiver) {
		this.receiver = receiver;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public Long getRid() {
		return rid;
	}

	public void setRid(Long rid) {
		this.rid = rid;
	}

}