package com.erranda.prototype.domain.results;

import java.util.Date;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.erranda.prototype.domain.PlatformUser;

@QueryResult
public interface FriendsResult {

	@ResultColumn("sender")
	public PlatformUser getSender();

	@ResultColumn("receiver")
	public PlatformUser getReceiver();

	@ResultColumn("receiverFriendsCount")
	public Integer getReceiverFriendsCount();

	@ResultColumn("ctime")
	public Date getCtime();

	@ResultColumn("id")
	public Long getId();

}
