package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

@RelationshipEntity(type = "GAVE_KUDOS")
public class Kudos extends RelationshipObject {

	private static final long serialVersionUID = 1L;

	@StartNode
	PlatformUser person;
	
	@EndNode
	Errand errand;

	Date ctime;
	
	public PlatformUser getPerson () {
		return person;
	}
	
	public Errand getErrand () {
		return errand;
	}

}