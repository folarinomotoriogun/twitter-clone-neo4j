package com.erranda.prototype.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.erranda.prototype.Config;
import com.erranda.prototype.MailMan;
import com.erranda.prototype.domain.repo.AbstractRepository;
import com.erranda.prototype.domain.repo.PageRepository;
import com.erranda.prototype.domain.repo.ConversationRepository;
import com.erranda.prototype.domain.repo.ErrandRepository;
import com.erranda.prototype.domain.repo.PlaceRepository;
import com.erranda.prototype.domain.repo.PlatformUserRepository;
import com.erranda.prototype.domain.repo.PostRepository;
import com.erranda.prototype.domain.repo.RelationshipRepository;
import com.erranda.prototype.domain.results.CommentResult;
import com.erranda.prototype.domain.results.ConversationResult;
import com.erranda.prototype.domain.results.FriendRequestResult;
import com.erranda.prototype.domain.results.MessageResult;
import com.erranda.prototype.domain.results.NotificationResult;
import com.erranda.prototype.domain.results.ProposalResult;
import com.erranda.prototype.domain.results.SuggestionResult;

@Component
public class ControllerBasedOnNeo4j implements Controller {

	@Autowired
	PlatformUserRepository personRepo;

	@Autowired
	ErrandRepository errandRepo;

	@Autowired
	ConversationRepository conversationRepo;

	@Autowired
	RelationshipRepository friendRequestRepo;

	@Autowired
	AbstractRepository abstractRepo;

	@Autowired
	PlaceRepository placeRepo;

	@Autowired
	PostRepository postRepo;

	@Autowired
	Neo4jTemplate template;

	@Autowired
	PageRepository pageRepo;

	/*
	 * Neo4jResultGenerator restService = new Neo4jResultGenerator(
	 * Config.DATABASE_ENDPOINT, Config.DATABASE_USERNAME,
	 * Config.DATABASE_PASSWORD);
	 */

	@Override
	@Transactional
	/**
	 * First name and Last name cannot be longer than 20 characters must not be null and must be alphabetical
	 */
	public PlatformUser signUp(PlatformUser person) throws ControllerException {
		assertNotNull(person, "person cannot be null");
		DomainValidator.validateFirstName(person.getFirstName());
		DomainValidator.validateLastName(person.getLastName());
		DomainValidator.validateEmail(person.getEmail(), this);
		DomainValidator.validateUsername(person.getUsername(), this);
		DomainValidator.validatePassword(person.getPassword());
		person.setCreated(new Date());
		if (person.getLocalityName() == null || person.getLocalityName().equals("Global")) {
			person.setLongitude(0.00);
			person.setLatitude(0.00);
			person.setLocation();
		} else {
			Double[] cords = geoLocator(person.getLocalityName());
			person.setLongitude(cords[1]);
			person.setLatitude(cords[0]);
			person.setLocation();
		}
		person.setType(Config.PLATFORM_TYPE_PERSON);
		person.setVerificationKey(UUID.randomUUID().toString());
		person.setVerified(false);
		person.setLocation();
		person = template.save(person);
		Notification n1 = new Notification();
		n1.setCtime(new Date());
		n1.setType("Nominate");
		n1.setDetails("We are throwing a party at Erranda");
		n1.setPictureLink("http://www.gambarphoto.com/wp-content/uploads/2014/07/bbm-dancing-emoticonsbest-fb---kl--2013-xtiu0jrt.png");
		n1.setLink("#");
		n1 = template.save(n1);
		PersonNotification pn = new PersonNotification();
		pn.isRead = false;
		pn.notification = n1;
		pn.receiver = person;
		template.save(pn);
		PlatformUser owner = personRepo
				.findByUsername("foo");
		PlatformUser prTeam = personRepo
				.findByUsername("errandaconnect");
		if (owner != null) {
			this.sendFriendRequest(owner.getId(), person.getId());
			this.sendMessage(
					owner.getId(),
					person.getId(),
					String.format(
							"Hi. You can talk to me anytime about your experience on Erranda.",
							person.getName().split(" ")[0]));
		}
		if (prTeam != null) {
			followUser(prTeam.getId(), person.getId());
		}

		return person;
	}

	private void validateUserName(String username) throws ControllerException {
		assertNotNull(username, "Username cannot be empty");
		username = username.toLowerCase().trim();
		assertNotNull(username, "Please supply a username");
		if (username.indexOf(" ") != -1)
			throw new ControllerException(
					"Your username cannot be more than a single word.");
		if (!StringUtils.isAlphanumeric(username))
			throw new ControllerException(
					"Invalid username! Alphanumerics only and not all numeric.");
		if (personRepo.findByUsername(username) != null)
			throw new ControllerException("Username has been taken");
	}

	@Transactional
	private Double[] geoLocator(String name) throws ControllerException {
		Double[] cord = searchLocationInDatabase(name);
		if (cord == null) {
			Double[] geolocation = GoogleGeocoder.performGeoCoding(name);
			if (geolocation == null
					&& Config.WICKET_DEPLOYMENT.toLowerCase().equals(
							"deployment"))
				throw new ControllerException(name + " not found");
			else {
				Place place = new Place(name, geolocation[0], geolocation[1]);
				placeRepo.save(place);
				return geolocation;
			}
		} else
			return cord;
	}

	private Double[] searchLocationInDatabase(String name) {
		List<Place> place = placeRepo.findByNameIgnoreCase(name);
		if (place.size() == 1) {
			return new Double[] { place.get(0).getLatitude(),
					place.get(0).getLongitude() };
		}

		else
			return null;
	}

	@Override
	@Transactional
	public void sendMessage(Long sid, Long rid, String data)
			throws ControllerException {
		assertNotNull(data, "Message cannot be empty");
		PlatformUser sender = validatePerson(sid);
		PlatformUser receiver = validatePerson(rid);
		unblockPerson(sid, rid);
		Message message = new Message();
		message.setSid(sender.getId());
		message.setRid(receiver.getId());
		message.setPayload(data);
		message.setIsRead(false);
		message.setSender(sender);
		message.setReceiver(receiver);
		Conversation c = getConversation(sid, rid);
		message.setCid(c.getId());
		message.setCtime(new Date());
		message = template.save(message);
		c.setLastMessageId(message.getId());
		template.save(c);
		Long cid = c.getId();
		if (!abstractRepo.conversationNotificationExists(cid)) {
			MessageNotification n = new MessageNotification();
			n.receiver = receiver;
			n.cid = cid;
			template.save(n);
		}
	}

	@Override
	@Transactional
	public PlatformUser login(String email, String password)
			throws ControllerException {
		if (email == null || password == null)
			throw new PersonNotFoundException(
					"User account details supplied does not match any record");
		PlatformUser p = personRepo.findByEmail(email.trim().toLowerCase());
		if (p == null) {
			p = personRepo.findByUsername(email.trim().toLowerCase());
		}
		if (p == null)
			throw new PersonNotFoundException(
					"User account details supplied does not match any record");
		if (p.getPassword().equals(password)) {
			if (p.getStatus().equals("DEACTIVATED")) {
				p.setStatus("ACTIVE");
				template.save(p);
			}
			return p;
		} else
			throw new PersonNotFoundException("Password incorrect");
	}

	@Override
	@Transactional
	public Errand postErrand(Errand errand, Long ownerId, List<PlatformUser> nominated)
			throws ControllerException {
		DomainValidator<Errand> d = new DomainValidator<Errand>();
		if (!d.valid(errand))
			throw new ErrandControllerException("Your request is not valid");
		PlatformUser person = this.findPerson(ownerId);
		if (!person.getStatus().equals("ACTIVE"))
			throw new ErrandControllerException("You are currently not active");
		person.setPreferredPrivacyOption(errand.getPrivacy());
		person = personRepo.save(person);

		if (errand.getPrivacy().equals("PUBLIC")) {
			errand.setMetaInfo("Shared with everyone");
			if (person.getFirstLocalPost()) {
				person.setFirstLocalPost(false);
				person = personRepo.save(person);
			}
		} else if (errand.getPrivacy().equals("FRIENDS")) {
			errand.setMetaInfo("Shared with Friends");
			if (person.getFirstFriendPost()) {
				person.setFirstFriendPost(false);
				person = personRepo.save(person);
			}
		} else if (errand.getPrivacy().equals("PRIVATE")) {
			errand.setMetaInfo("Shared privately");
			if (person.getFirstPrivatePost()) {
				person.setFirstPrivatePost(false);
				person = personRepo.save(person);
			}			
		} else if (errand.getPrivacy().equals("PUBLIC")) {
			errand.setMetaInfo("Shared with everyone");
		} else {
			throw new ControllerException("Select privacy");
		}
		PlatformUser owner = person;
		errand.setOwner(owner);
		errand.setOwnerId(owner.getId());
		errand.setLocation(person.getLatitude(), person.getLongitude());
		Post post = new Post();
		post.ctime = new Date ();
		post.owner = person;
		post.errand = errand;
		errand.setPost(post);
		errand = errandRepo.save(errand);
		if (nominated != null)
			nominatePerson(nominated, errand.getId(), ownerId, "");
		parseAndNominate(errand);
		return errand;
	}
	
	private void parseAndNominate(Errand errand) throws ControllerException {
		if (errand == null || errand.getDetails() == null)
			return;
		String details = errand.getDetails();
		char[] letters = details.toCharArray();
		StringBuffer current = new StringBuffer();
		Boolean appendNext = false;
		List<PlatformUser> nominatePeople = new ArrayList<PlatformUser>();

		for (int i = 0; i < letters.length; i++) {
			char c = letters[i];
			if (c == '@') {
				appendNext = true;
			} else if (c == ' ') {
				current.append(c);
				appendNext = false;
			} else if (c != ' ' && c != '@' && appendNext) {
				current.append(c);
			}
		}

		String[] usernames = current.toString().split(" ");

		for (String username : usernames) {
			username = username.toLowerCase();
			PlatformUser person = personRepo.findByUsername(username);
			if (person != null)
				nominatePeople.add(person);
			else if (username.length() >= 2) {
				PlatformUser newUser = new PlatformUser();
				newUser.setUsername(username);
				newUser.setName(StringUtils.capitalize(username));
				newUser.setCreated(new Date());
				newUser.setLocalityName("Global");
				newUser.setLongitude(0.00);
				newUser.setLatitude(0.00);
				newUser.setLocation();
				newUser.setType(Config.PLATFORM_TYPE_PAGE);
				nominatePeople.add(personRepo.save(newUser));
			}
		}
		nominatePerson(nominatePeople, errand.getId(), errand.getOwnerId(),
				null);
	}

	private void parseAndComment(Commented comment) throws ControllerException {
		if (comment == null || comment.getMessage() == null)
			return;
		String details = comment.getMessage();
		char[] letters = details.toCharArray();
		StringBuffer current = new StringBuffer();
		Boolean appendNext = false;
		List<PlatformUser> nominatePeople = new ArrayList<PlatformUser>();

		for (int i = 0; i < letters.length; i++) {
			char c = letters[i];
			if (c == '@') {
				appendNext = true;
			} else if (c == ' ') {
				current.append(c);
				appendNext = false;
			} else if (c != ' ' && c != '@' && appendNext) {
				current.append(c);
			}
		}

		String[] usernames = current.toString().split(" ");

		for (String username : usernames) {
			username = username.toLowerCase();
			PlatformUser person = personRepo.findByUsername(username);
			if (person != null)
				nominatePeople.add(person);
			else if (username.length() >= 2) {
				PlatformUser newUser = new PlatformUser();
				newUser.setUsername(username);
				newUser.setName(StringUtils.capitalize(username));
				newUser.setCreated(new Date());
				newUser.setLocalityName("Global");
				newUser.setLongitude(0.00);
				newUser.setLatitude(0.00);
				newUser.setLocation();
				newUser.setType(Config.PLATFORM_TYPE_PAGE);
				nominatePeople.add(personRepo.save(newUser));
			}
		}

		nominatePerson(nominatePeople, comment.getErrand().getId(), comment
				.getSender().getId(), null);
	}

	

	@Override
	@Transactional
	public void postErrand(Errand... errands) throws ControllerException {
		for (Errand errand : errands) {
			postErrand(errand);
		}
	}
	

	@Override
	@Transactional
	public List<PostViewItem> interestFeed(Long pid, int skip, int limit)
			throws ControllerException {
		System.out.println(skip + " " + limit);
		PlatformUser person = validatePerson(pid);
		List<PostViewItem> errands = new ArrayList<PostViewItem>();
		/*String spatialQuery = String
				.format("START n=node:ErrandLocation('withinDistance:[%.2f, %.2f, %.2f]') WITH n MATCH (session:PlatformUser) WHERE id (session) =  %s WITH n, session OPTIONAL MATCH (a:PlatformUser)-[x:GAVE_KUDOS]->n WITH n, session, count(x) as kudosCount SET n.kudosCount = kudosCount WITH n, session MATCH (owner)-[r:POSTED]->n WHERE n.privacy = \"PUBLIC\" AND NOT (session-[:BLOCKED]-owner) AND NOT (session-[:BLOCKED]-n) AND (owner.status = \"ACTIVE\") RETURN r ORDER BY r.ctime DESC SKIP %d LIMIT %d",
						person.getLongitude(), person.getLatitude(),
						person.getWidth(), person.getId(), skip, limit);
		
		Iterator<Errand> iteratorSpatial = template.query(spatialQuery, null)
				.to(Errand.class).iterator();
		while (iteratorSpatial.hasNext()) {
			errands.add(iteratorSpatial.next());
		}*/
		
		String fatQuery = String
				.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH session-[:FOLLOWS]->(owner:PlatformUser)-[r:RECOMMENDED|POSTED|GOT_NOMINATED]->n WHERE (n.privacy = 'FRIENDS' AND (session-[:FRIENDS]-owner)) OR n.privacy = 'PUBLIC' OR (n.privacy = 'PRIVATE' AND (owner-[:NOMINATED {eid:id(n)}]->session OR id(owner) = id(session))) AND NOT (session-[:BLOCKED]-owner) AND NOT (session-[:BLOCKED]-n) AND (owner.status = 'ACTIVE') WITH session, owner, n, r, r.ctime as timeline, n as errand, CASE WHEN owner-[:RECOMMENDED]->n THEN 'BROADCASTED' WHEN owner-[:POSTED]->n THEN 'POSTED' WHEN owner-[:GOT_NOMINATED]->n THEN 'NOMINATED' ELSE 'CONNECTED' END AS relationship RETURN DISTINCT r, relationship ORDER BY r.ctime DESC SKIP %d LIMIT %d",
						person.getId(), skip, limit);

		// Remove multiple copying in prod
		// errands.addAll(restService.errandQuery(query2));
		Iterator<PostViewItem> iteratorFriends = template.query(fatQuery, null)
				.to(PostViewItem.class).iterator();
		while (iteratorFriends.hasNext()) {
			PostViewItem next = iteratorFriends.next();
			if (!errands.contains(next))
				errands.add(next);
		}
		
		return errands;
	}

	private void p(Object...o) {
		for (Object obj : o)
			System.out.println(obj);
	}

	public static void assertNotNull(Object o, ControllerException e)
			throws ControllerException {
		if (o == null)
			throw e;
	}

	public static void assertNotNull(Object o, String e)
			throws ControllerException {
		if (o == null)
			throw new ControllerException(e);
	}

	public static void assertNotNull(String e, Object... o)
			throws ControllerException {
		for (Object obj : o)
			if (obj == null)
				throw new ControllerException(e);
	}

	@Override
	@Transactional
	public List<PostViewItem> personFeed(Long pid, Long viewerId, int skip, int limit)
			throws ControllerException {
		PlatformUser person = validatePerson(pid);
		if (!person.getStatus().equals("ACTIVE"))
			throw new ControllerException("Person not found");
		if (this.isBlockedTwoWay(pid, viewerId))
			throw new ControllerException("Person not found");
		// Be sure to specify the post type for privacy reasons
		// Things this user posted
		List<PostViewItem> errands = new ArrayList<PostViewItem>();
		String query = String
				.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH (owner:PlatformUser) WHERE id(owner) = %s MATCH (owner:PlatformUser)-[r:RECOMMENDED|POSTED|GOT_NOMINATED]->n WHERE (n.privacy = 'FRIENDS' AND (session-[:FRIENDS]-owner) OR id(session) = id(owner)) OR n.privacy = 'PUBLIC' OR (n.privacy = 'PRIVATE' AND (owner-[:NOMINATED {eid:id(n)}]->session OR id(owner) = id(session))) AND NOT (session-[:BLOCKED]-owner) AND NOT (session-[:BLOCKED]-n) AND (owner.status = 'ACTIVE')  WITH session, owner, n, r, r.ctime as timeline, n as errand, CASE WHEN owner-[:RECOMMENDED]->n THEN 'BROADCASTED' WHEN owner-[:POSTED]->n THEN 'POSTED' WHEN owner-[:GOT_NOMINATED]->n THEN 'NOMINATED' ELSE 'CONNECTED' END AS relationship RETURN relationship, r ORDER BY r.ctime DESC SKIP %d LIMIT %d",
						viewerId, person.getId(), skip, limit);
		Iterator<PostViewItem> iteratorPosts = template.query(query, null)
				.to(PostViewItem.class).iterator();
		while (iteratorPosts.hasNext()) {
			PostViewItem next = iteratorPosts.next();
			if (!errands.contains(next))
				errands.add(next);
		}
		
		return errands;
	}

	private PlatformUser validatePerson(PlatformUser person)
			throws ControllerException {
		DomainValidator.assertNotNull(person.getId(), new ControllerException(
				"Your request is not valid"));
		String pQuery = String
				.format("MATCH (person:PlatformUser) where id (person) = %s AND person.status = \"ACTIVE\" return person",
						person.getId());
		person = template.query(pQuery, null).to(PlatformUser.class)
				.singleOrNull();
		DomainValidator.assertNotNull(person, new ControllerException(
				"Person not found"));
		return person;
	}

	@Override
	@Transactional
	public PlatformUser updateProfile(PlatformUser person)
			throws ControllerException {
		PlatformUser previous = validatePerson(person);
		String bio = person.getBio();
		String username = person.getUsername();
		if (person.getUsername() != null
				&& !person.getUsername().equals(previous.getUsername()))
			validateUserName(username);
		if (!person.getLocalityName().equals(previous.getLocalityName()))
			updateLocation(person.getId(), person.getLocalityName());
		previous.setUsername((username != null) ? username : previous
				.getUsername());
		previous.setBio((bio != null) ? bio : previous.getBio());
		previous = personRepo.save(previous);
		if (!person.getLocalityName().equals(previous.getLocalityName()))
			updateLocation(person.getId(), person.getLocalityName());
		return previous;
	}

	@Override
	@Transactional
	public PlatformUser updatePassword(Long pid, String newPass)
			throws ControllerException {
		PlatformUser previous = validatePerson(pid);
		previous.setPassword(newPass);
		previous = personRepo.save(previous);
		Notification n = new Notification();
		n.setCtime(new Date());
		n.setDetails("You changed your password");
		n.setLink("#");
		n.setPictureLink(previous.getThumbNail());
		n.setType("Password Change");
		n = template.save(n);
		PersonNotification pn = new PersonNotification();
		pn.isRead = false;
		pn.notification = n;
		pn.receiver = previous;
		template.save(pn);
		return previous;
	}

	@Override
	@Transactional
	public void deactivateAccount(PlatformUser person)
			throws ControllerException {
		person = validatePerson(person);
		person.setStatus("DEACTIVATED");
		personRepo.save(person);
	}

	@Override
	@Transactional
	public void sendMail(Long pid, String message) throws ControllerException {
		PlatformUser person = validatePerson(pid);
		MailMan mailcomp = new MailMan();
		try {
			mailcomp.sendMessage("Hello", person.getEmail(), message);
		} catch (MessagingException e) {
			throw new ControllerException(
					"We couldn't send your notification email");
		}
	}

	@Override
	@Transactional
	public void sendMail(String email, String message)
			throws ControllerException {
		Boolean valid = isValidEmailAddress(email);
		if (!valid)
			throw new ControllerException("Email not valid");
		MailMan mailcomp = new MailMan();
		try {
			mailcomp.sendMessage("Hello", email, message);
		} catch (MessagingException e) {
			throw new ControllerException(
					"We couldn't send your notification email");
		}
	}

	@Override
	@Transactional
	public void updateLocation(Long pid, String placeName)
			throws ControllerException {
		PlatformUser person = validatePerson(pid);
		Double[] geolocation = geoLocator(placeName);
		if (geolocation == null) {
			geolocation = geoLocator(placeName);
			if (geolocation == null
					&& Config.WICKET_DEPLOYMENT.toLowerCase().equals(
							"deployment")) {
				throw new ControllerException(placeName + " not found");
			}
		}
		person.setLocalityName(placeName);
		person.setLongitude(geolocation[1]);
		person.setLatitude(geolocation[0]);
		person.setLocation();
		personRepo.save(person);
	}

	@Override
	@Transactional
	public PlatformUser changePassword(Long pid, String currentPassword,
			String newPassword, String confirmPassword)
			throws ControllerException {
		PlatformUser person = validatePerson(pid);
		if (person.getPassword().equals(currentPassword)) {
			if (newPassword.equals(confirmPassword)) {
				if (newPassword.length() >= 6) {
					person.setPassword(newPassword);
					person.setForgotPassword(null);
					template.save(person);
					return person;
				} else {
					throw new ControllerException(
							"Password cannot be less than 6 characters");
				}
			} else {
				throw new ControllerException(
						"New password does not match your confirmation password");
			}
		} else {
			throw new ControllerException("Password is not correct");
		}
	}

	@Override
	@Transactional
	public Conversation getConversation(Long sid, Long rid)
			throws ControllerException {
		ConversationResult r = conversationRepo.ourConversation(sid, rid);
		Conversation c = null;
		PlatformUser sender = validatePerson(sid);
		PlatformUser receiver = validatePerson(rid);
		receiver.setId(rid);
		if (r == null) {
			c = new Conversation(sender, receiver);
			c = conversationRepo.save(c);
		} else {
			c = new Conversation(r);
			c = template.save(c);
		}
		return c;
	}

	private PlatformUser validatePerson(Long pid) throws ControllerException {
		assertNotNull(pid, "Person not found");
		String pQuery = String
				.format("MATCH (person:PlatformUser) where id (person) = %s AND person.status = \"ACTIVE\" return person",
						pid);
		PlatformUser person = template.query(pQuery, null)
				.to(PlatformUser.class).singleOrNull();
		DomainValidator.assertNotNull(person, new ControllerException(
				"Person not found"));
		return person;
	}

	private Errand validateErrand(Long eid) throws ControllerException {
		assertNotNull(eid, "Id cannot be null");
		String query = String.format("MATCH (owner:PlatformUser)-[r:POSTED]->(n:Errand) WHERE id(n) = %s RETURN n LIMIT 1", eid);
		Post post = template.query(query, null).to(Post.class).singleOrNull();
		if (post == null)
			throw new ControllerException("Post not found");
		post.errand.setOwner(post.owner);
		return post.errand;
	}

	@Override
	@Transactional
	public void nominatePerson(List<PlatformUser> people, Long eid,
			Long personId, String data) throws ControllerException {
		Errand errand = validateErrand(eid);
		PlatformUser nominator = validatePerson(personId);
		if (nominator.getFirstNomination()) {
			nominator.setFirstNomination(false);
		}
		Notification n1 = new Notification();
		n1.setCtime(new Date());
		n1.setType("Nominate");
		n1.setDetails(nominator.getName() + " nominated you in this post");
		n1.setPictureLink(nominator.getThumbNail());
		n1.setLink(errand.getErrandLink());
		n1 = template.save(n1);
		for (PlatformUser p : people) {
			Boolean isOwner = errand.getOwnerId().equals(p.getId());
			if ((!areWeFriends(nominator.getId(), p.getId()) && equals(p.getType().equals(Config.PLATFORM_TYPE_PERSON))) && !isOwner) {
				Notification mention = new Notification();
				mention.setCtime(new Date());
				mention.setType("Mention");
				mention.setDetails(nominator.getName() + " mentioned you in this post");
				mention.setPictureLink(nominator.getThumbNail());
				mention.setLink(errand.getErrandLink());
				mention = template.save(mention);
				p = validatePerson(p);
				PersonNotification pn = new PersonNotification();
				pn.notification = mention;
				pn.receiver = p;
				pn.isRead = false;
				pn = template.save(pn);
				continue;
			} else if (!isOwner) {
				PlatformUser r = validatePerson(p);
				PersonNotification pn = new PersonNotification();
				pn.notification = n1;
				pn.receiver = r;
				pn.isRead = false;
				pn = template.save(pn);
				
				PostNominate nominate = getNomination(eid, r.getId());
				if (nominate == null) {
					nominate = new PostNominate();
					nominate.owner = r;
					nominate.errand = errand;
					nominate.ctime = new Date();
					template.save(nominate);
				} else {
					nominate.ctime = new Date();
					template.save(nominate);
				}
				
				if (!isNominatedByOwner(eid, r.getId())) {
					Nomination nom = new Nomination();
					nom.ctime = new Date();
					nom.nominator = nominator;
					nom.nominated = r;
					nom.eid = errand.getId();
					template.save(nom);
				}
			}
			
		}
	}

	private boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}

	@Override
	@Transactional
	public void acceptErrand(Long eid, Long pid, String data)
			throws ControllerException {
		Errand errand = validateErrand(eid);
		data = String
				.format("<a target='_blank' href='%s' title='Link to your errand'>Post Accepted! </a> ",
						errand.getErrandLink())
				+ data;
		this.sendMessage(pid, errand.getOwnerId(), data);
		PlatformUser person = validatePerson(pid);
		PlatformUser receiver = validatePerson(errand.getOwnerId());
		PostAccept postAccept = new PostAccept();
		postAccept.errand = errand;
		postAccept.owner = person;
		postAccept.ctime = new Date();
		template.save(postAccept);
		Proposal proposal = new Proposal();
		proposal.setEid(eid);
		proposal.setRid(receiver.getId());
		proposal.setSid(person.getId());
		proposal.setMessage("");
		proposal.setPerson(person);
		proposal.setConfirmed(false);
		proposal.setReceiver(receiver);
		proposal.setCtime(new Date());
		// Important otherwise template will overwrite the person object with
		// null
		proposal.setId(template.save(proposal).getId());
		addNotification(proposal, errand);
	}

	private void addNotification(Proposal proposal, Errand errand) {
		// Add a runnable job to a queue in production
		if (proposal.getId() != null) {
			List<ProposalResult> proposals = abstractRepo.proposalsByErrand(
					errand.getId(), 0, 100);
			Date ctime = proposal.getCtime();
			PlatformUser p = proposal.getPerson();
			Notification n = new Notification();
			n.setCtime(ctime);
			n.setType("Accept");
			n.setDetails(p.getName() + " also accepted this errand ");
			n.setLink(errand.getErrandLink());
			n.setPictureLink(p.getThumbNail());
			n = template.save(n);
			for (ProposalResult r : proposals) {
				if (r.getPerson().getId().equals(proposal.getSid()))
					continue;
				PersonNotification pn = new PersonNotification();
				pn.notification = n;
				pn.receiver = r.getPerson();
				pn.isRead = false;
				pn = template.save(pn);
			}
			Notification n1 = new Notification();
			n1.setCtime(ctime);
			n1.setType("Accept");
			n1.setDetails(p.getName() + " accepted your errand");
			n1.setLink(errand.getErrandLink());
			n1.setPictureLink(p.getThumbNail());
			n1 = template.save(n1);

			PersonNotification pn = new PersonNotification();
			pn.notification = n1;
			pn.receiver = errand.getOwner();
			pn.isRead = false;
			pn = template.save(pn);
			// Send push notifications and email for notification in production
		}
	}

	@Override
	@Transactional
	public void confirmProposal(Long proposalId, String reference)
			throws ControllerException {
		Proposal proposal = validateProposal(proposalId);
		proposal.setConfirmed(true);
		template.save(proposal);
		PlatformUser person = proposal.getPerson();
		Errand errand = validateErrand(proposal.getEid());
		// Dont do anything else if owner is the person confirming
		if (person.getId().equals(proposal.getRid()))
			return;
		ErrandPoint points = new ErrandPoint();
		points.errand = errand;
		points.eid = errand.getId();
		points.rid = person.getId();
		points.receiver = person;
		points.points = (errand.getBounty() > 0 ? errand.getBounty() : 100);
		points.prid = proposal.getId();
		points.ctime = new Date();
		template.save(points);
		Notification n1 = new Notification();
		n1.setCtime(new Date());
		n1.setType("Confirm");
		n1.setDetails(proposal.getReceiver().getName()
				+ " confirmed your errand");
		n1.setLink(errand.getErrandLink());
		n1.setPictureLink(proposal.getReceiver().getThumbNail());
		n1 = template.save(n1);
		PersonNotification pn = new PersonNotification();
		pn.notification = n1;
		pn.receiver = person;
		pn.isRead = false;
		pn = template.save(pn);

		// Send a notification message using our notification account
	}

	@Override
	@Transactional
	public void rejectProposal(Long proposalId) throws ControllerException {
		Proposal proposal = validateProposal(proposalId);
		template.delete(proposal);
	}

	private Proposal validateProposal(Long proposalId)
			throws ControllerException {
		assertNotNull(proposalId, "Proposal not found");
		ProposalResult result = abstractRepo.findOneProposal(proposalId);
		assertNotNull(result, new ControllerException("Proposal not found"));
		Proposal proposal = new Proposal(result);
		DomainValidator<Proposal> p = new DomainValidator<Proposal>();
		p.valid(proposal);
		return proposal;
	}

	@Override
	public void archiveErrand(Long eid) throws ControllerException {
		// Send errand location to the red sea
		Errand errand = validateErrand(eid);
		// errand.setLocation(22.0, 38.0);
		template.save(errand);
	}

	@Override
	@Transactional
	public FriendRequest sendFriendRequest(Long senderId, Long receiverId)
			throws ControllerException {
		PlatformUser sender = validatePerson(senderId);
		PlatformUser receiver = validatePerson(receiverId);
		if (isPendingFriendRequest(senderId, receiverId))
			throwEx("Request already sent");
		unblockPerson(senderId, receiverId);
		FriendRequest fr = new FriendRequest();
		fr.sender = sender;
		fr.receiver = receiver;
		fr.ctime = new Date();
		receiver.setUnseenFriendRequests(receiver.getUnseenFriendRequests() + 1);
		template.save(receiver);
		return template.save(fr);
	}

	private void throwEx(String message) throws ControllerException {
		throw new ControllerException(message);
	}

	@Override
	@Transactional
	public Node save(Node entity) throws ControllerException {
		assertNotNull(entity, "Cannot save null object");
		return template.save(entity);
	}

	@Override
	@Transactional
	public void acceptFriendRequest(Long frId) throws ControllerException {
		FriendRequest fr = new FriendRequest(validateFR(frId));
		Friends friend = new Friends();
		friend.sender = fr.sender;
		friend.receiver = fr.receiver;
		friend.ctime = new Date();
		template.save(friend);
		followUser(fr.sender.getId(), fr.receiver.getId());
		followUser(fr.receiver.getId(), fr.sender.getId());
		String query = String
				.format("MATCH (a:PlatformUser)-[r:FRIEND_REQUEST]-(b:PlatformUser) WHERE id (a) = %s AND id (b) = %s DELETE r",
						fr.sender.getId(), fr.receiver.getId());
		template.query(query, null);
		PlatformUser sender = fr.sender;
		Notification n = new Notification();
		n.setCtime(new Date());
		n.setLink(fr.receiver.getLink());
		n.setPictureLink(fr.receiver.getThumbNail());
		n.setType("Friends");
		n.setDetails(fr.receiver.name + " accepted your friend request");
		template.save(n);
		PersonNotification on = new PersonNotification();
		on.isRead = false;
		on.notification = n;
		on.receiver = sender;
		template.save(on);
	}

	private FriendRequestResult validateFR(Long frid)
			throws ControllerException {
		assertNotNull(frid, "You have no friend request sent to this person");
		FriendRequestResult fr = personRepo.findOneFriendRequest(frid);
		assertNotNull(fr, "You have no friend request sent to this person");
		return fr;
	}

	@Override
	@Transactional
	public Errand findErrand(Long eid) throws ControllerException {
		return validateErrand(eid);
	}

	@Override
	@Transactional
	public PlatformUser findPerson(Long pid) throws ControllerException {
		return validatePerson(pid);
	}

	@Override
	@Transactional
	public void removeFriend(Long sid, Long rid) throws ControllerException {
		unfollowUser(sid, rid);
		unfollowUser(rid, sid);
		dontRecommendPerson(sid, rid);
		dontRecommendPerson(rid, sid);
		personRepo.deleteFriendship(sid, rid);
	}

	@Override
	@Transactional
	public Commented comment(Long pid, Long eid, String message,
			Boolean anonymous) throws ControllerException {
		assertNotNull(message, new ControllerException(
				"Comment cannot be empty"));
		PlatformUser person = validatePerson(pid);
		Errand errand = validateErrand(eid);
		Commented comment = new Commented();
		comment.setAnonymous(anonymous);
		comment.setCtime(new Date());
		comment.setMessage(message);
		comment.setSender(person);
		comment.setErrand(errand);
		comment = template.save(comment);
		if (!errand.getPrivacy().equals("PRIVATE") || (errand.getPrivacy().equals("PRIVATE") && errand.getOwnerId().equals(person.getId())))
			parseAndComment(comment);
		addNotification(comment, errand, person);
		return comment;
	}

	private void addNotification(Commented comment, Errand errand,
			PlatformUser sender) throws ControllerException {
		// Add a runnable job to a queue in production
		if (comment.getId() != null) {
			Date ctime = comment.getCtime();
			Notification n = new Notification();
			n.setCtime(ctime);
			n.setType("Comment");
			if (comment.getAnonymous()) {
				n.setDetails("Anonymous also commented on this post");
				n.setPictureLink(new PlatformUser().getThumbNail());
			} else {
				n.setDetails(sender.getName() + " also commented on this post");
				n.setPictureLink(sender.getThumbNail());
			}
			n.setLink(comment.getErrand().getErrandLink());
			n = template.save(n);
			for (PlatformUser other : othersThatCommented(errand.getId(),
					sender.getId())) {
				if (other.getId() != errand.getOwnerId()) {
					PersonNotification pn = new PersonNotification();
					pn.notification = n;
					pn.receiver = other;
					pn.isRead = false;
					template.save(pn);
				}
			}

			if (sender.getId() != errand.getOwnerId()) {
				Notification n1 = new Notification();
				n1.setCtime(ctime);
				n1.setType("Comment");
				if (comment.getAnonymous()) {
					n1.setDetails("Anonymous commented on your post");
					n1.setPictureLink(new PlatformUser().getThumbNail());
				} else {
					n1.setDetails(sender.getName() + " commented on your post");
					n1.setPictureLink(sender.getThumbNail());
				}
				n1.setLink(comment.getErrand().getErrandLink());
				n1 = template.save(n1);
				PersonNotification pn = new PersonNotification();
				pn.notification = n1;
				pn.receiver = findPerson(errand.getOwnerId());
				pn.isRead = false;
				template.save(pn);
			}

		}
	}

	private Commented validateComment(Long cid) throws ControllerException {
		Commented c = null;
		try {
			c = template.findOne(cid, Commented.class);
		} catch (Exception e) {
			return null;
		}
		return c;
	}

	@Override
	@Transactional
	public void removeComment(Long cid) throws ControllerException {
		Commented c = validateComment(cid);
		if (c == null)
			return;
		template.delete(c);
	}

	@Override
	@Transactional
	public void rejectFriendRequest(Long frid) throws ControllerException {
		FriendRequest fr = new FriendRequest();
		fr.setId(frid);
		template.delete(fr);
	}

	@Override
	@Transactional
	public void rejectFriendRequest(Long sid, Long rid) {
		String query = String
				.format("MATCH (a:PlatformUser)-[r:FRIEND_REQUEST]->(b:PlatformUser) where id (a) = %s AND id (b) = %s DELETE r",
						sid, rid);
		template.query(query, null);
	}

	@Override
	@Transactional
	public List<Commented> getComments(Long viewer, Long eid, int skip,
			int limit) throws ControllerException {
		validateErrand(eid);
		String cQuery = String
				.format("MATCH (session:PlatformUser) where id(session) = %s MATCH (sender:PlatformUser)-[r:COMMENTED]->(errand:Errand) "
						+ " WHERE id (errand) = %s AND NOT (session-[:BLOCKED]-sender) AND (sender.status = \"ACTIVE\")"
						+ " return r" + " ORDER BY r.ctime ASC", viewer, eid);
		Iterator<Commented> results = template.query(cQuery, null)
				.to(Commented.class).iterator();
		List<Commented> comments = IteratorUtils.toList(results);
		return comments;
	}

	@Override
	@Transactional
	public List<Proposal> getProposalsByPerson(Long pid, int skip, int limit)
			throws ControllerException {
		validatePerson(pid);
		List<Proposal> proposals = new ArrayList<Proposal>();
		for (ProposalResult r : abstractRepo
				.proposalsByPerson(pid, skip, limit)) {
			proposals.add(new Proposal(r));
		}
		return proposals;
	}

	@Override
	@Transactional
	public List<Proposal> getProposalByErrand(Long eid, int skip, int limit)
			throws ControllerException {
		validateErrand(eid);
		List<Proposal> proposals = new ArrayList<Proposal>();
		for (ProposalResult r : abstractRepo
				.proposalsByErrand(eid, skip, limit)) {
			proposals.add(new Proposal(r));
		}
		return proposals;
	}

	@Override
	@Transactional
	public void blockPerson(Long sid, Long rid) throws ControllerException {
		PlatformUser sender = validatePerson(sid);
		PlatformUser receiver = validatePerson(rid);
		if (isBlocked(sid, rid))
			return;
		String query = String
				.format("MATCH (a:PlatformUser)-[r:FRIENDS|FRIEND_REQUEST]-(b:PlatformUser) where id (a) = %s AND id (b) = %s DELETE r",
						sid, rid);
		template.query(query, null);
		Block block = new Block();
		block.receiver = receiver;
		block.sender = sender;
		template.save(block);
	}

	@Override
	@Transactional
	public void unblockPerson(Long sid, Long rid) throws ControllerException {
		String query = String
				.format("MATCH (a:PlatformUser)-[r:BLOCKED]-(b:PlatformUser) where id (a) = %s AND id (b) = %s DELETE r",
						sid, rid);
		template.query(query, null);
	}

	@Override
	@Transactional
	public List<PlatformUser> friends(Long sid, Long pid, int skip, int limit)
			throws ControllerException {
		validatePerson(pid);
		String fQuery = String
				.format("match (session:PlatformUser) Where id (session) = %s match (sender:PlatformUser)-[r:FRIENDS]-receiver"
						+ " WHERE NOT session-[:BLOCKED]-receiver AND id (sender) = %s AND NOT (sender-[:BLOCKED]-receiver) AND receiver.status = \"ACTIVE\""
						+ " RETURN receiver " + " ORDER BY receiver.name ASC"
						+ " SKIP %s LIMIT %s", sid, pid, skip, limit);
		List<PlatformUser> friends = IteratorUtils.toList(template
				.query(fQuery, null).to(PlatformUser.class).iterator());
		return friends;
	}

	@Override
	@Transactional
	public Boolean isPendingFriendRequest(Long sid, Long rid) {
		return personRepo.didIsendARequest(sid, rid);
	}

	@Override
	@Transactional
	public FriendRequest theFriendRequestISent(Long sid, Long rid)
			throws ControllerException {
		return validateFriendRequest(sid, rid);
	}

	private FriendRequest validateFriendRequest(Long sid, Long rid)
			throws ControllerException {
		assertNotNull(sid, "Sender not found");
		assertNotNull(rid, "Receiver not found");
		FriendRequestResult frt = personRepo.friendRequestBetween(sid, rid);
		assertNotNull(frt, "You did not send this person a request");
		FriendRequest fr = new FriendRequest(frt);
		return fr;
	}
	
	private Boolean isNominated (Long eid, Long pid) {
		String query = String.format("MATCH (errand:Errand) WHERE id (errand) = %s MATCH (person:PlatformUser) WHERE id (person) = %s MATCH (errand)<-[r:GOT_NOMINATED]-(person) RETURN r IS NOT NULL", eid, pid);
		return template.query(query, null).to(Boolean.class).singleOrNull();
	}
	
	private PostNominate getNomination (Long eid, Long pid) {
		String query = String.format("MATCH (errand:Errand) WHERE id (errand) = %s MATCH (person:PlatformUser) WHERE id (person) = %s MATCH (errand)<-[r:GOT_NOMINATED]-(person) RETURN r LIMIT 1", eid, pid);
		return template.query(query, null).to(PostNominate.class).singleOrNull();
	}
	
	@Override
	public Boolean isNominatedByOwner(Long eid, Long pid) {
		String query = String.format("MATCH (n:Errand) WHERE id (n) = %s MATCH (owner:PlatformUser) WHERE id (owner) = n.ownerId MATCH (person:PlatformUser) WHERE id(person) = %s OPTIONAL MATCH owner-[r:NOMINATED {eid: id(n)}]->(person) RETURN r IS NOT NULL LIMIT 1", eid, pid);
		return template.query(query, null).to(Boolean.class).singleOrNull();
	}
	
	public PostRecommend isBroadcastedAlready (Long eid, Long pid) {
		String query = String.format("MATCH (n:Errand) WHERE id (n) = %s MATCH (person:PlatformUser) WHERE id(person) = %s OPTIONAL MATCH person-[r:RECOMMENDED]->(n) RETURN r LIMIT 1", eid, pid);
		return template.query(query, null).to(PostRecommend.class).singleOrNull();
	}

	@Override
	@Transactional
	public void deleteComment(Long cid) throws ControllerException {
		validateComment(cid);
		abstractRepo.deleteRelationship(cid);
	}

	@Override
	@Transactional
	public List<FriendRequest> getFriendRequestsByPerson(Long pid, int skip,
			int limit) throws ControllerException {
		validatePerson(pid);
		String frQuery = String
				.format(" match (sender:PlatformUser)-[r:FRIEND_REQUEST]->(receiver:PlatformUser) "
						+ " WHERE id (receiver) = %s  AND NOT (sender-[:BLOCKED]-receiver) AND (sender.status = \"ACTIVE\")"
						+ " return r " + " ORDER BY r.ctime DESC", pid);
		List<FriendRequest> requests = IteratorUtils.toList(template
				.query(frQuery, null).to(FriendRequest.class).iterator());
		return requests;
	}

	@Override
	@Transactional
	public List<Block> blockList(Long pid, int skip, int limit)
			throws ControllerException {
		validatePerson(pid);
		String bQuery = String
				.format("match (sender:PlatformUser)-[r:BLOCKED]-(receiver:PlatformUser) "
						+ " WHERE id (receiver) = %s AND (receiver.status = \"ACTIVE\")"
						+ " return r " + " ORDER BY r.ctime DESC", pid);
		List<Block> requests = IteratorUtils.toList(template
				.query(bQuery, null).to(Block.class).iterator());
		return requests;
	}

	@Override
	@Transactional
	public Integer commentCountByErrand(Long eid) throws ControllerException {
		assertNotNull(eid, "Id cannot be null");
		return errandRepo.commentsCount(eid);
	}

	@Override
	@Transactional
	public Integer nominationsCountByErrand(Long eid)
			throws ControllerException {
		assertNotNull(eid, "Id cannot be null");
		return errandRepo.nominatedErrandCount(eid);
	}

	@Override
	@Transactional
	public Integer proposeCountByErrand(Long eid) throws ControllerException {
		assertNotNull(eid, "Id cannot be null");
		return errandRepo.acceptErrandCount(eid);
	}

	@Override
	@Transactional
	public Commented findOneComment(Long cid) throws ControllerException {
		assertNotNull(cid, "Id cannot be null");
		CommentResult c = abstractRepo.findComments(cid);
		if (c == null)
			throw new ControllerException("Comment not found");
		return new Commented(c);
	}

	@Override
	@Transactional
	public List<PlatformUser> searchFriends(Long pid, String name) {
		if (pid == null || name == null)
			return Collections.emptyList();
		String query = String
				.format("match (sender:PlatformUser)-[r:FRIENDS]-(receiver:PlatformUser)  where id (sender) = %s AND NOT (receiver-[:BLOCKED]-sender) AND receiver.status = \"ACTIVE\" AND receiver.name =~ '(?i).*%s.*' return receiver SKIP 0 LIMIT 10 ",
						pid, name);
		List<PlatformUser> people = new ArrayList<PlatformUser>();
		Iterator<PlatformUser> iteratorPerson = template.query(query, null)
				.to(PlatformUser.class).iterator();
		while (iteratorPerson.hasNext()) {
			people.add(iteratorPerson.next());
		}

		if (personRepo != null && isValidEmailAddress(name)) {
			PlatformUser person = personRepo.findByEmail(name);
			MailMan man = new MailMan();
			try {
				man.sendMessage(validatePerson(pid).getName()
						+ " mentioned you in a post", name,
						"Join errandr at www.errandr.com");
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ControllerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			people.add(person);
		}
		return people;
	}

	@Override
	@Transactional
	public List<Notification> getMyNotifications(Long pid, int skip, int limit)
			throws ControllerException {
		assertNotNull(pid, "Person not found");
		ArrayList<Notification> out = new ArrayList<Notification>();
		for (NotificationResult result : abstractRepo.myNotifications(pid, 0,
				20))
			out.add(new Notification(result));
		return out;
	}

	@Override
	@Transactional
	public Notification findOneNotification(Long nid)
			throws ControllerException {
		assertNotNull(nid, "Notification not found");
		Notification n = template.findOne(nid, Notification.class);
		assertNotNull(n, "Notification not found");
		return n;
	}

	@Override
	@Transactional
	public Integer unseenNotificationsCount(Long pid)
			throws ControllerException {
		assertNotNull(pid, "Person not found");
		return abstractRepo.unseenNotificationsCount(pid);
	}

	@Override
	@Transactional
	public void markAllNotificationAsSeen(Long pid) throws ControllerException {
		assertNotNull(pid, "Person not found");
		abstractRepo.markAllNotificationsAsRead(pid);
	}

	@Override
	@Transactional
	public Proposal findOneProposal(Long pid) throws ControllerException {
		return validateProposal(pid);
	}

	@Override
	@Transactional
	public Integer countFriends(Long pid) throws ControllerException {
		return personRepo.countFriends(pid);
	}

	@Override
	@Transactional
	public Boolean areWeFriends(Long sid, Long rid) throws ControllerException {
		assertNotNull(sid, " Person not found");
		assertNotNull(rid, " Person not found");
		return personRepo.friendship(sid, rid) != null;
	}

	@Override
	@Transactional
	public List<Conversation> inbox(Long pid, int skip, int limit)
			throws ControllerException {
		ArrayList<Conversation> conversations = new ArrayList<Conversation>();
		for (ConversationResult r : conversationRepo.inbox(pid, skip, limit)) {
			conversations.add(new Conversation(r));
		}
		return conversations;
	}

	@Override
	@Transactional
	public List<Message> messages(Long cid, int skip, int limit)
			throws ControllerException {
		ArrayList<Message> messages = new ArrayList<Message>();
		for (MessageResult r : conversationRepo.conversationMessages(cid, skip,
				limit)) {
			messages.add(new Message(r));
		}
		return messages;
	}

	@Override
	@Transactional
	public Conversation findOneConversation(Long cid)
			throws ControllerException {
		assertNotNull(cid, "Conversation not found");
		ConversationResult r = conversationRepo.findOneConversation(cid);
		assertNotNull(r, "Conversation not found");
		return new Conversation(r);
	}

	@Override
	@Transactional
	public Message findOneMessage(Long mid) throws ControllerException {
		assertNotNull(mid, "Conversation not found");
		MessageResult r = conversationRepo.findOneMessage(mid);
		assertNotNull(r, "Conversation not found");
		return new Message(r);
	}

	@Override
	@Transactional
	public void markAllMessagesAsRead(Long cid, Long pid)
			throws ControllerException {
		conversationRepo.markMyMessagesReadInConversation(cid, pid);
	}

	@Override
	@Transactional
	public Integer pointsCount(Long pid) throws ControllerException {
		return errandRepo.confirmedPoints(pid);
	}

	@Override
	@Transactional
	public void deleteProposal(Long pid) throws ControllerException {
		errandRepo.deleteProposal(pid);
	}

	@Override
	@Transactional
	public void deleteErrand(Long eid) throws ControllerException {
		errandRepo.deleteErrand(eid);
	}

	@Override
	@Transactional
	public void reportPost(Long pid, Long eid, String type, String details)
			throws ControllerException {
		PlatformUser person = validatePerson(pid);
		Errand post = validateErrand(eid);
		Reported reported = new Reported();
		reported.ctime = new Date();
		reported.reasonType = type;
		reported.details = details;
		reported.reporter = person;
		reported.errand = post;

		Block block = new Block();
		block.ctime = new Date();
		block.receiver = post;
		block.sender = person;
		template.save(block);
		template.save(reported);
	}

	@Override
	@Transactional
	public List<Long> twoWayBlockList(Long pid) throws ControllerException {
		assertNotNull(pid, "Account not found");
		return personRepo.twoWayBlockListId(pid);
	}

	@Override
	@Transactional
	public Integer countInterestFeed(Long pid) throws ControllerException {

		return null;
	}

	@Override
	@Transactional
	public void batchInsertErrand(List<Errand> errands)
			throws ControllerException {
		assertNotNull(errands, "Cant be null");
		for (Errand errand : errands) {
			this.postErrand(errand);
		}

	}

	@Override
	@Transactional
	public FriendRequest findFriendRequest(Long frid)
			throws ControllerException {
		String query = String
				.format("MATCH (sender:PlatformUser)-[r:FRIEND_REQUEST]->receiver WHERE id(r) = %s AND (sender.status = \"ACTIVE\") AND (receiver.status = \"ACTIVE\") return r LIMIT 1",
						frid);
		return template.query(query, null).to(FriendRequest.class)
				.singleOrNull();
	}

	@Override
	@Transactional
	public void resetUnseenFriendRequestCount(Long pid)
			throws ControllerException {
		PlatformUser person = validatePerson(pid);
		person.setUnseenFriendRequests(0);
		template.save(person);
	}

	@Override
	@Transactional
	public PlatformUser updateProfilePicture(Long pid, String url,
			String thumbNail) throws ControllerException {
		PlatformUser person = validatePerson(pid);
		person.setProfilePictureUrl(url);
		person.setThumbNail(thumbNail);
		return template.save(person);
	}

	@Override
	@Transactional
	public List<PlatformUser> searchPeople(Long sid, String search, int skip,
			int limit) {
		if (search == null)
			return Collections.emptyList();
		String name = new String(search); 
		name = name.replace("@", "");
		name = name.replace("\\", "");
		name = name.replace("'", "\\'");
		String query = String
				.format("match (session:PlatformUser) where id (session) = %s match (person:PlatformUser) where (person.status = \"ACTIVE\") AND NOT (session-[:BLOCKED]-person) AND (person.name =~ '(?i).*%s.*' OR person.username =~ '(?i).*%s.*' OR person.bio =~ '(?i).*%s.*') WITH session, person OPTIONAL MATCH path=((session)-[:FRIENDS*..6]-(person)) WITH count(DISTINCT path) as socialStrength, person return person ORDER BY socialStrength DESC SKIP %s LIMIT %s",
						sid, name, name, name, skip, limit);
		if (isValidEmailAddress(search))
			 query = String
				.format("match (session:PlatformUser) where id (session) = %s match (person:PlatformUser) where (person.status = \"ACTIVE\") AND NOT (session-[:BLOCKED]-person) AND person.email =~ '(?i).*%s.*' WITH session, person OPTIONAL MATCH path=((session)-[:FRIENDS*..6]-(person)) WITH count(DISTINCT path) as socialStrength, person return person ORDER BY socialStrength DESC SKIP %s LIMIT %s",
						sid, name, skip, limit);
		List<PlatformUser> people = new ArrayList<PlatformUser>();
		Iterator<PlatformUser> iteratorPerson = template.query(query, null)
				.to(PlatformUser.class).iterator();
		while (iteratorPerson.hasNext()) {
			people.add(iteratorPerson.next());
		}
		return people;
	}

	@Override
	@Transactional
	public Integer searchSize(String name) throws ControllerException {
		if (name == null)
			return 0;
		String query = String
				.format("match (person:PlatformUser)  where person.name =~ '(?i).*%s.*' OR person.email =~ '(?i).*%s.*' OR person.username =~ '(?i).*%s.*' return count(person)",
						name, name, name);
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public Map<String, String> getPrivacySettings(PlatformUser person)
			throws ControllerException {
		assertNotNull(person, "Person cannot be null");
		if (person.getType().equals(Config.PLATFORM_TYPE_PERSON)) {
			Map<String, String> keyToValue = new LinkedHashMap<String, String>();
			keyToValue.put("Public", "PUBLIC");
			keyToValue.put("Friends", "FRIENDS");
			keyToValue.put("Private", "PRIVATE");
			return keyToValue;
		} else {
			Map<String, String> keyToValue = new LinkedHashMap<String, String>();
			keyToValue.put("Public", "PUBLIC");
			return keyToValue;
		}
	}

	@Override
	@Transactional
	public List<SuggestionObject> friendSuggestions(Long pid, int skip,
			int limit) throws ControllerException {
		String query = String
				.format("MATCH path=(a:PlatformUser{id:%s})-[:FRIENDS]-x-[:FRIENDS]-(b) WHERE NOT a-[:FRIENDS]-b AND NOT a-[:BLOCKED]-b AND NOT a-[:DONT_RECOMMEND]->b AND NOT a-[:FRIEND_REQUEST]-b AND b.status = \"ACTIVE\" AND x.status = \"ACTIVE\" return b as person, count(DISTINCT path) as mutualFriendsCount, collect(id(x)) as mutualFriendIds ORDER BY mutualFriendsCount DESC LIMIT 5",
						pid);
		Iterator<SuggestionResult> iresults = template.query(query, null)
				.to(SuggestionResult.class).iterator();
		ArrayList<SuggestionObject> results = new ArrayList<SuggestionObject>();
		while (iresults.hasNext()) {
			SuggestionResult result = iresults.next();
			SuggestionObject c = new SuggestionObject(result);
			if (result.getMutualFriendIds().size() > 0) {
				Integer size = result.getMutualFriendsCount();
				PlatformUser strongestLink = this.findPerson(result
						.getMutualFriendIds().get(0));
				String text = "In "
						+ strongestLink.getName()
						+ "'s network "
						+ (size - 1 > 2 ? " and " + (size - 1) + " others"
								: (size - 1) == 1 ? " and " + (size - 1)
										+ " other person" : "");
				c.metaData = text;
			}
			results.add(c);
		}
		return results;
	}

	@Override
	@Transactional
	public List<PlatformUser> mutualFriends(Long sid, Long rid, int skip,
			int limit) throws ControllerException {
		String query = String
				.format("MATCH (a:PlatformUser)-[:FRIENDS]-x-[:FRIENDS]-(b:PlatformUser) WHERE id (a) = %s AND id (b) = %s AND NOT id (a) = id(b) AND NOT a-[:BLOCKED]-b return x as person SKIP %s LIMIT %s;",
						sid, rid, skip, limit);
		Iterator<PlatformUser> iresults = template.query(query, null)
				.to(PlatformUser.class).iterator();
		ArrayList<PlatformUser> results = new ArrayList<PlatformUser>();
		while (iresults.hasNext())
			results.add(iresults.next());
		return results;
	}

	@Override
	@Transactional
	public Integer newMessagesCount(Long pid) throws ControllerException {
		return abstractRepo.newMessagesCount(pid);
	}

	@Override
	// @Transactional
	public void markAllNewMessages(Long pid) throws ControllerException {
		abstractRepo.markNewMessages(pid);
	}

	@Override
	@Transactional
	public void recommendPost(Long personId, Long postId, String message)
			throws ControllerException {
		PlatformUser person = validatePerson(personId);
		Errand errand = validateErrand(postId);
		if (person.getFirstBroadcast()) {
			person.setFirstBroadcast(false);
			template.save(person);
		}
		
		PostRecommend recommend = null; 
		if ((recommend = isBroadcastedAlready(postId, personId)) == null){
			recommend = new PostRecommend(); 
			recommend.owner = person;
			recommend.errand = errand;
			recommend.ctime = new Date();
			recommend.message = message;
			template.save(recommend);
		} else {
			recommend.ctime = new Date();
			template.save(recommend);
		}
	
		if (person.getId().equals(errand.getOwnerId()))
			return;
		Notification n = new Notification();
		n.setCtime(new Date());
		n.setDetails(String.format("%s broadcasted your post", person.getName()));
		n.setLink(errand.getErrandLink());
		n.setPictureLink(person.getThumbNail());
		n.setType("Broadcast");
		n = template.save(n);

		PersonNotification pn = new PersonNotification();
		pn.isRead = false;
		pn.notification = n;
		pn.receiver = validatePerson(errand.getOwnerId());
		template.save(pn);
	}

	@Override
	@Transactional
	public Integer recommendCount(Long postId) {
		String recommended = String
				.format("MATCH (p:PlatformUser)-[r:RECOMMENDED]->(n:Errand) WHERE id(n) = %d RETURN count(r)",
						postId);
		return template.query(recommended, null).to(Integer.class)
				.singleOrNull();
	}

	@Override
	@Transactional
	public Post findOnePost(Long pid) throws ControllerException {
		assertNotNull(pid, "Error with request");
		Post post = postRepo.findOne(pid);
		post.errand.setOwner(validatePerson(post.errand.getOwnerId()));
		return post;
	}

	@Override
	@Transactional
	public PlatformUser forgotPassword(Long pid) throws ControllerException {
		assertNotNull(pid, "Error with request");
		PlatformUser person = validatePerson(pid);
		person.setForgotPassword(UUID.randomUUID().toString());
		return (template.save(person));
	}

	@Override
	@Transactional
	public PlatformUser verifyUser(String key) throws ControllerException {
		assertNotNull(key, "Error with request");
		PlatformUser person = personRepo.findByVerificationKey(key);
		assertNotNull(person, "Your confirmation key is invalid");
		person.setVerified(true);
		person.setShowVerified(true);
		Notification n = new Notification();
		n.setCtime(new Date());
		n.setDetails(String.format("Your Erranda Account has been verified"));
		n.setLink(person.getLink());
		// Change to errandr logo
		n.setPictureLink(person.getThumbNail());
		n.setType("Broadcast");
		n = template.save(n);
		PersonNotification pn = new PersonNotification();
		pn.isRead = false;
		pn.notification = n;
		pn.receiver = person;
		template.save(pn);
		return template.save(person);
	}

	@Override
	@Transactional
	public PlatformUser tokenLogin(String key) throws ControllerException {
		assertNotNull(key, "Error with request");
		PlatformUser person = personRepo.findByVerificationKey(key);
		return person;
	}

	@Override
	@Transactional
	public PlatformUser removeProfilePicture(Long pid)
			throws ControllerException {
		PlatformUser person = validatePerson(pid);
		person.setProfilePictureUrl(new PlatformUser().getProfilePictureUrl());
		person.setThumbNail(new PlatformUser().getThumbNail());
		return template.save(person);
	}

	@Override
	@Transactional
	public List<PlatformUser> othersThatCommented(Long pid, Long senderId)
			throws ControllerException {
		String cQuery = String
				.format("match (session:PlatformUser) where id (session) = %s match (sender:PlatformUser)-[r:COMMENTED]->(errand:Errand)<-[:POSTED]-owner"
						+ " WHERE id (errand) = %s AND id (sender) <> errand.ownerId AND id (sender) <> id(session) AND NOT (session-[:BLOCKED]-sender) AND NOT (session-[:BLOCKED]-owner) AND sender.status = \"ACTIVE\" AND owner.status = \"ACTIVE\""
						+ " return distinct sender", senderId, pid);
		List<PlatformUser> commenters = IteratorUtils.toList(template
				.query(cQuery, null).to(PlatformUser.class).iterator());
		return commenters;
	}

	@Override
	@Transactional
	public List<PlatformUser> myBlockList(Long pid) throws ControllerException {
		validatePerson(pid);
		String bQuery = String
				.format("match (sender:PlatformUser)-[r:BLOCKED]->(receiver:PlatformUser) "
						+ " WHERE id (sender) = %s AND (receiver.status = \"ACTIVE\")"
						+ " return receiver " + " ORDER BY r.ctime DESC", pid);
		// Re turn relationship instead of person the view is losing information
		// this way
		List<PlatformUser> blocked = IteratorUtils.toList(template
				.query(bQuery, null).to(PlatformUser.class).iterator());
		return blocked;
	}

	@Override
	public Boolean isBlocked(Long sid, Long rid) throws ControllerException {
		return personRepo.isBlocked(sid, rid);
	}

	@Override
	public Boolean isBlockedTwoWay(Long sid, Long rid)
			throws ControllerException {
		String blocked = String
				.format("optional MATCH (a:PlatformUser)-[r:BLOCKED]-(b:PlatformUser) WHERE id(a) = %s AND id(b) = %s RETURN r IS NOT NULL",
						sid, rid);
		return template.query(blocked, null).to(Boolean.class).singleOrNull();
	}

	@Override
	public PlatformUser savePerson(PlatformUser person)
			throws ControllerException {
		return personRepo.save(person);
	}

	@Override
	@Transactional
	public void dontRecommendPerson(Long sid, Long rid) {
		if (sid == null || rid == null)
			return;
		DontRecommend r = new DontRecommend();
		r.sender = new PlatformUser(sid);
		r.receiver = new PlatformUser(rid);
		r.ctime = new Date();
		template.save(r);
	}

	@Override
	public PlatformUser findUsername(String username) {
		PlatformUser person = null;
		if (username == null)
			return null;
		else {
			username = username.toLowerCase().trim();
			person = personRepo.findByUsername(username);
		}
		return person;
	}

	@Override
	@Transactional
	// Create
	public Kudos kudos(Long eid, Long pid) throws ControllerException {
		PlatformUser person = validatePerson(pid);
		Errand errand = validateErrand(eid);
		// In future consider disallowing giving kudos to person that posted the errand.
		if (isKudos(eid, pid))
			return null;
		Kudos kudos = new Kudos();
		kudos.person = person;
		kudos.errand = errand;
		kudos.ctime = new Date();
		kudos = template.save(kudos);
		if (person.getId() != errand.getOwnerId()) {
			Notification n1 = new Notification();
			n1.setCtime(new Date());
			n1.setType("Kudos");
			n1.setDetails(person.getName() + " gave +1 Kudos on this post");
			n1.setPictureLink(person.getThumbNail());
			n1.setLink(errand.getErrandLink());
			n1 = template.save(n1);
			PersonNotification pn = new PersonNotification();
			pn.isRead = false;
			pn.notification = n1;
			pn.receiver = findPerson(errand.getOwnerId());
			template.save(pn);
		}
		
		return kudos;
	}

	@Override
	// Delete
	@Transactional
	public void minusKudos(Long eid, Long pid) throws ControllerException {
		String query = String
				.format("OPTIONAL MATCH (person:PlatformUser)-[r:GAVE_KUDOS]->(n:Errand) where id(person) = %s AND id(n) = %s DELETE r",
						pid, eid);
		template.query(query, null);
	}

	@Override
	@Transactional
	public Integer kudosCount(Long eid) throws ControllerException {
		String query = String
				.format("MATCH (person: PlatformUser)-[r:GAVE_KUDOS]->(n:Errand) WHERE id(n) = %s return count(r)",
						eid);
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public Boolean isKudos(Long eid, Long pid) throws ControllerException {
		String query = String
				.format("OPTIONAL MATCH (person:PlatformUser)-[r:GAVE_KUDOS]->(n:Errand) where id(person) = %s AND id(n) = %s RETURN r IS NOT NULL LIMIT 1",
						pid, eid);
		return template.query(query, null).to(Boolean.class).singleOrNull();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<PlatformUser> kudosGivePeople(Long eid, Long pid, int skip,
			int limit) throws ControllerException {
		String query = String
				.format("MATCH (session:PlatformUser) where id(session) = %s MATCH (n:Errand) WHERE id(n) = %s MATCH (person:PlatformUser)-[r:GAVE_KUDOS]->(n:Errand) WHERE NOT session-[:BLOCKED]-person RETURN person ORDER BY person.name SKIP %s LIMIT %s",
						pid, eid, skip, limit);
		List<PlatformUser> people = new ArrayList<PlatformUser>();
		Iterator<PlatformUser> i = template.query(query, null)
				.to(PlatformUser.class).iterator();
		people = IteratorUtils.toList(i);
		return people;
	}

	@Override
	@Transactional
	/**
	 * A page is a public entity owned by a single person but manageable by others.
	 * People can follow a page to see posts about the page in their timeline.
	 * People who manage a page can use the buisnesses post handle using the post as option.
	 * 
	 * @param pid
	 * @param name
	 * @param uniqueName
	 * @param description
	 * @param location
	 * @param team
	 * @return
	 * @throws ControllerException
	 */
	public Page createPage(Long pid, Page page, List<PlatformUser> team)
			throws ControllerException {
		if (page.getLocalityName() != null && !page.getLocalityName().equals("Global")){
			String location = page.getLocalityName();
			Double[] cords = geoLocator(location);
			page.setLatitude(cords[0]);
			page.setLongitude(cords[1]);
			page.setLocation();
			page.setLocalityName(location);
		}
		if (page.getName() == null)
			throwEx("Page name is empty");
		String uniqueName = page.getUsername();
		validateUserName(uniqueName);
		PlatformUser person = validatePerson(pid);
		page.setVerified(true);
		page.setType(Config.PLATFORM_TYPE_PAGE);
		page.setEmail("empty");
		template.save(page);

		PageOwner owner = new PageOwner();
		owner.setPage(page);
		owner.setOwner(person);
		owner.setIsPageAdministrator(true);
		owner.setIsPageCreator(true);
		template.save(owner);

		for (PlatformUser member : team) {
			if (member.getId() == null
					|| !areWeFriends(person.getId(), member.getId()))
				continue;
			member = validatePerson(member.getId());
			PageAdministrator admin = new PageAdministrator();
			admin.setPage(page);
			admin.setOwner(member);
			admin.setTitle("Manager");
			template.save(admin);
			Notification n = new Notification();
			n.setCtime(new Date());
			n.setLink(page.getLink());
			n.setPictureLink(person.getThumbNail());
			n.setType("Page Administrator");
			n.setDetails(person.name + " added you as Administrator to this page");
			template.save(n);
			PersonNotification on = new PersonNotification();
			on.isRead = false;
			on.notification = n;
			on.receiver = member;
			template.save(on);
		}
		return page;
	}

	/**
	 * A page can be deactivated at any time in which the page
	 * completely goes of the radar
	 * 
	 * @param sessionId
	 * @param pageId
	 * @throws ControllerException
	 */
	@Transactional
	public void deactivatePage(Long sessionId, Long pageId)
			throws ControllerException {
		if (!isPageOwner(sessionId, pageId))
			throw new ControllerException(
					"You are not authorized to perform this action");
		// Delete all relationships with the page
		Page page = validatePage(pageId);
		page.setStatus("DEACTIVATED");
		template.save(page);
	}

	/**
	 * This is a private method to remove a page completely from the
	 * database
	 * 
	 * @param sessionId
	 * @param pageId
	 * @throws ControllerException
	 */
	@Transactional
	public void removePage(Long sessionId, Long pageId)
			throws ControllerException {
		if (!isPageOwner(sessionId, pageId))
			throw new ControllerException(
					"You are not authorized to perform this action");
		String delete = String
				.format("MATCH ()-[r]-(page:Page) where id(page) = %s DELETE page, r",
						pageId);
		template.query(delete, null);
	}

	/**
	 * A page can be updated at any time by an administrator in which all
	 * team members are notified and followers of the page
	 * 
	 * @param personId
	 * @param pageId
	 * @param uniqueName
	 * @throws ControllerException
	 */
	@Transactional
	public void updatePageUniqueName(Long personId, Long pageId,
			String uniqueName) throws ControllerException {
		if (personId == null || pageId == null || uniqueName == null)
			throw new ControllerException(
					"There was an error with your request");
		uniqueName = uniqueName.toLowerCase().trim();
		if (!canManagePage(personId, pageId))
			throw new ControllerException(
					"You are not allowed to perform this action");
		if (isPageUniqueNameRegistered(uniqueName)) {
			throw new ControllerException("Page name " + uniqueName
					+ " is already registered");
		}
		Page owner = validatePage(pageId);
		owner.setUsername(uniqueName);
		template.save(owner);
		// Notify team members and followers
	}

	/**
	 * The page name is not unique just like a person's name you can change
	 * it any time.
	 * 
	 * @param personId
	 * @param pageId
	 * @param name
	 * @throws ControllerException
	 */
	@Transactional
	public void updatePageName(Long personId, Long pageId, String name)
			throws ControllerException {
		if (personId == null || pageId == null || name == null)
			throw new ControllerException(
					"There was an error with your request");
		if (!canManagePage(personId, pageId))
			throw new ControllerException(
					"You are not allowed to perform this action");
		Page page = validatePage(pageId);
		page.setName(name);
		template.save(page);
		// Nofify team members and followers
	}

	@Transactional
	public void updatePageDescription(Long personId, Long pageId,
			String description) throws ControllerException {
		if (personId == null || pageId == null || description == null)
			throw new ControllerException(
					"There was an error with your request");
		if (!canManagePage(personId, pageId))
			throw new ControllerException(
					"You are not allowed to perform this action");
		Page page = validatePage(pageId);
		page.setBio(description);
		template.save(page);
		// Nofify team members
	}

	/**
	 * Page locations/regions can change from time to time
	 * 
	 * @param personId
	 * @param pageId
	 * @param locationName
	 * @throws ControllerException
	 */
	@Transactional
	public void updatePageLocation(Long personId, Long pageId,
			String locationName) throws ControllerException {
		if (personId == null || pageId == null || locationName == null)
			throw new ControllerException(
					"There was an error with your request");
		if (!canManagePage(personId, pageId))
			throw new ControllerException(
					"You are not allowed to perform this action");
		Double[] geoLocator = geoLocator(locationName);
		Page page = validatePage(pageId);
		page.setLocalityName(locationName);
		page.setLongitude(geoLocator[1]);
		page.setLatitude(geoLocator[0]);
		template.save(page);
		// Update team members and locations. Team members will see who changed
		// the location.
	}

	public void removeAdministrator(Long personId, Long pageId,
			Long targetPersonId) throws ControllerException {
		if (personId == null || pageId == null || targetPersonId == null)
			throw new ControllerException(
					"There was an error with your request");
		if (isPageOwner(personId, pageId))
			throw new ControllerException(
					"You are not authorized to perform this action");
	}

	@Override
	@Transactional
	public Page findPageByUniqueName(String uniqueName)
			throws ControllerException {
		uniqueName = uniqueName.toLowerCase().trim();
		String query = String
				.format("MATCH (page:Page {uniqueName : %s}) RETURN page LIMIT 1",
						uniqueName);
		Page owner = template.query(query, null).to(Page.class).singleOrNull();
		if (owner == null)
			throw new ControllerException("Page page not found");
		return owner;
	}

	private Boolean isPageUniqueNameRegistered(String uniqueName) {
		uniqueName = uniqueName.toLowerCase().trim();
		String query = String
				.format("MATCH (page:Page {uniqueName : %s}) RETURN page IS NOT NULL LIMIT 1",
						uniqueName);
		Boolean owner = template.query(query, null).to(Boolean.class)
				.singleOrNull();
		return owner;
	}

	private Page validatePage(Long pageId) {
		String query = String
				.format("MATCH (page:PlatformUser) WHERE id(page) = %s RETURN page LIMIT 1",
						pageId);
		Page owner = template.query(query, null).to(Page.class).singleOrNull();
		return owner;
	}

	@Override
	@Transactional
	public Boolean canManagePage(Long sessionId, Long pageId)
			throws ControllerException {
		String query = String
				.format("OPTIONAL MATCH (session:PlatformUser)-[r:OWNS_PAGE|MANAGES_PAGE]->(page:Page) WHERE id(session) = %s AND id(page) = %s RETURN r IS NOT NULL LIMIT 1",
						sessionId, pageId);
		Boolean owner = template.query(query, null).to(Boolean.class)
				.singleOrNull();
		return owner;
	}

	// @Override
	@Transactional
	public Boolean isPageOwner(Long sessionId, Long pageId) {
		String query = String
				.format("MATCH (person:PlatformUser)-[r:OWNS_PAGE]->(page:Page) WHERE id(person) = %s AND id(page) = %s RETURN r IS NOT NULL LIMIT 1",
						sessionId, pageId);
		Boolean owner = template.query(query, null).to(Boolean.class)
				.singleOrNull();
		return owner;
	}

	@Override
	@Transactional
	public Integer countOfPeopleNominate(Long postId)
			throws ControllerException {
		validateErrand(postId);
		String query = String
				.format("MATCH (n:Errand)<-[r:GOT_NOMINATED]-(target:PlatformUser) where id(n) = %s RETURN count(DISTINCT r)",
						postId);
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public List<PlatformUser> peopleNominated(Long postId, Long sessionId)
			throws ControllerException {
		validateErrand(postId);
		String query = String
				.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH (n:Errand) WHERE id(n) = %s MATCH n<-[r:GOT_NOMINATED]-(target:PlatformUser) WHERE NOT session-[:BLOCKED]-target RETURN DISTINCT target",
						sessionId, postId);
		List<PlatformUser> peopleNominated = IteratorUtils.toList(template
				.query(query, null).to(PlatformUser.class).iterator());
		return peopleNominated;
	}

	@Override
	@Transactional
	public PostNominate findPrimaryNominated(Long sessionId, Long postId)
			throws ControllerException {
		validateErrand(postId);
		if (countOfPeopleNominate(postId) == 0)
			return null;
		String query = String
				.format("MATCH (session:PlatformUser) where id (session) = %s MATCH (n:Errand) WHERE id(n) = %s MATCH n<-[r:GOT_NOMINATED]-(target:PlatformUser) where r.primary = true AND NOT (session-[:BLOCKED]-target) RETURN r LIMIT 1",
						sessionId, postId);
		PostNominate user = template.query(query, null).to(PostNominate.class)
				.singleOrNull();
		if (user == null) {
			query = String
					.format("MATCH (session:PlatformUser) where id (session) = %s MATCH (n:Errand) WHERE id(n) = %s MATCH n<-[r:GOT_NOMINATED]-(target:PlatformUser) where r.primary = true AND NOT (session-[:BLOCKED]-target) RETURN r LIMIT 1",
							sessionId, postId);
			user = template.query(query, null).to(PostNominate.class)
					.singleOrNull();
		}
		return user;
	}

	@Override
	@Transactional
	public List<PlatformUser> switchAccounts(Long pid)
			throws ControllerException {
		PlatformUser person = validatePerson(pid);
		String query = String
				.format("MATCH (session:PlatformUser)-[r:OWNS_PAGE|MANAGES_PAGE]->(other:Page) where id (session) = %s AND other.status = 'ACTIVE' RETURN DISTINCT other",
						pid);
		List<PlatformUser> users = IteratorUtils.toList(template
				.query(query, null).to(PlatformUser.class).iterator());
		users.add(person);
		return users;

	}

	/**
	 * Friends can search through public posts around them and those of their
	 * friends network
	 */
	@Override
	@Transactional
	public List<Errand> findPostsByKeyword(Long session, String details,
			int skip, int limit) throws ControllerException {
		PlatformUser person = validatePerson(session);
		details = details.replace("\\", "");
		details = details.replace("'", "\\'");
		String spatialSearch = String
				.format("MATCH (session:PlatformUser) WHERE id (session) =  %s MATCH (owner:PlatformUser)-[r:POSTED]->(n:Errand) WHERE n.privacy = \"PUBLIC\" AND n.details =~ '(?i).*%s.*' AND NOT (session-[:BLOCKED]-owner) AND NOT (session-[:BLOCKED]-n) AND (owner.status = \"ACTIVE\") RETURN n ORDER BY r.ctime DESC SKIP %d LIMIT %d",
						person.getId(), details, skip, limit);

		String friendsSearch = String
				.format("MATCH (session:PlatformUser)-[:FRIENDS]-(owner:PlatformUser)-[r:POSTED]->n WHERE n.privacy = \"FRIENDS\"  AND id (session) =  %s AND n.details =~ '(?i).*%s.*' AND NOT (session-[:BLOCKED]-owner) AND NOT (session-[:BLOCKED]-n)  AND (owner.status = \"ACTIVE\") SET n.id = id (n), owner.id=id(owner) RETURN n ORDER BY r.ctime DESC SKIP %d LIMIT %d",
						person.getId(), details, skip, limit);

		String privateSearch = String
				.format("MATCH (session:PlatformUser)-[r:POSTED]->n WHERE id (session) = %s AND n.privacy = \"PRIVATE\" AND n.details =~ '(?i).*%s.*' RETURN n ORDER BY r.ctime DESC SKIP %d LIMIT %d",
						person.getId(), details, skip, limit);

		List<Errand> results = new ArrayList<Errand>();
		results.addAll(IteratorUtils.toList(template.query(spatialSearch, null)
				.to(Errand.class).iterator()));
		results.addAll(IteratorUtils.toList(template.query(friendsSearch, null)
				.to(Errand.class).iterator()));
		results.addAll(IteratorUtils.toList(template.query(privateSearch, null)
				.to(Errand.class).iterator()));
		Collections.sort(results, Collections.reverseOrder());
		return results;
	}

	@Override
	public String findPostsByKeywordCount(Long session, String details)
			throws ControllerException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String findAccountsByKeywordCount(Long session, String details)
			throws ControllerException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Page findPage(Long sessionId, Long pageId)
			throws ControllerException {
		validatePerson(sessionId);
		String query = String
				.format("MATCH (session:PlatformUser) where id (session) = %s MATCH (page:Page) where id (page) = %s AND NOT page-[:BLOCKED]-session RETURN page",
						sessionId, pageId);
		Page page = template.query(query, null).to(Page.class).singleOrNull();
		if (page == null)
			throw new ControllerException("Page not found");
		return page;
	}

	@Override
	@Transactional
	public void followUser(Long sid, Long rid) throws ControllerException {
		PlatformUser sender = validatePerson(sid);
		PlatformUser receiver = validatePerson(rid);
		if (isFollowingUser(sid, rid))
			return;
		Follow follow = new Follow();
		follow.sender = sender;
		follow.receiver = receiver;
		follow.ctime = new Date();
		template.save(follow);
		
		Notification n1 = new Notification();
		n1.setCtime(new Date());
		n1.setType("Following");
		n1.setDetails(sender.getName() + " is following your public posts");
		n1.setPictureLink(sender.getThumbNail());
		n1.setLink(sender.getLink());
		n1 = template.save(n1);
		PersonNotification pn = new PersonNotification();
		pn.isRead = false;
		pn.notification = n1;
		pn.receiver = receiver;
		template.save(pn);
		
	}

	@Override
	@Transactional
	public void unfollowUser(Long sid, Long rid) throws ControllerException {
		assertNotNull(sid, "Bad request");
		assertNotNull(rid, "Bad request");
		String query = String
				.format("OPTIONAL MATCH (sender:PlatformUser)-[r:FOLLOWS]->(receiver:PlatformUser) WHERE id(sender) = %s AND id (receiver) = %s  DELETE r",
						sid, rid);
		template.query(query, null);
	}

	@Override
	@Transactional
	public Boolean isFollowingUser(Long sid, Long rid)
			throws ControllerException {
		assertNotNull(sid, "Bad request");
		assertNotNull(rid, "Bad request");
		String query = String
				.format("OPTIONAL MATCH (sender:PlatformUser)-[r:FOLLOWS]->(receiver:PlatformUser) WHERE id(sender) = %s AND id (receiver) = %s  RETURN r IS NOT NULL",
						sid, rid);
		return template.query(query, null).to(Boolean.class).singleOrNull();
	}

	@Override
	@Transactional
	public CommentKudos commentKudos(Long cid, Long pid)
			throws ControllerException {
		if (isCommentKudos(cid, pid))
			return null;
		else {
			Commented comment = validateComment(cid);
			Errand errand = comment.getErrand();
			PlatformUser person = validatePerson(pid);
			CommentKudos cKudos = new CommentKudos();
			cKudos.sid = pid;
			cKudos.ctime = new Date();
			cKudos.cid = cid;
			cKudos =  template.save(cKudos);
			if (person.getId() != comment.getSender().getId()) {
				Notification n1 = new Notification();
				n1.setCtime(new Date());
				n1.setType("Kudos");
				n1.setDetails(person.getName() + " gave +1 Kudos on your comment");
				n1.setPictureLink(person.getThumbNail());
				n1.setLink(errand.getErrandLink());
				n1 = template.save(n1);
				PersonNotification pn = new PersonNotification();
				pn.isRead = false;
				pn.notification = n1;
				pn.receiver = findPerson(errand.getOwnerId());
				template.save(pn);
			}
			
			return cKudos;
		}
	}

	@Override
	@Transactional
	public void minusCommentKudos(Long sid, Long cid)
			throws ControllerException {
		if (!isCommentKudos(sid, cid))
			return;
		else {
			String query = String
					.format("MATCH (commentKudos: CommentKudos) WHERE commentKudos.sid = %s AND commentKudos.cid = %s DELETE commentKudos",
							sid, cid);
			template.query(query, null);
		}

	}

	@Override
	@Transactional
	public Integer commentKudosCount(Long cid) throws ControllerException {
		assertNotNull(cid, "Bad request");
		String query = String
				.format("MATCH (commentKudos: CommentKudos) WHERE commentKudos.cid = %s RETURN count(commentKudos)",
						cid);
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public Boolean isCommentKudos(Long sid, Long cid)
			throws ControllerException {
		assertNotNull(cid, "Bad request");
		String query = String
				.format("OPTIONAL MATCH (commentKudos: CommentKudos) WHERE commentKudos.sid = %s AND commentKudos.cid = %s RETURN commentKudos IS NOT NULL",
						sid, cid);
		return template.query(query, null).to(Boolean.class).singleOrNull();
	}

	@Override
	@Transactional
	public List<PlatformUser> commentKudosGivePeople(Long cid, Long sid,
			Integer skip, Integer limit) throws ControllerException {
		assertNotNull("Bad request", cid, sid, skip, limit);
		String query = String
				.format("MATCH (session:PlatformUser) where id(session) = %s MATCH (commentKudos: CommentKudos) WHERE commentKudos.cid = %s WITH session, collect (commentKudos.sid) as senders UNWIND senders AS sender WITH session, sender MATCH (owner:PlatformUser) WHERE id(owner) = sender AND NOT session-[:BLOCKED]-owner RETURN owner",
						sid, cid);
		return IteratorUtils.toList(template.query(query, null)
				.to(PlatformUser.class).iterator());
	}

	@Override
	@Transactional
	public Integer myKudosCount(Long rid) throws ControllerException {
		String query = String.format("MATCH (user:PlatformUser)  WHERE id(user) = %s OPTIONAL MATCH user-[:POSTED]->n<-[r:GAVE_KUDOS]-() WITH user, count (r) as postKudos OPTIONAL MATCH user-[c:COMMENTED]->(post:Errand) WITH user, postKudos, c OPTIONAL MATCH (commentKudos:CommentKudos) WHERE commentKudos.cid = id (c) RETURN postKudos + count (commentKudos)", rid);
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	
	@Override
	@Transactional
	public List<PlatformUser> peopleNominatedByOwner(Long sessionId, Long postId, int skip, int limit)
			throws ControllerException {
		Errand errand = validateErrand(postId);
		String query = String
				.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH (nominator:PlatformUser) WHERE id (nominator) = %s MATCH nominator-[r:NOMINATED {eid: %s}]->(nominated:PlatformUser) WHERE NOT session-[:BLOCKED]-nominated RETURN nominated ORDER BY r.ctime DESC SKIP %s LIMIT %s",
						sessionId, errand.getOwnerId(), postId, skip, limit);
		return IteratorUtils.toList(template.query(query, null)
				.to(PlatformUser.class).iterator());
	}

	@Override
	@Transactional
	public Integer countOfPeopleNominateByOwner(Long session, Long postId)
			throws ControllerException {
		Errand errand = validateErrand(postId);
		String query = String
				.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH (nominator:PlatformUser) WHERE id(nominator) = %s OPTIONAL MATCH nominator-[r:NOMINATED {eid: %s}]->(nominated:PlatformUser) WHERE NOT session-[:BLOCKED]-nominated RETURN count(nominated)",
						session, errand.getOwnerId(), postId);
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public Boolean areWeInSixthDegree(Long sid, Long rid) {
		String query = String.format("OPTIONAL MATCH path=((session:PlatformUser)-[:FRIENDS*..6]-(person:PlatformUser)) WHERE id (session) = %s AND id (person) = %s WITH count(DISTINCT path) as socialStrength RETURN socialStrength", sid, rid);
		Integer path = template.query(query, null).to(Integer.class).singleOrNull();
		if (path <= 0)
			return false;
		else {
			return true;
		}
	}

	@Override
	public Integer countConversation(Long sid) throws ControllerException {
		String query = String.format(" MATCH (n1:PlatformUser)-[r:CONVERSATION]-(n2:PlatformUser) "
			+ " WHERE id (n1) = %s RETURN count(DISTINCT r)", sid);
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	public Integer countMutualFriends(Long sid, Long rid)
			throws ControllerException {
		String query = String
				.format("MATCH (a:PlatformUser)-[:FRIENDS]-x-[:FRIENDS]-(b:PlatformUser) WHERE id (a) = %s AND id (b) = %s AND NOT id (a) = id(b) AND NOT a-[:BLOCKED]-b return count (b)",
						sid, rid);
		return template.query(query, null)
				.to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public void acceptFriendRequest(Long sid, Long rid)
			throws ControllerException {
		FriendRequest fr = theFriendRequestISent(rid, sid);
		assertNotNull(fr, "Friend request not available");
		acceptFriendRequest(fr.getId());
	}

	@Override
	@Transactional
	public Boolean isUsernameTaken(String name) throws ControllerException {
		name = name.toLowerCase().trim();
		String query = String.format("OPTIONAL MATCH (person:PlatformUser) WHERE person.username = '%s' RETURN person IS NOT NULL", name);
		return template.query(query, null).to(Boolean.class).singleOrNull();
	}

	@Override
	@Transactional
	public Boolean isEmailTaken(String email) throws ControllerException {
		email = email.toLowerCase().trim();
		String query = String.format("OPTIONAL MATCH (person:PlatformUser) WHERE person.email = '%s' RETURN person IS NOT NULL", email);
		return template.query(query, null).to(Boolean.class).singleOrNull();
	}

	@Override
	@Transactional
	public void switchPrivacy(Long sid, Long eid, String privacy)
			throws ControllerException {
		assertNotNull("Bad request", sid, eid, privacy);
		PlatformUser user = validatePerson(sid);
		Errand errand = validateErrand(eid);
		if (!user.getId().equals(errand.getOwnerId()))
			throw new ControllerException("You are not authorized to do this");
		if (!getPrivacySettings(user).containsKey(privacy))
			throw new ControllerException("Invalid privacy level chosen for your account");
		else {
			errand.setPrivacy(getPrivacySettings(user).get(privacy));
		}
		if (errand.getPrivacy().equals("PUBLIC")) {
			errand.setMetaInfo("Shared with " + user.getLocalityName());
		} else if (errand.getPrivacy().equals("FRIENDS")) {
			errand.setMetaInfo("Shared with Friends");
		} else if (errand.getPrivacy().equals("PRIVATE")) {
			errand.setMetaInfo("Shared privately");
		} else if (errand.getPrivacy().equals("PUBLIC")) {
			errand.setMetaInfo("Shared with everyone");
		} else {
			throw new ControllerException("Select privacy");
		}
		errandRepo.save(errand);
	}

	@Override
	@Transactional
	public Errand updatePostDetails(Long sid, Long eid, String details)
			throws ControllerException {
		assertNotNull("Bad request", sid, eid, details);
		String query = String.format("OPTIONAL MATCH (person:PlatformUser)-[r:POSTED]-(errand:Errand) WHERE id(person) = %s AND id(errand) = %s RETURN r", sid, eid);
		Post post = template.query(query, null).to(Post.class).single();
		PlatformUser user = validatePerson(sid);
		Errand errand = post.getErrand();
		if (!user.getId().equals(errand.getOwnerId()))
			throw new ControllerException("You are not authorized to do this");
		if (details == null || details.trim().equals(""))
			throw new ControllerException("Your edit seems to be empty");
		errand.setDetails(details);
		parseAndNominate(errand);
		return  postRepo.save(post).getErrand();
	}

	@Override
	@Transactional
	public List<Long> commentNotificationList(Long sid, Long eid)
			throws ControllerException {
		assertNotNull("Bad request", sid, eid);
		String query = String.format("match (session:PlatformUser) WHERE id (session) = %s match (sender:PlatformUser)-[r:COMMENTED]->(errand:Errand)"
			+ " RETURN distinct id(sender)", sid, eid);
		List<Long> people = IteratorUtils.toList(template.query(query, null).to(Long.class).iterator());
		Errand errand = validateErrand(eid);
		if (!sid.equals(errand.getOwnerId()))
			people.add(errand.getOwnerId());
		return people;
	}

	@Override
	@Transactional
	public List<Long> broadcastNotificationList(Long sid, Long eid)
			throws ControllerException {
		Errand errand = validateErrand(eid);
		return Arrays.asList(errand.getOwnerId());
	}

	@Override
	@Transactional
	public List<Long> kudosNotificationList(Long sid, Long eid)
			throws ControllerException {
		Errand errand = validateErrand(eid);
		return Arrays.asList(errand.getOwnerId());
	}

	@Override
	@Transactional
	public List<Long> friendRequestNotificationList(Long sid, Long rid)
			throws ControllerException {
		return Arrays.asList(rid);
	}

	@Override
	@Transactional
	public List<Long> messageNotificationList(Long sid, Long rid)
			throws ControllerException {
		return Arrays.asList(rid);
	}

	@Override
	@Transactional
	public List<Long> commentKudosNotificationList(Long cid)
			throws ControllerException {
		Commented commented = validateComment(cid);
		return Arrays.asList(commented.getSender().getId());
	}

	@Override
	@Transactional
	public List<PlatformUser> canManagePage(Long pageId)
			throws ControllerException {
		String query = String
				.format("MATCH (session:PlatformUser)-[r:MANAGES_PAGE]->(page:Page) WHERE id(page) = %s RETURN session",
						pageId);
		return IteratorUtils.toList(template.query(query, null).to(PlatformUser.class).iterator());
	}

	@Override
	@Transactional
	public Page updatePage(Long id, Page page, List<PlatformUser> team, Long sessionId) throws ControllerException {
		assertNotNull("Bad request", id, page, team, sessionId);
		Page previous = validatePage(page.getId());
		PlatformUser person = validatePerson(sessionId);
		if (!previous.getName().equals(page.getName()))
			previous.setName(page.getName());
		if (!previous.getUsername().equals(page.getUsername())){
			DomainValidator.validateUsername(page.getUsername(), this);
			previous.setUsername(page.getUsername());
		}
		if (!previous.getLocalityName().equals(page.getLocalityName())){
			Double[] geoCode = geoLocator(page.getLocalityName());
			previous.setLongitude(geoCode[1]);
			previous.setLatitude(geoCode[0]);
			previous.setLocation();
			previous.setLocalityName(page.getLocalityName());
		}
		if (previous.getBio() == null || !previous.getBio().equals(page.getBio())) {
			previous.setBio(page.getBio());
		}
		if (previous.getPhoneNumber() == null || !previous.getPhoneNumber().equals(page.getPhoneNumber())){
			previous.setPhoneNumber(page.getPhoneNumber());
		}
		setPageAdministrator(page, person, team);
		return template.save(page);
	}
	
	public void setPageAdministrator (Page page, PlatformUser person, List<PlatformUser> team) throws ControllerException {
		assertNotNull("Bad request", page, team);
		String query = String
				.format("OPTIONAL MATCH (session:PlatformUser)-[r:MANAGES_PAGE]->(page:Page) WHERE id(page) = %s DELETE r",
						page.getId());
		template.query(query, null);
		for (PlatformUser member : team) {
			if (member.getId() == null
					|| !areWeFriends(person.getId(), member.getId()))
				continue;
			PlatformUser adminUser = validatePerson(member.getId());
			PageAdministrator admin = new PageAdministrator();
			admin.setPage(page);
			admin.setOwner(adminUser);
			admin.setTitle("Manager");
			template.save(admin);
			Notification n1 = new Notification();
			n1.setCtime(new Date());
			n1.setType("Team");
			n1.setDetails(person.getName() + " added you as manager on this page");
			n1.setPictureLink(person.getThumbNail());
			n1.setLink(page.getLink());
			n1 = template.save(n1);
			PersonNotification pn = new PersonNotification();
			pn.isRead = false;
			pn.notification = n1;
			pn.receiver = person;
			template.save(pn);
		}
		
	}

	@Override
	@Transactional
	public void sendFollowRequest(Long sid, Long rid)
			throws ControllerException {
		assertNotNull("Bad request", sid, rid);
		Boolean blocked = isBlockedTwoWay(sid, rid);
		if (blocked)
			throw new ControllerException("You are not allowed to do this");
		PlatformUser sender = validatePerson(sid);
		PlatformUser receiver = validatePerson(rid);
		FollowBackRequest fbr = new FollowBackRequest();
		fbr.sender = sender;
		fbr.receiver = receiver;
		fbr.ctime = new Date();
		template.save(fbr);
		Notification n1 = new Notification();
		n1.setCtime(new Date());
		n1.setType("Follow Request");
		n1.setDetails(sender.getName() + " suggested you to follow");
		n1.setPictureLink(sender.getThumbNail());
		n1.setLink(sender.getLink());
		n1 = template.save(n1);
		PersonNotification pn = new PersonNotification();
		pn.isRead = false;
		pn.notification = n1;
		pn.receiver = receiver;
		template.save(pn);
	}

	@Override
	@Transactional
	public Integer countFollowing(Long sid, Long rid)
			throws ControllerException {
		assertNotNull("Bad request", sid, rid);
		Boolean blocked = isBlockedTwoWay(sid, rid);
		if (blocked)
			throw new ControllerException("You are not allowed to do this");
		String query = String.format("MATCH (follower:PlatformUser)-[r:FOLLOWS]->(following:PlatformUser) WHERE id (follower) = %s RETURN count(DISTINCT following)", rid); 
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public List<PlatformUser> following (Long sid, Long rid, int skip, int limit)
			throws ControllerException {
		assertNotNull("Bad request", sid, rid);
		Boolean blocked = isBlockedTwoWay(sid, rid);
		if (blocked)
			throw new ControllerException("You are not allowed to do this");
		String query = String.format("MATCH (session:PlatformUser) WHERE id (session) = %s MATCH (page:PlatformUser)-[r:FOLLOWS]->(following:PlatformUser) WHERE id (page) = %s AND NOT session-[:BLOCKED]-following AND following.status = 'ACTIVE' RETURN following ORDER BY following.name SKIP %s LIMIT %s", sid, rid, skip, limit); 
		return IteratorUtils.toList(template.query(query, null).to(PlatformUser.class).iterator());
	}
	
	@Override
	@Transactional
	public Integer countFollowers(Long sid, Long rid)
			throws ControllerException {
		assertNotNull("Bad request", sid, rid);
		Boolean blocked = isBlockedTwoWay(sid, rid);
		if (blocked)
			throw new ControllerException("You are not allowed to do this");
		String query = String.format("MATCH (follower:PlatformUser)-[r:FOLLOWS]->(following:PlatformUser) WHERE id (following) = %s RETURN count(DISTINCT follower)", rid); 
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public List<PlatformUser> followers (Long sid, Long rid, int skip, int limit)
			throws ControllerException {
		assertNotNull("Bad request", sid, rid);
		Boolean blocked = isBlockedTwoWay(sid, rid);
		if (blocked)
			throw new ControllerException("You are not allowed to do this");
		String query = String.format("MATCH (session:PlatformUser) WHERE id (session) = %s MATCH (follower:PlatformUser)-[r:FOLLOWS]->(following:PlatformUser) WHERE id (following) = %s AND NOT follower-[:BLOCKED]-session AND follower.status = 'ACTIVE' RETURN follower ORDER BY following.name  SKIP %s LIMIT %s", sid, rid, skip, limit); 
		return IteratorUtils.toList(template.query(query, null).to(PlatformUser.class).iterator());
	}

	@Override
	@Transactional
	public List<PlatformUser> friendsNominatedInPost(Long pid, Long eid)
			throws ControllerException {
		String query = String
				.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH (n:Errand) WHERE id(n) = %s MATCH n<-[r:GOT_NOMINATED]-(target:PlatformUser) WHERE (session-[:FRIENDS]-target OR session-[:FOLLOWS]->target OR id (session) = id(target)) AND NOT session-[:BLOCKED]-target RETURN DISTINCT target",
						pid, eid);
		List<PlatformUser> peopleNominated = IteratorUtils.toList(template
				.query(query, null).to(PlatformUser.class).iterator());
		return peopleNominated;
	}

	@Override
	@Transactional
	public Integer countFriendsNominatedInPost(Long pid, Long eid)
			throws ControllerException {
		String query = String
				.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH (n:Errand) WHERE id(n) = %s MATCH n<-[r:GOT_NOMINATED]-(target:PlatformUser) WHERE (session-[:FRIENDS]-target OR session-[:FOLLOWS]->target) AND NOT session-[:BLOCKED]-target RETURN count(DISTINCT target)",
						pid, eid);
		return template
				.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public PlatformUser getGuest() {
		PlatformUser guest = findUsername("guest");
		if (guest == null) {
			guest = new PlatformUser("Public User", "User", "public@public.com",
					"Password", "1989", "Male", "St Andrews, Fife, United Kingdom",
					null);
			guest.setFirstAccept(false);
			guest.setVerified(true);
			guest.setStatus("READONLY");
			guest.setShowWelcome(false);
			guest = template.save(guest);
		}
		return guest;
	}

	@Override
	@Transactional
	public List<PlatformUser> directory(int skip, int limit) {
		String query = String
				.format("MATCH (user:PlatformUser) WHERE user.status = 'ACTIVE' AND NOT user.email = 'public@public.com' RETURN DISTINCT user ORDER BY user.name SKIP %s LIMIT %s",
						skip, limit);
		return IteratorUtils.toList(template.query(query, null).to(PlatformUser.class).iterator());
	}
	
	@Override
	@Transactional
	public Integer countDirectory() {
		String query = String
				.format("MATCH (user:PlatformUser) RETURN DISTINCT count(user)");
		return template.query(query, null).to(Integer.class).singleOrNull();
	}

	@Override
	@Transactional
	public List<PlatformUser> friendsWhoBroadcastedPost(Long sid, Long eid, int skip, int limit)
			throws ControllerException {
		String query = String.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH (n:Errand) WHERE id(n) = %s MATCH (session)-[:FOLLOWS]->(person:PlatformUser)-[r:RECOMMENDED]->n RETURN DISTINCT person SKIP %d LIMIT %d",sid, eid, skip, limit);
		List<PlatformUser> users = IteratorUtils.toList(template.query(query, null).to(PlatformUser.class).iterator());
		if(users != null) return users;
		return Collections.emptyList();
	}

	@Override
	@Transactional
	public Integer countFriendsWhoBroadcastedPost(Long sid, Long eid)
			throws ControllerException {
		String query = String.format("MATCH (session:PlatformUser) WHERE id(session) = %s MATCH (n:Errand) WHERE id(n) = %s MATCH (session)-[:FOLLOWS]->(person:PlatformUser)-[r:RECOMMENDED]->n RETURN count(DISTINCT person)",sid, eid);
		return template.query(query, null).to(Integer.class).singleOrNull();
	}



	// To do: Test so far, add registeration, signin, profile page with update,
	// and public news feed
	
	

}
