package com.erranda.prototype.domain;

public class InvalidEmailException extends PersonControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public InvalidEmailException(String message) {
		super(message);
	}

}
