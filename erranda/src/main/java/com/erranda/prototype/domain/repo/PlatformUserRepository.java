package com.erranda.prototype.domain.repo;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Pageable;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;
import com.erranda.prototype.domain.results.BlockResult;
import com.erranda.prototype.domain.results.FriendRequestResult;
import com.erranda.prototype.domain.results.SuggestionResult;

public interface PlatformUserRepository extends GraphRepository<PlatformUser> {

	Set<PlatformUser> findByNameLike(String name);

	PlatformUser findByEmail(String email);

	PlatformUser findByVerificationKey(String email);

	PlatformUser findByForgotPassword(String email);

	PlatformUser findByEmailIgnoreCase(String email);

	@Query(" match (person:PlatformUser)-[f:FOLLOWS]->leader "
			+ " where id(person) = ({0})" + " return leader "
			+ " ORDER BY leader.name ASC")
	List<PlatformUser> following(Long id, Pageable pageRequest);

	@Query(" match follower-[f:FOLLOWS]->person) " + " where person.id = {0}"
			+ " return follower" + " ORDER BY follower.name ASC")
	List<PlatformUser> followers(PlatformUser person, Pageable pageRequest);

	@Query("match follower-[f:FOLLOWS]->person" + " where id(person) = {0}"
			+ " return count(follower)")
	int countFollowers(Long pid);

	@Query("match follower-[f:FOLLOWS]->person"
			+ " where person.username = {0}" + " return count(follower)")
	int countFollowers(String username);

	@Query(" match (person:PlatformUser)-[f:FOLLOWS]->leader "
			+ " where id(person) = ({0})" + " return count(leader) ")
	int countFollowing(Long pid);

	@Query(" match follower-[f:FOLLOWS]->person"
			+ " where follower.username = {0}" + " return count(person)")
	int countFollowing(String username);

	@Query(" match (person:PlatformUser)-[f:POSTED]->errand "
			+ " where id(person) = ({0})" + " return count(errand)")
	int countErrands(Long pid);

	@Query(" match follower-[f:FOLLOWS]->person"
			+ " where id (person) = {0} AND id (follower) = {1}"
			+ " return f IS NOT NULL")
	boolean isFollowing(Long leader, Long follower);

	@Query(" match (person:PlatformUser)-[r:REPLIED]->errand "
			+ " where id (person) = {0} " + "return errand "
			+ " ORDER BY errand.added DESC")
	List<Errand> replies(Long person);

	@Query(" match errand-[r:NOMINATION]->person "
			+ " where id (person) = {0} " + " return errand "
			+ " ORDER BY errand.added DESC")
	List<Errand> nomination(Long person);

	PlatformUser findByUsername(String contactInfo);

	PlatformUser findByUsernameIgnoreCase(String contactInfo);

	List<PlatformUser> findByUsernameLike(String details);

	List<PlatformUser> findByUsernameLikeIgnoreCaseOrderByNameAsc(String details,
			Pageable pageRequest);

	List<PlatformUser> findByNameLikeIgnoreCaseOrderByNameAsc(String details,
			Pageable pageRequest);

	List<PlatformUser> findByUsernameContainsIgnoreCaseOrderByNameAsc(String details,
			Pageable pageRequest);

	List<PlatformUser> findByNameContainsIgnoreCaseOrderByNameAsc(String details,
			Pageable pageRequest);

	List<PlatformUser> findByNameStartingWithIgnoreCaseOrderByNameAsc(String details,
			Pageable pageRequest);

	@Query(" match sender-[r:FRIENDS]-receiver "
			+ " where id (sender) = {0} AND receiver.name =~ {1}"
			+ " return receiver" + " SKIP {2} LIMIT {3}")
	List<PlatformUser> friendsByName(Long pid, String name, int limit, int skip);

	List<PlatformUser> findByFriendsNameContainsIgnoreCaseOrderByNameAsc(String name,
			Long pid);

	@Query(" match sender-[r:BLOCKED]-receiver " + " WHERE id (receiver) = {0}"
			+ " return sender, receiver, r.ctime as ctime, id (r) as id "
			+ " ORDER BY sender.name ASC" + " SKIP {1} LIMIT {2}")
	List<BlockResult> blockList(Long rid, int skip, int limit);

	@Query(" match (sender:PlatformUser)-[r:BLOCKED]->(receiver:PlatformUser) "
			+ " WHERE id (sender) = {0} AND NOT (receiver-[:BLOCKED]->sender)"
			+ " return receiver")
	List<PlatformUser> myBlockList(Long rid);

	@Query(" match sender-[r:BLOCKED]-receiver " + " WHERE id (receiver) = {0}"
			+ " return sender, receiver, r.ctime as ctime, id (r) as id "
			+ " ORDER BY sender.name ASC")
	List<BlockResult> blockList(Long rid);

	@Query(" match sender-[r:BLOCKED]-receiver " + " WHERE id (sender) = {0}"
			+ " return id (receiver)")
	List<Long> twoWayBlockListId(Long rid);

	@Query(" OPTIONAL MATCH (person:PlatformUser)-[b:BLOCKED]->(blocked:PlatformUser) "
			+ " where id (person) = {0} AND id (blocked) = {1}"
			+ " RETURN b IS NOT NULL LIMIT 1")
	Boolean isBlocked(Long person, Long blocked);

	@Query(" match sender-[r:FRIENDS]-receiver " + " where id(sender) = {0}"
			+ " return count (receiver)")
	Integer countFriends(Long sid);

	@Query(" MATCH (session:PlatformUser) WHERE id(session) = {0} MATCH (sender:PlatformUser)-[r:FRIENDS]-receiver WHERE id (sender) = {1} AND receiver.status = 'ACTIVE'"
			+ " WITH sender, receiver MATCH (receiver:PlatformUser)-[r:FRIENDS]-other WITH count (other) as count, receiver"
			+ " SET receiver.friendsCount = count, receiver.id = id (receiver) RETURN receiver "
			+ " ORDER BY receiver.name ASC" + " SKIP {2} LIMIT {3}")
	List<PlatformUser> myFriends(Long sessionId, Long sid, int skip, int limit);

	@Query(" match sender-[r:FRIENDS]-receiver "
			+ " where id (sender) = {0} AND id (receiver) = {1}"
			+ " return r IS NOT NULL ")
	Boolean areWeFriends(Long sid, Long rid);
	
	@Query(" match sender-[r:FRIENDS]-receiver "
			+ " where id (sender) = {0} AND id (receiver) = {1}"
			+ " return r IS NOT NULL ")
	Boolean areWe2ndDegree(Long sid, Long rid);

	@Query(" match (sender:PlatformUser)-[r:FRIENDS]-receiver WHERE id (sender) = {0} AND id (receiver) = {1}"
			+ " WITH sender, receiver MATCH (receiver:PlatformUser)-[r:FRIENDS]-other WITH count (other) as count, receiver"
			+ " SET receiver.friendsCount = count, receiver.id = id (receiver) RETURN receiver ")
	PlatformUser friendship(Long sid, Long rid);

	@Query(" match (sender:PlatformUser)-[r:FRIENDS]-receiver WHERE id (sender) = {0} AND id (receiver) = {1}"
			+ " delete r")
	PlatformUser deleteFriendship(Long sid, Long rid);

	@Query(" optional match sender-[r:FRIEND_REQUEST]->receiver "
			+ " WHERE id (sender) = {0} AND id (receiver) = {1} "
			+ " return r IS NOT  NULL")
	Boolean didIsendARequest(Long sid, Long rid);

	@Query(" match sender-[r:FRIEND_REQUEST]->receiver "
			+ " WHERE id (sender) = {0} AND id (receiver) = {1} SET sender.id = id(sender) , receiver.id = id (receiver)"
			+ " return sender as sender, receiver as receiver, r.message as message, r.ctime as ctime, id (r) as id"
			+ " LIMIT 1")
	FriendRequestResult friendRequestBetween(Long sid, Long rid);

	@Query(" match sender-[r:FRIEND_REQUEST]->receiver "
			+ " WHERE id (r) = {0} SET sender.id = id(sender) , receiver.id = id (receiver) "
			+ " return sender as sender, receiver as receiver, r.message as message, r.ctime as ctime, id (r) as id"
			+ " LIMIT 1")
	FriendRequestResult findOneFriendRequest(Long frid);

	@Query(" match sender-[r:FRIEND_REQUEST]->receiver "
			+ " WHERE id (receiver) = {0} SET sender.id = id(sender) , receiver.id = id (receiver) "
			+ " return sender, receiver, r.ctime as ctime, id (r) as id "
			+ " ORDER BY r.ctime DESC" + " SKIP {1} LIMIT {2}")
	List<FriendRequestResult> myFriendRequests(Long rid, int skip, int limit);

	@Query(" match sender-[r:FRIEND_REQUEST]->receiver "
			+ " WHERE id (receiver) = {0}" + " return count (r)")
	Integer friendRequestsSize(Long rid);

	@Query("match (n:PlatformUser)-[r:FRIEND_REQUEST]-R delete r;")
	public void deleteAllFR();

	@Query("match (n:PlatformUser)-[r:FRIENDS]-R delete r;")
	public void deleteAllF();

	@Query("MATCH (a:PlatformUser)-[r:FRIENDS*2..2]-b WHERE a.id = {0} return b as person SKIP {1} LIMIT {2};")
	public List<SuggestionResult> suggestion(Long pid, int skip, int limit);

	@Query("MATCH (a:PlatformUser)-[r:FRIENDS]-x-[:FRIENDS]-b WHERE a.id = {0} return b as person SKIP {1} LIMIT {2};")
	public List<PlatformUser> mutualFriends(Long pid, Long rid, int skip, int limit);

}
