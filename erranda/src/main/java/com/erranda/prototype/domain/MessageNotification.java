package com.erranda.prototype.domain;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.RelatedTo;

public class MessageNotification extends AbstractNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@RelatedTo(type = "NEW_MESSAGE", direction = Direction.OUTGOING)
	PlatformUser receiver;

	// Conversation id
	Long cid;

	public MessageNotification() {
	}

}