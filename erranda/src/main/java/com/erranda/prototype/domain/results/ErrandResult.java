package com.erranda.prototype.domain.results;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.erranda.prototype.domain.Errand;
import com.erranda.prototype.domain.PlatformUser;

@QueryResult
public interface ErrandResult {

	@ResultColumn("person")
	PlatformUser getSender();

	@ResultColumn("errand")
	Errand getErrand();

	@ResultColumn("id")
	public Long getId();
}