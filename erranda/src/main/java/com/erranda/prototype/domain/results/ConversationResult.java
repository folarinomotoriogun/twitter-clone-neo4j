package com.erranda.prototype.domain.results;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.neo4j.annotation.QueryResult;
import org.springframework.data.neo4j.annotation.ResultColumn;

import com.erranda.prototype.domain.Message;
import com.erranda.prototype.domain.PlatformUser;

// Use interfaces and the Result Column annotation
@QueryResult
public interface ConversationResult extends Serializable {

	@ResultColumn("ctime")
	public Date getCtime();

	@ResultColumn("lastMessage")
	public Message getLastMessage();

	@ResultColumn("receiver")
	public PlatformUser getReceiver();

	@ResultColumn("sender")
	public PlatformUser getSender();

	@ResultColumn("id")
	public Long getId();
}
