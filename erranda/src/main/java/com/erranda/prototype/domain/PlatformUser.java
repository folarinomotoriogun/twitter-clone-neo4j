package com.erranda.prototype.domain;

import static org.neo4j.graphdb.Direction.BOTH;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.wicket.util.string.Strings;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.RelatedTo;
import org.springframework.data.neo4j.support.index.IndexType;
import org.springframework.util.StringUtils;

import com.erranda.prototype.Config;

public class PlatformUser extends NamedNode {

	/**
	 * 
	 */
	private static final long serialVersionUID = 662262252883820030L;

	@Indexed
	@NotNull
	private String email;
	
	@Indexed(unique = true)
	@NotNull public String username;
	
	@Indexed
	@NotNull
	private String firstName;

	@Indexed
	@NotNull
	private String lastName;
	
	@Indexed
	@NotNull
	private String bio = "Hey there, ask me anything on Erranda";

	private String joinDate;

	private String status = "ACTIVE";

	private String birthyear;

	private Boolean verified = false;
	
	private Boolean showVerified = false;
	
	private Boolean firstLocalPost = true;
	
	private Boolean firstFriendPost = true;
	
	private Boolean firstPrivatePost = true;
	
	private String verificationKey = "";

	private String profilePictureUrl = "http://www.realestatetaxgroup.com/wp-content/uploads/2013/03/empty-profile.png";
	
	private String thumbNail = profilePictureUrl;

	private String localityName = "Global";

	private Integer friendsCount;

	private Integer newPoints;

	private String preferredPrivacyOption = "PUBLIC";

	private String forgotPassword;

	private Boolean showWelcome = true;

	private Boolean firstBroadcast = true;

	private Boolean firstNomination = true;

	private Boolean firstAccept = true;

	@NotNull
	@Size(min = 6)
	private String password;
	
	@Indexed(indexType = IndexType.POINT, indexName = "PersonLocation")
	private String wkt;
	
	public void setLocation() {
		this.wkt = String.format("POINT( %.2f %.2f )", latitude, longitude);
	}

	private List<String> interests;

	// add birthday for items, sex, and picture, direct message
	private Date created;

	private Double longitude = 0.00;

	private Double latitude = 0.00;

	// Longest marathon
	private Double width = 42.195;

	private Integer points = 0;

	private Date lastClickNotificationTime;

	private Date lastClickMessageTime;

	private Integer unseenMessages = 0;

	private Integer unseenFriendRequests = 0;

	@RelatedTo(type = "FRIENDS", direction = BOTH)
	private Set<PlatformUser> friends;
	
	private String type = Config.PLATFORM_TYPE_PERSON;

	public Integer getUnseenFriendRequests() {
		return unseenFriendRequests != null ? unseenFriendRequests : 0;
	}

	public void setUnseenFriendRequests(Integer unseenFriendRequests) {
		this.unseenFriendRequests = unseenFriendRequests;
	}

	public Integer getUnseenMessages() {
		return unseenMessages != null ? unseenMessages : 0;
	}

	public void setUnseenMessages(Integer unseenMessages) {
		this.unseenMessages = unseenMessages;
	}

	// A notification system can help where people anonymously post trips that
	// receive alerts
	public PlatformUser() {
	}

	public PlatformUser(String name, String username, String email, String password,
			String birthday, String sex, String location, List<String> interests) {
		this.setName(name);
		this.setFirstName(name.split(" ") [0]);
		this.setLastName(name.split(" ") [1]);
		this.setUsername(username);
		this.setEmail(email);
		this.setPassword(password);
		this.setLocalityName(location);
		this.setCreated(new Date());
		this.setInterests(interests);
	}
	
	private String phoneNumber;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		if (email != null)
			email  = Strings.escapeMarkup(email).toString().toLowerCase();
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		if (email == null)
			return;
		this.email = email.toLowerCase().trim();
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return email + " " + name;
	}

	@Override
	public String getName() {
		if (name != null)
			name  = Strings.escapeMarkup(name).toString();
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	public String getNameRaw() {
		return name;
	}
	
	public void setNameRaw(String name) {
		this.name = StringUtils.capitalize(name);
	}

	public String getUsername() {
		if (username != null)
			return StringUtils.capitalize(Strings.escapeMarkup(username).toString());
		return username;
	}

	public void setUsername(String username) {
		this.username = username.toLowerCase().trim();
	}
	
	public String getUsernameRaw() {
		return username;
	}

	public void setUsernameRaw(String username) {
		this.username = username.toLowerCase().trim();
	}

	public String getProfilePictureUrl() {
		return profilePictureUrl;
	}

	public void setProfilePictureUrl(String profilePictureUrl) {
		this.profilePictureUrl = profilePictureUrl;
	}

	public List<String> getInterests() {
		return interests;
	}

	public void setInterests(List<String> interests) {
		this.interests = interests;
	}

	public String getBio() {
		if (bio != null)
			bio  = Strings.escapeMarkup(bio).toString();
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLink() {
		return Config.PROFILE_NAME_LINK + "/" + username;
	}

	public String getBirthyear() {
		return birthyear;
	}

	public void setBirthyear(String birthyear) {
		this.birthyear = birthyear;
	}

	public String getLocalityName() {
		if (localityName != null)
			localityName  = Strings.escapeMarkup(localityName).toString();
		return localityName;
	}

	public void setLocalityName(String localityName) {
		this.localityName = localityName;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public Date getLastClickNotificationTime() {
		return lastClickNotificationTime;
	}

	public void setLastClickNotificationTime(Date lastClickNotificationTime) {
		this.lastClickNotificationTime = lastClickNotificationTime;
	}

	public Date getLastClickMessageTime() {
		return lastClickMessageTime;
	}

	public void setLastClickMessageTime(Date lastClickMessageTime) {
		this.lastClickMessageTime = lastClickMessageTime;
	}

	public Integer getFriendsCount() {
		return friendsCount;
	}

	public void setFriendsCount(Integer integer) {
		this.friendsCount = integer;
	}

	public Integer getNewPoints() {
		return newPoints;
	}

	public void setNewPoints(Integer newPoints) {
		this.newPoints = newPoints;
	}

	public String getPreferredPrivacyOption() {
		return preferredPrivacyOption;
	}

	public void setPreferredPrivacyOption(String preferredPrivacyOption) {
		this.preferredPrivacyOption = preferredPrivacyOption;
	}

	public PlatformUser(Long id) {
		this.id = id;
	}

	public String getForgotPassword() {
		return forgotPassword;
	}

	public void setForgotPassword(String forgotPassword) {
		this.forgotPassword = forgotPassword;
	}

	public Boolean getVerified() {
		return verified;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

	public String getVerificationKey() {
		return verificationKey;
	}

	public void setVerificationKey(String verificationKey) {
		this.verificationKey = verificationKey;
	}

	public Boolean getShowWelcome() {
		return showWelcome;
	}

	public void setShowWelcome(Boolean showWelcome) {
		this.showWelcome = showWelcome;
	}

	public Boolean getFirstBroadcast() {
		return firstBroadcast == null ? true : firstBroadcast;
	}

	public void setFirstBroadcast(Boolean firstBroadcast) {
		this.firstBroadcast = firstBroadcast;
	}

	public Boolean getFirstNomination() {
		return firstNomination == null ? true : firstNomination;
	}

	public void setFirstNomination(Boolean firstNomination) {
		this.firstNomination = firstNomination;
	}

	public Boolean getFirstAccept() {
		return firstAccept == null ? true : firstAccept;
	}

	public void setFirstAccept(Boolean firstAccept) {
		this.firstAccept = firstAccept;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getThumbNail() {
		return thumbNail;
	}

	public void setThumbNail(String thumbNail) {
		this.thumbNail = thumbNail;
	}

	public Boolean getShowVerified() {
		return showVerified;
	}

	public void setShowVerified(Boolean showVerified) {
		this.showVerified = showVerified;
	}

	public Boolean getFirstLocalPost() {
		return firstLocalPost;
	}

	public void setFirstLocalPost(Boolean firstLocalPost) {
		this.firstLocalPost = firstLocalPost;
	}

	public Boolean getFirstFriendPost() {
		return firstFriendPost;
	}

	public void setFirstFriendPost(Boolean firstFriendPost) {
		this.firstFriendPost = firstFriendPost;
	}

	public Boolean getFirstPrivatePost() {
		return firstPrivatePost;
	}

	public void setFirstPrivatePost(Boolean firstPrivatePost) {
		this.firstPrivatePost = firstPrivatePost;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
