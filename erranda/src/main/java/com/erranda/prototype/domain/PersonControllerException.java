package com.erranda.prototype.domain;

public class PersonControllerException extends ControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public PersonControllerException(String message) {
		super(message);
	}

}
