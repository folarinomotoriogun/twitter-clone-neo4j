package com.erranda.prototype.domain;

import javax.validation.constraints.NotNull;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.support.index.IndexType;

public class NamedNode extends AbstractNode implements Comparable<NamedNode> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Indexed(indexType = IndexType.FULLTEXT, indexName = "searchName")
	@NotNull
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public NamedNode() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		// type check fails because we are querying the whole graph
		return (id.equals(((NamedNode) obj).id) && name
				.equals(((NamedNode) obj).name));
	}

	@Override
	public int hashCode() {
		int one = id == null ? 0 : id.hashCode();
		int two = one + (name == null ? 0 : name.hashCode());
		return two;
	}

	@Override
	public int compareTo(NamedNode o) {
		return this.name.compareToIgnoreCase(o.getName());
	}

}
