package com.erranda.prototype.domain;

public class InvalidPasswordException extends PersonControllerException {

	private static final long serialVersionUID = -8692416416165881154L;

	public InvalidPasswordException(String message) {
		super(message);
	}

}
