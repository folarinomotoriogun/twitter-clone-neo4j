package com.erranda.prototype.domain;

import java.util.Date;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.erranda.prototype.domain.results.ConversationResult;

/**
 * Used to efficiently navigate to messages
 * 
 * @author Folarin
 * 
 */
@RelationshipEntity(type = "CONVERSATION")
public class Conversation extends Node {

	private static final long serialVersionUID = 1L;

	private Date ctime;

	private @StartNode PlatformUser sender;

	private @EndNode PlatformUser receiver;

	private Long lastMessageId;

	transient private Integer unreadCount;

	transient private Message lastMessage;

	public Conversation(PlatformUser sender2, PlatformUser receiver2) {
		sender = sender2;
		receiver = receiver2;
		ctime = new Date();
	}

	public Conversation() {
	}

	public Conversation(ConversationResult result) {
		if (result != null) {
			sender = result.getSender();
			receiver = result.getReceiver();
			ctime = result.getCtime();
			if (result.getLastMessage() != null) {
				lastMessage = result.getLastMessage();
				lastMessageId = result.getLastMessage().getId();
			}
			id = result.getId();
		}
	}

	public PlatformUser getSender() {
		return sender;
	}

	public void setSender(PlatformUser sender) {
		this.sender = sender;
	}

	public PlatformUser getReceiver() {
		return receiver;
	}

	public void setReceiver(PlatformUser receiver) {
		this.receiver = receiver;
	}

	public Long getLastMessageId() {
		return lastMessageId;
	}

	public void setLastMessageId(Long lastMessageId) {
		this.lastMessageId = lastMessageId;
	}

	public Date getCtime() {
		return ctime;
	}

	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}

	public Message getLastMessage() {
		return lastMessage;
	}

	public void setLastMessage(Message lastMessage) {
		this.lastMessage = lastMessage;
	}

	public Integer getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(Integer unreadCount) {
		this.unreadCount = unreadCount;
	}
}
